//
//  ImageUploading.h
//  GoSnow
//
//  Created by Rishabh on 28/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#define kImageUploaded         @"kImageUploaded"
@protocol ImageUploaderDelegate <NSObject>

- (void)imageUpoladerDidFailed:(NSMutableDictionary *)downloader;
- (void)imageUploaderDidFinish:(NSMutableDictionary *)downloader;
@end

@interface ImageUploading : NSObject
{
    
    BOOL iscancelled;
    NSInteger last;
    
    NSInteger totalCount;
}
@property (strong,nonatomic)ASINetworkQueue *network;
@property (nonatomic, assign) id <ImageUploaderDelegate> delegate;
+(ImageUploading *)sharedInstance;
-(void)UploadImage:(NSMutableDictionary *)dictionary index:(NSInteger )index;
// 4: Declare a designated initializer.


@end


