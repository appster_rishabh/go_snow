//
//  ImageUploading.m
//  GoSnow
//
//  Created by Rishabh on 28/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ImageUploading.h"

@implementation ImageUploading

-(id)init
{
    if (self = [super init]) {
        _network = [[ASINetworkQueue alloc] init];
        _network.uploadProgressDelegate = self;
        // 2: Set the properties.
        
    }
    return self;
}
+(ImageUploading *)sharedInstance {
    static dispatch_once_t pred;
    static ImageUploading *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[ImageUploading alloc] init];
    });
    
    return sharedInstance;
}
-(void)UploadImage:(NSMutableDictionary *)dictionary index:(NSInteger )index{
    // NSString *idd=[[self.Total_arr objectAtIndex:0] valueForKey:@"id"];
    NSError* error;
    NSData* uploadData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    //_network = [[ASINetworkQueue alloc] init];
   // _network.uploadProgressDelegate = self;
    // NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@user/addPhoto",KSERVICEURL]]];
    
    //[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //[request setHTTPMethod:@"POST"];    NSString *string=@"text=good";
    
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@user/addPhoto",KSERVICEURL]]];
    [request setTag:index];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request addRequestHeader:@"content-type" value:@"application/json"];
    [request appendPostData:uploadData];
    
    // [request setStringEncoding:NSUTF8StringEncoding];
    [request setDelegate:self];
    //[request startAsynchronous];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDidFinishSelector:@selector(requestFinished:)];
    //[request startAsynchronous];
     [_network  addOperation:request];
    //
     [_network go];
    
}
-(void)requestFinished:(ASIFormDataRequest *)request
{
    if (iscancelled ) {
        return;
    }
    
    NSLog(@"%li",(long)[request tag]);
    last=[request tag];
    NSString* response = [request responseString];
    
    NSData *responseData = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    NSMutableDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    NSLog(@"%@",dictionary);
    [self updateProfileImages:[dictionary valueForKey:@"profile"]];
   // NSInteger meta=[[[dictionary valueForKey:@"meta"] valueForKey:@"code"] integerValue];
   // if ([_delegate respondsToSelector:@selector(imageUploaderDidFinish:)])
      //  [_delegate imageUploaderDidFinish:dictionary];
  //  else
   [[NSNotificationCenter defaultCenter] postNotificationName:kImageUploaded object:@"kImageUploaded" userInfo:dictionary];
    
    
    
    // NSLog(@"response string--> %@", respondingString);
}
-(void)requestFailed:(ASIFormDataRequest *)request
{
    
    NSLog(@"requestFailed%@",request.error);
    
    NSString *respondingString = [request responseString];
    NSLog(@"response failed string--> %@", respondingString);
   
   // [_delegate imageDownloaderDidFailed:[]];
}
-(void)updateProfileImages:(NSMutableDictionary *)dict
{
    [ProfileEntity sharedInstance].userPhotos=[dict valueForKey:@"userPhotos"];
}
@end
