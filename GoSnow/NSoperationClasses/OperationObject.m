//
//  OperationObject.m
//  GoSnow
//
//  Created by Rishabh on 27/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "OperationObject.h"

@implementation OperationObject
@synthesize pendingOperations = _pendingOperations;
#pragma mark -
#pragma mark - Lazy instantiation


- (PendingOperations *)pendingOperations {
    if (!_pendingOperations) {
        _pendingOperations = [[PendingOperations alloc] init];
    }
    return _pendingOperations;
}


- (void)UploadImageOperation:(NSMutableDictionary *)dictionary
{
NSError* error;
NSData* uploadData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];

    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@user/addPhoto",KSERVICEURL]]];

[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
[request setHTTPMethod:@"POST"];
request.HTTPBody = uploadData;

        // 2: Use AFHTTPRequestOperation class, alloc and init it with the request.
        AFHTTPRequestOperation *datasource_download_operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        // 3: Give the user feedback, while downloading the data source by enabling network activity indicator.
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        // 4: By using setCompletionBlockWithSuccess:failure:, you can add two blocks: one for the case where the operation finishes successfully, and one for the case where it fails.
        [datasource_download_operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // 5: In the success block, download the property list as NSData, and then by using toll-free bridging for data into CFDataRef and CFPropertyList, convert it into NSDictionary.
            NSData *datasource_data = (NSData *)responseObject;
            CFPropertyListRef plist =  CFPropertyListCreateFromXMLData(kCFAllocatorDefault, (__bridge CFDataRef)datasource_data, kCFPropertyListImmutable, NULL);
            
            NSDictionary *datasource_dictionary = (__bridge NSDictionary *)plist;
            
            // 6: Create a NSMutableArray and iterate through all objects and keys in the dictionary, create a PhotoRecord instance, and store it in the array.
            NSMutableArray *records = [NSMutableArray array];
            
            for (NSString *key in datasource_dictionary) {
            
            }
            
            // 7: Once you are done, point the _photo to the array of records, reload the table view and stop the network activity indicator. You also release the ìplistî instance variable.
           
            
            CFRelease(plist);
            
         
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error){
            
            // 8: In case you are not successful, you display a message to notify the user.
            // Connection error message
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            alert = nil;
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        }];
        
        // 9: Finally, add ìdatasource_download_operationî to ìdownloadQueueî of PendingOperations.
        [self.pendingOperations.downloadQueue addOperation:datasource_download_operation];
    

}
#pragma mark -
#pragma mark - Cancelling, suspending, resuming queues / operations


- (void)suspendAllOperations {
    [self.pendingOperations.downloadQueue setSuspended:YES];
    [self.pendingOperations.filtrationQueue setSuspended:YES];
}


- (void)resumeAllOperations {
    [self.pendingOperations.downloadQueue setSuspended:NO];
    [self.pendingOperations.filtrationQueue setSuspended:NO];
}


- (void)cancelAllOperations {
    [self.pendingOperations.downloadQueue cancelAllOperations];
    [self.pendingOperations.filtrationQueue cancelAllOperations];
}

#pragma mark -
#pragma mark - ImageDownloader delegate


- (void)imageDownloaderDidFinish:(ImageUploader *)downloader {
    
    // 1: Check for the indexPath of the operation, whether it is a download, or filtration.
  //  NSIndexPath *indexPath = downloader.indexPathInTableView;
    
    // 2: Get hold of the PhotoRecord instance.
  //  UploadRecord *theRecord = downloader.photoRecord;
    
    // 3: Replace the updated PhotoRecord in the main data source (Photos array).
   // [self.photos replaceObjectAtIndex:indexPath.row withObject:theRecord];
    
    // 4: Update UI.
    //[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    // 5: Remove the operation from downloadsInProgress (or filtrationsInProgress).
   // [self.pendingOperations.downloadsInProgress removeObjectForKey:indexPath];
}

@end
