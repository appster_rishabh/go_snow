//
//  ImageUploader.h
//  GoSnow
//
//  Created by Rishabh on 27/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
// 1: Import PhotoRecord.h so that you can independently set the image property of a PhotoRecord once it is successfully downloaded. If downloading fails, set its failed value to YES.
#import "UploadRecord.h"

// 2: Declare a delegate so that you can notify the caller once the operation is finished.
@protocol ImageUploaderDelegate;

@interface ImageUploader : NSObject
{
    BOOL iscancelled;
    NSInteger last;
   
    NSInteger totalCount;
}

@property (nonatomic, assign) id <ImageUploaderDelegate> delegate;

// 3: Declare indexPathInTableView for convenience so that once the operation is finished, the caller has a reference to where this operation belongs to.
@property  NSInteger imageCount;
@property (nonatomic, readonly, strong) UploadRecord *photoRecord;
@property (strong,nonatomic)ASINetworkQueue *network;
@property (strong,nonatomic) NSOperationQueue *oprtnQueue;
//-(id)init;
-(void)UploadImage:(NSMutableDictionary *)dictionary index:(NSInteger )index;
// 4: Declare a designated initializer.
- (id)initWithPhotoRecord:(UploadRecord *)record atIndexPath:(NSInteger )imagecount delegate:(id<ImageUploaderDelegate>) theDelegate;

@end

@protocol ImageUploaderDelegate <NSObject>

// 5: In your delegate method, pass the whole class as an object back to the caller so that the caller can access both indexPathInTableView and photoRecord. Because you need to cast the operation to NSObject and return it on the main thread, the delegate method canít have more than one argument.
- (void)imageDownloaderDidFailed:(ImageUploader *)downloader;
- (void)imageDownloaderDidFinish:(ImageUploader *)downloader;
@end