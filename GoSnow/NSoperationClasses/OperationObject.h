//
//  OperationObject.h
//  GoSnow
//
//  Created by Rishabh on 27/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UploadRecord.h"
#import "AFNetworking.h"
#import "PendingOperations.h"
#import "ImageUploader.h"

@protocol ImageUploadingDelegate <NSObject>
- (void)ImageUploadingDelegate:(UploadRecord *)filtration;

@end

@protocol ImageUploadingDelegate;

@interface OperationObject : NSObject<ImageUploadingDelegate>
@property (nonatomic, weak) id <ImageUploadingDelegate> delegate;
@property (nonatomic, strong) PendingOperations *pendingOperations;
- (void)UploadImageOperation:(NSMutableDictionary *)dictionary;
@end

