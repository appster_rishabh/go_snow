//
//  UploadRecord.h
//  GoSnow
//
//  Created by Rishabh on 27/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UploadRecord : NSObject
@property (nonatomic, strong) NSString *sessionId;  // To store the name of image
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, readonly) BOOL hasUploadImage; // Return YES if image is uploaded.
@property (nonatomic, getter = isFiltered) BOOL filtered; // Return YES if image is sepia-filtered
@property (nonatomic, getter = isFailed) BOOL failed;
@end
