//
//  ImageUploader.m
//  GoSnow
//
//  Created by Rishabh on 27/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ImageUploader.h"


@interface ImageUploader ()

@property (nonatomic, readwrite, strong) UploadRecord *photoRecord;
@end


@implementation ImageUploader
@synthesize delegate = _delegate;

@synthesize photoRecord = _photoRecord;

#pragma mark -
#pragma mark - Life Cycle
-(id)init
{
    if (self = [super init]) {
        _network = [[ASINetworkQueue alloc] init];
        _network.uploadProgressDelegate = self;
        // 2: Set the properties.
        
    }
    return self;
}
- (id)initWithPhotoRecord:(UploadRecord *)record atIndexPath:(NSInteger )imagecount delegate:(id<ImageUploaderDelegate>)theDelegate {
    
    if (self = [super init]) {
        _network = [[ASINetworkQueue alloc] init];
        _network.uploadProgressDelegate = self;
        // 2: Set the properties.
        self.delegate = theDelegate;
       self.imageCount = imagecount;
        self.photoRecord = record;
    }
    return self;
}
-(void)UploadImage:(NSMutableDictionary *)dictionary index:(NSInteger )index{
    // NSString *idd=[[self.Total_arr objectAtIndex:0] valueForKey:@"id"];
    NSError* error;
    NSData* uploadData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    _network = [[ASINetworkQueue alloc] init];
    _network.uploadProgressDelegate = self;
   // NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@user/addPhoto",KSERVICEURL]]];
    
    //[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //[request setHTTPMethod:@"POST"];    NSString *string=@"text=good";
    
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@user/addPhoto",KSERVICEURL]]];
    [request setTag:index];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request addRequestHeader:@"content-type" value:@"application/json"];
    [request appendPostData:uploadData];
    
   // [request setStringEncoding:NSUTF8StringEncoding];
    [request setDelegate:self];
    //[request startAsynchronous];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDidFinishSelector:@selector(requestFinished:)];
    [request startAsynchronous];
   // [_network  addOperation:request];
    //
   // [_network go];
    
}
-(void)requestFinished:(ASIFormDataRequest *)request
{
    if (iscancelled ) {
        return;
    }
   
    NSLog(@"%li",(long)[request tag]);
    last=[request tag];
    NSString* response = [request responseString];
    
    NSData *responseData = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    NSLog(@"%@",dictionary);
    NSInteger meta=[[[dictionary valueForKey:@"meta"] valueForKey:@"code"] integerValue];
    if (meta!=200) {
        iscancelled=YES;
        UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Insta" message:[[dictionary valueForKey:@"meta"] valueForKey:@"error_message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
        
        // [_network reset];
        [_network cancelAllOperations];
        return;
    }
    [_delegate imageDownloaderDidFinish:self];
    
    // NSLog(@"response string--> %@", respondingString);
}
-(void)requestFailed:(ASIFormDataRequest *)request
{
    
    NSLog(@"requestFailed%@",request.error);
    //NSError *error = [request error];
    //NSLog(@"%@", [error description]);
    NSString *respondingString = [request responseString];
    NSLog(@"response failed string--> %@", respondingString);
    
    NSLog(@"operationCount %d",(int)[_network operationCount]);
    [_delegate imageDownloaderDidFailed:self];
}
#pragma mark -
#pragma mark - Downloading image

// 3: Regularly check for isCancelled, to make sure the operation terminates as soon as possible.
/*
- (void)main {
    
    // 4: Apple recommends using @autoreleasepool block instead of alloc and init NSAutoreleasePool, because blocks are more efficient. You might use NSAuoreleasePool instead and that would be fine.
    @autoreleasepool {
        
        if (self.isCancelled)
            return;
        
        NSData *imageData = [[NSData alloc] initWithContentsOfURL:@""];
        
        if (self.isCancelled) {
            imageData = nil;
            return;
        }
        
        if (imageData) {
            UIImage *downloadedImage = [UIImage imageWithData:imageData];
            self.photoRecord.image = downloadedImage;
        }
        else {
            self.photoRecord.failed = YES;
        }
        
        imageData = nil;
        
        if (self.isCancelled)
            return;
        
        // 5: Cast the operation to NSObject, and notify the caller on the main thread.
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(imageDownloaderDidFinish:) withObject:self waitUntilDone:NO];
        
    }
}
*/
@end