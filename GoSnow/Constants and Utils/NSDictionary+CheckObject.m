//
//  NSDictionary+CheckObject.m
//  GoSnow
//
//  Created by Varsha Singh on 20/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "NSDictionary+CheckObject.h"

@implementation NSDictionary (CheckObject)

#pragma mark - Check Value
- (NSString*)checkValueforKey:(NSString *)Key {
    if ([self objectForKey:Key] && ![[self objectForKey:Key] isKindOfClass:[NSNull class]]) {
        return [self objectForKey:Key];
    }
    return @"";
}

#pragma mark - Check Number
- (NSString*)checkNumberForKey:(NSString *)Key {
    if ([self objectForKey:Key]!=nil) {
        if ([[self objectForKey:Key] isKindOfClass:[NSNumber class]]&& ![[self objectForKey:Key] isKindOfClass:[NSNull class]]) {
            NSNumber *num = [NSNumber numberWithInteger:[[self objectForKey:Key] integerValue]];
            return [num stringValue];
        }
    }
    return [[NSNumber numberWithInt:0] stringValue];
}

@end
