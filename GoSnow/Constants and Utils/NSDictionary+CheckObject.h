//
//  NSDictionary+CheckObject.h
//  GoSnow
//
//  Created by Varsha Singh on 20/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (CheckObject)
- (NSString*)checkValueforKey:(NSString *)Key;
- (NSString*)checkNumberForKey:(NSString *)Key;
@end
