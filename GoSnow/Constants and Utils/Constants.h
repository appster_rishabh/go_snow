//
//  Constants.h
//  GoSnow
//
//  Created by Varsha Singh on 20/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#ifndef GoSnow_Constants_h
#define GoSnow_Constants_h

typedef enum {
    
    SnowOperationLogin = 0,
    SnowOperationSignUp,
    
    
}SnowOperationType;

static NSString * const kLastDataSync = @"LastDataSync";
static NSString * const kDataSyncFrequency = @"DataSyncFrequency";

#define HEIGHT      [[UIScreen mainScreen]bounds].size.height
#define WIDTH       [[UIScreen mainScreen]bounds].size.width
#define X           [[UIScreen mainScreen]bounds].origin.x
#define Y           [[UIScreen mainScreen]bounds].origin.y

#define KHOSTURL @"http://54.245.110.34/gosnow/index.php/service/auth/"
#define KHOSTURL1 @"http://54.245.110.34/gosnow/index.php/service/"
#define KSERVICEURL @"http://54.245.110.34/gosnow/index.php/service/"
#define KArticleHeaderRefresh @"refreshHeader"
#define kNewsFeedScreenRefresh @"RefreshScreen"
#define KAboutMe @"aboutMe"
#define KRiderType @"riderType"
#define KSkillLevel @"skillLevel"
#define KplannedTrip @"plannedTrip"
#define KCurrentLoc @"currentLoc"

#define messageWidth 260
#define kLatitude       @"Latitude"
#define kLongtitude     @"Longtitude"

#define ktextByme       @"textByme"
#define ktextbyother    @"textByOther"
#define kImageByme      @"ImageByme"
#define kImageByOther   @"ImageByOther"

#define kStatusSeding   @"Sending"
#define kStatusSent     @"Sent"
#define kStatusFailed   @"Failed"

#define kMayBeLater         @"MayBeLater"
#define kRideTogether         @"RideTogether"
#define kAcceptRequest         @"AcceptRequest"
#define kDeclineRequest         @"DeclineRequest"

#define NUM_TOP_ITEMS 5
#define NUM_SUBITEMS 4

#define LEFT_PADDING  20
#define BOTTOM_MARGIN  155
#define TOP_PADDING   10
#define BOTTOMVIEW_PADING  185
#define SIDE_MARGIN  55
#define TB_SLIDER_SIZE 320
#define ARC_TOP_MARGIN 155
#define HEIGHT_CONSTRAINTS   40 ;

// For Swipe Left and Right
#define ACTION_MARGIN 120 //%%% distance from center where the action applies. Higher = swipe further in order for the action to be called
#define SCALE_STRENGTH 4 //%%% how quickly the user shrinks. Higher = slower shrinking
#define SCALE_MAX .93 //%%% upper bar for how much the user shrinks. Higher = shrinks less
#define ROTATION_MAX 1 //%%% the maximum rotation allowed in radians.  Higher = user can keep rotating longer
#define ROTATION_STRENGTH 320 //%%% strength of rotation. Higher = weaker rotation
#define ROTATION_ANGLE M_PI/8 //%%% Higher = stronger rotation angle

// For HighLighted Image View
#define UIImageViewHighlightedWebCacheOperationKey @"highlightedImage"

// For UIImageView+ProgressView
#define TAG_PROGRESS_VIEW 149462

// For SWUtilityButton View
#define kUtilityButtonWidthDefault 90

// For SWTableView Cell
#define kSectionIndexWidth 15
#define kAccessoryTrailingSpace 15
#define kLongPressMinimumDuration 0.16f

// For SPHBubble Cell
#define messageWidth 260

// For Rider Manager
#define kImageUploaded         @"kImageUploaded"

// For RESideMenu
#define kSideMenuLoaded         @"kSideMenuLoaded"

// For My Profile Location
#define METERS_PER_MILE 1009.344


// for view controllers, changing notification icon at run time

#define KNotificationRecieved           @"NotificationRecieved"
#define KNotificationFromNoticationCentre           @"NotificationRecievedFromNotificationCentre"
#endif
