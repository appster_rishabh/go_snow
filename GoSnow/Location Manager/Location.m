//
//  Location.m
//  GoSnow
//
//  Created by Varsha Singh on 23/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "Location.h"

@implementation Location

#pragma mark -  shared Instance of Location
+ (Location *)sharedInstance {
    static dispatch_once_t pred;
    static Location *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[Location alloc] init];
    });
    
    return sharedInstance;
}


#pragma mark - Get Coordinates for Location

-(CLLocationCoordinate2D)getCoordinates
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    CLLocation *location = [locationManager location];
    
    CLLocationCoordinate2D coordinate;
    coordinate = [location coordinate];
    return coordinate;
}

#pragma mark - Get Current Location of User

-(void)startUpdateLocation
{
    [_locationManager startUpdatingLocation];
}
-(void)stopUpdateLocation
{
    [_locationManager stopUpdatingLocation];
}

#pragma mark - Location Manager Stack

- (CLLocationManager *)callLocationManager
{
    if (_locationManager != nil)
    {
        return _locationManager;
    }
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate=self;
    //_locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    _locationManager.distanceFilter=kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
   
    [self checkRequestAuthorization];
    return _locationManager;
}

#pragma mark - Check Location Request Authorization

- (void)checkRequestAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Settings", nil];
       // [alertView show];
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined)
    {
        if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
          [_locationManager requestWhenInUseAuthorization];
        }
       
        
    }
}

#pragma mark - Location Stack

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        [self stopUpdateLocation];
        NSString *stringLatitude = [NSString stringWithFormat:@"%.8f",currentLocation.coordinate.latitude];
        NSString *stringLongitude = [NSString stringWithFormat:@"%.8f",currentLocation.coordinate.longitude];
        [[NSUserDefaults standardUserDefaults] setObject:stringLatitude forKey:kLatitude];
        [[NSUserDefaults standardUserDefaults] setObject:stringLongitude forKey:kLongtitude];
        [[NSUserDefaults standardUserDefaults] synchronize];
//        [ProfileEntity sharedInstance].longitude=stringLongitude;
//        [ProfileEntity sharedInstance].latitude=stringLatitude;
        [self.delegate gotLocationSuccessfully:stringLatitude longitude:stringLongitude];
    }
   
    currentLocation = newLocation;
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [self.delegate gotErrorInFindingLocation];
}

@end
