//
//  Location.h
//  GoSnow
//
//  Created by Varsha Singh on 23/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationManagerDelegate <NSObject>

@optional

-(void)gotLocationSuccessfully:(NSString *)latitudeValue longitude:(NSString *)longitudeValue;
-(void)gotErrorInFindingLocation;

@end

@interface Location : NSObject<CLLocationManagerDelegate>
@property(nonatomic,strong)CLGeocoder *geocoder;

@property (nonatomic, weak) id<LocationManagerDelegate> delegate;
@property (nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic,retain) CLLocation        *currentLocation;

+(Location*)sharedInstance;

-(CLLocationManager *)callLocationManager;
-(CLLocationCoordinate2D)getCoordinates;

-(void)startUpdateLocation;
-(void)stopUpdateLocation;
@end
