//
//  InternetCheck.h
//  GoSnow
//
//  Created by Rishabh on 02/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Reachability;
@interface InternetCheck : NSObject
{
     Reachability* hostReach;
}


@property(nonatomic,assign) BOOL internetWorking;

/**
 *	@functionName	: sharedInstance
 *	@parameters		:
 *	@return			: Object of the class
 *	@description	: Creates the singleton object of the class
 */
+ (InternetCheck*) sharedInstance;
//+ (InternetCheck*) SharedInstance ;
@end
