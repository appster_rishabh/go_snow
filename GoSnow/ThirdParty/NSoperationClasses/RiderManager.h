#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#define kImageUploaded         @"kImageUploaded"
@protocol RiderManagerDelegate <NSObject>

- (void)riderRequestDidFailed:(NSMutableDictionary *)dictionary;
- (void)riderRequestDidFinish:(NSMutableDictionary *)dictionary;
@end

@interface RiderManager : NSObject
{
    
    BOOL iscancelled;
    NSInteger last;
    
    NSInteger totalCount;
}
@property (strong,nonatomic)ASINetworkQueue *network;
@property (nonatomic, assign) id <RiderManagerDelegate> delegate;
+(RiderManager *)sharedInstance;
-(void)callWebservice:(NSMutableDictionary *)dictionary methodName:(NSString *)methodName;

// 4: Declare a designated initializer.


@end