//
//  ImageUploading.h
//  GoSnow
//
//  Created by Rishabh on 28/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#define kImageUploaded         @"kImageUploaded"
@protocol ImageUploaderDelegate;


@interface ImageUploading : NSObject
{
    
    BOOL iscancelled;
    NSInteger last;
    
    NSInteger totalCount;
}
@property (strong,nonatomic)ASINetworkQueue *network;
@property (nonatomic, assign) id <ImageUploaderDelegate> delegate;
+(ImageUploading *)sharedInstance;
-(void)updatePhotos:(NSMutableArray *)totalPicArray selectedArray:(NSMutableArray *)selectedPicArray;
-(void)UploadImage:(NSMutableDictionary *)dictionary index:(NSInteger )index;
-(void)updatePhotosNew:(NSMutableArray *)totalPicArray selectedArray:(NSMutableArray *)selectedPicArray;
// 4: Declare a designated initializer.


@end
@protocol ImageUploaderDelegate <NSObject>
@required
- (void)imageUploaderDidFailed:(NSMutableDictionary *)downloader;
- (void)imageUploaderDidFinish:(NSMutableDictionary *)downloader;
@end

