//
//  RiderManager.m
//  GoSnow
//
//  Created by Rishabh on 03/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "RiderManager.h"

@implementation RiderManager
-(id)init
{
    if (self = [super init]) {
        _network = [[ASINetworkQueue alloc] init];
        _network.uploadProgressDelegate = self;
        // 2: Set the properties.
        
    }
    return self;
}
+(RiderManager *)sharedInstance {
    static dispatch_once_t pred;
    static RiderManager *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[RiderManager alloc] init];
    });
    
    return sharedInstance;
}
-(void)callWebservice:(NSMutableDictionary *)dictionary methodName:(NSString *)methodName{
    
    NSError* error;
    NSData* uploadData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KSERVICEURL,methodName]]];
    
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request addRequestHeader:@"content-type" value:@"application/json"];
    [request appendPostData:uploadData];
    
    // [request setStringEncoding:NSUTF8StringEncoding];
    [request setDelegate:self];
    //[request startAsynchronous];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDidFinishSelector:@selector(requestFinished:)];
    //[request startAsynchronous];
    [_network  addOperation:request];
    //
    [_network go];
    
}
-(void)requestFinished:(ASIFormDataRequest *)request
{
    if (iscancelled ) {
        return;
    }
    
    NSLog(@"%li",(long)[request tag]);
    last=[request tag];
    NSString* response = [request responseString];
    
    NSData *responseData = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    NSMutableDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    NSLog(@"%@",dictionary);
  
}
-(void)requestFailed:(ASIFormDataRequest *)request
{
    
    NSLog(@"requestFailed%@",request.error);
    
    NSString *respondingString = [request responseString];
    NSLog(@"response failed string--> %@", respondingString);
    
    // [_delegate imageDownloaderDidFailed:[]];
}
@end
