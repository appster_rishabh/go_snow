//
//  CustomPageControl.m
//  GoSnow
//
//  Created by Rishabh on 23/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "CustomPageControl.h"

@implementation CustomPageControl

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    activeImage = [UIImage imageNamed:@"swipe_active"];
    inactiveImage = [UIImage imageNamed:@"swipe_stroke"];
    self.tintColor=[UIColor colorWithPatternImage:inactiveImage];
    self.currentPageIndicatorTintColor=[UIColor colorWithPatternImage:activeImage];
    return self;
}

-(void) updateDots
{
    NSArray *subview=self.subviews;
    for (int i = 0; i < [self.subviews count]; i++)
    {
        NSArray *subviewss=[self.subviews objectAtIndex:i];
       // NSLog(@"%@",subviewss);
       /* UIImageView* dot = [self.subviews objectAtIndex:i];
        if (i == self.currentPage) dot.image = activeImage;
        else dot.image = inactiveImage;*/
    }
}

-(void) setCurrentPage:(NSInteger)page
{
    [super setCurrentPage:page];
    [self updateDots];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
