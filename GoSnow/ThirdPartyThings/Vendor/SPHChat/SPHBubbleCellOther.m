//
//  SPHBubbleCellOther.m
//  ChatBubble
//
//  Created by ivmac on 10/2/13.
//  Copyright (c) 2013 Conciergist. All rights reserved.
//

#import "SPHBubbleCellOther.h"
#import "RobotCondensedLightLabel.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#define messageWidth 260


@implementation SPHBubbleCellOther

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
        
    }
    return self;
}

-(void)SetCellData:(Messages *)messageData targetedView:(id)ViewControllerObject Atrow:(NSInteger)indexRow;
{
    NSString *messageText = messageData.messageText;
    CGSize boundingSize = CGSizeMake(messageWidth-20, 10000000);
    CGSize itemTextSize = [messageText sizeWithFont:[UIFont fontWithName:@"RobotoCondensed-Light" size:12.0]
                                  constrainedToSize:boundingSize
                                      lineBreakMode:NSLineBreakByWordWrapping];
    float textHeight = itemTextSize.height+7;
    int x=0;
    if (textHeight>200)
    {
        x=65;
    }else
        if (textHeight>150)
        {
            x=50;
        }
        else if (textHeight>80)
        {
            x=30;
        }else
            if (textHeight>50)
            {
                x=20;
            }else
                if (textHeight>30) {
                    x=8;
                }
    
    
    UIImageView *bubbleImage=[[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"msg_bg"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]];
    bubbleImage.tag=55;
    [self.contentView addSubview:bubbleImage];
    [bubbleImage setFrame:CGRectMake(WIDTH-itemTextSize.width-17-60,18,itemTextSize.width+30,textHeight+4)];
    
   // 375-50-17-60
    RobotCondensedLightLabel *messageTextview=[[RobotCondensedLightLabel alloc]initWithFrame:CGRectMake(10,0,itemTextSize.width, textHeight)];
    [bubbleImage addSubview:messageTextview];
   // messageTextview.editable=NO;
    messageTextview.numberOfLines=0;
    messageTextview.text = messageText;
    //messageTextview.dataDetectorTypes=UIDataDetectorTypeAll;
    messageTextview.textAlignment=NSTextAlignmentJustified;
    messageTextview.font=[UIFont fontWithName:@"RobotoCondensed-Light" size:12.0];
    messageTextview.backgroundColor=[UIColor clearColor];
    messageTextview.tag=indexRow;
   
    if (messageData.recieverImageUrl.length>0) {
        [self.Avatar_Image setImageWithURL:[NSURL URLWithString:messageData.recieverImageUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [self.Avatar_Image setImage:[UIImage imageNamed:@"Shared_users.png"]];
    }
    
    self.time_Label.text=messageData.messageTime;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    //messageTextview.scrollEnabled=NO;
    
    if ([messageData.messageStatus isEqualToString:kStatusSent]) {
        
        self.statusindicator.alpha=0.0;
        [self.statusindicator stopAnimating];
        self.statusImage.alpha=1.0;
        [self.statusImage setImage:[UIImage imageNamed:@"success"]];
        
    }else  if ([messageData.messageStatus isEqualToString:kStatusSeding])
    {
        self.statusImage.alpha=0.0;
        self.statusindicator.alpha=1.0;
        [self.statusindicator startAnimating];
        
    }
    else
    {
        self.statusindicator.alpha=0.0;
        [self.statusindicator stopAnimating];
        self.statusImage.alpha=1.0;
        [self.statusImage setImage:[UIImage imageNamed:kStatusFailed]];
        
    }
    
    self.Avatar_Image.layer.cornerRadius = 20.0;
    self.Avatar_Image.layer.masksToBounds = YES;
   // self.Avatar_Image.layer.borderColor = [UIColor whiteColor].CGColor;
    //self.Avatar_Image.layer.borderWidth = 2.0;
    
    
  
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
