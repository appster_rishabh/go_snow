//
//  RequestUsers.m
//  GoSnow
//
//  Created by Rishabh on 11/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "RequestUsers.h"

@implementation RequestUsers
@synthesize user_id;
@synthesize aboutMe;
@synthesize skillLevel;
@synthesize skillLevelID;
@synthesize riderTypeId;
@synthesize riderType;
@synthesize currentLocation;
@synthesize userPhotos;
@synthesize latitude;
@synthesize longtitude,address;
@synthesize firstName,lastName;
@synthesize facebookId,sessionId;
@synthesize age,plannedTripName;
@synthesize distance;
@synthesize friendCount;
@synthesize interestCount;
@synthesize locationCount;
@synthesize interestName;
@synthesize locationName;
@synthesize contactsName;
@end
