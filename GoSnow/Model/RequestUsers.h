//
//  RequestUsers.h
//  GoSnow
//
//  Created by Rishabh on 11/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestUsers : NSObject
@property (nonatomic, copy) NSString * facebookId;
@property (nonatomic, copy) NSString * sessionId;
@property (nonatomic, copy) NSString * user_id;
@property (nonatomic, copy) NSString * age;
@property (nonatomic, copy) NSString * aboutMe;
@property (nonatomic, copy) NSString * firstName;
@property (nonatomic, copy) NSString * lastName;
@property (nonatomic, copy) NSString * riderType;
@property (nonatomic, copy) NSString * riderTypeId;
@property (nonatomic, copy) NSString * skillLevel;
@property (nonatomic, copy) NSString * skillLevelID;
@property (nonatomic, copy) NSString * plannedTripId;
@property (nonatomic, copy) NSString * plannedTripName;
@property (nonatomic, copy) NSString * currentLocation;
@property (nonatomic, copy) NSString * latitude;
@property (nonatomic, copy) NSString * longtitude;
@property (nonatomic, copy) NSString * userPhotos;
@property (nonatomic, copy) NSString * address;
@property (nonatomic, copy) NSString *distance;
@property (nonatomic, copy) NSString * interestName;
@property (nonatomic, copy) NSString * locationName;
@property (nonatomic, copy) NSString * contactsName;
@property (nonatomic, copy) NSString * friendCount;
@property (nonatomic, copy) NSString * interestCount;
@property (nonatomic, copy) NSString * locationCount;

@end
