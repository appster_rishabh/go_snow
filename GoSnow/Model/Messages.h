//
//  Messages.h
//  GoSnow
//
//  Created by Rishabh on 10/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Messages : NSObject
@property(nonatomic,copy)NSString *messageText;
@property(nonatomic,copy)NSString *messageId;
@property(nonatomic,copy)NSString *messageTime;
@property(nonatomic,copy)NSString *bubbleImageName;
@property(nonatomic,copy)NSString *messageType;
@property(nonatomic,copy)NSString *messageStatus;
@property(nonatomic,copy)NSString *readStatus;
@property(nonatomic,copy)NSString *messagesfrom;
@property(nonatomic,copy)NSString *senderImageUrl;
@property(nonatomic,copy)NSString *recieverImageUrl;
@property(nonatomic,copy)NSString *messageSenderId;
@property(nonatomic,copy)NSString *messageRecieverID;
@end
