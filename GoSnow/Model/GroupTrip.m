//
//  GroupTrip.m
//  GoSnow
//
//  Created by Varsha Singh on 23/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "GroupTrip.h"

@implementation GroupTrip

@synthesize destinationId;
@synthesize tripDescription;
@synthesize destinationName;
@synthesize season;
@synthesize groupId;
@synthesize groupOwnerName;
@synthesize tripUsersCount;
@synthesize tripUsersDetails;
@synthesize ownerId;
@synthesize isPending;
@synthesize tripUserId;
@synthesize isJoiningRequest;
@synthesize joinRequests;
@end
