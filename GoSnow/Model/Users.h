//
//  Users.h
//  GoSnow
//
//  Created by Rishabh on 02/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Users : NSObject
@property (nonatomic, copy) NSString * facebookId;
@property (nonatomic, copy) NSString * sessionId;
@property (nonatomic, copy) NSString * gender;
@property (nonatomic, copy) NSString * user_id;
@property (nonatomic, copy) NSString * deletedContactId;
@property (nonatomic, copy) NSString * age;
@property (nonatomic, copy) NSString * aboutMe;
@property (nonatomic, copy) NSString * firstName;
@property (nonatomic, copy) NSString * lastName;
@property (nonatomic, copy) NSString * riderType;
@property (nonatomic, copy) NSString * riderTypeId;
@property (nonatomic, copy) NSString * skillLevel;
@property (nonatomic, copy) NSString * skillLevelID;
@property (nonatomic, copy) NSString * plannedTripId;
@property (nonatomic, copy) NSString * plannedTripName;
@property (nonatomic, copy) NSString * currentLocation;
@property (nonatomic, copy) NSString * latitude;
@property (nonatomic, copy) NSString * longitude;
@property (nonatomic, copy) NSString * userPhotos;
@property (nonatomic, copy) NSString * userPhotos250;
@property (nonatomic, copy) NSString * address;
@property (nonatomic, copy) NSString * distance;
@property (nonatomic, copy) NSString * profileImage;
@property (nonatomic, copy) NSString * interestId;
@property (nonatomic, copy) NSString * interestName;
@property (nonatomic, copy) NSString * locationName;
@property (nonatomic, copy) NSString * model;
@property (nonatomic, copy) NSString * tripDecription;
@property (nonatomic, copy) NSString * locationId;
@property (nonatomic, copy) NSString * contactsName;
@property (nonatomic, copy) NSString * friendCount;
@property (nonatomic, copy) NSString * interestCount;
@property (nonatomic, copy) NSString * locationCount;
@property (nonatomic, copy) NSString * inContactFrom;
@property  BOOL isSelect;
@property BOOL isInvited;

@end
