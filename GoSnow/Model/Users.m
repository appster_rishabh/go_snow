//
//  Users.m
//  GoSnow
//
//  Created by Rishabh on 02/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "Users.h"

@implementation Users
@synthesize user_id;
@synthesize aboutMe;
@synthesize skillLevel;
@synthesize skillLevelID;
@synthesize riderTypeId;
@synthesize riderType;
@synthesize currentLocation;
@synthesize userPhotos,userPhotos250;
@synthesize latitude;
@synthesize longitude,address;
@synthesize firstName,lastName;
@synthesize facebookId,sessionId;
@synthesize age,plannedTripName;
@synthesize distance;
@synthesize gender,interestId;
@synthesize profileImage;
@synthesize isSelect;
@synthesize friendCount;
@synthesize interestCount;
@synthesize locationCount;
@synthesize interestName;
@synthesize locationName;
@synthesize contactsName;
@synthesize locationId;
@synthesize isInvited;
@synthesize inContactFrom;
@synthesize model;
@synthesize tripDecription;
@synthesize deletedContactId;
@end
