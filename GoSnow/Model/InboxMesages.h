//
//  InboxMesages.h
//  GoSnow
//
//  Created by Rishabh on 09/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InboxMesages : NSObject

@property (nonatomic, copy) NSString * sender_id;
@property (nonatomic, copy) NSString * reciever_id;
@property (nonatomic, copy) NSString * tripMessageId;
@property (nonatomic, copy) NSString * age;
@property (nonatomic, copy) NSString * messageDate;
@property (nonatomic, copy) NSString * firstName;
@property (nonatomic, copy) NSString * lastName;
@property (nonatomic, copy) NSString * riderType;
@property (nonatomic, copy) NSString * profilePic;
@property (nonatomic, copy) NSString * userMessage;
@property (nonatomic, copy) NSString * messageStatus;
@property (nonatomic, copy) NSString * unreadMessageCount;

@property BOOL isSelect;
@end
