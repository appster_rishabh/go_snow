//
//  ModelMapper.h
//  GoSnow
//
//  Created by Rishabh Yadav on 21/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FacebookEntity.h"


@interface ModelMapper : NSObject

@end


@interface FacebookEntity (ASSOCIATION)

-(void) associatePropertyFromDict:(NSDictionary *)profileDictionary;

@end


