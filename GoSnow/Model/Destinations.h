//
//  Destinations.h
//  GoSnow
//
//  Created by Varsha Singh on 03/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Destinations : NSObject

@property (nonatomic, copy) NSString * destinationName;
@property (nonatomic, copy) NSString * destinationId;
@property (nonatomic, copy) NSString * latitude;
@property (nonatomic, copy) NSString * longitude;
@property BOOL isSelect;
@end
