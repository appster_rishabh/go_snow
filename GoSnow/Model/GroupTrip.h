//
//  GroupTrip.h
//  GoSnow
//
//  Created by Varsha Singh on 23/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupTrip : NSObject

@property (nonatomic, copy) NSString * tripDescription;
@property (nonatomic, copy) NSString * destinationId;
@property (nonatomic, copy) NSString * destinationName;
@property (nonatomic, copy) NSString * season;
@property (nonatomic, copy) NSString * groupId;
@property (nonatomic, copy) NSString * ownerId;
@property BOOL isPending;//tripUserId
@property BOOL isJoiningRequest;
//joinRequests
@property (nonatomic, copy) NSMutableArray * joinRequests;
@property (nonatomic, copy) NSString * tripUserId;
@property (nonatomic, copy) NSString * groupOwnerName;
@property (nonatomic, copy) NSString * tripUsersCount;
@property (nonatomic, copy) NSMutableArray * tripUsersDetails;

@end
