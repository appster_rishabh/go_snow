//
//  GroupTripManager.m
//  GoSnow
//
//  Created by Varsha Singh on 23/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "GroupTripManager.h"
#import "Users.h"
@implementation GroupTripManager

#pragma mark - Shared Instance
+ (GroupTripManager *)sharedInstance {
    static dispatch_once_t pred;
    static GroupTripManager *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[GroupTripManager alloc] init];
    });
    
    return sharedInstance;
}

-(id)init
{
    if (self = [super init]) {
        _arrayTrips = [[NSMutableArray alloc] init];
        _arraySnowBuddies = [[NSMutableArray alloc]init];
    }
    return self;
}

#pragma mark - Web Service Method
-(void)handleDataForGroupTrip:(NSMutableDictionary *)params method:(NSString *)method
{
    if (![self isNetworkAvailable]) {
        
        [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    __weak typeof(self) weakself = self;
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:method withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        //[sel];
        [weakself networkCallCompletion:successData withError:error];
    }];
    gsRequestWebservices = nil;
    
}

#pragma mark - Network Manager Delegate
- (void)networkCallCompletion:(NSData *)successData withError:(NSError *)error
{
    NSDictionary *dict = [successData objectFromJSONData];
    if (!error && dict!=nil)
    {
        if ([[dict objectForKey:@"status"] isEqualToString:@"OK"])
        {
            if ([[dict valueForKey:@"api"] isEqualToString:@"trip/getByUserId"]) {
                [self parseDataForGettingGroupDetails:[dict valueForKey:@"trips"]];
            }
            else if ([[dict valueForKey:@"api"] isEqualToString:@"trip/add"] || [[dict valueForKey:@"api"] isEqualToString:@"trip/update"])
            {
                [self parseDataForCreatingGroup:[dict valueForKey:@"currentTrip"]];
            }
            else if ([[dict valueForKey:@"api"] isEqualToString:@"contact/search"])
            {
                [self fetchSnowBuddies:[dict valueForKey:@"contacts"]];
            }
            else if ([[dict valueForKey:@"api"] isEqualToString:@"tripUser/leave"])
            {
                [self snowBuddyLeftGroup];
            }
            else if ([[dict valueForKey:@"api"] isEqualToString:@"tripUser/add"])
            {
                [self inviteSnowBuddies];
            }////tripUser/accept
            else if ([[dict valueForKey:@"api"] isEqualToString:@"tripUser/accept"] ||[[dict valueForKey:@"api"] isEqualToString:@"tripUser/acceptToJoin"] )
            {
                [_delegate requestAcceptedSuccesfully:dict];
            }
            else if ([[dict valueForKey:@"api"] isEqualToString:@"tripUser/reject"] ||[[dict valueForKey:@"api"] isEqualToString:@"tripUser/rejectToJoin"] )
            {
                 [_delegate requestRejectedSuccesfully:nil];
            }
        }
        else
        {
            if ([_delegate respondsToSelector:@selector(failedWithError:)]){
                [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:[dict valueForKey:@"apiMessage"] forKey:@"Error"]];
            }
        }
    }
    else
    {
        if ([_delegate respondsToSelector:@selector(failedWithError:)]){
            [_delegate failedWithError:[NSDictionary dictionaryWithObjectsAndKeys:@"Network error. Please try again!", @"Error", nil]];
        }
    }
}

#pragma mark - Parse Data


-(void)parseDataForGettingGroupDetails:(NSMutableArray *)arrayTrips
{
    [_arrayTrips removeAllObjects];
    for (int i=0; i<[arrayTrips count]; i++) {
        GroupTrip *groupTrip=[[GroupTrip alloc] init];
        
        NSMutableDictionary *dict=[arrayTrips objectAtIndex:i];
        groupTrip.destinationId = [dict valueForKey:@"destinationId"];
        groupTrip.season = [dict valueForKey:@"season"];
        groupTrip.tripDescription =[self getTripDesriptionFromBase64: [dict valueForKey:@"tripDescription"]];
        groupTrip.groupId = [dict valueForKey:@"id"];
        groupTrip.destinationName = [dict valueForKey:@"destinationName"];
        groupTrip.tripUsersCount = [dict valueForKey:@"tripUsersCount"];
        groupTrip.tripUsersDetails = [dict valueForKey:@"tripUsers"];
        groupTrip.tripUserId=[dict valueForKey:@"tripUserId"];
        groupTrip.ownerId=[dict valueForKey:@"ownerUserId"];
        groupTrip.isPending=[[dict valueForKey:@"tripUserStatus"] boolValue];
        int joiningRequestCount=[[dict valueForKey:@"joinRequestsCount"] intValue];
        if (joiningRequestCount>0) {
            groupTrip.isJoiningRequest=YES;
        }
        else
        {
            groupTrip.isJoiningRequest=NO;
        }
        groupTrip.joinRequests=[dict valueForKey:@"joinRequests"];
        NSString *stringFullName;
       
        if ([[dict valueForKey:@"firstName"]length]>0)
            stringFullName = [NSString stringWithFormat:@"%@",[dict valueForKey:@"firstName"]];
        
        if ([[dict valueForKey:@"lastName"]length]>0)
            stringFullName =[stringFullName stringByAppendingString:[NSString stringWithFormat:@" %@",[dict valueForKey:@"lastName"]]];
        
        groupTrip.groupOwnerName = stringFullName;
        
        [_arrayTrips addObject:groupTrip];
    }
    [_delegate fetchGroupDetails:_arrayTrips];
}

-(void)parseDataForCreatingGroup:(NSMutableDictionary *)dictGroup
{
    GroupTrip *groupTrip=[[GroupTrip alloc] init];
    
   // NSMutableDictionary *dict=[dictGroup objectAtIndex:0];
    groupTrip.destinationId = [dictGroup valueForKey:@"destinationId"];
    groupTrip.season = [dictGroup valueForKey:@"season"];
    groupTrip.tripDescription =[self getTripDesriptionFromBase64: [dictGroup valueForKey:@"tripDescription"]];
    groupTrip.groupId = [dictGroup valueForKey:@"id"];
    groupTrip.destinationName = [dictGroup valueForKey:@"destinationName"];
    
    [_delegate groupCreatedSuccessfully:groupTrip];
}

-(void)snowBuddyLeftGroup
{
    [_delegate snowBuddyLeftGroup];
}
-(void)inviteSnowBuddies
{
    [_delegate inviteSnowBuddiesToGroup];
}
-(void)fetchSnowBuddies:(NSMutableArray *)arraySnowBuddies
{
    [_arrayTrips removeAllObjects];
    for (int i=0; i<[arraySnowBuddies count]; i++) {
        Users *users=[[Users alloc] init];
        NSMutableDictionary *dict=[arraySnowBuddies objectAtIndex:i];
        users.firstName=[dict valueForKey:@"contactFirstName"];
        users.lastName=[dict valueForKey:@"contactLastName"];
        users.facebookId=[dict valueForKey:@"contactFacebookId"];
        users.skillLevel=[dict valueForKey:@"contactSkillLevelName"];
        users.skillLevelID=[dict valueForKey:@"contactSkillLevelId"];
        users.riderType=[dict valueForKey:@"contactRiderTypeName"];
        users.riderTypeId=[dict valueForKey:@"contactRiderTypeId"];
        users.userPhotos=[dict valueForKey:@"contactPhotos"];
        users.user_id=[dict valueForKey:@"contactId"];
        users.profileImage=[dict valueForKey:@"contactProfileThumb"];
        users.isInvited=[[dict valueForKey:@"isInvited"] boolValue];
        users.inContactFrom=[dict valueForKey:@"inContactsFor"];
        users.isSelect=NO;
     
        [_arrayTrips addObject:users];
    }
     [_delegate addSnowBudiesToGroup:_arrayTrips];
}

#pragma mark network check
-(BOOL)isNetworkAvailable
{
    if ([InternetCheck sharedInstance].internetWorking)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}
-(NSString *)getTripDesriptionFromBase64:(NSString *)base64
{
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64 options:0];
    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    NSData *newdata=[decodedString dataUsingEncoding:NSUTF8StringEncoding
                                allowLossyConversion:YES];
    NSString *descriptionData=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
    return descriptionData;
}
@end
