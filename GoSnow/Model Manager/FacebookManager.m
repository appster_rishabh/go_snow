//
//  FacebookManager.m
//  GoSnow
//
//  Created by Rishabh Yadav on 21/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "FacebookManager.h"
#import "UserProfile.h"
#import "UserInfoEntity+Update.h"
@implementation FacebookManager
{
    AppDelegate *appdelegate;
}
static FacebookManager* facebookManger;

#pragma - Facebook shared Instance
+(FacebookManager*)sharedInstance
{
    if (facebookManger == nil) {
        facebookManger = [[FacebookManager alloc] init];
    }
    return facebookManger;
}

#pragma - Fecth Facebook Profile
-(void)fetchProfilewithDelegate:(id<FacebookDelegate>)delegate
{
    _delegate = delegate;
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSArray *permissions = @[@"public_profile",@"user_likes",@"user_friends",@"email",@"user_location",@"user_birthday",@"user_hometown"];
    
    [FBSession.activeSession closeAndClearTokenInformation];
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState state,
                                                      NSError *error)
     {
         // if login fails for any reason, display alert
         if (error)
         {
             [_delegate loginFailedwitError:[NSDictionary dictionaryWithObjectsAndKeys:@"Facebook login error.", @"Error", nil]];
         }
         else if (session.isOpen)
         {
             [self performSelectorOnMainThread:@selector(FacebookRegister) withObject:nil waitUntilDone:YES];
         }
         else
         {
             
         }
     }];
}

#pragma - Register with Facebook
-(void)FacebookRegister
{
    [_delegate showLoader];
    [FBRequestConnection startWithGraphPath:@"/me"
                          completionHandler:^(FBRequestConnection *connection, id<FBGraphUser> result, NSError *error)
     {
         if (error)
         {
             [_delegate hideLoader];
             [_delegate loginFailedwitError:[NSDictionary dictionaryWithObjectsAndKeys:@"Facebook error.", @"Error", nil]];
         }
         else
         {
             [self performSelectorOnMainThread:@selector(callAPIForFacebook:) withObject:result waitUntilDone:YES];
         }
     }];
}

#pragma - Get Information from Facebook
-(void)callAPIForFacebook:(NSDictionary*)dictionaryGetUserInfo
{
    NSMutableDictionary *dictionarySetUserInfo = [NSMutableDictionary dictionary];
    
    if ([dictionaryGetUserInfo count]>0) {
        if ([[dictionaryGetUserInfo objectForKey:@"id"]length]>0) {
            [dictionarySetUserInfo setObject:[dictionaryGetUserInfo objectForKey:@"id"] forKey:@"facebookId"];
        }
        if ([[dictionaryGetUserInfo objectForKey:@"first_name"]length]>0) {
            NSData *nameData=[[dictionaryGetUserInfo objectForKey:@"first_name"] dataUsingEncoding:NSUTF8StringEncoding];
            NSString *nameBase64=[nameData base64EncodedStringWithOptions:0];
            [dictionarySetUserInfo setObject:nameBase64 forKey:@"firstName"];
        }
        if ([[dictionaryGetUserInfo objectForKey:@"last_name"]length]>0) {
            NSData *nameData=[[dictionaryGetUserInfo objectForKey:@"last_name"] dataUsingEncoding:NSUTF8StringEncoding];
            NSString *nameBase64=[nameData base64EncodedStringWithOptions:0];
            [dictionarySetUserInfo setObject:nameBase64 forKey:@"lastName"];
        }
        if ([[dictionaryGetUserInfo objectForKey:@"gender"]length]>0) {
            [dictionarySetUserInfo setObject:[dictionaryGetUserInfo objectForKey:@"gender"] forKey:@"gender"];
        }
        if ([[dictionaryGetUserInfo objectForKey:@"birthday"]length]>0) {
            [dictionarySetUserInfo setObject:[[UserProfile sharedInstance]getBirthDay:[dictionaryGetUserInfo objectForKey:@"birthday"]] forKey:@"age"];
        }
        
        NSString *deviceToken=[[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
        
        if ([deviceToken length]>0)
            [dictionarySetUserInfo setObject:deviceToken forKey:@"deviceToken"];
    }
    if (![self isNetworkAvailable]) {
    
        [_delegate loginFailedwitError:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
  __weak typeof(self) weakself = self;
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:@"auth/signin" withDictionary:dictionarySetUserInfo andCompletionHandler:^(NSError *error, NSData * successData){
        [weakself networkCallCompletion:successData withError:error];
    }];
    
    gsRequestWebservices = nil;
}

-(void)fetchContactwithDelegate:(id<FacebookDelegate>)delegate
{
    
}

#pragma mark - Network Manager Protocol

- (void)networkCallCompletion:(NSData *)successData withError:(NSError *)error
{
    NSDictionary *dict = [successData objectFromJSONData];
    [_delegate hideLoader];
    if (!error && dict!=nil)
    {
        if ([[dict objectForKey:@"status"] isEqualToString:@"OK"]) {
            if ([_delegate respondsToSelector:@selector(loginSuccessfully:)])
            {
                [UserInfoEntity addUserInfo:[dict valueForKey:@"profile"] sessionId:[dict valueForKey:@"sessionId"] inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
                
                [_delegate loginSuccessfully:dict];
            }
        }
        else
        {
            if ([_delegate respondsToSelector:@selector(loginFailedwitError:)]){
                [_delegate loginFailedwitError:dict];
            }
        }
    }
    else
    {
        if ([_delegate respondsToSelector:@selector(loginFailedwitError:)]){
            [_delegate loginFailedwitError:[NSDictionary dictionaryWithObjectsAndKeys:@"Network error. Please try again!", @"Error", nil]];
        }
    }
}

#pragma mark - Get Friends from Facebook
-(void)getFriendsFromFacebook
{
    [self fetchUserInfoFromCoreData];
    
    FBRequest *friendsRequest = [FBRequest requestForMyFriends];
    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  NSDictionary* result,
                                                  NSError *error) {
        
        if (error) {
            NSLog(@"Error: %@", [error localizedDescription]);
        } else {
            NSArray *friends = [result objectForKey:@"data"];
            NSMutableArray *arrayFriendById = [[NSMutableArray alloc]init];
            for (NSDictionary<FBGraphUser>* friend in friends)
            {
                [arrayFriendById addObject:friend.objectID];
            }
            if ([arrayFriendById count]>0)
                [self updateFacebookFriends:arrayFriendById];
        }
    }];
    [self getInterests];
}


#pragma mark - Get Interests from Facebook
-(void)getInterests
{
    [FBRequestConnection startWithGraphPath:@"/me/likes"
                          completionHandler:^(FBRequestConnection *connection, id<FBGraphUser> result, NSError *error)
     {
         if (error) {
             NSLog(@"Error: %@", [error localizedDescription]);
         } else {
             NSArray *friendsLikes = [result objectForKey:@"data"];
             NSMutableArray *arrayLikes= [[NSMutableArray alloc]init];
             
             for (int count = 0; count <[friendsLikes count]; count++)
             {
                 [[arrayLikes valueForKey:@"name"]objectAtIndex:count];
             }
             
             if ([arrayLikes count]>0)
                 [self updateFacebookInterests:arrayLikes];
         }
     }];
}

#pragma mark - Fetch User Info From Core Data
-(void)fetchUserInfoFromCoreData
{
    userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}

#pragma mark - Update Facebook Information Ids
-(void)updateFacebookFriends:(NSMutableArray *)arrayFriendsIds
{
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:userInfoEntity.riderTypeId forKey:@"riderTypeId"];
    [params setObject:userInfoEntity.skillLevelId forKey:@"skillLevelId"];
    
    if ([arrayFriendsIds count]>0)
        [params setObject:arrayFriendsIds forKey:@"facebookIds"];
    [UserProfile Callback:params method:@"user/updateProfile" completion:^(BOOL succeeded, NSDictionary *dict) { {
        if (succeeded) {
          [UserInfoEntity updateUserInfo:[dict valueForKey:@"profile"] sessionId:[dict valueForKey:@"sessionId"] inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        }
        
    }
    }];
}

#pragma mark - Update Facebook Information Interests
-(void)updateFacebookInterests:(NSMutableArray *)arrayFriendsInterests
{
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:userInfoEntity.riderTypeId forKey:@"riderTypeId"];
    [params setObject:userInfoEntity.skillLevelId forKey:@"skillLevelId"];
    
    if ([arrayFriendsInterests count]>0)
        [params setObject:arrayFriendsInterests forKey:@"facebookInterests"];
    [UserProfile Callback:params method:@"user/updateProfile" completion:^(BOOL succeeded, NSDictionary *dict) { {
        if (succeeded) {
            [UserInfoEntity updateUserInfo:[dict valueForKey:@"profile"] sessionId:[dict valueForKey:@"sessionId"] inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        }
        
    }
    }];

   }


#pragma mark - Facebook Logout
+ (void)facebookLogout
{
    [FBSession.activeSession closeAndClearTokenInformation];
}

#pragma mark
-(BOOL)isNetworkAvailable
{
    if ([InternetCheck sharedInstance].internetWorking)
    {
        return YES;
    }
    else
    {
        return YES;
    }
    return YES;
}
@end
