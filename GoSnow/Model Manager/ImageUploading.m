//
//  ImageUploading.m
//  GoSnow
//
//  Created by Rishabh on 28/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ImageUploading.h"

@implementation ImageUploading

-(id)init
{
    if (self = [super init]) {
        _network = [[ASINetworkQueue alloc] init];
        _network.uploadProgressDelegate = self;
        
    }
    return self;
}

#pragma mark -  Shared Instance
+(ImageUploading *)sharedInstance {
    static dispatch_once_t pred;
    static ImageUploading *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[ImageUploading alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark -  Upload Photos
-(void)updatePhotos:(NSMutableArray *)totalPicArray selectedArray:(NSMutableArray *)selectedPicArray
{
    if (![self isNetworkAvailable]) {
        
        [_delegate imageUploaderDidFailed:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    
    for (int i=0; i<[totalPicArray count]; i++) {
        NSMutableDictionary *dict=[totalPicArray objectAtIndex:i];
        BOOL isSelected=[[dict valueForKey:@"isSelected"] boolValue];
        BOOL isFromPicker=[[dict valueForKey:@"isFromPicker"] boolValue];
        
        if (isFromPicker) {
            NSMutableDictionary *params=[NSMutableDictionary dictionary];
            NSData* imageData = UIImageJPEGRepresentation([dict valueForKey:@"image"], 0.6);
            NSString *imageBase=[imageData base64EncodedStringWithOptions:0];
            [params setObject:imageBase forKey:@"image"];
            if (isSelected) {
                [params setObject:@"1" forKey:@"isProfileImage"];
            }
            else
            {
                [params setObject:@"0" forKey:@"isProfileImage"];
            }
            NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
            [params setObject:sessionId forKey:@"sessionId"];
            [self UploadImage:params index:0];
            [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isFromPicker"];
        }
        else
        {
            if (isSelected)
            {
                NSMutableDictionary *params=[NSMutableDictionary dictionary];
                [params setObject:@"1" forKey:@"isProfileImage"];
                NSArray *arr=[[dict valueForKey:@"image"] componentsSeparatedByString:@"/"];
                NSString *profileImageName=[arr lastObject];
                [params setObject:profileImageName forKey:@"imageUrl"];
                NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
                [params setObject:sessionId forKey:@"sessionId"];
                [self UploadImage:params index:0];
            }
        }
    }
}

#pragma mark -  Upload Image
-(void)UploadImage:(NSMutableDictionary *)dictionary index:(NSInteger )index{
    // NSString *idd=[[self.Total_arr objectAtIndex:0] valueForKey:@"id"];
    NSError* error;
    NSData* uploadData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@user/addPhoto",KSERVICEURL]]];
    [request setTag:index];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request addRequestHeader:@"content-type" value:@"application/json"];
    [request appendPostData:uploadData];
    
    [request setDelegate:self];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDidFinishSelector:@selector(requestFinished:)];
    [_network  addOperation:request];
    [_network go];
}

#pragma mark -  ASIFormDataRequest Methods
-(void)requestFinished:(ASIFormDataRequest *)request
{
    if (iscancelled ) {
        return;
    }
    
    last=[request tag];
    NSString* response = [request responseString];
    
    NSData *responseData = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    NSMutableDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    if ([[dictionary objectForKey:@"status"] isEqualToString:@"OK"]) {
        
        //[self updateProfileImages:[dictionary valueForKey:@"profile"]];
        UserInfoEntity *updatedUserInfoEntity=[UserInfoEntity updateUserPhotos:[dictionary valueForKey:@"profile"] sessionId:[dictionary valueForKey:@"sessionId"] inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        
        if ([_delegate respondsToSelector:@selector(imageUploaderDidFinish:)])
        {
            [_delegate imageUploaderDidFinish:updatedUserInfoEntity];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kImageUploaded object:userInfoEntity userInfo:dictionary];
        }
    }
}

-(void)updateProfileImages:(NSMutableDictionary *)dict
{
    userInfoEntity.userPhotos=[dict valueForKey:@"userPhotos"];
    userInfoEntity.profileImage=[dict valueForKey:@"profileImage"];
}

-(void)requestFailed:(ASIFormDataRequest *)request
{
    [_delegate imageUploaderDidFailed:nil];
    // NSString *respondingString = [request responseString];
    // NSLog(@"response failed string--> %@", respondingString);
}
-(BOOL)isNetworkAvailable
{
    if ([InternetCheck sharedInstance].internetWorking)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}

@end
