//
//  DestinationManager.h
//  GoSnow
//
//  Created by Varsha Singh on 03/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Destinations.h"

@protocol DestinationDelegate <NSObject>

@optional
-(void)fetchDestinationSuccessfully:(NSMutableArray *)arrayDestinations;
-(void)failedWithError:(NSDictionary*)dictionaryError;
-(void)showLoader;
-(void)hideLoader;
@end

@interface DestinationManager : NSObject

@property(nonatomic,strong)NSMutableArray *arrayTotalDestinations;
@property(nonatomic,weak) id <DestinationDelegate> delegate;

+(DestinationManager *)sharedInstance;
-(void)getDestinationsFromWebService;

@end
