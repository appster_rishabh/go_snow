//
//  ImageUploading.h
//  GoSnow
//
//  Created by Rishabh on 28/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@protocol ImageUploaderDelegate;


@interface ImageUploading : NSObject
{
    BOOL iscancelled;
    NSInteger last;
    NSInteger totalCount;
    UserInfoEntity *userInfoEntity;
}
@property (strong,nonatomic)ASINetworkQueue *network;
@property (nonatomic, weak) id <ImageUploaderDelegate> delegate;

+(ImageUploading *)sharedInstance;

-(void)UploadImage:(NSMutableDictionary *)dictionary index:(NSInteger )index;
-(void)updatePhotos:(NSMutableArray *)totalPicArray selectedArray:(NSMutableArray *)selectedPicArray;

@end
@protocol ImageUploaderDelegate <NSObject>
@required
- (void)imageUploaderDidFailed:(NSMutableDictionary *)errorDictionary;
- (void)imageUploaderDidFinish:(UserInfoEntity *)userInfoEntity;
@end

