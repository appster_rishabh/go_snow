//
//  SuggestedUsers.m
//  GoSnow
//
//  Created by Rishabh on 02/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "SuggestedUsers.h"

@implementation SuggestedUsers

#pragma mark - Shared Instance
+ (SuggestedUsers *)sharedInstance {
    static dispatch_once_t pred;
    static SuggestedUsers *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[SuggestedUsers alloc] init];
    });
    
    return sharedInstance;
}

-(id)init
{
    if (self = [super init]) {
        _totalUsers = [[NSMutableArray alloc] init];
       // _network.uploadProgressDelegate = self;
        // 2: Set the properties.
        
    }
    return self;
}

#pragma mark - Web Serice Method
-(void)WebSericeCallForSuggestedRiders:(NSString *)method sessionId:(NSString *)sessionId
{
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    
    [params setObject:sessionId forKey:@"sessionId"];
    [params setObject:@"1" forKey:@"selectAllFields"];
    NSString *latitude=[[NSUserDefaults standardUserDefaults] valueForKey:kLatitude];
     NSString *longtitude=[[NSUserDefaults standardUserDefaults] valueForKey:kLongtitude];
    if (latitude.length>0) {
        [params setObject:latitude forKey:@"latitude"];
        [params setObject:longtitude forKey:@"longitude"];
    }
     __weak typeof(self) weakself = self;
    
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:method withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        
        [weakself networkCallCompletion:successData withError:error];
    }];
    gsRequestWebservices=nil;
    
}
-(void)WebSericeCall:(NSString *)destinationId sessionId:(NSString *)sessionId latitude:(NSString *)latitude longtitude:(NSString *)longtitude groupTripId:(NSString *)grouptripId
{
    if (![self isNetworkAvailable]) {
        
        [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    if (grouptripId.length>0) {
        [params setObject:grouptripId forKey:@"excludeTripId"];
    }
    [params setObject:sessionId forKey:@"sessionId"];
    [params setObject:@"1" forKey:@"selectAllFields"];
    NSString *stringlatitude=[[NSUserDefaults standardUserDefaults] valueForKey:kLatitude];
    NSString *stringlongtitude=[[NSUserDefaults standardUserDefaults] valueForKey:kLatitude];
    if (latitude.length>0) {
        [params setObject:stringlatitude forKey:@"latitude"];
        [params setObject:stringlongtitude forKey:@"longitude"];
    }
    [self callWebServieAPI:params];
}

-(void)callWebServieAPI:(NSMutableDictionary *)params
{
    if (![self isNetworkAvailable]) {
        
        [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
     __weak typeof(self) weakself = self;
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:@"user/listByDistance" withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        //[sel];
        [weakself networkCallCompletion:successData withError:error];
    }];
    gsRequestWebservices = nil;
}

#pragma mark - Network Delegate
- (void)networkCallCompletion:(NSData *)successData withError:(NSError *)error
{
    NSDictionary *dict = [successData objectFromJSONData];
    
    if (!error && dict!=nil)
    {
        if ([[dict objectForKey:@"status"] isEqualToString:@"OK"])
        {
            if ([[dict valueForKey:@"api"] isEqualToString:@"user/listUserTripByDistance"])
            {
                [self parseData:[dict valueForKey:@"suggestions"]];
            }
            else
            {
                 [self parseData:[dict valueForKey:@"users"]];
            }
            
        }
        else
        {
            if ([_delegate respondsToSelector:@selector(failedWithError:)]){
               [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:[dict valueForKey:@"apiMessage"] forKey:@"Error"]];
            }
        }
    }
    else
    {
        if ([_delegate respondsToSelector:@selector(failedWithError:)]){
            
            [_delegate failedWithError:[NSDictionary dictionaryWithObjectsAndKeys:@"Network error. Please try again!", @"Error", nil]];
        }
    }
}

#pragma mark - Parse Data
-(void)parseData:(NSMutableArray *)userArray
{
    [_totalUsers removeAllObjects];
    for (int i=0; i<[userArray count]; i++) {
        Users *users=[[Users alloc] init];
        NSMutableDictionary *dict=[userArray objectAtIndex:i];
        users.firstName=[dict valueForKey:@"firstName"];
        users.lastName=[dict valueForKey:@"lastName"];
        users.facebookId=[dict valueForKey:@"facebookId"];
        users.skillLevel=[dict valueForKey:@"skillLevelName"];
        users.skillLevelID=[dict valueForKey:@"skillLevelId"];
        users.riderType=[dict valueForKey:@"riderTypeName"];
        users.riderTypeId=[dict valueForKey:@"riderTypeId"];
        users.userPhotos=[dict valueForKey:@"userPhotos"];
        users.user_id=[dict valueForKey:@"id"];
        users.age=[dict valueForKey:@"age"];
        users.plannedTripName=[dict valueForKey:@"nextDestinationName"];
        users.plannedTripId=[dict valueForKey:@"nextDestinationId"];
        users.aboutMe=[self getStringFromBase64:[dict valueForKey:@"aboutMe"]];
        users.address=[dict valueForKey:@"address"];
        users.userPhotos250=[dict valueForKey:@"userPhotos250"];
        users.friendCount=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sharedContactCount"]];
        users.interestCount=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sharedInterestCount"]];
        users.locationCount=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sharedLocationCount"]];
        users.tripDecription=[self getStringFromBase64:[dict valueForKey:@"tripDescription"]];
        users.interestName=[self getIneterests:[dict valueForKey:@"sharedInterests"]];
         users.locationName=[self getLocations:[dict valueForKey:@"sharedLocations"]];
         users.contactsName=[self getContacts:[dict valueForKey:@"sharedContacts"]];
        users.distance=[dict valueForKey:@"distance"];
        users.model=[dict valueForKey:@"model"];
        [_totalUsers addObject:users];
    }
    [_delegate fetchUsersSuccessfully:_totalUsers];
}
-(NSString *)getIneterests:(NSArray *)interestArray
{
    NSString *interests=[[NSString alloc] init];;
    for (int i=0; i<[interestArray count]; i++) {
        interests= [interests stringByAppendingString:[[interestArray objectAtIndex:i] valueForKey:@"interestName"]];
        if (i<[interestArray count]-1)
            interests= [interests stringByAppendingString:@", "];
        
    }
    if (interests.length<1) {
        return  interests=@"";
    }
    return interests;
}
-(NSString *)getLocations:(NSArray *)locationsArray
{
    NSString *locations=[[NSString alloc] init];;
    for (int i=0; i<[locationsArray count]; i++) {
        locations=[locations stringByAppendingString:[[locationsArray objectAtIndex:i] valueForKey:@"destinationName"]];
        
        if (i<[locationsArray count]-1)
            locations= [locations stringByAppendingString:@", "];
    }
    if (locations.length<1) {
        return  locations=@"";
    }
    return locations;
}
-(NSString *)getContacts:(NSArray *)contactsArray
{
    NSString *contacts=[[NSString alloc] init];;
    for (int i=0; i<[contactsArray count]; i++) {
        contacts= [contacts stringByAppendingString:[[contactsArray objectAtIndex:i] valueForKey:@"contactProfileThumb"]];
        if (i<[contactsArray count]-1)
            contacts= [contacts stringByAppendingString:@","];
        
    }
    if (contacts.length<1) {
        return contacts=@"";
    }
    return contacts;
}
-(BOOL)isNetworkAvailable
{
    if ([InternetCheck sharedInstance].internetWorking)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}
-(NSString *)removeLastCharacter:(NSString *)stringText
{
    NSString *lastCharacter=[stringText substringFromIndex:[stringText length]-1];
    if ([lastCharacter isEqualToString:@","]) {
        stringText=[stringText substringToIndex:[stringText length]-1];
    }
    return stringText;
}
-(NSString *)getStringFromBase64:(NSString *)base64
{
    if (base64.length<1) {
        return @"";
    }
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64 options:0];
    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    NSData *newdata=[decodedString dataUsingEncoding:NSUTF8StringEncoding
                                allowLossyConversion:YES];
    NSString *descriptionData=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
    return descriptionData;
}
@end
