//
//  DestinationManager.m
//  GoSnow
//
//  Created by Varsha Singh on 03/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "DestinationManager.h"

@implementation DestinationManager

#pragma mark - Shared Instance
+ (DestinationManager *)sharedInstance {
    static dispatch_once_t pred;
    static DestinationManager *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[DestinationManager alloc] init];
    });
    
    return sharedInstance;
}

-(id)init
{
    if (self = [super init]) {
        _arrayTotalDestinations = [[NSMutableArray alloc] init];
        
    }
    return self;
}

#pragma mark - Web Service Method
-(void)getDestinationsFromWebService
{
    if (![self isNetworkAvailable]) {
        
        [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    __weak typeof(self) weakself = self;
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:@"destination/list" withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        //[sel];
        [weakself networkCallCompletion:successData withError:error];
    }];
    gsRequestWebservices = nil;
    
   
}

#pragma mark - Network Manager Delegate
- (void)networkCallCompletion:(NSData *)successData withError:(NSError *)error
{
    NSDictionary *dict = [successData objectFromJSONData];
    [_delegate hideLoader];
    if (!error && dict!=nil)
    {
        if ([[dict objectForKey:@"status"] isEqualToString:@"OK"]) {
            if ([_delegate respondsToSelector:@selector(fetchDestinationSuccessfully:)])
            {
                [self parseData:[dict valueForKey:@"destinations"]];
            }
        }
        else
        {
            if ([_delegate respondsToSelector:@selector(failedWithError:)]){
                //[_delegate failedWithError:[NSDictionary dictionaryWithObjectsAndKeys:@"No result found.", @"Error", nil]];
                [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:[dict valueForKey:@"apiMessage"] forKey:@"Error"]];

            }
        }
    }
    else
    {
        if ([_delegate respondsToSelector:@selector(failedWithError:)]){
            [_delegate failedWithError:[NSDictionary dictionaryWithObjectsAndKeys:@"Network error. Please try again!", @"Error", nil]];
        }
    }
}

#pragma mark - Parse Data
-(void)parseData:(NSMutableArray *)arrayDestinations
{
    [_arrayTotalDestinations removeAllObjects];
    for (int i=0; i<[arrayDestinations count]; i++) {
        Destinations *destinations=[[Destinations alloc] init];
        destinations.isSelect = NO;
        
        NSMutableDictionary *dict=[arrayDestinations objectAtIndex:i];
        destinations.destinationName = [dict valueForKey:@"destinationName"];
        destinations.destinationId = [dict valueForKey:@"id"];
        destinations.latitude = [dict valueForKey:@"latitude"];
        destinations.longitude = [dict valueForKey:@"longitude"];
       
        [_arrayTotalDestinations addObject:destinations];
    }
    [_delegate fetchDestinationSuccessfully:_arrayTotalDestinations];
}
-(BOOL)isNetworkAvailable
{
    if ([InternetCheck sharedInstance].internetWorking)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}

@end
