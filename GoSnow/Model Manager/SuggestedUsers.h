//
//  SuggestedUsers.h
//  GoSnow
//
//  Created by Rishabh on 02/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Users.h"

@protocol SuggestedUsersDelegate <NSObject>

@optional
-(void)fetchUsersSuccessfully:(NSMutableArray*)successArray;
-(void)failedWithError:(NSDictionary*)errorDictionary;
@end

@interface SuggestedUsers : NSObject

@property(nonatomic,strong)NSMutableArray *totalUsers;
@property (nonatomic, weak) id<SuggestedUsersDelegate> delegate;
+(SuggestedUsers *)sharedInstance;
-(void)WebSericeCallForSuggestedRiders:(NSString *)method sessionId:(NSString *)sessionId;
-(void)WebSericeCall:(NSString *)destinationId sessionId:(NSString *)sessionId latitude:(NSString *)latitude longtitude:(NSString *)longtitude groupTripId:(NSString *)grouptripId;
@end
