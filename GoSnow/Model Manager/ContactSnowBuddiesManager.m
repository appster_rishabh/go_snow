//
//  ContactSnowBuddiesManager.m
//  GoSnow
//
//  Created by Madhavi Yalamaddi on 25/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ContactSnowBuddiesManager.h"
#import "Users.h"
@implementation ContactSnowBuddiesManager
#pragma mark - Shared Instance
+ (ContactSnowBuddiesManager *)sharedInstance {
    static dispatch_once_t pred;
    static ContactSnowBuddiesManager *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[ContactSnowBuddiesManager alloc] init];
    });
    
    return sharedInstance;
}
-(id)init
{
    if (self = [super init]) {
          _arrayContactSnowBuddies = [[NSMutableArray alloc]init];
    }
    return self;
}

#pragma mark - Web Service Method
-(void)handleDataForContactSnowBuddies:(NSMutableDictionary *)params method:(NSString *)method
{
    if (![self isNetworkAvailable]) {
        
        [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    __weak typeof(self) weakself = self;
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:method withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        //[sel];
        [weakself networkCallCompletion:successData withError:error];
    }];
    
    gsRequestWebservices = nil;
   
}
#pragma mark - Network Manager Delegate
- (void)networkCallCompletion:(NSData *)successData withError:(NSError *)error
{
    NSDictionary *dict = [successData objectFromJSONData];
    if (!error && dict!=nil)
    {
        if ([[dict objectForKey:@"status"] isEqualToString:@"OK"])
        {
            if ([[dict valueForKey:@"api"] isEqualToString:@"contact/search"])
            {
                [self fetchSnowBuddies:[dict valueForKey:@"contacts"]];
            }
            else if ([[dict valueForKey:@"api"] isEqualToString:@"contact/delete"])
            {
                [_delegate deleteContactSnowBuddies];
            }
            else if ([[dict valueForKey:@"api"] isEqualToString:@"user/search"])
            {
                
            }
        }
        else
        {
            if ([_delegate respondsToSelector:@selector(failedWithError:)]){
               [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:[dict valueForKey:@"apiMessage"] forKey:@"Error"]];
            }
        }
    }
    else
    {
        if ([_delegate respondsToSelector:@selector(failedWithError:)]){
            [_delegate failedWithError:[NSDictionary dictionaryWithObjectsAndKeys:@"Network error. Please try again!", @"Error", nil]];
        }
    }
}

#pragma mark - Parse Data
//-(void)fetchSnowBuddies:(NSMutableArray *)arraySnowBuddies
//{
//    [_delegate getContactSnowBudies:arraySnowBuddies];
//}

-(void)fetchSnowBuddies:(NSMutableArray *)arraySnowBuddies
{
    [_arrayContactSnowBuddies removeAllObjects];
    for (int i=0; i<[arraySnowBuddies count]; i++) {
        Users *users=[[Users alloc] init];
        NSMutableDictionary *dict=[arraySnowBuddies objectAtIndex:i];
        users.firstName=[dict valueForKey:@"contactFirstName"];
        users.lastName=[dict valueForKey:@"contactLastName"];
        users.facebookId=[dict valueForKey:@"contactFacebookId"];
        users.skillLevel=[dict valueForKey:@"contactSkillLevelName"];
        users.skillLevelID=[dict valueForKey:@"contactSkillLevelId"];
        users.riderType=[dict valueForKey:@"contactRiderTypeName"];
        users.riderTypeId=[dict valueForKey:@"contactRiderTypeId"];
        users.userPhotos=[dict valueForKey:@"contactPhotos"];
        users.user_id=[dict valueForKey:@"contactId"];
        users.age=[dict valueForKey:@"contactAge"];
        users.profileImage=[dict valueForKey:@"contactProfileThumb"];
        users.plannedTripName=[dict valueForKey:@"contactNextDestinationName"];
        users.plannedTripId=[dict valueForKey:@"contactNextDestinationId"];
        users.aboutMe=[self getStringFromBase64:[dict valueForKey:@"contactAboutMe"]];
        users.deletedContactId=[dict valueForKey:@"id"];
        users.address=[dict valueForKey:@"contactAddress"];
        users.userPhotos250=[dict valueForKey:@"userPhotos250"];
        users.friendCount=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sharedLocationCount"]];
        users.interestCount=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sharedInterestCount"]];
        users.locationCount=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sharedLocationCount"]];
        users.interestName=[self getIneterests:[dict valueForKey:@"sharedInterests"]];
        users.locationName=[self getLocations:[dict valueForKey:@"sharedLocations"]];
        users.contactsName=[self getContacts:[dict valueForKey:@"sharedContacts"]];
        users.distance=[dict valueForKey:@"contactDistance"];
        users.inContactFrom=[dict valueForKey:@"inContactsFor"];
        [_arrayContactSnowBuddies addObject:users];
    }
    [_delegate getContactSnowBudies:_arrayContactSnowBuddies];
}

-(NSString *)getIneterests:(NSArray *)interestArray
{
    NSString *interests=[[NSString alloc] init];;
    for (int i=0; i<[interestArray count]; i++) {
        interests= [interests stringByAppendingString:[[interestArray objectAtIndex:i] valueForKey:@"interestName"]];
        if (i<[interestArray count]-1)
            interests= [interests stringByAppendingString:@", "];
        
    }
    if (interests.length<1) {
        return  interests=@"";
    }
    return interests;
}

-(NSString *)getLocations:(NSArray *)locationsArray
{
    NSString *locations=[[NSString alloc] init];;
    for (int i=0; i<[locationsArray count]; i++) {
        locations=[locations stringByAppendingString:[[locationsArray objectAtIndex:i] valueForKey:@"destinationName"]];
        
        if (i<[locationsArray count]-1)
            locations= [locations stringByAppendingString:@", "];
    }
    if (locations.length<1) {
        return  locations=@"";
    }
    return locations;
}

-(NSString *)getContacts:(NSArray *)contactsArray
{
    NSString *contacts=[[NSString alloc] init];;
    for (int i=0; i<[contactsArray count]; i++) {
        contacts= [contacts stringByAppendingString:[[contactsArray objectAtIndex:i] valueForKey:@"contactProfileThumb"]];
        if (i<[contactsArray count]-1)
            contacts= [contacts stringByAppendingString:@","];
       
    }
    if (contacts.length<1) {
        return contacts=@"";
    }
    return contacts;
}

-(BOOL)isNetworkAvailable
{
    if ([InternetCheck sharedInstance].internetWorking)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}

-(NSString *)removeLastCharacter:(NSString *)stringText
{
    NSString *lastCharacter=[stringText substringFromIndex:[stringText length]-1];
    if ([lastCharacter isEqualToString:@","]) {
        stringText=[stringText substringToIndex:[stringText length]-1];
    }
    return stringText;
}
-(NSString *)getStringFromBase64:(NSString *)base64
{
    if (base64.length<1) {
        return @"";
    }
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64 options:0];
    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    NSData *newdata=[decodedString dataUsingEncoding:NSUTF8StringEncoding
                                allowLossyConversion:YES];
    NSString *descriptionData=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
    return descriptionData;
}
@end
