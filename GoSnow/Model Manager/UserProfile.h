//
//  UserProfile.h
//  GoSnow
//
//  Created by Rishabh on 23/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ProfileUpdateDelegate <NSObject>

@optional
-(void)imageDeletedSuccessfully:(UserInfoEntity *)updatedUserInfoEntity;
-(void)profileUpdateSuccessfully:(UserInfoEntity *)userInfoEntity;
-(void)userLogoutSuccessfully;
-(void)failedWithError:(NSDictionary*)errorDictionary;
@end

@interface UserProfile : NSObject
{
    
}
@property (nonatomic, weak) id<ProfileUpdateDelegate> delegate;
+(UserProfile *)sharedInstance;
-(NSString *)getBirthDay:(NSString *)stringDOB;
-(NSString *)CalculateYear:(NSDate *)today date:(NSDate *)bday;
-(NSDate *)calculateDobFromAge:(NSString *)dob;

+(void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock;
+(void)Callback:(NSDictionary *)dictionary method:(NSString *)methodName completion:(void(^)(BOOL succeeded, NSDictionary *dict))completionBlock;
-(void)updateUserLocation:(NSMutableDictionary *)params method:(NSString *)method;
-(void)updateProfile:(NSMutableDictionary *)params method:(NSString *)method;
-(void)deleteUserPic:(NSMutableDictionary *)params method:(NSString *)method;
-(void)logoutUser:(NSMutableDictionary *)params method:(NSString *)method;
@end
