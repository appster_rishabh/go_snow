//
//  MessageManager.h
//  GoSnow
//
//  Created by Rishabh on 09/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol MessageManagerDelegate <NSObject>

@optional
- (void)requestDidFailed:(NSDictionary *)errorDictionary;
-(void)getAllContactList:(NSMutableArray *)contactList;
- (void)messageSentSuccessfully:(NSDictionary *)dictionary;
- (void)getAllMessageSuccessfully:(NSMutableArray *)messageArray;
- (void)getAllConversationSuccessfully:(NSMutableArray *)conversationArray messagesUnreadCounts:(int )counts;

@end

@interface MessageManager : NSObject
@property (nonatomic, weak) id <MessageManagerDelegate> delegate;
@property(nonatomic,strong)NSMutableArray *totalMessagesArray;
@property(nonatomic,strong)NSMutableArray *totalConversationArray;
@property(nonatomic,strong)NSMutableArray *totalContactArray;
+(MessageManager *)sharedInstance;
-(void)callWebService:(NSMutableDictionary *)dictionary methodName:(NSString *)methodName;
-(void)callWebServiceOnTimer:(NSMutableDictionary *)params methodName:(NSString *)methodName;
@end
