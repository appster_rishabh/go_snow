//
//  GroupTripManager.h
//  GoSnow
//
//  Created by Varsha Singh on 23/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GroupTrip.h"

@protocol GroupTripDelegate <NSObject>

@optional
-(void)fetchGroupDetails:(NSMutableArray *)arrayTrips;
-(void)groupCreatedSuccessfully:(GroupTrip *)groupTripDetail;
-(void)addSnowBudiesToGroup:(NSMutableArray *)arraySnowBuddies;
-(void)leaveGroupSuccesfully:(NSMutableArray *)arraySnowBuddies;
-(void)snowBuddyLeftGroup;
-(void)failedWithError:(NSDictionary*)errorDictionary;
-(void)requestAcceptedSuccesfully:(NSDictionary *)dictionary;
-(void)requestRejectedSuccesfully:(NSDictionary *)dictionary;
-(void)inviteSnowBuddiesToGroup;
@end
//tripUser/accept
@interface GroupTripManager : NSObject

@property(nonatomic,strong)NSMutableArray *arrayTrips;
@property(nonatomic,strong)NSMutableArray *arraySnowBuddies;

@property(nonatomic,weak) id <GroupTripDelegate> delegate;

+(GroupTripManager *)sharedInstance;
-(void)handleDataForGroupTrip:(NSMutableDictionary *)params method:(NSString *)method;

@end
