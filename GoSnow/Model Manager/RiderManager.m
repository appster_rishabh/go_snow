//
//  RiderManager.m
//  GoSnow
//
//  Created by Rishabh on 03/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "RiderManager.h"
#import "RequestUsers.h"
@implementation RiderManager
-(id)init
{
    if (self = [super init]) {
        _network = [[ASINetworkQueue alloc] init];
        _network.uploadProgressDelegate = self;
         _totalRequest = [[NSMutableArray alloc] init];
        
    }
    return self;
}

#pragma mark - Shared Instance
+(RiderManager *)sharedInstance {
    static dispatch_once_t pred;
    static RiderManager *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[RiderManager alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark - Web service Method

-(void)CallWebService:(NSMutableDictionary *)dictionary methodName:(NSString *)methodName{
    
    if (![self isNetworkAvailable]) {
       
        [_delegate requestDidFailed:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    NSError* error;
    NSData* uploadData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KSERVICEURL,methodName]]];
    
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request addRequestHeader:@"content-type" value:@"application/json"];
    [request appendPostData:uploadData];
    [request setDelegate:self];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setDidFinishSelector:@selector(requestFinished:)];
    //[request startAsynchronous];
    [_network  addOperation:request];
    [_network go];
    
}

#pragma mark -  ASIFormDataRequest Methods

-(void)requestFinished:(ASIFormDataRequest *)request
{
    if (iscancelled ) {
        return;
    }
    last=[request tag];
    NSString* response = [request responseString];
    NSData *responseData = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    NSMutableDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    if ([[dictionary objectForKey:@"status"] isEqualToString:@"OK"]) {
        
        if ([[dictionary valueForKey:@"api"] isEqualToString:@"contact/reject"]) {
            [_delegate riderRequestRejectedSuccessfully:dictionary];
        }
        else if ([[dictionary valueForKey:@"api"] isEqualToString:@"contact/accept"])
        {
            [_delegate riderRequestAcceptedSuccessfully:dictionary];
        }
        else if ([[dictionary valueForKey:@"api"] isEqualToString:@"contact/add"])
        {
            [_delegate riderRequestSentSuccessfully:dictionary];
        }
        else if ([[dictionary valueForKey:@"api"] isEqualToString:@"photoFlag/add"])
        {
            [_delegate riderFlaggedSuccessfully:dictionary];
        }
        else if ([[dictionary valueForKey:@"api"] isEqualToString:@"contact/getByReceiverId"])
        {
            [self parseData:[dictionary valueForKey:@"contacts"]];
        }
        else if ([[dictionary valueForKey:@"api"] isEqualToString:@"tripUser/requestToJoin"])
        {
            [_delegate riderRequestSentSuccessfully:dictionary];
        }
        //tripUser/requestToJoin
    }
    else
    {
        [_delegate requestDidFailed:[NSMutableDictionary dictionaryWithObject:[dictionary valueForKey:@"apiMessage"] forKey:@"Error"]];
    }
}

-(void)requestFailed:(ASIFormDataRequest *)request
{
   [_delegate requestDidFailed:[NSMutableDictionary dictionaryWithObject:@"Network error. Please try again!" forKey:@"Error"]];
}

#pragma mark - Parse Data
-(void)parseData:(NSMutableArray *)userArray
{
    [_totalRequest removeAllObjects];
    for (int i=0; i<[userArray count]; i++) {
        RequestUsers *users=[[RequestUsers alloc] init];
        NSMutableDictionary *dict=[userArray objectAtIndex:i];
        users.firstName=[dict valueForKey:@"senderFirstName"];
        users.lastName=[dict valueForKey:@"senderLastName"];
       // users.facebookId=[dict valueForKey:@"facebookId"];
        users.skillLevel=[dict valueForKey:@"senderSkillLevelName"];
        users.skillLevelID=[dict valueForKey:@"senderSkillLevelId"];
        users.riderType=[dict valueForKey:@"senderRiderTypeName"];
        users.riderTypeId=[dict valueForKey:@"senderRiderTypeId"];
        users.userPhotos=[dict valueForKey:@"senderProfileImage"];
        users.user_id=[dict valueForKey:@"id"];
        users.age=[dict valueForKey:@"senderAge"];
        users.plannedTripName=[dict valueForKey:@"senderNextDestinationName"];
        users.plannedTripId=[dict valueForKey:@"senderNextDestinationId"];
        users.aboutMe=[dict valueForKey:@"senderAboutMe"];
        users.address=[dict valueForKey:@"senderAddress"];
        users.friendCount=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sharedContactCount"]];
        users.interestCount=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sharedInterestCount"]];
        users.locationCount=[NSString stringWithFormat:@"%@",[dict valueForKey:@"sharedLocationCount"]];
        users.interestName=[self getIneterests:[dict valueForKey:@"sharedInterests"]];
        users.locationName=[self getLocations:[dict valueForKey:@"sharedLocations"]];
        users.contactsName=[self getContacts:[dict valueForKey:@"sharedContacts"]];
        users.distance=[dict valueForKey:@"senderDistance"];
       // users.userPhotos=[dict valueForKey:@"senderPhotos"];
        [_totalRequest addObject:users];
    }
    [_delegate getRequestSuccessfully:_totalRequest];
}
-(NSString *)getIneterests:(NSArray *)interestArray
{
    NSString *interests=[[NSString alloc] init];;
    for (int i=0; i<[interestArray count]; i++) {
        interests= [interests stringByAppendingString:[[interestArray objectAtIndex:i] valueForKey:@"interestName"]];
        if (i<[interestArray count]-1)
            interests= [interests stringByAppendingString:@", "];
        
    }
    if (interests.length<1) {
        return  interests=@"";
    }
    return interests;
}
-(NSString *)getLocations:(NSArray *)locationsArray
{
    NSString *locations=[[NSString alloc] init];;
    for (int i=0; i<[locationsArray count]; i++) {
        locations=[locations stringByAppendingString:[[locationsArray objectAtIndex:i] valueForKey:@"destinationName"]];
        
        if (i<[locationsArray count]-1)
            locations= [locations stringByAppendingString:@", "];
    }
    if (locations.length<1) {
        return  locations=@"";
    }
    return locations;
}
-(NSString *)getContacts:(NSArray *)contactsArray
{
    NSString *contacts=[[NSString alloc] init];;
    for (int i=0; i<[contactsArray count]; i++) {
        contacts= [contacts stringByAppendingString:[[contactsArray objectAtIndex:i] valueForKey:@"contactProfileThumb"]];
        if (i<[contactsArray count]-1)
            contacts= [contacts stringByAppendingString:@","];
        
    }
    if (contacts.length<1) {
        return contacts=@"";
    }
    return contacts;
}
-(BOOL)isNetworkAvailable
{
    if ([InternetCheck sharedInstance].internetWorking)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}
-(NSString *)removeLastCharacter:(NSString *)stringText
{
    NSString *lastCharacter=[stringText substringFromIndex:[stringText length]-1];
    if ([lastCharacter isEqualToString:@","]) {
        stringText=[stringText substringToIndex:[stringText length]-1];
    }
    return stringText;
}

-(NSString *)getStringFromBase64:(NSString *)base64
{
    if (base64.length<1) {
        return @"";
    }
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64 options:0];
    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    NSData *newdata=[decodedString dataUsingEncoding:NSUTF8StringEncoding
                                allowLossyConversion:YES];
    NSString *descriptionData=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
    return descriptionData;
}
@end
