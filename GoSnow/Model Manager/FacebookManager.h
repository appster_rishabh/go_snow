//
//  FacebookManager.h
//  GoSnow
//
//  Created by Rishabh Yadav on 21/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

#import "UserProfile.h"
@protocol FacebookDelegate <NSObject>

@optional

-(void)showLoader;
-(void)hideLoader;
-(void)loginSuccessfully:(NSDictionary*)successDictionary;
-(void)loginFailedwitError:(NSDictionary*)errorDictionary;

@end

@interface FacebookManager : NSObject <ProfileUpdateDelegate>
{
    UserInfoEntity *userInfoEntity;
}
@property (nonatomic, weak) id<FacebookDelegate> delegate;

+(FacebookManager*)sharedInstance;

-(void)fetchProfilewithDelegate:(id<FacebookDelegate>)delegate;
-(void)fetchContactwithDelegate:(id<FacebookDelegate>)delegate;
-(void)getFriendsFromFacebook;
-(void)getInterests;

+(void)facebookLogout;

@end
