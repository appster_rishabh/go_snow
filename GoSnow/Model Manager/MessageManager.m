//
//  MessageManager.m
//  GoSnow
//
//  Created by Rishabh on 09/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "MessageManager.h"
#import "InboxMesages.h"
#import "Messages.h"
#import "Users.h"
@implementation MessageManager
+ (MessageManager *)sharedInstance {
    static dispatch_once_t pred;
    static MessageManager *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[MessageManager alloc] init];
    });
    
    return sharedInstance;
}
-(id)init
{
    if (self = [super init]) {
        _totalMessagesArray = [[NSMutableArray alloc] init];
         _totalConversationArray = [[NSMutableArray alloc] init];
         _totalContactArray = [[NSMutableArray alloc] init];
        
    }
    return self;
}
-(void)callWebService:(NSMutableDictionary *)params methodName:(NSString *)methodName{
    if (![self isNetworkAvailable]) {
        
        [_delegate requestDidFailed:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    __weak typeof(self) weakself = self;
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:methodName withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        //[sel];
        [weakself networkCallCompletion:successData withError:error];
    }];
    gsRequestWebservices = nil;
    
}
-(void)callWebServiceOnTimer:(NSMutableDictionary *)params methodName:(NSString *)methodName{
    if (![self isNetworkAvailable]) {
        
        //[_delegate requestDidFailed:nil];
        return;
    }
    __weak typeof(self) weakself = self;
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:methodName withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        //[sel];
        [weakself networkCallCompletion:successData withError:error];
    }];
    gsRequestWebservices = nil;
    
}

#pragma mark NETWORK DELEGATE
- (void)networkCallCompletion:(NSData *)successData withError:(NSError *)error
{
    NSDictionary *dictionary = [successData objectFromJSONData];
    
    if (!error && dictionary!=nil)
    {
        if ([[dictionary objectForKey:@"status"] isEqualToString:@"OK"]) {
            if ([[dictionary valueForKey:@"api"] isEqualToString:@"tripMessage/add"]) {
                [_delegate messageSentSuccessfully:dictionary];
            }
            else if ([[dictionary valueForKey:@"api"] isEqualToString:@"tripMessage/getMyMessages"])
            {
                [self parseAllMessageData:[dictionary valueForKey:@"messages"]];
            }
            else if ([[dictionary valueForKey:@"api"] isEqualToString:@"tripMessage/getConversationList"])
            {
                [self parseConversationData:[dictionary valueForKey:@"contacts"]];
            }
            else if ([[dictionary valueForKey:@"api"] isEqualToString:@"tripMessage/messagesent"])
            {
                // [self parseDataDemo:[dictionary valueForKey:@""]];
            }
            else if ([[dictionary valueForKey:@"api"] isEqualToString:@"contact/search"])
            {
                [self parseComposeData:[dictionary valueForKey:@"contacts"]];
            }
            
            
        }
        else
        {
           [_delegate requestDidFailed:[NSMutableDictionary dictionaryWithObject:[dictionary valueForKey:@"apiMessage"] forKey:@"Error"]];
        }
    }
    else
    {
        [_delegate requestDidFailed:[NSMutableDictionary dictionaryWithObject:@"Network error. Please try again!" forKey:@"Error"]];
    }
}
-(void)parseDataDemo:(NSMutableArray *)userArray
{
    [_delegate getAllMessageSuccessfully:_totalMessagesArray];
}
-(void)parseConversationData:(NSMutableArray *)userArray
{
    [_totalConversationArray removeAllObjects];
    int unreadCount=0;
    for (int i=0; i<[userArray count]; i++) {
        InboxMesages *inboxmessage=[[InboxMesages alloc] init];
        
        NSMutableDictionary *dict=[userArray objectAtIndex:i];
        inboxmessage.unreadMessageCount=[dict valueForKey:@"unreadCount"];
        inboxmessage.firstName=[dict valueForKey:@"contactFirstName"];
        inboxmessage.lastName=[dict valueForKey:@"contactLastName"];
        inboxmessage.messageStatus=[dict valueForKey:@"readStatus"];
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[dict valueForKey:@"lastMessage"] options:0];
        NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        NSData *newdata=[decodedString dataUsingEncoding:NSUTF8StringEncoding
                                    allowLossyConversion:YES];
        NSString *message=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
        inboxmessage.userMessage=message;
        inboxmessage.messageDate=[dict valueForKey:@"timeElapsed"];
        inboxmessage.unreadMessageCount=[dict valueForKey:@"unreadCount"];
        inboxmessage.profilePic=[dict valueForKey:@"contactProfileThumb"];
        inboxmessage.sender_id=[dict valueForKey:@"contactId"];
        unreadCount+=[[dict valueForKey:@"unreadCount"] intValue];
        [_totalConversationArray addObject:inboxmessage];
        
    }
    if ([_delegate respondsToSelector:@selector(getAllConversationSuccessfully: messagesUnreadCounts:)]) {
        [_delegate getAllConversationSuccessfully:_totalConversationArray messagesUnreadCounts:unreadCount];
    }
    
}
-(void)parseAllMessageData:(NSMutableArray *)userArray
{
    UserInfoEntity   *userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    [_totalMessagesArray removeAllObjects];
    for (int i=0; i<[userArray count]; i++) {
        Messages *messageData=[[Messages alloc] init];
        NSMutableDictionary *dict=[userArray objectAtIndex:i];
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[dict valueForKey:@"message"] options:0];
        NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        NSData *newdata=[decodedString dataUsingEncoding:NSUTF8StringEncoding
                                    allowLossyConversion:YES];
        NSString *message=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
        messageData.messageText=message;
        
        messageData.recieverImageUrl=[dict valueForKey:@"senderProfileThumb"];
        messageData.messageTime=[dict valueForKey:@"createDate"];
        messageData.messageId=[dict valueForKey:@"tripMessageReceiverId"];
        messageData.messageStatus=[dict valueForKey:@"readStatus"];
        messageData.messageSenderId=[dict valueForKey:@"senderId"];
        messageData.messageRecieverID=userInfoEntity.user_id;
        if ([messageData.messageSenderId isEqualToString:messageData.messageRecieverID]) {
            messageData.messageType=ktextByme;
            // messageData.senderImageUrl=[dict valueForKey:@"senderProfileThumb"];
        }
        else
        {
            messageData.messageType=ktextbyother;
            // messageData.senderImageUrl=[dict valueForKey:@"receiverProfileImage"];
        }
        
        [_totalMessagesArray addObject:messageData];
        //users.firstName=[[userArray objectAtIndex:i] valueForKey:@"firstname"];
    }
    if ([_delegate respondsToSelector:@selector(getAllMessageSuccessfully:)])
    {
        [_delegate getAllMessageSuccessfully:_totalMessagesArray];
    }
    
}
#pragma mark - Parse Compose Data
-(void)parseComposeData:(NSMutableArray *)userArray
{
    [_totalContactArray removeAllObjects];
    for (int i=0; i<[userArray count]; i++) {
        InboxMesages *users=[[InboxMesages alloc] init];
        NSMutableDictionary *dict=[userArray objectAtIndex:i];
        users.firstName=[dict valueForKey:@"contactFirstName"];
        users.lastName=[dict valueForKey:@"contactLastName"];
      
        users.riderType=[dict valueForKey:@"contactRiderTypeId"];
       
        users.age=[dict valueForKey:@"contactAge"];
        users.sender_id=[dict valueForKey:@"contactId"];
        users.isSelect=NO;
        users.profilePic=[dict valueForKey:@"contactProfileImage250"];
        // users.distance=[dict valueForKey:@"distance"];
        [_totalContactArray addObject:users];
    }
    if ([_delegate respondsToSelector:@selector(getAllContactList:)]) {
        [_delegate getAllContactList:_totalContactArray];
    }
    
}

-(BOOL)isNetworkAvailable
{
    if ([InternetCheck sharedInstance].internetWorking)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}
@end
