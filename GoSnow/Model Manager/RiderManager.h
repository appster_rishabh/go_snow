#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@protocol RiderManagerDelegate <NSObject>

@optional
- (void)requestDidFailed:(NSMutableDictionary *)errorDictionary;
- (void)riderRequestSentSuccessfully:(NSMutableDictionary *)dictionary;
- (void)riderRequestAcceptedSuccessfully:(NSMutableDictionary *)dictionary;
- (void)riderRequestRejectedSuccessfully:(NSMutableDictionary *)dictionary;
- (void)getRequestSuccessfully:(NSMutableArray *)requestArray;
- (void)riderFlaggedSuccessfully:(NSMutableDictionary *)dictionary;

@end

@interface RiderManager : NSObject
{
    BOOL iscancelled;
    NSInteger last;
    
    NSInteger totalCount;
}
@property(nonatomic,strong)NSMutableArray *totalRequest;
@property (strong,nonatomic)ASINetworkQueue *network;
@property (nonatomic, weak) id <RiderManagerDelegate> delegate;
+(RiderManager *)sharedInstance;
-(void)CallWebService:(NSMutableDictionary *)dictionary methodName:(NSString *)methodName;

// 4: Declare a designated initializer.


@end