//
//  UserProfile.m
//  GoSnow
//
//  Created by Rishabh on 23/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "UserProfile.h"
static UserProfile* userProfile;
@implementation UserProfile

#pragma - User Profile shared Instance

+(UserProfile*)sharedInstance
{
    if (userProfile == nil) {
        userProfile = [[UserProfile alloc] init];
    }
    return userProfile;
}
#pragma mark -  Calculate DOB Methods
-(NSString *)getBirthDay:(NSString *)stringDOB
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/mm/yyyy"];
    NSDate *dateDOB = [dateFormat dateFromString:stringDOB];
    
    NSString *stringCalculatedAge = [self CalculateYear:[NSDate date] date:dateDOB];
    return stringCalculatedAge;
}

-(NSString *)CalculateYear:(NSDate *)today date:(NSDate *)bday
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:bday
                                                          toDate:today
                                                         options:0];
    NSInteger days=components.day;
    NSInteger year=days/365;
    NSString *dob=nil;
    
    if (year==0) {
        dob=[NSString stringWithFormat:@"%ld",(long)0];
    }
    else
    {
        dob=[NSString stringWithFormat:@"%ld",(long)year];
    }
    return dob;
}

-(NSDate *)calculateDobFromAge:(NSString *)dob
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    NSDate *date = [NSDate date];
    NSDateComponents *comps = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                                     fromDate:date];
    NSDate *today = [cal dateFromComponents:comps];
    int agee=[dob intValue];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:-1];
    [components setYear:-agee];
    NSDate *dateOfBirth = [cal dateByAddingComponents:components toDate:today options:0];
    return dateOfBirth;
    
}
#pragma - User Profile Arrays

-(void)updateProfile:(NSMutableDictionary *)params method:(NSString *)method
{
    if (![self isNetworkAvailable]) {
        
        [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    __weak typeof(self) weakself = self;
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:method withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        //[sel];
        [weakself networkCallCompletion:successData withError:error];
    }];
    gsRequestWebservices = nil;
    
}
-(void)deleteUserPic:(NSMutableDictionary *)params method:(NSString *)method
{
    if (![self isNetworkAvailable]) {
        
        [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    __weak typeof(self) weakself = self;
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:method withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        //[sel];
        [weakself networkCallCompletion:successData withError:error];
    }];
    gsRequestWebservices = nil;
    
}
-(void)updateUserLocation:(NSMutableDictionary *)params method:(NSString *)method
{
    if (![self isNetworkAvailable]) {
        
        [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    __weak typeof(self) weakself = self;
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:method withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        //[sel];
        [weakself networkCallCompletion:successData withError:error];
    }];
    gsRequestWebservices = nil;
}
-(void)logoutUser:(NSMutableDictionary *)params method:(NSString *)method
{
    if (![self isNetworkAvailable]) {
        
        [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:@"No internet connection available. Please try again!" forKey:@"Error"]];
        return;
    }
    __weak typeof(self) weakself = self;
    GSRequestWebservices *gsRequestWebservices = [[GSRequestWebservices alloc] initWithMethod:method withDictionary:params andCompletionHandler:^(NSError *error, NSData * successData){
        //[sel];
        [weakself networkCallCompletion:successData withError:error];
    }];
    gsRequestWebservices = nil;
}
#pragma mark - Network Manager Protocol

- (void)networkCallCompletion:(NSData *)successData withError:(NSError *)error
{
    NSDictionary *dictionaryResponse = [successData objectFromJSONData];
    if (!error && dictionaryResponse!=nil)
    {
        if ([[dictionaryResponse objectForKey:@"status"] isEqualToString:@"OK"])
        {
             UserInfoEntity *userInfoEntity= [UserInfoEntity updateUserInfo:[dictionaryResponse valueForKey:@"profile"] sessionId:[dictionaryResponse valueForKey:@"sessionId"] inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
            if ([[dictionaryResponse valueForKey:@"api"] isEqualToString:@"user/deletePhoto"]) {
                [_delegate imageDeletedSuccessfully:userInfoEntity];
            }
            else if ([[dictionaryResponse valueForKey:@"api"] isEqualToString:@"user/updateLocation"])
                {
                    
                    
                }
            else if([[dictionaryResponse valueForKey:@"api"] isEqualToString:@"user/logout"])
            {
                
                [_delegate userLogoutSuccessfully];
            }
            else
            {
             
                [_delegate profileUpdateSuccessfully:userInfoEntity];
            }
            
        }
        else
        {
             [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:[dictionaryResponse valueForKey:@"apiMessage"] forKey:@"Error"]];
        }
    }
    else
    {
       [_delegate failedWithError:[NSMutableDictionary dictionaryWithObject:@"Network error. Please try again!" forKey:@"Error"]];
    }
}

#pragma mark - Download Image
+(void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                   queue:[NSOperationQueue mainQueue]
       completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
           if ( !error )
           {
               UIImage *image = [[UIImage alloc] initWithData:data];
               completionBlock(YES,image);
           } else{
               completionBlock(NO,nil);
           }
       }];
}
#pragma mark callback method
+(void)Callback:(NSDictionary *)dictionary method:(NSString *)methodName completion:(void(^)(BOOL succeeded, NSDictionary *dict))completionBlock
{
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSData* cData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KSERVICEURL,methodName ]] ];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    request.HTTPBody = cData;
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   NSDictionary *dict = [data objectFromJSONData];
                                   
                                   completionBlock(YES,dict);                               } else{
                                       completionBlock(YES,nil);
                                   }
                           }];
}

-(BOOL)isNetworkAvailable
{
    if ([InternetCheck sharedInstance].internetWorking)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return YES;
}
@end
