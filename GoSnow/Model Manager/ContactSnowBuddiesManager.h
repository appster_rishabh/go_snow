//
//  ContactSnowBuddiesManager.h
//  GoSnow
//
//  Created by Madhavi Yalamaddi on 25/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol ContactSnowBuddiesDelegate <NSObject>

@optional
-(void)getContactSnowBudies:(NSMutableArray *)arrayContactSnowBuddies;
-(void)deleteContactSnowBuddies;
-(void)failedWithError:(NSDictionary*)dictionaryError;
@end

@interface ContactSnowBuddiesManager : NSObject 
@property(nonatomic,strong)NSMutableArray *arrayContactSnowBuddies;

@property(nonatomic,weak) id <ContactSnowBuddiesDelegate> delegate;

+(ContactSnowBuddiesManager *)sharedInstance;
-(void)handleDataForContactSnowBuddies:(NSMutableDictionary *)params method:(NSString *)method;
@end
