
//

#import "CoreDataManager.h"
#import "Reachability.h"


@implementation CoreDataManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static CoreDataManager *manager;

+(CoreDataManager* ) sharedInstance {
    if (!manager) {
        @synchronized([UIApplication sharedApplication]){
            manager = [[CoreDataManager alloc] init];
        }
    }
    return manager;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges]) {
            if (![managedObjectContext save:&error]) {
                
                abort();
            }else {
                
            }
        }else {
            
        }
    }
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
    
    if (![NSThread isMainThread])
        NSLog(@"MainObjectContext used from thread other than main thread");
    if (_managedObjectContext)
        return _managedObjectContext;
    
    /* Create the main context only on the main thread */
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(managedObjectContext)
                               withObject:nil
                            waitUntilDone:YES];
        return _managedObjectContext;
    }
   
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"GoSnow" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"GoSnow.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        abort();
    }
    return _persistentStoreCoordinator;
}

#pragma mark- Network Check-
- (BOOL) internetNotWorking
{
    Reachability *networkReachability = [Reachability reachabilityWithHostName:KHOSTURL];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    return (networkStatus == NotReachable);
}



@end
