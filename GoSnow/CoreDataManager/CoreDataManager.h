//
//  CoreDataManager.h
//  Jirno
//
//  Created by Anand Mishra on 20/01/15.
//  Copyright (c) 2015 AppSter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (CoreDataManager*)sharedInstance;


@end
