//
//  ContactSnowBuddiesViewController.m
//  GoSnow
//
//  Created by Madhavi Yalamaddi on 25/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ContactSnowBuddiesViewController.h"
#import "ContactSnowBuddiesManager.h"
#import "ContactSnowBuddiesCell.h"
#import "Users.h"
#import "ViewUserProfileViewController.h"
@interface ContactSnowBuddiesViewController ()

@end
int selectedIndex;
@implementation ContactSnowBuddiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    _contactSnowBuddiesTable.hidden=YES;
    _contactSnowBuddiesTable.separatorColor=[UIColor grayColor];
    contactSnowBuddiesArray=[[NSMutableArray alloc]init];
    [self updateMenuIcon];
    [self getSnowBuddies];
}
#pragma mark viewWillAppear method
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenuIconByNotification) name:KNotificationRecieved object:nil];
    //[self ad];
}
#pragma mark viewWillDisappear method
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KNotificationRecieved object:nil];
}
#pragma mark updateMenuIconByNotification method
-(void)updateMenuIconByNotification
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    [menuIcon setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
}
#pragma mark updateMenuIcon method
-(void)updateMenuIcon
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    int notificationCount=[UserInfoEntity fetchPendingRequestsAndMessagesCountsinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    if (notificationCount>0)
        [menuIcon setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
    else
        [menuIcon setImage:[UIImage imageNamed:@"menuSetting"] forState:UIControlStateNormal];
}


#pragma mark - Response From Web Service
-(void) getSnowBuddies
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:sessionId forKey:@"sessionId"];
    
    [[ContactSnowBuddiesManager sharedInstance]setDelegate:self];
    [[ContactSnowBuddiesManager sharedInstance] handleDataForContactSnowBuddies:params method:@"contact/search"];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [contactSnowBuddiesArray count];
}
#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ViewUserProfileViewController *viewUserProfileViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"ViewUserProfileViewController"];
    viewUserProfileViewController.fromScreenType=ContactSnowBuddiesScreenType;
    // [myProfileDetailVC sett]
    viewUserProfileViewController.otherUsersObject=[contactSnowBuddiesArray objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:viewUserProfileViewController animated:YES];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    static NSString *CellIdentifier = @"ContactSnowBuddiesCell";
    ContactSnowBuddiesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ContactSnowBuddiesCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.delegate=self;
        cell.sender=self;
    }
    cell.rightUtilityButtons=[self rightButtons];
    
    Users *contactsInfo=[contactSnowBuddiesArray objectAtIndex:indexPath.row];
    [cell setLayout:contactsInfo index:indexPath];
    
    return cell;
}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor: [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] title:@"DELETE"];
    
    
    return rightUtilityButtons;
}

#pragma mark - SWTableView Delegate Methods
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *cellIndexPath = [_contactSnowBuddiesTable indexPathForCell:cell];
    selectedIndex = (int)cellIndexPath.row;
    switch (index)
    {
        case 0:
        {
            NSLog(@"edit button was pressed");
            // share button was pressed ...
            [cell hideUtilityButtonsAnimated:YES];
            [self deleteCellFromIndexPath:cellIndexPath];
            // [self hideBottomElement];
        }
            break;
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    
    return YES;
}
- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            //            if (isEditAll) {
            //                return NO;
            //            }
            
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}

-(void)showLeftUtilityButtonsAnimated:(BOOL)animated
{
    
}

-(void)deleteCellFromIndexPath:(NSIndexPath *)indexPath
{
    [_contactSnowBuddiesTable beginUpdates];
    Users *contactsInfo=[contactSnowBuddiesArray objectAtIndex:indexPath.row];
    [self deleteContactFromList:contactsInfo];
    
    NSArray *deletedIndexPaths = [[NSArray alloc] initWithObjects:
                                  [NSIndexPath indexPathForRow:indexPath.row inSection:0],
                                  nil];
    [_contactSnowBuddiesTable deleteRowsAtIndexPaths:deletedIndexPaths withRowAnimation:UITableViewRowAnimationFade];
    [contactSnowBuddiesArray removeObjectAtIndex:indexPath.row];
    [_contactSnowBuddiesTable endUpdates];
}

#pragma mark delete contact web service
-(void)deleteContactFromList:(Users *)contactsInfo
{
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:sessionId forKey:@"sessionId"];
    [params setObject:contactsInfo.deletedContactId forKey:@"contactId"];
    [[ContactSnowBuddiesManager sharedInstance]setDelegate:self];
    [[ContactSnowBuddiesManager sharedInstance] handleDataForContactSnowBuddies:params method:@"contact/delete"];
}

#pragma mark -  Response Successful
-(void)getContactSnowBudies:(NSMutableArray *)arraySnowBuddies;
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if ([arraySnowBuddies count]<1) {
        _noSnowBuddyFoundView.hidden=NO;
        return;
    }
    [contactSnowBuddiesArray removeAllObjects];
    [contactSnowBuddiesArray addObjectsFromArray:arraySnowBuddies];
    _contactSnowBuddiesTable.hidden = NO;
    [_contactSnowBuddiesTable reloadData];
}
#pragma mark delete contact
-(void) deleteContactSnowBuddies
{
    
}
#pragma mark - Response Failed
-(void)failedWithError:(NSDictionary*)dictionaryError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[dictionaryError valueForKey:@"Error"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    if ([contactSnowBuddiesArray count]<1) {
        _noSnowBuddyFoundView.hidden=NO;
        return;
    }
}

@end
