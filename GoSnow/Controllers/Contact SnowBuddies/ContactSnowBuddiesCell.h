//
//  ContactSnowBuddiesCell.h
//  GoSnow
//
//  Created by Madhavi Yalamaddi on 25/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "Users.h"

@interface ContactSnowBuddiesCell : SWTableViewCell
@property id sender;
-(void)setLayout:(Users *)contactsInfo index:(NSIndexPath *)path;
@end
