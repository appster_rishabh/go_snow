//
//  ContactSnowBuddiesViewController.h
//  GoSnow
//
//  Created by Madhavi Yalamaddi on 25/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactSnowBuddiesManager.h"
#import "SWTableViewCell.h"

@interface ContactSnowBuddiesViewController : UIViewController <UITableViewDelegate,ContactSnowBuddiesDelegate,SWTableViewCellDelegate>
{
    NSMutableArray *contactSnowBuddiesArray;
    UserInfoEntity *userInfoEntity;
}
@property (weak, nonatomic) IBOutlet UIView *noSnowBuddyFoundView;
@property (weak, nonatomic) IBOutlet UITableView *contactSnowBuddiesTable;

@end
