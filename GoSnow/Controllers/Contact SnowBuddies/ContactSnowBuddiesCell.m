//
//  ContactSnowBuddiesCell.m
//  GoSnow
//
//  Created by Madhavi Yalamaddi on 25/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ContactSnowBuddiesCell.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import <QuartzCore/QuartzCore.h>
@implementation ContactSnowBuddiesCell
UIImageView *imageViewSnowBuddy;

- (void)awakeFromNib {
    // Initialization code
    imageViewSnowBuddy = (UIImageView *)[self viewWithTag:1100];
    imageViewSnowBuddy.layer.cornerRadius = 30;
}
-(void)setLayout:(Users *)contactsInfo index:(NSIndexPath *)path
{
    UIImageView *contactSnowBuddyImage=(UIImageView *)[self viewWithTag:1100];
    contactSnowBuddyImage.layer.cornerRadius =25;
    contactSnowBuddyImage.layer.masksToBounds=YES;
    UILabel *contactSnowBuddyName=(UILabel *)[self viewWithTag:1101];
    UILabel *contactSnowBuddyMatchedFrom=(UILabel *)[self viewWithTag:1102];
    contactSnowBuddyMatchedFrom.text=contactsInfo.inContactFrom;;
    // cont
    if (contactsInfo.profileImage.length>0) {
        [contactSnowBuddyImage setImageWithURL:[NSURL URLWithString:contactsInfo.profileImage] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [contactSnowBuddyImage setImage:[UIImage imageNamed:@"smallProfile"]];
    }
    
    if (contactsInfo.firstName.length>0 && contactsInfo.lastName.length>0) {
        contactSnowBuddyName.text=[NSString stringWithFormat:@"%@ %@",contactsInfo.firstName,contactsInfo.lastName];
    }
    else
    {
        contactSnowBuddyName.text=[NSString stringWithFormat:@"%@",contactsInfo.firstName];
    }
    
    self.selectionStyle=UITableViewCellSelectionStyleNone;
}
-(void)editSingleRow:(NSMutableDictionary *)dict isedit:(BOOL)isEdit indexPath:(NSIndexPath *)indexPath
{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
