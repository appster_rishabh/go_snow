//
//  AppDelegate.h
//  GoSnow
//
//  Created by Varsha Singh on 18/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import "RESideMenu.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,LocationManagerDelegate,RESideMenuDelegate>
{
    Location *locationObject;
    RESideMenu *sideMenuViewController;
}
@property (strong, nonatomic) UIWindow *window;

@end

