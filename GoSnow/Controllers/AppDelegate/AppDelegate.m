//
//  AppDelegate.m
//  GoSnow
//
//  Created by Varsha Singh on 18/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "AppDelegate.h"
#import "UserProfile.h"
#import "HomeViewController.h"
#import "LoginViewController.h"
#import "LeftSideViewController.h"
#import "RootViewController.h"
#import "ChatViewController.h"
#import "MessagesViewController.h"
#import "RequestViewController.h"
#import "GroupTripViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - Application Life Cycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window=[[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    UIStoryboard  *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController=nil;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification)
        [self application:application didReceiveRemoteNotification:(NSDictionary*)notification];
    
    UserInfoEntity *userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    if ((userInfoEntity.sessionId.length>0 || userInfoEntity.sessionId!=nil) && userInfoEntity.skillLevelId.length>0)
    {
        HomeViewController *homeViewController=nil;
        if (WIDTH>320) {
            homeViewController=[[HomeViewController alloc] initWithNibName:@"HomeViewController_iPhone6" bundle:nil];
        }
        else
        {
            homeViewController=[[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        }
        navigationController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    }
    else
    {
        navigationController = [[UINavigationController alloc] initWithRootViewController:[storyBoard instantiateViewControllerWithIdentifier:@"LoginViewController"]];
    }
    [InternetCheck sharedInstance];
    LeftSideViewController *leftMenuViewController = [storyBoard instantiateViewControllerWithIdentifier:@"LeftMenuViewController"];
    
   sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController leftMenuViewController:leftMenuViewController rightMenuViewController:nil];
    sideMenuViewController.menuPreferredStatusBarStyle = 1;
    sideMenuViewController.delegate = self;
    sideMenuViewController.contentViewShadowColor = [UIColor whiteColor];
    sideMenuViewController.contentViewShadowOffset = CGSizeMake(0, 0);
    sideMenuViewController.contentViewShadowOpacity = 0.6;
    sideMenuViewController.contentViewShadowRadius = 12;
    sideMenuViewController.contentViewShadowEnabled = YES;
    self.window.rootViewController = sideMenuViewController;
    
   // self.window.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
   // [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
   
    [self callLocationUpdate];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application
}

#pragma mark - Push Notification Delegates

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *device = [deviceToken description];
    device = [device stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    device = [device stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:device forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    
    NSLog(@"Error %@",error);
    
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if (application.applicationState == UIApplicationStateInactive )
    {
        [self manageNotificationsFromNotificationCentre:userInfo];

    }
    else if(application.applicationState == UIApplicationStateActive )
    {
        [self manageNotificationsFromServer:userInfo];
    }
   
}
/*
 NOTIFICATION_TEST, 0
 NOTIFICATION_CONTACT_REQUEST_RECEIVED, 1
 NOTIFICATION_CONTACT_REQUEST_ACCEPTED, 2
 NOTIFICATION_CONTACT_REQUEST_REJECTED, 3
 NOTIFICATION_TRIP_REQUEST_RECEIVED, 4
 NOTIFICATION_TRIP_REQUEST_ACCEPTED, 5
 NOTIFICATION_TRIP_REQUEST_REJECTED, 6
 NOTIFICATION_TRIP_REMOVED, 7
 NOTIFICATION_TRIP_LEFT, 8
 NOTIFICATION_MESSAGE_RECEIVED, 9
 NOTIFICATION_MESSAGE_SENT, 10
 NOTIFICATION_MESSAGE_READ, 11
 NOTIFICATION_USER_DISABLED, 12
 NOTIFICATION_USER_ENABLED, 13
 */
-(void)manageNotificationsFromNotificationCentre:(NSDictionary *)userInfo
{
    // 0,2,3,5,6,7,8,10,
    RESideMenu *resideMenu=(RESideMenu *)self.window.rootViewController;
    UINavigationController *navigationController=(UINavigationController *)resideMenu.contentViewController;
    NSArray *viewControllerArray=navigationController.viewControllers;
    id someVc= [viewControllerArray lastObject];
    NSString *notificationType=[[userInfo valueForKey:@"aps"] valueForKey:@"notificationId"];
    if ([notificationType isEqualToString:@"1"]) {
        
        [UserInfoEntity increaseRequestAndmessagesCounts:@"1" messagesCounts:@"" tripRequestCounts:@"" inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo valueForKey:@"aps"] valueForKey:@"badge"] integerValue];
        
       [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationFromNoticationCentre object:notificationType];
        [self postNotificationToOpenViewController:someVc];
    }
    else if ([notificationType isEqualToString:@"4"])
    {
        [self showAlertView:[[userInfo valueForKey:@"aps"] valueForKey:@"alert"]];
        [UserInfoEntity increaseRequestAndmessagesCounts:@"" messagesCounts:@"" tripRequestCounts:@"1"  inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo valueForKey:@"aps"] valueForKey:@"badge"] integerValue];
        [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationFromNoticationCentre object:notificationType];
        [self postNotificationToOpenViewController:someVc];
    }
    else if ([notificationType isEqualToString:@"9"])
    {
        if ([someVc isKindOfClass:[ChatViewController class]]) {
            
        }
        else
        {
            [UserInfoEntity increaseRequestAndmessagesCounts:@"" messagesCounts:@"1" tripRequestCounts:@""  inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        }
        [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo valueForKey:@"aps"] valueForKey:@"badge"] integerValue];
        [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationFromNoticationCentre object:notificationType];
        [self postNotificationToOpenViewController:someVc];
        
    }
    else
    {
       
    }
    //[self postNotificationToOpenViewController:someVc];
    
}
-(void)manageNotificationsFromServer:(NSDictionary *)userInfo
{
   // 0,2,3,5,6,7,8,10,
    RESideMenu *resideMenu=(RESideMenu *)self.window.rootViewController;
    UINavigationController *navigationController=(UINavigationController *)resideMenu.contentViewController;
    NSArray *viewControllerArray=navigationController.viewControllers;
    id someVc= [viewControllerArray lastObject];
    NSString *notificationType=[[userInfo valueForKey:@"aps"] valueForKey:@"notificationId"];
    if ([notificationType isEqualToString:@"1"]) {
        [self showAlertView:[[userInfo valueForKey:@"aps"] valueForKey:@"alert"]];
        [UserInfoEntity increaseRequestAndmessagesCounts:@"1" messagesCounts:@"" tripRequestCounts:@"" inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo valueForKey:@"aps"] valueForKey:@"badge"] integerValue];
        [self postNotificationToOpenViewController:someVc];
    }
    else if ([notificationType isEqualToString:@"4"])
    {
        [self showAlertView:[[userInfo valueForKey:@"aps"] valueForKey:@"alert"]];
       [UserInfoEntity increaseRequestAndmessagesCounts:@"" messagesCounts:@"" tripRequestCounts:@"1"  inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo valueForKey:@"aps"] valueForKey:@"badge"] integerValue];
        [self postNotificationToOpenViewController:someVc];
    }
    else if ([notificationType isEqualToString:@"9"])
    {
        if ([someVc isKindOfClass:[ChatViewController class]]) {
            
        }
        else
        {
       [UserInfoEntity increaseRequestAndmessagesCounts:@"" messagesCounts:@"1" tripRequestCounts:@""  inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        }
        [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo valueForKey:@"aps"] valueForKey:@"badge"] integerValue];
      [self postNotificationToOpenViewController:someVc];
        
    }
    else
    {
        [self showAlertView:[[userInfo valueForKey:@"aps"] valueForKey:@"alert"]];
    }
    //[self postNotificationToOpenViewController:someVc];
    
}
-(void)showAlertView:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
-(void)postNotificationToOpenViewController:(id)ClassName
{
  [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationRecieved object:nil];
}


#pragma mark - Facebook Handle Open URL

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                    fallbackHandler:^(FBAppCall *call) {
                        NSLog(@"In fallback handler");
                    }];
}

#pragma mark - Update Location

-(void)callLocationUpdate
{
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    if (sessionId.length>0) {
        locationObject = [Location sharedInstance];
        locationObject.delegate = self;
        [locationObject callLocationManager];
        [locationObject startUpdateLocation];
    }
    
}

#pragma mark - Location Delegates

-(void)gotLocationSuccessfully:(NSString *)latitudeValue longitude:(NSString *)longitudeValue
{
    [locationObject stopUpdateLocation];
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:latitudeValue forKey:@"latitude"];
    [params setObject:longitudeValue forKey:@"longitude"];
    [params setObject:sessionId forKey:@"sessionId"];
    [params setObject:@"1" forKey:@"includeCount"];
    [params setObject:@"1" forKey:@"selectAllFields"];
    [UserProfile Callback:params method:@"user/updateProfile" completion:^(BOOL succeeded, NSDictionary *dict) { {
        if (succeeded) {
            if ([[dict objectForKey:@"status"] isEqualToString:@"OK"])
            {

            [UserInfoEntity updateUserLocation:[dict valueForKey:@"profile"] sessionId:[dict valueForKey:@"sessionId"] inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
            }
        }
    }
    }];
}

-(void)gotErrorInFindingLocation
{
    
}

#pragma mark - RESideMenu Delegates

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kSideMenuLoaded object:nil];
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}
@end
