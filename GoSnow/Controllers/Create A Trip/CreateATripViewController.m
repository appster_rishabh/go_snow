//
//  CreateATripViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 11/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//


#import "CreateATripViewController.h"
#import "MyProfilePlannedTripVC.h"
#import "GroupTripManager.h"
#import "GroupTripDetailViewController.h"

@interface CreateATripViewController ()


@end

@implementation CreateATripViewController
BOOL isAnimateDescriptionView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpInitials];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self updateCreatedGroupInformation];
}

#pragma mark - Set Up Initial Information
-(void)setUpInitials
{
    [self.navigationController.navigationBar setHidden:YES];
    groupTripObject = [[GroupTrip alloc]init];
    [self createPickerView];
    [_viewDescription addSubview:[self getTextField]];
}

-(void)updateCreatedGroupInformation
{
    _labelLocation.hidden = NO;
    
    if(_isEditMode)
    {
        groupTripObject = _groupTripDetail;
        
        _headingLabel.text = @"EDIT A TRIP";
        [_doneButton setTitle:@"Done" forState:UIControlStateNormal];
        _labelSeason.text = groupTripObject.season;
        _labelLocation.text = groupTripObject.destinationName;
        [(UITextField *)[_viewDescription viewWithTag:311]setText:groupTripObject.tripDescription];
    }
    else
    {
        _labelSeason.text =@"Winter";
        _labelLocation.text =groupTripObject.destinationName;
    }
}

#pragma mark -
#pragma mark IBActions
-(IBAction)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)chooseLocationAction:(id)sender
{
    if(isAnimateDescriptionView){
        isAnimateDescriptionView = NO;
        [self closeAnimationForWriteAShortDescription];
    }
    
    MyProfilePlannedTripVC *myProfilePlannedTripVC =
    [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfilePlannedTripVC"];
    myProfilePlannedTripVC.isGroupTripPage = YES;
    myProfilePlannedTripVC.groupTripData= groupTripObject;
    [self.navigationController pushViewController:myProfilePlannedTripVC animated:YES];
}

-(IBAction)selectSeasonAction:(id)sender
{
    if (!isAnimateDescriptionView)
    {
        isAnimateDescriptionView = YES;
        [self openAnimationForWriteAShortDescription];
    }
    else{
        isAnimateDescriptionView = NO;
        [self closeAnimationForWriteAShortDescription];
    }
}

-(IBAction)createEditATripAction:(id)sender
{
    NSString *stringAlert;
    UITextField *textField = (UITextField *)[self.view viewWithTag:311];
    [textField resignFirstResponder];
    
    if ([groupTripObject.destinationId length]==0)
    {
        stringAlert = @"Please select destination.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:stringAlert delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([_labelSeason.text length]==0)
    {
        stringAlert = @"Please select season.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:stringAlert delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([textField.text length]==0)
    {
        stringAlert = @"Please write a short desciption about your trip.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:stringAlert delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:sessionId forKey:@"sessionId"];
    
    if ([textField.text length]>0)
    {
        NSData *data = [textField.text dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *encodedString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSData *descriptionData=[encodedString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *descriptionString=[descriptionData base64EncodedStringWithOptions:0];
        [params setObject:descriptionString forKey:@"tripDescription"];
    }
    
    if ([groupTripObject.destinationId length]>0)
        [params setObject:groupTripObject.destinationId forKey:@"destinationId"];
    
    if([_labelSeason.text length]>0)
        [params setObject:_labelSeason.text forKey:@"season"];
    
    
    if(_isEditMode)
        [self editATripAction:params];
    else
        [self createATripAction:params];
}


#pragma mark -
#pragma mark selectors

-(void)createATripAction:(NSMutableDictionary *)params
{
    [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"trip/add"];
    [[GroupTripManager sharedInstance]setDelegate:self];
}

-(void)editATripAction:(NSMutableDictionary *)params
{
    // TO DO set proper tripId
    [params setObject:groupTripObject.groupId forKey:@"tripId"];
    [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"trip/update"];
    [[GroupTripManager sharedInstance]setDelegate:self];
}

#pragma mark - Maintain Animations Of Views
-(void)openAnimationForWriteAShortDescription
{
    NSInteger height = pickerViewSeason.frame.size.height;
    pickerViewSeason.frame = CGRectMake(_viewSeason.frame.origin.x,
                                        _viewSeason.frame.origin.y,
                                        pickerViewSeason.frame.size.width,
                                        0);
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         pickerViewSeason.hidden = NO;
                         
                         pickerViewSeason.frame = CGRectMake(_viewSeason.frame.origin.x,
                                                             _viewSeason.frame.origin.y+_viewSeason.frame.size.height,
                                                             pickerViewSeason.frame.size.width,
                                                             height);
                         pickerViewSeason.transform = CGAffineTransformMakeRotation(0.0f);
                         
                         _viewDescription.frame = CGRectMake(_viewDescription.frame.origin.x,
                                                             pickerViewSeason.frame.origin.y+pickerViewSeason.frame.size.height,
                                                             _viewDescription.frame.size.width,
                                                             _viewDescription.frame.size.height);
                         _viewDescription.transform = CGAffineTransformMakeRotation(0.0f);
                         
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}

-(void)closeAnimationForWriteAShortDescription
{
    CGRect frame = pickerViewSeason.frame;
    pickerViewSeason.hidden = YES;  // Remove if not looks good
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         pickerViewSeason.frame = CGRectMake(pickerViewSeason.frame.origin.x,
                                                             _viewSeason.frame.origin.y,
                                                             pickerViewSeason.frame.size.width, 0);
                         pickerViewSeason.transform = CGAffineTransformMakeRotation(0.0f);
                         
                         _viewDescription.frame = CGRectMake(_viewDescription.frame.origin.x,
                                                             _viewSeason.frame.origin.y+_viewSeason.frame.size.height,
                                                             _viewDescription.frame.size.width,
                                                             _viewDescription.frame.size.height);
                         _viewDescription.transform = CGAffineTransformMakeRotation(0.0f);
                     }
                     completion:^(BOOL finished){
                         pickerViewSeason.hidden = YES;
                         pickerViewSeason.frame = frame;
                         
                     }];
    
}

#pragma mark -
#pragma mark Get Text Field -

- (UITextField*)getTextField
{
    UITextField *textFieldShortNote = [[UITextField alloc] initWithFrame:CGRectMake(_viewDescription.frame.origin.x+13, _viewDescription.frame.origin.x+5, _viewDescription.frame.size.width-30, _viewDescription.frame.size.height-10)];
    textFieldShortNote.tag =311;
    textFieldShortNote.delegate = self;
    textFieldShortNote.placeholder = @"Write a short description";
    textFieldShortNote.textColor  = [UIColor lightGrayColor];
    textFieldShortNote.backgroundColor = [UIColor clearColor];
    textFieldShortNote.autocorrectionType = UITextAutocorrectionTypeNo;
    textFieldShortNote.borderStyle = UITextBorderStyleNone;
    textFieldShortNote.clearButtonMode = UITextFieldViewModeWhileEditing;
    textFieldShortNote.returnKeyType = UIReturnKeyDone;
    textFieldShortNote.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textFieldShortNote.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:14.0f];
    return textFieldShortNote;
}

#pragma -
#pragma mark - Create UIPickerView
-(UIPickerView *)createPickerView
{
    pickerViewSeason = [[UIPickerView alloc] initWithFrame:CGRectMake(_viewSeason.frame.origin.x, _viewSeason.frame.origin.y-60, _viewSeason.frame.size.width,  80)];
    pickerViewSeason.showsSelectionIndicator = YES;
    pickerViewSeason.backgroundColor = [UIColor whiteColor];
    pickerViewSeason.delegate = self;
    pickerViewSeason.hidden = YES;
    [self.view addSubview:pickerViewSeason];
    return pickerViewSeason;
}

#pragma mark - UIPickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component; {
    return 4;
}

#pragma - UIPickerView Delegate
-(NSString*)pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[self itemsInPickerView] objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    _labelSeason.text = [[self itemsInPickerView] objectAtIndex:row];
}

#pragma mark - Data generators

- (NSArray *)itemsInPickerView {
    NSMutableArray *items = [NSMutableArray array];
    [items addObject:@"Spring"];
    [items addObject:@"Summer"];
    [items addObject:@"Fall"];
    [items addObject:@"Winter"];
    
    return items;
}

# pragma -
# pragma mark - UITextField Delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if(isAnimateDescriptionView){
        isAnimateDescriptionView = NO;
        [self closeAnimationForWriteAShortDescription];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    groupTripObject.tripDescription= textField.text;
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    if (range.length==1) {
        return YES;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 60) ? NO : YES;
    return YES;
}

#pragma mark -  Response Successful
-(void)groupCreatedSuccessfully:(GroupTrip *)groupTripDetail
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    GroupTripDetailViewController *groupTripDetailViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"GroupTripDetailViewController"];
    groupTripDetailViewController.groupDetails= [self updatedGroupDetail:groupTripDetail];
    [self.navigationController pushViewController:groupTripDetailViewController animated:YES];
}
-(GroupTrip *)updatedGroupDetail:(GroupTrip *)groupDetail
{
    groupDetail.tripUsersDetails=_groupTripDetail.tripUsersDetails;
    groupDetail.ownerId=_groupTripDetail.ownerId;
    return groupDetail;
}
#pragma mark - Response Failed
-(void)failedWithError:(NSDictionary*)dictionaryError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
