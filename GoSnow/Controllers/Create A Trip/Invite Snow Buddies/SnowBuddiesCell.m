//
//  SnowBuddiesCell.m
//  GoSnow
//
//  Created by Varsha Singh on 24/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "SnowBuddiesCell.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIColor+Color.h"
@implementation SnowBuddiesCell

- (void)awakeFromNib {
    // Initialization code
    UIImageView *imageViewGroup = (UIImageView *)[self viewWithTag:1001];
    imageViewGroup.layer.cornerRadius = 30;
}

-(void)SetLayout:(Users *)userInfo
{
    UILabel *userName =(UILabel *)[self viewWithTag:61];
    UILabel *labelDescription =(UILabel *)[self viewWithTag:62];
     UIImageView *contactSnowBuddyImage = (UIImageView *)[self viewWithTag:1001];
    contactSnowBuddyImage.layer.cornerRadius=contactSnowBuddyImage.frame.size.height/2;
    contactSnowBuddyImage.layer.masksToBounds=YES;
    // UILabel *labelFroupOwner =(UILabel *)[self viewWithTag:61];
    labelDescription.text=userInfo.inContactFrom;
    //NSString *buddyFullName;
    if (userInfo.profileImage.length>0) {
        [contactSnowBuddyImage setImageWithURL:[NSURL URLWithString:userInfo.profileImage] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [contactSnowBuddyImage setImage:[UIImage imageNamed:@"smallProfile"]];
    }
    
    if (userInfo.firstName.length>0 && userInfo.lastName.length>0) {
        userName.text=[NSString stringWithFormat:@"%@ %@",userInfo.firstName,userInfo.lastName];
    }
    else
    {
        userName.text=[NSString stringWithFormat:@"%@",userInfo.firstName];
    }
    UIButton *buttonSelect = (UIButton *)[self viewWithTag:631];
    
    if (userInfo.isInvited)
    {
       
        [buttonSelect setTitle:@"Invited" forState:UIControlStateNormal];
        [buttonSelect setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
       // buttonSelect.userInteractionEnabled=NO;
       
    }
    else{
        // buttonSelect.userInteractionEnabled=YES;
        if (userInfo.isSelect) {
            [buttonSelect setTitle:@"Invite" forState:UIControlStateNormal];
            [buttonSelect setTitleColor:[UIColor colorwithHexString:@"#04CF51" alpha:1.0] forState:UIControlStateNormal];
            //[buttonSelect setImage:[UIImage imageNamed:@"tick_icon"] forState:UIControlStateNormal];
        }
        else
        {
            [buttonSelect setTitle:@"Invite" forState:UIControlStateNormal];
             [buttonSelect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
           // [buttonSelect setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        }
       
    }
}

@end
