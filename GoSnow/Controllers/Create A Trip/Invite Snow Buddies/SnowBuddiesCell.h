//
//  SnowBuddiesCell.h
//  GoSnow
//
//  Created by Varsha Singh on 24/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Users.h"
@interface SnowBuddiesCell : UITableViewCell

@property id sender;
-(void)SetLayout:(Users *)userInfo;
@end
