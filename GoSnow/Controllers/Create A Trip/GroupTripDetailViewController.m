//
//  GroupTripDetailViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 23/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "GroupTripDetailViewController.h"
#import "AddSnowBuddiesViewController.h"
#import "FindRidersViewController.h"
#import "GroupTripViewController.h"
#import "UsersViewController.h"
#import "UIImage+ImageShape.h"
#import "TripUserGoingCell.h"
@interface GroupTripDetailViewController ()

@end

@implementation GroupTripDetailViewController
@synthesize groupDetails;
- (void)viewDidLoad {
    [super viewDidLoad];
    isRidersVisible = NO;
    goingRidersArray=[[NSMutableArray alloc] init];
    [goingRidersArray addObjectsFromArray:groupDetails.tripUsersDetails];
    if ([goingRidersArray count]>1) {
        _labelDescription.text=[NSString stringWithFormat:@"%lu rider accepted group request.",(unsigned long)[goingRidersArray count]-1];
        [_topRightButton setImage:[UIImage CreateAndReturnImagesCircle:@"#2AB73C" rect:_topRightButton.frame  text:[NSString stringWithFormat:@"%lu",(unsigned long)[goingRidersArray count]]] forState:UIControlStateNormal];
    }
    else
    {
        [_topRightButton setImage:[UIImage CreateAndReturnImagesCircle:@"#2AB73C" rect:_topRightButton.frame  text:@"x"] forState:UIControlStateNormal];
    }
    
}

#pragma mark - IBActions
-(IBAction)doneButtonAction
{
    for (UIViewController *view in self.navigationController.childViewControllers) {
        if ([view isKindOfClass:[GroupTripViewController class]]) {
            [self.navigationController popToViewController:view animated:YES];
            return;
        }
    }
}

-(IBAction)displayAddedRiderListAction
{
    if ([goingRidersArray count]==0) {
        return;
    }
    if(!isRidersVisible)
        [self openAnimationForRidersView];
    else
        [self closeAnimationForRidersView];
}

-(IBAction)addSnowBuddiesAction
{
    AddSnowBuddiesViewController *addSnowBuddiesViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"AddSnowBuddiesViewController"];
    addSnowBuddiesViewController.groupDetails=groupDetails;
    [self.navigationController pushViewController:addSnowBuddiesViewController animated:YES];
}

-(IBAction)findRidersAction
{
    UsersViewController *usersViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"UsersViewController"];
    usersViewController.groupTrip=groupDetails;
    usersViewController.isFromGroupTrip= YES;
    [self.navigationController pushViewController:usersViewController animated:YES];
}

#pragma mark - Maintain Animations Of Views
-(void)openAnimationForRidersView
{
    UIView *headerView = [self.view viewWithTag:101];
    isRidersVisible = YES;
    CGRect frame = _ridersListTableView.frame;
    _ridersListTableView.frame = CGRectMake(0,
                                            headerView.frame.size.height,
                                            _ridersListTableView.frame.size.width,
                                            0);
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _ridersListTableView.hidden = NO;
                         _ridersListTableView.frame = frame;
                         [_topRightButton setImage:[UIImage CreateAndReturnImagesCircle:@"#2AB73C" rect:_topRightButton.frame  text:@"x"] forState:UIControlStateNormal];
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}

-(void)closeAnimationForRidersView
{
    isRidersVisible = NO;
    CGRect frame = _ridersListTableView.frame;
    CGRect tmpFrame = _ridersListTableView.frame;
    tmpFrame.size.height = 0;
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _ridersListTableView.frame = tmpFrame;
                     }
                     completion:^(BOOL finished){
                         _ridersListTableView.hidden = YES;
                         _ridersListTableView.frame = frame;
                         [_topRightButton setImage:[UIImage CreateAndReturnImagesCircle:@"#2AB73C" rect:_topRightButton.frame  text:[NSString stringWithFormat:@"%lu",(unsigned long)[goingRidersArray count]]] forState:UIControlStateNormal];
                     }];
    
}

#pragma mark UITableView Datasource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return [goingRidersArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    // [tableView setSeparatorColor:[UIColor clearColor]];
    static NSString *CellIdentifier = @"TripUserGoingCell";
    TripUserGoingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TripUserGoingCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    
    [cell setlayout:[goingRidersArray objectAtIndex:indexPath.row] ownerId:self.groupDetails.ownerId];
    
    return cell;
}


@end
