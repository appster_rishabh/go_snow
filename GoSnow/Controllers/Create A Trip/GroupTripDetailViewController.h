//
//  GroupTripDetailViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 23/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupTrip.h"

@interface GroupTripDetailViewController : UIViewController
{
    BOOL isRidersVisible;
    NSMutableArray *goingRidersArray;
}
@property(nonatomic,weak)IBOutlet UILabel *labelDescription;
@property(nonatomic,weak)IBOutlet UIButton *topRightButton;
@property(nonatomic,strong)GroupTrip *groupDetails;
@property(nonatomic,weak)IBOutlet UITableView *ridersListTableView;
//@property(nonatomic,weak)IBOutlet UIButton *ridersListTableView;

-(IBAction)displayAddedRiderListAction;
-(IBAction)addSnowBuddiesAction;
-(IBAction)findRidersAction;
-(IBAction)doneButtonAction;

@end
