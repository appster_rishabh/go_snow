//
//  AddSnowBuddiesViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 24/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//


#import "AddSnowBuddiesViewController.h"
#import "SnowBuddiesCell.h"

@interface AddSnowBuddiesViewController ()

@end
NSMutableArray *arrayUserIds;
@implementation AddSnowBuddiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   // _tableSnowBuddies.hidden = YES;
    snowBuddiesArray = [[NSMutableArray alloc]init];
    invitedUserIdArray = [[NSMutableArray alloc]init];
    
    [self fetchSnowBuddies];
}

#pragma mark - Response From Web Service
-(void)fetchSnowBuddies
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    if (_groupDetails.groupId.length>0)
        [params setObject:_groupDetails.groupId forKey:@"tripId"];
    
    [params setObject:sessionId forKey:@"sessionId"];
     [[GroupTripManager sharedInstance]setDelegate:self];
    [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"contact/search"];
   
}

#pragma mark -
#pragma mark IBActions
-(IBAction)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)inviteSnowBuddiesAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:_tableSnowBuddies];
    NSIndexPath *indexPath = [_tableSnowBuddies indexPathForRowAtPoint:buttonPosition];
    
    [self selectSnowBuddies:indexPath];
  /*
    if([invitedUserIdArray count]>0)
    {
        NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        
        NSMutableDictionary *params=[NSMutableDictionary dictionary];
        if (_groupDetails.groupId.length>0)
            [params setObject:_groupDetails.groupId forKey:@"selectedTripId"];
        [params setObject:invitedUserIdArray forKey:@"selectedUserIds"];
        [params setObject:sessionId forKey:@"sessionId"];
        [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"tripUser/add"];
        [[GroupTripManager sharedInstance]setDelegate:self];
        [invitedUserIdArray removeAllObjects];
    }*/
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [snowBuddiesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    static NSString *CellIdentifier = @"SnowBuddiesCell";
    SnowBuddiesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SnowBuddiesCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.sender=self;
    }
    [cell SetLayout:[snowBuddiesArray objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self selectSnowBuddies:indexPath];
   /* if ([[[snowBuddiesArray objectAtIndex:indexPath.row] valueForKey:@"isInvited"] integerValue] == 0)
    {
        if(![invitedUserIdArray containsObject:[[snowBuddiesArray objectAtIndex:indexPath.row] valueForKey:@"id"]])
        {
            [invitedUserIdArray addObject:[[snowBuddiesArray objectAtIndex:indexPath.row] valueForKey:@"id"]];
        }
    }*/
}
#pragma button action
-(void)selectSnowBuddies:(NSIndexPath *)indexPath
{
    Users *userInfo=[snowBuddiesArray objectAtIndex:indexPath.row];
    if (userInfo.isInvited) {
        return;
    }
    BOOL isSeleted=userInfo.isSelect;
    if (isSeleted){
        
        userInfo.isSelect=NO;
        [_tableSnowBuddies reloadData];
        return;
    }
        if (isSeleted){
       
        userInfo.isSelect=NO;
    }
    else{
       
        userInfo.isSelect=YES;
    }
    
    [snowBuddiesArray replaceObjectAtIndex:indexPath.row withObject:userInfo];
    [_tableSnowBuddies reloadData];
}
-(IBAction)doneAction:(id)sender
{
    NSMutableArray *userIds=[NSMutableArray array];
   // NSString *locationsId=[[NSString alloc] init];
    for (int i=0; i<[snowBuddiesArray count]; i++)
    {
        Users *userInfo=[snowBuddiesArray objectAtIndex:i];
        BOOL isSeleted =userInfo.isSelect;
        if (isSeleted)
        {
            [userIds addObject:userInfo.user_id];
       
        }
    }
    if (userIds.count>0) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        
        NSMutableDictionary *params=[NSMutableDictionary dictionary];
        if (_groupDetails.groupId.length>0)
            [params setObject:_groupDetails.groupId forKey:@"selectedTripId"];
        [params setObject:userIds forKey:@"selectedUserIds"];
        [params setObject:sessionId forKey:@"sessionId"];
        [[GroupTripManager sharedInstance]setDelegate:self];
        [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"tripUser/add"];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Select contact." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}
-(NSString *)removeLastCharacter:(NSString *)stringText
{
    NSString *lastCharacter=[stringText substringFromIndex:[stringText length]-1];
    if ([lastCharacter isEqualToString:@","]) {
        stringText=[stringText substringToIndex:[stringText length]-1];
    }
    return stringText;
}
#pragma mark -
#pragma mark - Invite Snow buddies Method

#pragma mark -  Response Successful
-(void)addSnowBudiesToGroup:(NSMutableArray *)arraySnowBuddies;
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [snowBuddiesArray removeAllObjects];
    [snowBuddiesArray addObjectsFromArray:arraySnowBuddies];
    _tableSnowBuddies.hidden = NO;
    [_tableSnowBuddies reloadData];
}

-(void)inviteSnowBuddiesToGroup
{
     [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Response Failed
-(void)failedWithError:(NSDictionary*)dictionaryError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


@end
