//
//  GroupTripCell.h
//  GoSnow
//
//  Created by Varsha Singh on 20/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupTrip.h"
#import "SWTableViewCell.h"

@interface GroupTripCell : SWTableViewCell

@property id sender;
-(void)setLayout:(GroupTrip *)groupInfo index:(NSIndexPath *)path;


@end
