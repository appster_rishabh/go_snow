//
//  TripUserGoingCell.m
//  GoSnow
//
//  Created by Rishabh on 29/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "TripUserGoingCell.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import <QuartzCore/QuartzCore.h>
@implementation TripUserGoingCell

- (void)awakeFromNib {
    // Initialization code
}
-(void)setlayout:(NSMutableDictionary *)userDict ownerId:(NSString *)ownerId
{
    UILabel *userName=(UILabel *)[self viewWithTag:102];
    UILabel *adminLabel=(UILabel *)[self viewWithTag:103];
     UIImageView *userImage=(UIImageView *)[self viewWithTag:101];
    userImage.layer.masksToBounds=YES;
    userImage.layer.cornerRadius=userImage.frame.size.width/2;
    NSString *stringFullName;
    if ([[userDict valueForKey:@"firstName"]length]>0)
        stringFullName = [NSString stringWithFormat:@"%@",[userDict valueForKey:@"firstName"]];
    
    if ([[userDict valueForKey:@"lastName"]length]>0)
        stringFullName =[stringFullName stringByAppendingString:[NSString stringWithFormat:@" %@",[userDict valueForKey:@"lastName"]]];
    userName.text=stringFullName;
    NSString *imageUrl=[userDict valueForKey:@"userThumb"];
    if (imageUrl.length<1) {
        [userImage setImage:[UIImage imageNamed:@"smallProfile"]];
    }
    else
    {
        [userImage setImageWithURL:[NSURL URLWithString:imageUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
        if ([[userDict valueForKey:@"userId"] isEqualToString:ownerId]) {
        adminLabel.hidden=NO;
    }
    else
    {
     adminLabel.hidden=YES;
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
