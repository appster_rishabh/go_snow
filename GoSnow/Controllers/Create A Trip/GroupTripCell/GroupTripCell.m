//
//  GroupTripCell.m
//  GoSnow
//
//  Created by Varsha Singh on 20/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "GroupTripCell.h"
#import <CoreGraphics/CoreGraphics.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIColor+Color.h"
@implementation GroupTripCell

- (void)awakeFromNib {
    // Initialization code
    //UIImageView *imageViewGroup = (UIImageView *)[self viewWithTag:1001];
    //imageViewGroup.layer.cornerRadius = 35;
}

-(void)setLayout:(GroupTrip *)groupInfo index:(NSIndexPath *)path
{
    UILabel *labelFroupOwner =(UILabel *)[self viewWithTag:61];
    if (groupInfo.groupOwnerName) {
        NSString *stringTripCreatedBy =[NSString stringWithFormat:@"Trip by %@",groupInfo.groupOwnerName];
        labelFroupOwner.text =stringTripCreatedBy;
    }
    
    CGSize cellSize;
    BOOL isread=groupInfo.isPending;
    UILabel *labelDescription =(UILabel *)[self viewWithTag:62];
    UIImageView *readImage=(UIImageView *)[self viewWithTag:107];
    UIImageView *seperator=(UIImageView *)[self viewWithTag:66];
    //66

    if (groupInfo.tripDescription)
    {
        cellSize =  [self getSizeForText:groupInfo.tripDescription maxWidth:labelDescription.frame.size.width font:[UIFont fontWithName:@"RobotoCondensed-Regular" size:17.0f]];
       // labelDescription.frame = CGRectMake(labelDescription.frame.origin.x, labelDescription.frame.origin.y, labelDescription.frame.size.width, cellSize.height);
        labelDescription.text = groupInfo.tripDescription;
        if (!isread) {
            readImage.hidden=NO;
            labelDescription.frame=CGRectMake(labelDescription.frame.origin.x+14, labelDescription.frame.origin.y, labelDescription.frame.size.width, cellSize.height);
        }
        else
        {
            BOOL isJoiningRequest=groupInfo.isJoiningRequest;
            if (isJoiningRequest) {
                readImage.hidden=NO;
                labelDescription.frame=CGRectMake(labelDescription.frame.origin.x+14, labelDescription.frame.origin.y, labelDescription.frame.size.width, cellSize.height);
            }
            else
            {
                readImage.hidden=YES;
                labelDescription.frame=CGRectMake(labelDescription.frame.origin.x, labelDescription.frame.origin.y, labelDescription.frame.size.width, cellSize.height);
            }
        }
    }

    UIButton *buttonSelect = (UIButton *)[self viewWithTag:63];
    buttonSelect.hidden = YES;
    
    UIImageView *imageViewGroup = (UIImageView *)[self viewWithTag:1001];
    NSMutableArray *tripUsers=groupInfo.tripUsersDetails;
    [imageViewGroup addSubview:[self createImageView:tripUsers]];
    seperator.frame=CGRectMake(0, self.frame.size.height-1, WIDTH, 1);
    // Riders count to be passed
    

}

-(UIImageView *)createImageView:(NSMutableArray *)tripUsers
{
    CGRect tmpFrame = [[self viewWithTag:1001] frame];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, tmpFrame.size.width, tmpFrame.size.height)];
    if([tripUsers count] == 1)
    {
        imageView.layer.masksToBounds=YES;
        imageView.layer.cornerRadius = imageView.frame.size.width/2;
        [self setImageviewImage:imageView url:[[tripUsers objectAtIndex:0] valueForKey:@"userImage250"]];
        return imageView;
    }
    else if([tripUsers count] == 2)
    {
        CGRect frame = imageView.frame;
        UIImageView *tmpImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, (frame.size.height/4), frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView setBackgroundColor:[UIColor blackColor]];
        tmpImgView.layer.cornerRadius = tmpImgView.frame.size.height/2;
        tmpImgView.layer.masksToBounds=YES;
        [self setImageviewImage:tmpImgView url:[[tripUsers objectAtIndex:0] valueForKey:@"userThumb"]];
       
        [imageView addSubview:tmpImgView];
        
        UIImageView *tmpImgView2 = [[UIImageView alloc]initWithFrame:CGRectMake(frame.size.width/2, (frame.size.height/4), frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView2 setBackgroundColor:[UIColor brownColor]];
        tmpImgView2.layer.cornerRadius = 17;
        tmpImgView2.layer.masksToBounds=YES;
        [imageView addSubview:tmpImgView2];
        [self setImageviewImage:tmpImgView2 url:[[tripUsers objectAtIndex:1] valueForKey:@"userThumb"]];
        return imageView;
    }
    
    else if([tripUsers count] == 3)
    {
        CGRect frame = imageView.frame;
        UIImageView *tmpImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView setBackgroundColor:[UIColor blackColor]];
        tmpImgView.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView];
       [self setImageviewImage:tmpImgView url:[[tripUsers objectAtIndex:0] valueForKey:@"userThumb"]];
        UIImageView *tmpImgView2 = [[UIImageView alloc]initWithFrame:CGRectMake(frame.size.width/2, 0, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView2 setBackgroundColor:[UIColor brownColor]];
        tmpImgView2.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView2];
        [self setImageviewImage:tmpImgView2 url:[[tripUsers objectAtIndex:1] valueForKey:@"userThumb"]];
        UIImageView *tmpImgView3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, frame.size.height/2, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView3 setBackgroundColor:[UIColor redColor]];
        tmpImgView3.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView3];
        tmpImgView.layer.masksToBounds=YES;
        tmpImgView2.layer.masksToBounds=YES;
        tmpImgView3.layer.masksToBounds=YES;
        [self setImageviewImage:tmpImgView3 url:[[tripUsers objectAtIndex:2] valueForKey:@"userThumb"]];
        return imageView;
    }
    
    else if([tripUsers count] == 4)
    {
        CGRect frame = imageView.frame;
        UIImageView *tmpImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView setBackgroundColor:[UIColor blackColor]];
        tmpImgView.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView];
       
        UIImageView *tmpImgView2 = [[UIImageView alloc]initWithFrame:CGRectMake(frame.size.width/2, 0, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView2 setBackgroundColor:[UIColor brownColor]];
        tmpImgView2.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView2];
        
        UIImageView *tmpImgView3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, frame.size.height/2, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView3 setBackgroundColor:[UIColor redColor]];
        tmpImgView3.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView3];
        
        UIImageView *tmpImgView4 = [[UIImageView alloc]initWithFrame:CGRectMake(frame.size.width/2, frame.size.height/2, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView4 setBackgroundColor:[UIColor grayColor]];
        tmpImgView4.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView4];
        tmpImgView.layer.masksToBounds=YES;
        tmpImgView2.layer.masksToBounds=YES;
        tmpImgView3.layer.masksToBounds=YES;
         tmpImgView4.layer.masksToBounds=YES;
        
        [self setImageviewImage:tmpImgView url:[[tripUsers objectAtIndex:0] valueForKey:@"userThumb"]];
        [self setImageviewImage:tmpImgView2 url:[[tripUsers objectAtIndex:1] valueForKey:@"userThumb"]];
        [self setImageviewImage:tmpImgView3 url:[[tripUsers objectAtIndex:2] valueForKey:@"userThumb"]];[self setImageviewImage:tmpImgView4 url:[[tripUsers objectAtIndex:3] valueForKey:@"userThumb"]];
        return imageView;
    }
    
    else if([tripUsers count]>4)
    {
        CGRect frame = imageView.frame;
        UIImageView *tmpImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView setBackgroundColor:[UIColor blackColor]];
        tmpImgView.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView];
        
        UIImageView *tmpImgView2 = [[UIImageView alloc]initWithFrame:CGRectMake(frame.size.width/2, 0, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView2 setBackgroundColor:[UIColor brownColor]];
        tmpImgView2.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView2];
        
        UIImageView *tmpImgView3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, frame.size.height/2, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView3 setBackgroundColor:[UIColor redColor]];
        tmpImgView3.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView3];
        
        UIImageView *tmpImgView4 = [[UIImageView alloc]initWithFrame:CGRectMake(frame.size.width/2, frame.size.height/2, frame.size.width/2-2, frame.size.height/2-2)];
        [tmpImgView4 setBackgroundColor:[UIColor colorwithHexString:@"#1C7CE9" alpha:1.0]];
        tmpImgView.layer.masksToBounds=YES;
        tmpImgView2.layer.masksToBounds=YES;
        tmpImgView3.layer.masksToBounds=YES;
        tmpImgView4.layer.masksToBounds=YES;
        UILabel *label = [[UILabel alloc]initWithFrame:tmpImgView4.frame];
        label.text = [NSString stringWithFormat:@"+%lu",[tripUsers count]-3 ];
        label.font=[UIFont fontWithName:@"PassionOne-Bold" size:14.0f];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setTextColor:[UIColor whiteColor]];
        tmpImgView4.layer.cornerRadius = 17;
        [imageView addSubview:tmpImgView4];
        [imageView addSubview:label];
        [self setImageviewImage:tmpImgView url:[[tripUsers objectAtIndex:0] valueForKey:@"userThumb"]];
        [self setImageviewImage:tmpImgView2 url:[[tripUsers objectAtIndex:1] valueForKey:@"userThumb"]];
        [self setImageviewImage:tmpImgView3 url:[[tripUsers objectAtIndex:2] valueForKey:@"userThumb"]];
        return imageView;
    }
    
    else
     return imageView;
}

#pragma mark - Get Text Size
- (CGSize)getSizeForText:(NSString *)text maxWidth:(CGFloat)width font:(UIFont *)font  {
    NSArray *arr=[NSArray arrayWithObject:text];
    CGSize DXsize = [[arr objectAtIndex:0] sizeWithFont:font constrainedToSize:CGSizeMake(width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    return DXsize;
}
-(void)setImageviewImage:(UIImageView *)imageView url:(NSString *)imageUrl
{
    if (imageUrl.length<1) {
        [imageView setImage:[UIImage imageNamed:@"smallProfile"]];
    }
    else
    {
        [imageView setImageWithURL:[NSURL URLWithString:imageUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
}
@end
