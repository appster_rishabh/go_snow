//
//  TripUserGoingCell.h
//  GoSnow
//
//  Created by Rishabh on 29/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripUserGoingCell : UITableViewCell
-(void)setlayout:(NSMutableDictionary *)userDict ownerId:(NSString *)ownerId;
@end
