//
//  GroupTripRequestViewController.m
//  GoSnow
//
//  Created by Rishabh on 30/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "GroupTripRequestViewController.h"
#import "UIImage+ImageShape.h"
#import "TripUserGoingCell.h"
#import "ViewUserProfileViewController.h"

@interface GroupTripRequestViewController ()

@end

@implementation GroupTripRequestViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    isRidersVisible = NO;
    requestUserArray=[[NSMutableArray alloc]init];
    _ridersListTableView.hidden=YES;
    _draggableBackground.delegate=self;
    [self setUpArray];
    [self setUpInitials];
}
-(void)setUpArray
{
    if (_groupTripDetails.isJoiningRequest) {
        for (int i=0; i<[_groupTripDetails.joinRequests count]; i++) {
            Users *users=[[Users alloc] init];
            NSMutableDictionary *userDict=[_groupTripDetails.joinRequests objectAtIndex:i];
            users.firstName=[userDict valueForKey:@"firstName"];
            users.lastName=[userDict valueForKey:@"lastName"];
            users.facebookId=[userDict valueForKey:@"facebookId"];
            users.skillLevel=[userDict valueForKey:@"skillLevelName"];
            users.skillLevelID=[userDict valueForKey:@"skillLevelId"];
            users.riderType=[userDict valueForKey:@"riderTypeName"];
            users.riderTypeId=[userDict valueForKey:@"riderTypeId"];
            users.userPhotos=[userDict valueForKey:@"userPhotos"];
            users.user_id=[userDict valueForKey:@"id"];
            users.age=[userDict valueForKey:@"age"];
            users.plannedTripName=[userDict valueForKey:@"nextDestinationName"];
            users.plannedTripId=[userDict valueForKey:@"nextDestinationId"];
            users.aboutMe=[userDict valueForKey:@"aboutMe"];
            users.address=[userDict valueForKey:@"address"];
            users.userPhotos250=[userDict valueForKey:@"userPhotos250"];
                       users.interestName=[self getIneterests:[userDict valueForKey:@"sharedInterests"]];
            users.locationName=[self getLocations:[userDict valueForKey:@"sharedLocations"]];
            users.contactsName=[self getContacts:[userDict valueForKey:@"sharedContacts"]];
            users.distance=[userDict valueForKey:@"distance"];
           
            [requestUserArray addObject:users];
            
        }
        
    }
    else
    {
        if (_groupTripDetails.isPending) {
            _noUserFoundView.hidden=NO;
            _draggableBackground.hidden=YES;
        }
        else
        {
            for (int i=0; i<[_groupTripDetails.tripUsersDetails count]; i++) {
                Users *users=[[Users alloc] init];
                NSDictionary *userDict=[_groupTripDetails.tripUsersDetails objectAtIndex:i];
                users.firstName=[userDict valueForKey:@"firstName"];
                users.lastName=[userDict valueForKey:@"lastName"];
                users.facebookId=[userDict valueForKey:@"facebookId"];
                users.skillLevel=[userDict valueForKey:@"skillLevelName"];
                users.skillLevelID=[userDict valueForKey:@"skillLevelId"];
                users.riderType=[userDict valueForKey:@"riderTypeName"];
                users.riderTypeId=[userDict valueForKey:@"riderTypeId"];
                users.userPhotos=[userDict valueForKey:@"userPhotos"];
                users.user_id=[userDict valueForKey:@"id"];
                users.age=[userDict valueForKey:@"age"];
                users.plannedTripName=[userDict valueForKey:@"nextDestinationName"];
                users.plannedTripId=[userDict valueForKey:@"nextDestinationId"];
                users.aboutMe=[userDict valueForKey:@"aboutMe"];
                users.address=[userDict valueForKey:@"address"];
                users.userPhotos250=[userDict valueForKey:@"userPhotos250"];
                users.interestName=[self getIneterests:[userDict valueForKey:@"sharedInterests"]];
                users.locationName=[self getLocations:[userDict valueForKey:@"sharedLocations"]];
                users.contactsName=[self getContacts:[userDict valueForKey:@"sharedContacts"]];
                users.distance=[userDict valueForKey:@"distance"];
                
                [requestUserArray addObject:users];
  
        }
        
        }
    }
    [_draggableBackground SetUpInitials:requestUserArray fromScreen:GGOverlayViewModeGroupRequestScreen];
   // goingRidersArray
    
}
-(void)manageDraggableView
{
    currentIndex=0;
    _draggableBackground.delegate=self;
    
   
      //  [_draggableBackground SetUpInitials:_suggestedUsers fromScreen:GGOverlayViewModeGroupScreen];
    
    
    [self updateUserNameFirst:0];
    
}
#pragma mark - Set Up Initial Information
-(void)setUpInitials
{
    goingRidersArray=[[NSMutableArray alloc] init];
    [goingRidersArray addObjectsFromArray:_groupTripDetails.tripUsersDetails];
    
    [_topRightButton setImage:[UIImage CreateAndReturnImagesCircle:@"#2AB73C" rect:_topRightButton.frame  text:[NSString stringWithFormat:@"%lu",(unsigned long)[goingRidersArray count]]] forState:UIControlStateNormal];
    
    if (_groupTripDetails.destinationName.length>0)
        _labelDestination.text = [_groupTripDetails.destinationName uppercaseString];

    if (_groupTripDetails.tripDescription.length>0)
        _labelDescription.text = _groupTripDetails.tripDescription;
    
    if (_groupTripDetails.season.length>0)
        _labelSeason.text = _groupTripDetails.season;
    
    if (!_groupTripDetails.isPending)
    {
        _declineButton.hidden=NO;
        _acceptButton.hidden=NO;
    }
    else
    {
        _declineButton.hidden=YES;
        _acceptButton.hidden=YES;
    }
    if (_groupTripDetails.isJoiningRequest) {
        [self updateUserNameFirst:0];
    }
    else
    {
        _userName.text=_groupTripDetails.groupOwnerName;
    }
    
}

#pragma mark - IBActions
-(IBAction)doneButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)declineRequestAction:(NSInteger)index
{
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:sessionId forKey:@"sessionId"];
    
    [[GroupTripManager sharedInstance]setDelegate:self];
    if (_groupTripDetails.isJoiningRequest) {
         Users *userInfo=[requestUserArray objectAtIndex:index];
        [params setObject:userInfo.user_id forKey:@"tripUserId"];
        [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"tripUser/rejectToJoin"];
    }
    else
    {
        [params setObject:_groupTripDetails.tripUserId forKey:@"tripUserId"];
        [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"tripUser/reject"];
    }
   

}
-(void)acceptRequestAction:(NSInteger)index
{
//http://localhost/gosnow/index.php/service/tripUser/accept
    //   [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:sessionId forKey:@"sessionId"];
    
     [[GroupTripManager sharedInstance]setDelegate:self];
    if (_groupTripDetails.isJoiningRequest) {
        Users *userInfo=[requestUserArray objectAtIndex:index];
        [params setObject:userInfo.user_id forKey:@"tripUserId"];
        [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"tripUser/acceptToJoin"];
    }
    else
    {
        [params setObject:_groupTripDetails.tripUserId forKey:@"tripUserId"];
       [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"tripUser/accept"];
    }
   
   

}

-(IBAction)displayAddedRiderListAction
{
    if(!isRidersVisible)
        [self openAnimationForRidersView];
    else
        [self closeAnimationForRidersView];
}
#pragma mark update messagesCount
-(void)updateRequestCount
{
    [UserInfoEntity updateRequestAndmessagesCounts:@"" messagesCounts:@"" tripRequestCounts:@"1"  inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}
#pragma mark - Maintain Animations Of Views
-(void)openAnimationForRidersView
{
    UIView *headerView = [self.view viewWithTag:101];
    isRidersVisible = YES;
    _ridersListTableView.hidden=NO;
    CGRect frame = _ridersListTableView.frame;
    _ridersListTableView.frame = CGRectMake(0,
                                            headerView.frame.size.height,
                                            _ridersListTableView.frame.size.width,
                                            0);
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _ridersListTableView.hidden = NO;
                         _ridersListTableView.frame = CGRectMake(0,
                                                                 64,
                                                                 _ridersListTableView.frame.size.width,
                                                                 frame.size.height);
                         [_topRightButton setImage:[UIImage CreateAndReturnImagesCircle:@"#2AB73C" rect:_topRightButton.frame  text:@"x"] forState:UIControlStateNormal];
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}

-(void)closeAnimationForRidersView
{
    isRidersVisible = NO;
    CGRect frame = _ridersListTableView.frame;
    CGRect tmpFrame = _ridersListTableView.frame;
    tmpFrame.size.height = 0;
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _ridersListTableView.frame = tmpFrame;
                     }
                     completion:^(BOOL finished){
                         _ridersListTableView.hidden = YES;
                         _ridersListTableView.frame = frame;
                         [_topRightButton setImage:[UIImage CreateAndReturnImagesCircle:@"#2AB73C" rect:_topRightButton.frame  text:[NSString stringWithFormat:@"%lu",(unsigned long)[goingRidersArray count]]] forState:UIControlStateNormal];
                         _ridersListTableView.hidden=YES;
                     }];
    
}

#pragma mark UITableView Datasource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return [goingRidersArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    // [tableView setSeparatorColor:[UIColor clearColor]];
    static NSString *CellIdentifier = @"TripUserGoingCell";
    TripUserGoingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TripUserGoingCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    
    [cell setlayout:[goingRidersArray objectAtIndex:indexPath.row] ownerId:_groupTripDetails.ownerId];
    
    return cell;
}
#pragma mark web service response
-(void)failedWithError:(NSDictionary*)errorDictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(void)requestAcceptedSuccesfully:(NSDictionary *)dictionary
{
    [self updateRequestCount];
     [MBProgressHUD hideHUDForView:self.view animated:YES];
    _declineButton.hidden=YES;
    _acceptButton.hidden=YES;
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Request accepted." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //alert.tag=401;
    [alert show];
}
-(void)requestRejectedSuccesfully:(NSDictionary *)dictionary
{
     [self updateRequestCount];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    _acceptButton.hidden=YES;
    _declineButton.hidden=YES;
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Request rejected." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //alert.tag=401;
    [alert show];
}
#pragma mark uialert button action
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag==401) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark - Notification Action
-(void)swipeLeftAfterDelay
{
    [self performSelector:@selector(swipeLeft) withObject:nil afterDelay:0.3];
}
-(void)swipeRightAfterDelay
{
    [self performSelector:@selector(swipeRight) withObject:nil afterDelay:0.3];
}
-(void)swipeLeft
{
    [_draggableBackground swipeLeft];
}
-(void)swipeRight
{
    [_draggableBackground swipeRight];
}
#pragma mark - Swipe Action

-(void)userSwipedLeftAction:(UIView *)user
{
    [self updateUserName:user.tag-400];
    currentIndex=user.tag-400;
    [self declineRequestAction:currentIndex];
}
-(void)userSwipedRightAction:(UIView *)user
{
    [self updateUserName:user.tag-400];
    currentIndex=user.tag-400;
    
    [self acceptRequestAction:currentIndex];
   }
-(void)userTappedAction:(UIView *)user
{
    [self myProfile:user.tag-400];
}

-(void)myProfile:(NSInteger)tag
{
    Users *userInfo=[requestUserArray objectAtIndex:tag];
//    if ([userInfo.model isEqualToString:@"trip"]) {
//        return;
//    }
    
    ViewUserProfileViewController *viewUserProfileViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"ViewUserProfileViewController"];
    viewUserProfileViewController.otherUsersObject=userInfo;
            viewUserProfileViewController.fromScreenType=GGOverlayViewModeGroupRequestScreen;
       CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromTop;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:viewUserProfileViewController animated:NO];
}
#pragma mark  - Update User Name
-(void)updateUserNameFirst:(NSInteger)index
{
    UIImageView *imageViewLeft=(UIImageView *)[self.view viewWithTag:901];
    UIImageView *imageViewRight=(UIImageView *)[self.view viewWithTag:902];
    
    if (index>=[requestUserArray count]) {
            //   self.userName.text=@"REQUEST (0)";
        _noUserFoundView.hidden=NO;
        _draggableBackground.hidden=YES;
       
        return;
        
    }
    Users *userInfo=[requestUserArray objectAtIndex:index];
       NSString *usersName;
    if (userInfo.firstName.length>0 && userInfo.lastName.length>0) {
        usersName=[NSString stringWithFormat:@"%@ %@",userInfo.firstName,userInfo.lastName];
    }
    else
    {
        usersName=[NSString stringWithFormat:@"%@",userInfo.firstName];
    }
    if (userInfo.age.length>0) {
        usersName= [usersName stringByAppendingString:[NSString stringWithFormat:@", %@",userInfo.age]];
    }
    
    
    self.userName.text=[usersName uppercaseString];
    imageViewLeft.hidden=NO;
    imageViewRight.hidden=NO;
}
#pragma mark  - Update User Name
-(void)updateUserName:(NSInteger)index
{
    
    UIImageView *imageViewLeft=(UIImageView *)[self.view viewWithTag:901];
    UIImageView *imageViewRight=(UIImageView *)[self.view viewWithTag:902];
    
    if (index+1>=[requestUserArray count]) {
         _noUserFoundView.hidden=NO;
        _draggableBackground.hidden=YES;
       
       // self.userName.text=@"REQUEST (0)";
       
        return;
        
    }
    Users *userInfo=[requestUserArray objectAtIndex:index+1];
   
    NSString *usersName;
    if (userInfo.firstName.length>0 && userInfo.lastName.length>0) {
        usersName=[NSString stringWithFormat:@"%@ %@",userInfo.firstName,userInfo.lastName];
    }
    else
    {
        usersName=[NSString stringWithFormat:@"%@",userInfo.firstName];
    }
    if (userInfo.age.length>0) {
        usersName= [usersName stringByAppendingString:[NSString stringWithFormat:@", %@",userInfo.age]];
    }
    
    
    self.userName.text=[usersName uppercaseString];
    imageViewLeft.hidden=NO;
    imageViewRight.hidden=NO;
}
#pragma mark intereset, location and friend
-(NSString *)getIneterests:(NSArray *)interestArray
{
    NSString *interests=[[NSString alloc] init];;
    for (int i=0; i<[interestArray count]; i++) {
        interests= [interests stringByAppendingString:[[interestArray objectAtIndex:i] valueForKey:@"interestName"]];
        if (i<[interestArray count]-1)
            interests= [interests stringByAppendingString:@", "];
        
    }
    if (interests.length<1) {
        return  interests=@"";
    }
    return interests;
}
-(NSString *)getLocations:(NSArray *)locationsArray
{
    NSString *locations=[[NSString alloc] init];;
    for (int i=0; i<[locationsArray count]; i++) {
        locations=[locations stringByAppendingString:[[locationsArray objectAtIndex:i] valueForKey:@"destinationName"]];
        
        if (i<[locationsArray count]-1)
            locations= [locations stringByAppendingString:@", "];
    }
    if (locations.length<1) {
        return  locations=@"";
    }
    return locations;
}
-(NSString *)getContacts:(NSArray *)contactsArray
{
    NSString *contacts=[[NSString alloc] init];;
    for (int i=0; i<[contactsArray count]; i++) {
        contacts= [contacts stringByAppendingString:[[contactsArray objectAtIndex:i] valueForKey:@"contactProfileThumb"]];
        if (i<[contactsArray count]-1)
            contacts= [contacts stringByAppendingString:@","];
        
    }
    if (contacts.length<1) {
        return contacts=@"";
    }
    return contacts;
}
@end


