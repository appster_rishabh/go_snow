//
//  CreateATripViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 11/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupTripManager.h"
#import "GroupTrip.h"

@interface CreateATripViewController : UIViewController<UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate, GroupTripDelegate>
{
    UIPickerView *pickerViewSeason;
    UserInfoEntity *userInfoEntity;
    GroupTrip *groupTripObject;
}
@property(nonatomic)BOOL isEditMode;
@property(nonatomic,strong)GroupTrip *groupTripDetail;
@property(nonatomic,weak)IBOutlet UIView *viewLocation;
@property(nonatomic,weak)IBOutlet UIView *viewSeason;
@property(nonatomic,weak)IBOutlet UIView *viewDescription;
@property(nonatomic,weak)IBOutlet UILabel *labelSeason;
@property(nonatomic,weak)IBOutlet UILabel *labelLocation;
@property(nonatomic,weak)IBOutlet UILabel *headingLabel;
@property(nonatomic,weak)IBOutlet UIButton *doneButton;

-(IBAction)backButtonAction;
-(IBAction)chooseLocationAction:(id)sender;
-(IBAction)selectSeasonAction:(id)sender;
-(IBAction)createEditATripAction:(id)sender;

@end
