//
//  GroupTripRequestViewController.h
//  GoSnow
//
//  Created by Rishabh on 30/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupTrip.h"
#import "GroupTripManager.h"
#import "DraggableViewBackground.h"
#import "Users.h"
@interface GroupTripRequestViewController : UIViewController<GroupTripDelegate,GoSnowSwipeDelegate>
{
    BOOL isRidersVisible;
    NSMutableArray *goingRidersArray;
    NSMutableArray *requestUserArray;
    NSInteger currentIndex;
}
@property(nonatomic,weak)IBOutlet DraggableViewBackground *draggableBackground;
@property(nonatomic,assign)GroupTrip *groupTripDetails;
@property(nonatomic,weak)IBOutlet UITableView *ridersListTableView;
@property(nonatomic,weak)IBOutlet UIView *noUserFoundView;
@property(nonatomic,weak)IBOutlet UILabel *labelDescription;
@property(nonatomic,weak)IBOutlet UILabel *labelSeason;
@property(nonatomic,weak)IBOutlet UILabel *userName;
@property(nonatomic,weak)IBOutlet UILabel *labelDestination;
@property(nonatomic,weak)IBOutlet UIButton *topRightButton;
@property(nonatomic,weak)IBOutlet UIButton *declineButton;
@property(nonatomic,weak)IBOutlet UIButton *acceptButton;
-(IBAction)doneButtonAction;
@end
