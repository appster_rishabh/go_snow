//
//  TripInformationViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 25/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupTripManager.h"

@interface TripInformationViewController : UIViewController<GroupTripDelegate>
{
    BOOL isRidersVisible;
    NSMutableArray *goingRidersArray;
}

@property(nonatomic,assign)GroupTrip *groupTripDetails;
@property(nonatomic,weak)IBOutlet UITableView *ridersListTableView;
@property(nonatomic,weak)IBOutlet UILabel *labelDescription;
@property(nonatomic,weak)IBOutlet UILabel *labelSeason;
@property(nonatomic,weak)IBOutlet UIButton *topRightButton;
@property(nonatomic,weak)IBOutlet UILabel *labelDestination;

-(IBAction)doneButtonAction;
-(IBAction)editTripButtonAction;
-(IBAction)displayAddedRiderListAction;

@end
