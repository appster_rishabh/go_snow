//
//  AddSnowBuddiesViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 24/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupTrip.h"
#import "GroupTripManager.h"

@interface AddSnowBuddiesViewController : UIViewController<GroupTripDelegate>
{
    UserInfoEntity *userInfoEntity;
    NSMutableArray *snowBuddiesArray, *invitedUserIdArray;
}
@property(nonatomic,strong)GroupTrip *groupDetails;
@property(nonatomic,weak)IBOutlet UITableView *tableSnowBuddies;

-(IBAction)backButtonAction;
-(IBAction)inviteSnowBuddiesAction:(id)sender;
-(IBAction)doneAction:(id)sender;
@end

