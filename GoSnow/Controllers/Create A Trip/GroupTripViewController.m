//
//  GroupTripViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 20/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "GroupTripViewController.h"
#import "GroupTripCell.h"
#import "CreateATripViewController.h"
#import "GroupTripDetailViewController.h"
#import "TripInformationViewController.h"
#import "GroupTripRequestViewController.h"

@interface GroupTripViewController ()

@end

@implementation GroupTripViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
    
    _tableViewGroups.hidden = YES;
    groupsArray = [[NSMutableArray alloc]init];
    [self updateMenuIcon];
    userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}

#pragma mark viewWillAppear method
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self fetchCreatedTrip];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenuIconByNotification) name:KNotificationRecieved object:nil];
}
#pragma mark viewWillDisappear method
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KNotificationRecieved object:nil];
}
#pragma mark updateMenuIconByNotification method
-(void)updateMenuIconByNotification
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    [menuIcon setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
}
#pragma mark updateMenuIcon method
-(void)updateMenuIcon
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    int notificationCount=[UserInfoEntity fetchPendingRequestsAndMessagesCountsinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    if (notificationCount>0)
        [menuIcon setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
    else
        [menuIcon setImage:[UIImage imageNamed:@"menuSetting"] forState:UIControlStateNormal];
}

#pragma mark - IBActions
-(IBAction)createGroupAction
{
    CreateATripViewController *createATripViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"CreateATripViewController"];
    [self.navigationController pushViewController:createATripViewController animated:YES];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float cellHeight;
    id cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];

    //GroupTripCell *cell=(GroupTripCell *)[tableView cellForRowAtIndexPath:indexPath];
    UILabel *labelDescription =(UILabel *)[cell viewWithTag:62];
    GroupTrip *groupTrip = [groupsArray objectAtIndex:indexPath.row];
    CGSize rowHeight =[self getSizeForText:groupTrip.tripDescription maxWidth:labelDescription.frame.size.width font:[UIFont fontWithName:@"RobotoCondensed-Regular" size:17.0f]];
    cellHeight = rowHeight.height;
    if (rowHeight.height<23) {
        return 90;
    }
    return cellHeight+60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [groupsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    static NSString *CellIdentifier = @"GroupTripCell";
    GroupTripCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GroupTripCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.delegate=self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    GroupTrip *groupTripObject=[groupsArray objectAtIndex:indexPath.row];
    cell.rightUtilityButtons=[self rightButtons:groupTripObject];
    [cell setLayout:groupTripObject index:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Set background color of cell here if you don't want default white
}


#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroupTrip *groupTripDetails=[groupsArray objectAtIndex:indexPath.row];
    
    if ([groupTripDetails.ownerId isEqualToString:userInfoEntity.user_id]) {
        [self displayGroupTrip:indexPath];
    }
    else
    {
        [self displayGroupTrip:indexPath];
    }
    
}

#pragma mark -  Response Succesful
-(void)fetchGroupDetails:(NSMutableArray *)arrayTrips
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [groupsArray removeAllObjects];
    [groupsArray addObjectsFromArray:arrayTrips];
    
    [self hideDisplayElements:arrayTrips];
    
}

-(void)hideDisplayElements:(NSMutableArray *)arrayUsed
{
    UIImageView *imageView = (UIImageView *)[self.view viewWithTag:301];
    
    if ([arrayUsed count]==0) {
        _viewNoGroupFound.hidden=NO;
        _labelUnreadGroup.hidden=YES;
        _bottomSeperatorImage.hidden=YES;
       // _labelTitle.text=@"GROUP TRIP (0)";
        _labelNoGroupFound.text=@"No Groups Available";
        imageView.hidden = YES;
        [_tableViewGroups reloadData];
        //return;
    }
    else{
        imageView.hidden = YES;
        _viewNoGroupFound.hidden=YES;
        _labelUnreadGroup.hidden=NO;
        _bottomSeperatorImage.hidden=YES;
        _tableViewGroups.hidden = NO;
        [_tableViewGroups reloadData];
    }
}

-(void)snowBuddyLeftGroup
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self hideDisplayElements:groupsArray];
}

#pragma mark - Response Failed
-(void)failedWithError:(NSDictionary*)errorDictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self hideDisplayElements:groupsArray];
    
}

#pragma mark - Add Swipe Options
- (NSArray *)rightButtons:(GroupTrip *)groupTripDetail
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    if ([groupTripDetail.ownerId isEqualToString:userInfoEntity.user_id]) {
        if([groupTripDetail.tripUsersDetails count]>0)
            [rightUtilityButtons sw_addUtilityButtonWithColor:
             [UIColor colorWithRed:0.512f green:0.918f blue:0.945f alpha:1.0] title:@"INVITE"];
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0] title:@"EDIT"];
    }
    
    
    if ([groupTripDetail.tripUsersDetails count]>1)
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] title:@"LEAVE"];
    
    else if([groupTripDetail.tripUsersDetails count] == 1)
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] title:@"DELETE"];
    
    
    
    return rightUtilityButtons;
}

#pragma mark - SWTableView Delegate Methods
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *cellIndexPath = [_tableViewGroups indexPathForCell:cell];
    GroupTrip *groupTripDetail = [groupsArray objectAtIndex:cellIndexPath.row];
    
    switch (index)
    {
        case 0:
        {
            if ([groupTripDetail.ownerId isEqualToString:userInfoEntity.user_id])
            {
                if([groupTripDetail.tripUsersDetails count]>0)
                {
                    [cell hideUtilityButtonsAnimated:YES];
                    
                    [self addSnowBuddies:groupTripDetail];
                }
            }
            else{
                [cell hideUtilityButtonsAnimated:YES];
                [self deleteCellFromIndexPath:cellIndexPath];
            }
            
        }
            break;
        case 1:
        {
            if ([groupTripDetail.ownerId isEqualToString:userInfoEntity.user_id])
            {
                if([groupTripDetail.tripUsersDetails count]>0)
                {
                    [cell hideUtilityButtonsAnimated:YES];
                    [self editGroupTrip:cellIndexPath];
                }
            }
            else
            {
                [cell hideUtilityButtonsAnimated:YES];
                [self deleteCellFromIndexPath:cellIndexPath];
            }
            
            break;
        }
        case 2:
        {
            [cell hideUtilityButtonsAnimated:YES];
            [self deleteCellFromIndexPath:cellIndexPath];
        }
            break;
        default:
            break;
    }
}
-(void)addSnowBuddies:(GroupTrip *)tripDetail
{
    GroupTripDetailViewController *groupTripDetailViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"GroupTripDetailViewController"];
    groupTripDetailViewController.groupDetails=tripDetail;
    [self.navigationController pushViewController:groupTripDetailViewController animated:YES];
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    
    return YES;
}
- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            //            if (isEditAll) {
            //                return NO;
            //            }
            
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}

-(void)showLeftUtilityButtonsAnimated:(BOOL)animated
{
    
}

-(void)deleteCellFromIndexPath:(NSIndexPath *)indexPath
{
    [_tableViewGroups beginUpdates];
    GroupTrip *groupTrip=[groupsArray objectAtIndex:indexPath.row];
    BOOL isread=groupTrip.isPending;
    if (!isread) {
        [self updateRequestCount];
    }
    [self deleteGroupWebservice:groupTrip];
    NSArray *deletedIndexPaths = [[NSArray alloc] initWithObjects:
                                  [NSIndexPath indexPathForRow:indexPath.row inSection:0],
                                  nil];
    [_tableViewGroups deleteRowsAtIndexPaths:deletedIndexPaths withRowAnimation:UITableViewRowAnimationFade];
    [groupsArray removeObjectAtIndex:indexPath.row];
    [_tableViewGroups endUpdates];
}

#pragma mark update messagesCount
-(void)updateRequestCount
{
    [UserInfoEntity updateRequestAndmessagesCounts:@"" messagesCounts:@"" tripRequestCounts:@"1"  inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    [self updateMenuIcon];
}

#pragma mark - Edit/Delete Trip
-(void)fetchCreatedTrip
{
    UIImageView *imageView = (UIImageView *)[self.view viewWithTag:301];
    imageView.hidden = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:sessionId forKey:@"sessionId"];
    [params setObject:@"1" forKey:@"includeOwnerDetails"];
    [params setObject:@"1" forKey:@"includeDestinationDetails"];
    
    [[GroupTripManager sharedInstance] handleDataForGroupTrip:params method:@"trip/getByUserId"];
    [[GroupTripManager sharedInstance]setDelegate:self];
}

-(void)deleteGroup:(NSString *)groupIdValue
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Are you sure you want to leave the group?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    alert.accessibilityValue = groupIdValue;
    [alert show];
}

-(void)editGroupTrip:(NSIndexPath *)indexPath
{
    TripInformationViewController *tripInformationViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"TripInformationViewController"];
    tripInformationViewController.groupTripDetails= [groupsArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:tripInformationViewController animated:YES];
}
-(void)displayGroupTrip:(NSIndexPath *)indexPath
{
    GroupTripRequestViewController *tripInformationViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"GroupTripRequestViewController"];
    tripInformationViewController.groupTripDetails= [groupsArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:tripInformationViewController animated:YES];
}
#pragma mark delete group
-(void)deleteGroupWebservice:(GroupTrip *)groupTrip
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:sessionId forKey:@"sessionId"];
    [params setObject:groupTrip.groupId forKey:@"tripId"];
    [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"tripUser/leave"];
    [[GroupTripManager sharedInstance]setDelegate:self];
}
#pragma mark - AlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
        NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
        
        NSMutableDictionary *params=[NSMutableDictionary dictionary];
        [params setObject:sessionId forKey:@"sessionId"];
        [params setObject:alertView.accessibilityValue forKey:@"tripId"];
        [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"tripUser/leave"];
        [[GroupTripManager sharedInstance]setDelegate:self];
    }
}
#pragma mark - Get Text Size
- (CGSize)getSizeForText:(NSString *)text maxWidth:(CGFloat)width font:(UIFont *)font  {
    NSArray *arr=[NSArray arrayWithObject:text];
    CGSize DXsize = [[arr objectAtIndex:0] sizeWithFont:font constrainedToSize:CGSizeMake(width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    return DXsize;
}
@end
