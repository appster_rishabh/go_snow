//
//  TripInformationViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 25/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "TripInformationViewController.h"
#import "GroupTripViewController.h"
#import "CreateATripViewController.h"
#import "TripUserGoingCell.h"
#import "UIImage+ImageShape.h"
@interface TripInformationViewController ()

@end

@implementation TripInformationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    isRidersVisible = NO;
    
    [self setUpInitials];
}

#pragma mark - Set Up Initial Information
-(void)setUpInitials
{
    goingRidersArray=[[NSMutableArray alloc] init];
    [goingRidersArray addObjectsFromArray:_groupTripDetails.tripUsersDetails];
    //if ([goingRidersArray count]>0) {
       // [_topRightButton setImage:[UIImage drawText:[NSString stringWithFormat:@"%d",[goingRidersArray count]] inImage:[UIImage imageNamed:@"more_blue@2x"] atPoint:CGPointMake(0, 0)] forState:UIControlStateNormal];
   // [_topRightButton setImage:[UIImage addText:[UIImage imageNamed:@"more_blue"] text:@"5"] forState:UIControlStateNormal];
    [_topRightButton setImage:[UIImage CreateAndReturnImagesCircle:@"#2AB73C" rect:_topRightButton.frame  text:[NSString stringWithFormat:@"%lu",(unsigned long)[goingRidersArray count]]] forState:UIControlStateNormal];
   // }
    if (_groupTripDetails.tripDescription.length>0)
        _labelDescription.text = _groupTripDetails.tripDescription;
    
    if (_groupTripDetails.destinationName.length>0)
        _labelDestination.text = [_groupTripDetails.destinationName uppercaseString];
    
    if (_groupTripDetails.season.length>0)
        _labelSeason.text = _groupTripDetails.season;
}

#pragma mark - IBActions
-(IBAction)doneButtonAction
{
    for (UIViewController *view in self.navigationController.childViewControllers) {
        if ([view isKindOfClass:[GroupTripViewController class]]) {
            [self.navigationController popToViewController:view animated:YES];
            return;
        }
    }
}

-(IBAction)editTripButtonAction
{
    CreateATripViewController *createATripViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"CreateATripViewController"];
    [createATripViewController setGroupTripDetail:_groupTripDetails];
    [createATripViewController setIsEditMode:YES];
    [self.navigationController pushViewController:createATripViewController animated:YES];
}

-(IBAction)displayAddedRiderListAction
{
    if(!isRidersVisible)
        [self openAnimationForRidersView];
    else
        [self closeAnimationForRidersView];
}

#pragma mark - Maintain Animations Of Views
-(void)openAnimationForRidersView
{
    UIView *headerView = [self.view viewWithTag:101];
    isRidersVisible = YES;
    CGRect frame = _ridersListTableView.frame;
    _ridersListTableView.frame = CGRectMake(0,
                                            headerView.frame.size.height,
                                            _ridersListTableView.frame.size.width,
                                            0);
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _ridersListTableView.hidden = NO;
                         _ridersListTableView.frame = frame;
                        [_topRightButton setImage:[UIImage CreateAndReturnImagesCircle:@"#2AB73C" rect:_topRightButton.frame  text:@"x"] forState:UIControlStateNormal];
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}

-(void)closeAnimationForRidersView
{
    isRidersVisible = NO;
    CGRect frame = _ridersListTableView.frame;
    CGRect tmpFrame = _ridersListTableView.frame;
    tmpFrame.size.height = 0;
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _ridersListTableView.frame = tmpFrame;
                     }
                     completion:^(BOOL finished){
                         _ridersListTableView.hidden = YES;
                         _ridersListTableView.frame = frame;
                         [_topRightButton setImage:[UIImage CreateAndReturnImagesCircle:@"#2AB73C" rect:_topRightButton.frame  text:[NSString stringWithFormat:@"%lu",(unsigned long)[goingRidersArray count]]] forState:UIControlStateNormal];
                     }];
    
}

#pragma mark UITableView Datasource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return [goingRidersArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    // [tableView setSeparatorColor:[UIColor clearColor]];
    static NSString *CellIdentifier = @"TripUserGoingCell";
    TripUserGoingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TripUserGoingCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    
    [cell setlayout:[goingRidersArray objectAtIndex:indexPath.row] ownerId:_groupTripDetails.ownerId];
    
    return cell;
}

@end
