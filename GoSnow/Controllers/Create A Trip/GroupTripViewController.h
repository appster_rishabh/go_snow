//
//  GroupTripViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 20/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupTripManager.h"
#import "SWTableViewCell.h"

@interface GroupTripViewController : UIViewController<GroupTripDelegate,SWTableViewCellDelegate>
{
    UserInfoEntity *userInfoEntity;
    NSMutableArray *groupsArray;
}
@property(nonatomic,weak)IBOutlet UITableView *tableViewGroups;
@property(nonatomic,weak)IBOutlet UILabel *labelTitle;
@property(nonatomic,weak)IBOutlet UILabel *labelNoGroupFound;
@property(nonatomic,weak)IBOutlet UILabel *bottomSeperatorImage;
@property(nonatomic,weak)IBOutlet UILabel *labelUnreadGroup;

@property(nonatomic,weak)IBOutlet UIView *viewNoGroupFound;

-(IBAction)createGroupAction;
@end


