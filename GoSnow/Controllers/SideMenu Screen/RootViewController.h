//
//  RootViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 19/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "RESideMenu.h"

@interface RootViewController : RESideMenu<RESideMenuDelegate>

@end
