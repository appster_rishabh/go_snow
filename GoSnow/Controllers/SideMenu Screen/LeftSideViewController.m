//
//  LeftSideViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 18/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "LeftSideViewController.h"
#import "MessagesViewController.h"
#import "LoginViewController.h"
#import "GroupTripViewController.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "ViewUserProfileViewController.h"
#import "HomeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Color.h"
@interface LeftSideViewController ()

@end

@implementation LeftSideViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpInitials) name:kSideMenuLoaded object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpInitials) name:KNotificationRecieved object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openViewcontrolerFromNotification:) name:KNotificationFromNoticationCentre object:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    //[self setUpInitials];
}

#pragma mark - Set Up Initial Information
-(void)setUpInitials
{
    [self.navigationController.navigationBar setHidden:YES];
    
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-menu.jpg"]];
    
    [self fetchUserInformationFromCoreData];
    [self.tableViewMenu reloadData];
    self.imageViewProfilePic.layer.cornerRadius = 45.0f;
    self.imageViewProfilePic.layer.masksToBounds=YES;
    if (userInfoEntity.profileImage.length>0) {
        [self.imageViewProfilePic setImageWithURL:[NSURL URLWithString:userInfoEntity.profileImage] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        if ([userInfoEntity.gender isEqualToString:@"female"])
            [self.imageViewProfilePic setImage:[UIImage imageNamed:@"menu_v2_avatar_female.png"]];
        else
            [self.imageViewProfilePic setImage:[UIImage imageNamed:@"menu_v2_avatar_male.png"]];
    }
    if(userInfoEntity.riderTypeName.length>0)
      self.labelRiderType.text=[userInfoEntity.riderTypeName uppercaseString];
    
    NSString *stringRiderName;
    if(userInfoEntity.firstName.length>0)
        stringRiderName = [NSString stringWithFormat:@"%@ ",userInfoEntity.firstName];
    if(userInfoEntity.lastName.length>0)
        stringRiderName = [stringRiderName stringByAppendingString:userInfoEntity.lastName];
    self.labelUserName.text=stringRiderName;
}

#pragma mark - Fetch Profile Information From CoreDate
-(void)fetchUserInformationFromCoreData
{
    userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    if (userInfoEntity==nil) {
        return;
    }
}

#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSArray *tilesArray = @[@"DISCOVER", @"MESSAGES", @"SNOW BUDDIES", @"NOTIFICATIONS", @"GROUP TRIPS"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UILabel *titleLabel  =[[UILabel alloc]initWithFrame:CGRectMake(0, 5, 160,  cell.frame.size.height)];
        titleLabel.font = [UIFont fontWithName:@"PassionOne-Bold" size:15.0f];
        titleLabel.tag=201;
        titleLabel.textColor = [UIColor colorwithHexString:@"#FAFBFD" alpha:1.0];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        UIButton *notificationButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [notificationButton setBackgroundImage:[UIImage imageNamed:@"notification_bg"] forState:UIControlStateNormal];
        //[notificationButton setBackgroundColor:[UIColor redColor]];
        [notificationButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        notificationButton.titleLabel.font=[UIFont fontWithName:@"RobotoCondensed-Bold" size:9.0f];
        
        notificationButton.tag=202;
        [cell.contentView addSubview:notificationButton];
        [cell.contentView addSubview:titleLabel];
    }
    UILabel *titleLabel=(UILabel *)[cell.contentView viewWithTag:201];
    UIButton *notificationButton=(UIButton *)[cell.contentView viewWithTag:202];
    titleLabel.text = tilesArray[indexPath.row];
    notificationButton.hidden=NO;
    if (indexPath.row==1) {
        
        notificationButton.frame=CGRectMake(114, 13, 13, 13);

        int messageCounts=[userInfoEntity.messagesCounts intValue];
        if (messageCounts<1) {

            notificationButton.hidden=YES;
            return cell;
        }
        [notificationButton setTitle:userInfoEntity.messagesCounts forState:UIControlStateNormal];
    }
    else if (indexPath.row==3)
    {
        int requestCounts=[userInfoEntity.requestCounts intValue];
        if (requestCounts<1) {
            notificationButton.hidden=YES;
            return cell;
        }
        [notificationButton setTitle:userInfoEntity.requestCounts forState:UIControlStateNormal];
        
        notificationButton.frame=CGRectMake(129, 13, 13, 13);
        //notificationButton.layer.cornerRadius=notificationButton.frame.size.width/2;
    }
    else if (indexPath.row==4)
    {
         notificationButton.frame=CGRectMake(122, 13, 13, 13);
        int requestCounts=[userInfoEntity.tripRequestCount intValue];
        if (requestCounts<1) {
            notificationButton.hidden=YES;
            return cell;
        }
        [notificationButton setTitle:userInfoEntity.tripRequestCount forState:UIControlStateNormal];
        
    }
    else
    {
        notificationButton.hidden=YES;
    }
    return cell;
}

#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row=indexPath.row;
    switch (row) {
        case 0:
        {
            HomeViewController *homeViewController=nil;
            if (WIDTH>320)
                homeViewController=[[HomeViewController alloc] initWithNibName:@"HomeViewController_iPhone6" bundle:nil];
            
            else
                homeViewController=[[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
            
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:homeViewController]animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            
        }
            break;
        case 1:
        {
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MessagesViewController"]]animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            
        }
            break;
        case 2:
        {
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ContactSnowBuddiesViewController"]]animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            
        }
            break;
        case 3:
        {
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"RequestViewController"]]animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;
        case 4:
        {
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"GroupTripViewController"]]animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - IBActions

-(IBAction)visitUserProfileAction:(id)sender
{
    ViewUserProfileViewController *viewUserProfileViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"ViewUserProfileViewController"];
    [self fetchUserInformationFromCoreData];
    viewUserProfileViewController.fromScreenType=MyprofileScreenType;
    viewUserProfileViewController.otherUsersObject=[self getCurrentUserInfo];
    
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:viewUserProfileViewController]animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

-(IBAction)settingsButtonAction
{
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"]]animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

-(IBAction)logOutButtonAction
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:@"Logout"
                                                    otherButtonTitles:nil,nil];
    [actionSheet setActionSheetStyle:UIActionSheetStyleDefault];
    [actionSheet showInView:self.view];
}

#pragma mark - Get Current User Information
-(Users *)getCurrentUserInfo
{
    Users *userInfo=[[Users alloc] init];
    userInfo.firstName=userInfoEntity.firstName;
    userInfo.lastName=userInfoEntity.lastName;
    userInfo.aboutMe=userInfoEntity.aboutMe;
    userInfo.age=userInfoEntity.age;
    userInfo.gender=userInfoEntity.gender;
    userInfo.userPhotos=userInfoEntity.userPhotos;
    userInfo.skillLevel=userInfoEntity.skillLevelName;
    userInfo.riderType=userInfoEntity.riderTypeName;
    userInfo.skillLevelID=userInfoEntity.skillLevelId;
    userInfo.plannedTripName=userInfoEntity.nextDestinationName;
    userInfo.contactsName=userInfoEntity.facebookFriends;
    userInfo.locationName=userInfoEntity.locationName;
    userInfo.interestName=userInfoEntity.interestName;
    userInfo.profileImage=userInfoEntity.profileImage;
    userInfo.latitude=userInfoEntity.latitude;
    userInfo.longitude=userInfoEntity.longitude;
    userInfo.address=userInfoEntity.address;
    userInfo.interestId=userInfoEntity.interestId;

    return userInfo;
}

#pragma mark  - Action Sheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==0) {
        [self logoutWebservice];
    }
}

#pragma mark  -Logout Webservice Method
-(void)logoutWebservice
{
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"]]animated:YES];
    [self.sideMenuViewController hideMenuViewController];
    
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [UserProfile sharedInstance].delegate=self;
    [[UserProfile sharedInstance] logoutUser:params method:@"user/logout"];
    [UserInfoEntity deleteUserprofileinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    
}
-(void)openViewcontrolerFromNotification:(id)notification
{
    UIStoryboard  *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NSString *viedId=[notification object];
    if ([viedId isEqualToString:@"1"]) {
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[storyBoard instantiateViewControllerWithIdentifier:@"RequestViewController"]]animated:YES];
        [self.sideMenuViewController hideMenuViewController];
        
    }
    else if ([viedId isEqualToString:@"9"])
    {
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[storyBoard instantiateViewControllerWithIdentifier:@"MessagesViewController"]]animated:YES];
        [self.sideMenuViewController hideMenuViewController];
    }
    else if ([viedId isEqualToString:@"4"])
    {
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[storyBoard instantiateViewControllerWithIdentifier:@"GroupTripViewController"]]animated:YES];
        [self.sideMenuViewController hideMenuViewController];
    }
    
}
#pragma mark - user logout succesfully
-(void)userLogoutSuccessfully
{
    
}
-(void)failedWithError:(NSDictionary*)errorDictionary
{
    // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[errorDictionary valueForKey:@"Error"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //[alert show];
}

@end
