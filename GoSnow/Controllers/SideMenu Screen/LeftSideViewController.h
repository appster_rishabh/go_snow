//
//  LeftSideViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 18/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "UserProfile.h"
@interface LeftSideViewController : UIViewController<RESideMenuDelegate,UIActionSheetDelegate,ProfileUpdateDelegate>
{
    UserInfoEntity *userInfoEntity;
}
@property(nonatomic,weak)IBOutlet UILabel *labelUserName;
@property(nonatomic,weak)IBOutlet UILabel *labelRiderType;
@property (nonatomic,weak) IBOutlet UITableView *tableViewMenu;
@property (nonatomic,weak) IBOutlet UIImageView *imageViewProfilePic;

-(IBAction)visitUserProfileAction:(id)sender;
-(IBAction)logOutButtonAction;
-(IBAction)settingsButtonAction;

@end
