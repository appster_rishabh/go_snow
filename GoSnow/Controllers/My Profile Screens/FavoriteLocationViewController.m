
//
//  MyProfilePlannedTripVC.m
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "FavoriteLocationViewController.h"
#import "DestinationManager.h"
#import "SignUpStepsViewController.h"
#import "MyProfileDetailViewController.h"
#import "DestinationCell.h"
#import "CreateATripViewController.h"

@interface FavoriteLocationViewController ()

@end

@implementation FavoriteLocationViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
    
    // Do any additional setup after loading the view.
    favLocationArray = [[NSMutableArray alloc]init];
    [self handleDestinationManager];
    
    UILabel *labelTitle = (UILabel *)[self.view viewWithTag:200];
    labelTitle.text = @"CHOOSE LOCATION";
    
    
}

#pragma Handle Destination Manager
-(void)handleDestinationManager
{
    [self showLoader];
    [[DestinationManager sharedInstance]getDestinationsFromWebService];
    [DestinationManager sharedInstance].delegate = self;;
}

#pragma mark -
#pragma mark IBActions

- (IBAction)doneButtonAction
{
    NSString *locationsName=[[NSString alloc] init];
     NSString *locationsId=[[NSString alloc] init];
    for (int i=0; i<[favLocationArray count]; i++)
    {
        Destinations *destinationObject=[favLocationArray objectAtIndex:i];
        BOOL isSeleted =destinationObject.isSelect;
        if (isSeleted)
        {
            locationsName=[locationsName stringByAppendingString:destinationObject.destinationName];
            locationsName=[locationsName stringByAppendingString:@","];
            
            locationsId=[locationsId stringByAppendingString:destinationObject.destinationId];
            locationsId=[locationsId stringByAppendingString:@","];
          
        }
        }
    if (locationsName.length>0) {
        _userInfo.locationName=[self removeLastCharacter:locationsName ];
        _userInfo.locationId=[self removeLastCharacter:locationsId];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(NSString *)removeLastCharacter:(NSString *)stringText
{
    NSString *lastCharacter=[stringText substringFromIndex:[stringText length]-1];
    if ([lastCharacter isEqualToString:@","]) {
        stringText=[stringText substringToIndex:[stringText length]-1];
    }
    return stringText;
}
- (IBAction)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Network Manager Protocol
-(void)showLoader
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}
-(void)hideLoader
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)fetchDestinationSuccessfully:(NSMutableArray *)arrayDestinations
{
    [favLocationArray removeAllObjects];
    [favLocationArray addObjectsFromArray:arrayDestinations];
    if ([favLocationArray count]>0) {
        // _tableViewDestination.hidden = NO;
        [self checkSelectedLocation];
       // [_tableViewFavLocationTrip reloadData];
    }
}

-(void)failedWithError:(NSDictionary*)dictionaryError
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[dictionaryError valueForKey:@"Error"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
#pragma mark update selected location to selected icon
-(void)checkSelectedLocation
{
    NSArray *checkedLocations=[_userInfo.locationName componentsSeparatedByString:@","];
    for (int i=0; i<[favLocationArray count]; i++) {
         Destinations *destinations=[favLocationArray objectAtIndex:i];
        if ([checkedLocations containsObject:destinations.destinationName]) {
            destinations.isSelect=YES;
        }
    }
    [_tableViewFavLocationTrip reloadData];
}
#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [favLocationArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    // [tableView setSeparatorColor:[UIColor clearColor]];
    static NSString *CellIdentifier = @"DestinationCell";
    DestinationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DestinationCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.sender=self;
    }
    
    Destinations *destonationObject=[favLocationArray objectAtIndex:indexPath.row];
    [cell SetLayout:destonationObject.destinationName isSelect:destonationObject.isSelect index:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self selectFavoriteLocation:indexPath];
}

#pragma mark -
#pragma mark Check Uncheck Selected Row Method
-(void)selectFavoriteLocation:(NSIndexPath *)indexPath
{
    Destinations *destonationObject=[favLocationArray objectAtIndex:indexPath.row];
    
    BOOL isSeleted=destonationObject.isSelect;
    if (isSeleted){
        _labelPlannedTrip.text =@"Not Chosen Yet";
        destonationObject.isSelect=NO;
        [_tableViewFavLocationTrip reloadData];
        return;
    }
    if ([self isAlready3Selected]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"You can select up to 3 locations." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if (isSeleted){
        _labelPlannedTrip.text =@"Not Chosen Yet";
        destonationObject.isSelect=NO;
    }
    else{
        _labelPlannedTrip.text =destonationObject.destinationName;
        destonationObject.isSelect=YES;
    }
    
    [favLocationArray replaceObjectAtIndex:indexPath.row withObject:destonationObject];
    [_tableViewFavLocationTrip reloadData];
}
-(BOOL)isAlready3Selected
{
    int count=0;
    for (int i=0; i<[favLocationArray count]; i++)
    {
        Destinations *destinationObject=[favLocationArray objectAtIndex:i];
        BOOL isSeleted =destinationObject.isSelect;
        if (isSeleted)
        {
            count++;
        }
    }
    if (count>=3) {
        return YES;
    }
        return NO;
}
@end

