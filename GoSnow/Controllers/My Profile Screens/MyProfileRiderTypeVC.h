//
//  MyProfileRiderTypeVC.h
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserProfile.h"
#import "Users.h"

@interface MyProfileRiderTypeVC : UIViewController
{
    UserProfile *userProfile;
    NSMutableArray *riderTypeArray;
}
@property(nonatomic,assign)Users *userInfo;

@property (weak, nonatomic)IBOutlet UITableView *tableViewRiderType;
//code review no need to give strong property and alloc both if we are not accesing this access array outside to 
- (IBAction)doneButtonAction;
- (IBAction)backButtonAction;
- (void)selectValue:(NSIndexPath *)indexPath;

@end
