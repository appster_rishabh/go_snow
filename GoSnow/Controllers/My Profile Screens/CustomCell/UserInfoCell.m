//
//  UserInfoCell.m
//  GoSnow
//
//  Created by Rishabh on 01/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "UserInfoCell.h"

@implementation UserInfoCell

- (void)awakeFromNib {
    // Initialization code
}
#pragma mark -
#pragma mark Set Layout for Cell

-(void)SetLayout:(NSMutableDictionary *)dict index:(NSIndexPath *)path
{
    UILabel *textLabel=(UILabel *)[self viewWithTag:301];
    UILabel *infoLabel=(UILabel *)[self viewWithTag:302];
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *stringRowValue=[dict valueForKey:@"value"];
    
    textLabel.text=stringRowValue;
    infoLabel.text=stringRowValue;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
