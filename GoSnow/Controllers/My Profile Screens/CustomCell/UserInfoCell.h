//
//  UserInfoCell.h
//  GoSnow
//
//  Created by Rishabh on 01/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoCell : UITableViewCell
-(void)SetLayout:(NSMutableDictionary *)dict index:(NSIndexPath *)path;
@end
