//
//  ProfileCell.h
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelProfileCell;
@property (weak, nonatomic) IBOutlet UIButton *checkUncheck;
@property id sender;

-(void)SetLayout:(NSMutableDictionary *)dict index:(NSIndexPath *)path;
-(IBAction)selectedValuesAction:(id)sender;

@end
