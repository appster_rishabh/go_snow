//
//  ProfileCell.m
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ProfileCell.h"
#import "MyProfilePlannedTripVC.h"
#import "MyProfileRiderTypeVC.h"
#import "SignUpInterestsViewController.h"

@implementation ProfileCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark -
#pragma mark Set Layout for Cell

-(void)SetLayout:(NSMutableDictionary *)dict index:(NSIndexPath *)path
{
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *stringRowValue=[dict valueForKey:@"value"];
    
    _labelProfileCell.text=stringRowValue;
    _checkUncheck.tag=path.row;
    BOOL isSeleted=[[dict valueForKey:@"isSelect"] boolValue];
    if (isSeleted)
        [_checkUncheck setImage:[UIImage imageNamed:@"tick_icon.png"] forState:UIControlStateNormal];
    
    else
        [_checkUncheck setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark IBAction Check Uncheck

-(IBAction)selectedValuesAction:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    [self.sender selectValue:[NSIndexPath indexPathForRow:btn.tag inSection:0]];
}

@end
