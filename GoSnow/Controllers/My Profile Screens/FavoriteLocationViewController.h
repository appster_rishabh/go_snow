//
//  FavoriteLocationViewController.h
//  GoSnow
//
//  Created by Rishabh on 24/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "DestinationManager.h"
#import "Users.h"
#import "GroupTrip.h"

@interface FavoriteLocationViewController : UIViewController<DestinationDelegate>
{
    NSMutableArray *favLocationArray;
}
@property(nonatomic,assign)Users *userInfo;

@property (weak, nonatomic) IBOutlet UITableView *tableViewFavLocationTrip;
@property (weak, nonatomic) IBOutlet UILabel *labelPlannedTrip;


- (IBAction)doneButtonAction;
- (IBAction)backButtonAction;
- (void)selectFavoriteLocation:(NSIndexPath *)indexPath;

@end
