//
//  DestinationCell.m
//  GoSnow
//
//  Created by Varsha Singh on 03/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "DestinationCell.h"
#import "MyProfilePlannedTripVC.h"

@implementation DestinationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark -
#pragma mark Set Layout for Cell

-(void)SetLayout:(NSString *)destinationName isSelect:(BOOL)isSelect index:(NSIndexPath *)path
{
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
   // NSString *stringRowValue=[dictDestination valueForKey:@"value"];
    
    _labelDestination.text=destinationName;
    _checkUncheck.tag=path.row;
    BOOL isSeleted=isSelect;
    if (isSeleted)
        [_checkUncheck setImage:[UIImage imageNamed:@"tick_icon.png"] forState:UIControlStateNormal];
    
    else
        [_checkUncheck setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
}


@end
