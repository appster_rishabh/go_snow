//
//  DestinationCell.h
//  GoSnow
//
//  Created by Varsha Singh on 03/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DestinationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelDestination;
@property (weak, nonatomic) IBOutlet UIButton *checkUncheck;
@property id sender;

-(void)SetLayout:(NSString *)destinationName isSelect:(BOOL)isSelect index:(NSIndexPath *)path;

@end
