//
//  MyProfileCurrentLocationVC.h
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Users.h"
@interface MyProfileCurrentLocationVC : UIViewController<LocationManagerDelegate,MKMapViewDelegate>
{
    Location *locationObject;
    NSString *locatedaddress;
    BOOL doneInitialZoom;
}
@property(nonatomic,assign)Users *userInfo;
@property(nonatomic,strong)CLGeocoder *geocoder;
@property(weak,nonatomic)IBOutlet MKMapView *mapView;
@property(weak,nonatomic)IBOutlet UILabel *labelCurrentLocation;

- (IBAction)doneButtonAction;
- (IBAction)backButtonAction;
- (void)getName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate;

@end
