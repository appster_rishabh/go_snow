//
//  MyProfileAboutMeVC.m
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "MyProfileAboutMeVC.h"

@interface MyProfileAboutMeVC ()

@end

@implementation MyProfileAboutMeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
    if (_userInfo.aboutMe.length>0) {
        _textViewAboutMe.text=_userInfo.aboutMe;
        _labelPleaseEnter.hidden=YES;
    }
    [_textViewAboutMe becomeFirstResponder];
}

-(void)showLabel
{
    _labelPleaseEnter.hidden=NO;
}

-(void)hideLabel
{
    _labelPleaseEnter.hidden=YES;
}

#pragma mark - Text View Delegates

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:
(NSRange)range replacementText:(NSString *)text{
    //[textView setBackgroundColor:[UIColor yellowColor]];
    if (range.location == 0 && range.length==1)
    {
        if (textView.text.length+text.length-1==0)
            [self showLabel];
        
        else
            [self hideLabel];
        
    }
    else
    {
        if (textView.text.length+text.length==0)
            [self showLabel];
        
        else
            [self hideLabel];
    }
//    if ([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//    }
    if (range.length==1) {
        return YES;
    }
    NSUInteger newLength = [textView.text length] + [text length] - range.length;
    return (newLength > 300) ? NO : YES;
    return YES;

    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    //_labelPleaseEnter.hidden = YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{

}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
    return YES;
}

#pragma mark -
#pragma mark - IBActions

- (IBAction)doneButtonAction
{
    _userInfo.aboutMe = _textViewAboutMe.text;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end