//
//  MyProfileDetailViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "MyProfileDetailViewController.h"
#import "MyProfileAboutMeVC.h"
#import "MyProfileCurrentLocationVC.h"
#import "MyProfilePlannedTripVC.h"
#import "MyProfileRiderTypeVC.h"
#import "MyProfileSkillLevelVC.h"
#import "ProfilePic.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "ViewUserProfileViewController.h"
#import "FavoriteLocationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SignUpInterestsViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface MyProfileDetailViewController ()

@end

@implementation MyProfileDetailViewController
@synthesize photosHorizontalView;
@synthesize pageControl;
#pragma mark View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialSetUp];
}

-(void)initialSetUp
{
    [self.navigationController.navigationBar setHidden:YES];
    [self fetchUserInfoFromCoreData];
    storyboardObject = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _viewSkillLevel.layer.cornerRadius = 8.0;
    
    if (HEIGHT==568)
        self.infoScrollView.contentSize=CGSizeMake(WIDTH, self.infoScrollView.frame.size.height+450);
    else
        self.infoScrollView.contentSize=CGSizeMake(WIDTH, self.infoScrollView.frame.size.height+700);
    totalPicArray=[[NSMutableArray alloc] init];
    selectedPicArray=[[NSMutableArray alloc] init];
    self.photosHorizontalView.delegate = self;
    self.photosHorizontalView.dataSource = self;
    self.photosHorizontalView.pageControl = pageControl;
    self.photosHorizontalView.minimumPageAlpha = 0.3;
    self.photosHorizontalView.minimumPageScale = 0.9;
    
    [self setImagesOnScrollview:_otherUsersObject.userPhotos];
    [self hideUnhideNoImage];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addImageToScrollView:) name:kImageUploaded object:nil];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updateDots];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setValuesToField];
}
#pragma marl fetc user info from database
-(void)fetchUserInfoFromCoreData
{
    userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}
#pragma mark - Set Older Values
-(void)setValuesToField
{
    if(_otherUsersObject.firstName.length>0)
        _firstName.text=_otherUsersObject.firstName;
    
    if(_otherUsersObject.lastName.length>0)
        _lastName.text=_otherUsersObject.lastName;
    
    if(_otherUsersObject.gender.length>0){
        _gender.text=_otherUsersObject.gender;
        if ([_gender.text isEqualToString:@"female"])
            _gender.text = @"Female";
        else
            _gender.text = @"Male";
    }
    
    if(_otherUsersObject.locationName.length>0)
        _favrouiteLocation.text=_otherUsersObject.locationName;
  
    if(_otherUsersObject.age.length>0)
        _age.text=_otherUsersObject.age;
    
    if(_otherUsersObject.interestName.length>0)
        _interest.text=_otherUsersObject.interestName;
    
    if(_otherUsersObject.aboutMe.length>0)
        _aboutMe.text=_otherUsersObject.aboutMe;
    
    if(_otherUsersObject.skillLevel.length>0){
        _skillLevel.text=_otherUsersObject.skillLevel;
        UIImageView *imageViewSkillType = (UIImageView *)[self.view viewWithTag:101];
        
        if(_otherUsersObject.skillLevelID.length>0)
            imageViewSkillType.image =[UIImage imageNamed:[NSString stringWithFormat:@"level%@.png",_otherUsersObject.skillLevelID]];
        
    }
    if(_otherUsersObject.riderType.length>0)
        _riderType.text=_otherUsersObject.riderType;
    
    if(_otherUsersObject.address.length>0)
        _currentLocation.text=_otherUsersObject.address;
    
    if(_otherUsersObject.plannedTripName.length>0)
        _plannedTrip.text=_otherUsersObject.plannedTripName;
    
}

#pragma mark Button Action
-(IBAction)pageControlValueChangeAction:(id)sender
{
    UIPageControl *pageControlValue = sender;
    [self.photosHorizontalView scrollToPage:pageControlValue.currentPage];
}

-(IBAction)backButtonAction
{
    [self updateCurrentUserInfo:userInfoEntity];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)doneButtonAction
{
    [actionSheetAge removeFromSuperview];
    [self CallUpdateProfile];
    [self UploadImageToWebservices];
}

-(IBAction)nextButtonAction:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 401:
        {
            [self updateFirstAndLastName:@"First Name" storedName:_firstName.text isFirstName:YES];
        }
            break;
        case 402:
        {
            [self updateFirstAndLastName:@"Last Name" storedName:_lastName.text isFirstName:NO];
        }
            break;
        case 403:
        {
            [self createActionSheet:@"Age"];
        }
            break;
        case 404:
        {
            [self createActionSheet:@"Gender"];
        }
            break;
        case 405:
        {
            MyProfileAboutMeVC *myProfileAboutMeVC =
            [storyboardObject instantiateViewControllerWithIdentifier:@"MyProfileAboutMeVC"];
            myProfileAboutMeVC.userInfo=_otherUsersObject;
            [self.navigationController pushViewController:myProfileAboutMeVC animated:YES];
        }
            break;
        case 406:
        {
            MyProfileRiderTypeVC *myProfileRiderTypeVC =
            [storyboardObject  instantiateViewControllerWithIdentifier:@"MyProfileRiderTypeVC"];
            myProfileRiderTypeVC.userInfo=_otherUsersObject;
            [self.navigationController pushViewController:myProfileRiderTypeVC animated:YES];
        }
            break;
        case 407:
        {
            UIButton *maskButon = [[UIButton alloc]initWithFrame:self.view.frame];
            [maskButon setTag:10001];
            [maskButon setBackgroundColor:[UIColor blackColor]];
            [maskButon setAlpha:0.7];
            [maskButon addTarget:self action:@selector(removeMaskView) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:maskButon];
            [self addPopUpView];
        }
            break;
        case 408:
        {
            MyProfilePlannedTripVC *myProfilePlannedTripVC =
            [storyboardObject  instantiateViewControllerWithIdentifier:@"MyProfilePlannedTripVC"];
            myProfilePlannedTripVC.userInfo=_otherUsersObject;
            [self.navigationController pushViewController:myProfilePlannedTripVC animated:YES];
        }
            break;
        case 409:
        {
            MyProfileCurrentLocationVC *myProfileCurrentLocationVC =
            [storyboardObject  instantiateViewControllerWithIdentifier:@"MyProfileCurrentLocationVC"];
            myProfileCurrentLocationVC.userInfo=_otherUsersObject;
            [self.navigationController pushViewController:myProfileCurrentLocationVC animated:YES];
        }
            break;
        case 410:
        {
            FavoriteLocationViewController *favoriteLocationViewController =
            [storyboardObject  instantiateViewControllerWithIdentifier:@"FavoriteLocationViewController"];
            favoriteLocationViewController.userInfo=_otherUsersObject;
            [self.navigationController pushViewController:favoriteLocationViewController animated:YES];
        }
            break;
        case 411:
        {
            SignUpInterestsViewController *signUpInterestsViewController =
            [storyboardObject instantiateViewControllerWithIdentifier:@"SignUpInterestsViewController"];
            signUpInterestsViewController.userInfo=_otherUsersObject;
            [self.navigationController pushViewController:signUpInterestsViewController animated:YES];
        }
            break;
        default:
            break;
    }
}

-(void)removeMaskView
{
    UIButton *buttonMask = (UIButton *)[self.view viewWithTag:10001];
    if (buttonMask)
        [buttonMask removeFromSuperview];
    _viewSkillLevel.hidden = YES;
    
}

-(void)addPopUpView
{
    _viewSkillLevel.hidden = NO;
    [_viewSkillLevel setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_viewSkillLevel];
}

-(IBAction)skillLevelAction:(id)sender
{
    UIButton *buttonMask = (UIButton *)[self.view viewWithTag:10001];
    if (buttonMask)
        [buttonMask removeFromSuperview];
    UIButton *buttonSkillLevel = (UIButton *)sender;
    
    if(buttonSkillLevel.tag == 21)
    {
        _otherUsersObject.skillLevelID=@"1";
        _otherUsersObject.skillLevel=@"Green";
    }
    else if(buttonSkillLevel.tag == 22)
    {
        _otherUsersObject.skillLevelID=@"2";
        _otherUsersObject.skillLevel=@"Red";
        
    }
    else if(buttonSkillLevel.tag == 23)
    {
        _otherUsersObject.skillLevelID=@"3";
        _otherUsersObject.skillLevel=@"Black";
        
    }
    else if(buttonSkillLevel.tag == 24)
    {
        _otherUsersObject.skillLevelID=@"4";
        _otherUsersObject.skillLevel=@"Double Black";
    }
    
    _skillLevel.text=_otherUsersObject.skillLevel;
    UIImageView *imageViewSkillType = (UIImageView *)[self.view viewWithTag:101];
    
    if(_otherUsersObject.skillLevelID.length>0)
    {
        imageViewSkillType.image =[UIImage imageNamed:[NSString stringWithFormat:@"level%@.png",_otherUsersObject.skillLevelID]];
    }
    _viewSkillLevel.hidden = YES;
}

#pragma mark - Take Picture
-(IBAction)takePhotoAction
{
    NSInteger totalImages=[totalPicArray count];
    
    if (totalImages>=6) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"GoSnow"
                                                            message:@"You have exceeded 6 photos limit." delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
        return;
    }
    
    UIActionSheet *actionSheetPic = [[UIActionSheet alloc] initWithTitle:nil
                                                                delegate:self
                                                       cancelButtonTitle:@"Cancel"
                                                  destructiveButtonTitle:nil
                                                       otherButtonTitles:@"Photo Library",@"Camera",nil];
    actionSheetPic.tag=501;
    [actionSheetPic setActionSheetStyle:UIActionSheetStyleDefault];
    [actionSheetPic showInView:self.view];
}
-(BOOL)checkCameraPermission
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    BOOL isAceesed;
    if(authStatus == AVAuthorizationStatusAuthorized) {
        // do your logic
        isAceesed=YES;
    } else if(authStatus == AVAuthorizationStatusDenied){
         isAceesed=NO;
        [self showAlertView:@"It looks like your privacy settings are preventing us from accessing your camera. You can fix this by doing the following:\n\n1. Go to Settings.\n\n2. Open GoSnow app.\n\n3. Turn the Camera on.\n\n4. Open this app and try again."];
        // denied
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
        [self showAlertView:@"You've been restricted from using the camera on this device. Without camera access this feature won't work"];
        isAceesed=NO;
        
    } else
    {
        isAceesed=YES;
    }
    return isAceesed;
}
#pragma mark alert view
-(void)showAlertView:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Select Picture
-(IBAction)selectPhotoAction:(id)sender
{
    UIButton *buttonSelectPhoto=(UIButton *)sender;
    UIView *profilePhoto=[buttonSelectPhoto superview];
    NSMutableDictionary *dict=[totalPicArray objectAtIndex:profilePhoto.tag];
    BOOL isSelected=[[dict valueForKey:@"isSelected"] boolValue];
    if (isSelected){
        
        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
        [self.photosHorizontalView reloadData];
        return;
    }
    
    for (int i=0; i<[totalPicArray count]; i++) {
        NSMutableDictionary *dict = [totalPicArray objectAtIndex:i];
        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
    }
    
    if (!isSelected)
        
    {
        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
        
    }
    
    else
    {
        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
    }
    // [self.photosHorizontalView updateSelectedImage:profilePhoto];
    [self.photosHorizontalView reloadData];
    [self.photosHorizontalView scrollToPage:profilePhoto.tag];
    // [self.photosHorizontalView reloadData];
}

#pragma mark - Delete Picture
-(IBAction)deletePhotoAction:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:@"Delete"
                                                    otherButtonTitles:nil,nil];
    
    UIButton *buttonDelete=(UIButton *)sender;
    toDeleteView=[buttonDelete superview];
    delete_index=101;
    [actionSheet setActionSheetStyle:UIActionSheetStyleDefault];
    
    [actionSheet showInView:self.view];
}

#pragma mark - For Age and Gender
-(void)createActionSheet:(NSString *)selectedValue
{
    CGPoint pt;
    pt.x = 0;
    
    if ([selectedValue isEqualToString:@"Age"])
    {
        [actionSheetAge removeFromSuperview];
        actionSheetAge = [[UIActionSheet alloc] initWithTitle:nil
                                                     delegate:nil
                                            cancelButtonTitle:nil
                                       destructiveButtonTitle:nil
                                            otherButtonTitles:nil];
        NSDateFormatter *formatr=[[NSDateFormatter alloc] init];
        [formatr setDateFormat:@"MM/dd/yyyyy hh:mm:ss"];
        NSDate *dateMaximum=[NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MMM/yyyy"];
        [actionSheetAge setActionSheetStyle:UIActionSheetStyleDefault];
        CGRect pickerFrame = CGRectMake(0,30,0,0);
        datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        [datePicker addTarget:self action:@selector(pickerValueChanged:)               forControlEvents:UIControlEventValueChanged];
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker setMaximumDate:dateMaximum];
        [datePicker setDate:[[UserProfile sharedInstance] calculateDobFromAge:_otherUsersObject.age]];
        [actionSheetAge addSubview:datePicker];
        actionSheetAge.backgroundColor=[UIColor whiteColor];
        actionSheetAge.frame=CGRectMake(0, HEIGHT-200, WIDTH, 200);
        [self.view bringSubviewToFront:actionSheetAge];
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, actionSheetAge.frame.size.width, 44)];
        toolbar.barStyle = UIBarStyleBlackOpaque;
        toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style: UIBarButtonItemStylePlain target: self action:@selector(donePressed)];
        doneButton.tintColor=[UIColor whiteColor];
        toolbar.items = [NSArray arrayWithObjects:flexibleItem,doneButton, nil];
        [datePicker addSubview:toolbar];
        [actionSheetAge addSubview:toolbar];
        [self.view addSubview:actionSheetAge];
        pt.y=pt.y+60;
        [self.infoScrollView setContentOffset:pt animated:YES];
        
    }
    else if ([selectedValue isEqualToString:@"Gender"]) {
        UIActionSheet *genderActionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                         destructiveButtonTitle:nil
                                                              otherButtonTitles:@"MALE", @"FEMALE", nil];
        [genderActionSheet showInView:self.view];
        genderActionSheet.tag = 502;
        pt.y=pt.y+120;
        [self.infoScrollView setContentOffset:pt animated:YES];
    }
}

-(void)donePressed
{
    [actionSheetAge removeFromSuperview];
}

-(void)pickerValueChanged:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *userAge=[[UserProfile sharedInstance] CalculateYear:[NSDate date] date:datePicker.date];
    _age.text=userAge;
    _otherUsersObject.age = _age.text;
}

- (void)changeDateInLabel:(id)sender{
    //Use NSDateFormatter to write out the date in a friendly format
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
}

#pragma mark - For First and Last Name
-(void)updateFirstAndLastName:(NSString *)stringValue storedName:
(NSString *)updateValue isFirstName:(BOOL )isFirstName
{
    InfoViewController* infoView = [[InfoViewController alloc] initWithNibName:@"InfoViewController" bundle:nil];
    infoView.userName = updateValue;
    infoView.isFirstName=isFirstName;
    infoView.view.frame=self.view.bounds;
    infoView.delegate = self;
    
   [self.view addSubview:infoView.view];
    [self addChildViewController:infoView];
    /*
    NSString *stringTitle = [NSString stringWithFormat:@"Update %@",stringValue];
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:stringTitle message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"CANCEL",@"UPDATE", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView textFieldAtIndex:0].text = updateValue;
    [alertView textFieldAtIndex:0].delegate=self;
    [alertView textFieldAtIndex:0].autocapitalizationType=UITextAutocapitalizationTypeWords;
    [alertView textFieldAtIndex:0].frame=CGRectMake(15, 90, 260, 63);
    alertView.tag=22;
    alertView.accessibilityValue = isFirstName;
    [alertView show];*/
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    if (range.length==1) {
        return YES;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 20) ? NO : YES;
    
    return YES;
}

#pragma mark - Delegate Methods

#pragma mark - Alert View
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag]==22)
    {
        if (buttonIndex==1) {
            NSString *nameValue=[alertView textFieldAtIndex:0].text;
            nameValue= [nameValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if ([alertView.accessibilityValue isEqualToString:@"YES"]) {
                if ([nameValue length]>0)
                {
                    _firstName.text=nameValue;
                    _otherUsersObject.firstName=_firstName.text;
                }
                else{
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"First name can't be blank." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                }
            }
            else if ([alertView.accessibilityValue isEqualToString:@"NO"]) {
                if ([nameValue length]>0)
                {
                    _lastName.text=nameValue;
                    _otherUsersObject.lastName=_lastName.text;
                }
                else{
                    _lastName.text=@"";
                    _otherUsersObject.lastName=_lastName.text;
                }
            }
        }
    }
}

#pragma mark - UIAction sheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag==501) {
        if (buttonIndex==0) {
            [self choosePicker:buttonIndex];
        }
        else if(buttonIndex==1)
        {
            [self choosePicker:buttonIndex];
        }
    }
    else if (actionSheet.tag == 502) {
        if (buttonIndex == 0)
            _gender.text = @"Male";
        
        else
            _gender.text = @"Female";
        
        _otherUsersObject.gender =_gender.text;
    }
    else
    {
        if (buttonIndex==0) {
            [self deleteImageWebservice:toDeleteView.tag];
            if (toDeleteView.tag==[totalPicArray count]-1) {
                pageControl.currentPage=[totalPicArray count]-2;
                [self updateDots];
            }
            [totalPicArray removeObjectAtIndex:toDeleteView.tag];
            [self hideUnhideNoImage];
            [self.photosHorizontalView reloadData];
            
        }
    }
}

-(void)doneWithUpdatedInformation
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ImagePicker
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage *new=[self squareImageWithImage:image scaledToSize:CGSizeMake(200, 200)];
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isFromPicker"];
    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
    [dict setObject:new forKey:@"image"];
    
    [totalPicArray addObject:dict];
    [self hideUnhideNoImage];
    [self.photosHorizontalView reloadData];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Image Upload/Download Stack
-(void)choosePicker:(NSInteger)item
{
    
    UIImagePickerController  *pickerOne = [[UIImagePickerController alloc] init];
    pickerOne.delegate = self;
    if (item==0) {
        pickerOne.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    else
    {
        BOOL isAllowed=[self checkCameraPermission];
        if (!isAllowed) {
            return;
        }
        pickerOne.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    [self presentViewController:pickerOne animated:YES completion:nil];
}

#pragma mark set image on scroll view
-(void)setImagesOnScrollview:(NSString *)userPhotos
{
    if (userPhotos.length<1) {
        if ([totalPicArray count]<1) {
           _noImage.hidden=NO;
        }
        
        return;
    }
    [totalPicArray removeAllObjects];
    
    NSArray *imageArray=[userPhotos componentsSeparatedByString:@";"];
    selectedIndex=0;
    for (int i=0; i<[imageArray count]; i++) {
        NSMutableDictionary *dict=[NSMutableDictionary dictionary];
        if ([[imageArray objectAtIndex:i] isEqualToString:_otherUsersObject.profileImage]) {
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
            selectedIndex=i;
        }
        else
        {
            [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
        }
        
        [dict setObject:[imageArray objectAtIndex:i] forKey:@"image"];
        [totalPicArray addObject:dict];
    }
    pageControl.currentPage=selectedIndex;
    [self.photosHorizontalView reloadData];
    
    [self performSelector:@selector(setIndex) withObject:nil afterDelay:0.5];
}
#pragma mark set index at middle
-(void)setIndex
{
    [self.photosHorizontalView scrollToPageNoAnimation:selectedIndex];
    pageControl.currentPage=selectedIndex;
}
-(void)hideUnhideNoImage
{
    if ([totalPicArray count]>0)
        _noImage.hidden=YES;
    else
        _noImage.hidden=NO;
}

#pragma mark scale image
- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    double ratio;
    double delta;
    CGPoint offset;
    
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.width);
    
    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }
    
    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);
    
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark - Web service Methods
-(void)CallUpdateProfile
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    
    if(_otherUsersObject.firstName.length>0){
        NSData *nameData=[_otherUsersObject.firstName dataUsingEncoding:NSUTF8StringEncoding];
        NSString *nameBase64=[nameData base64EncodedStringWithOptions:0];
        [params setObject:nameBase64 forKey:@"firstName"];
    }
    if(_otherUsersObject.lastName.length>0){
        NSData *nameData=[_otherUsersObject.lastName dataUsingEncoding:NSUTF8StringEncoding];
        NSString *nameBase64=[nameData base64EncodedStringWithOptions:0];
        [params setObject:nameBase64 forKey:@"lastName"];
    }
    else
    {
        NSData *nameData=[_otherUsersObject.firstName dataUsingEncoding:NSUTF8StringEncoding];
        NSString *nameBase64=[nameData base64EncodedStringWithOptions:0];
        [params setObject:nameBase64 forKey:@"lastName"];
    }
    if(_otherUsersObject.gender.length>0)
        [params setObject:_otherUsersObject.gender forKey:@"gender"];
    
    if(_otherUsersObject.age.length>0)
        [params setObject:_otherUsersObject.age forKey:@"age"];
    
    if (_otherUsersObject.plannedTripId.length>0)
        [params setObject:_otherUsersObject.plannedTripId forKey:@"nextDestinationId"];
    if (_otherUsersObject.longitude.length>0)
        [params setObject:_otherUsersObject.longitude forKey:@"longitude"];
    
    if (_otherUsersObject.latitude.length>0)
        [params setObject:_otherUsersObject.latitude forKey:@"latitude"];
    
    if(_otherUsersObject.skillLevelID.length>0)
        [params setObject:_otherUsersObject.skillLevelID forKey:@"skillLevelId"];
    
    if(_otherUsersObject.riderTypeId.length>0)
        [params setObject:_otherUsersObject.riderTypeId forKey:@"riderTypeId"];
    
    if(_otherUsersObject.interestId.length>0)
        [params setObject:_otherUsersObject.interestId forKey:@"interestId"];
    
    if (_otherUsersObject.aboutMe.length>0){
        NSData *data = [_otherUsersObject.aboutMe dataUsingEncoding:NSNonLossyASCIIStringEncoding];
        NSString *encodedString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSData *textdata=[encodedString dataUsingEncoding:NSUTF8StringEncoding];
        NSString *baseString=[textdata base64EncodedStringWithOptions:0];
        
        [params setObject:baseString forKey:@"aboutMe"];
    }
    
    if (_otherUsersObject.locationId.length>0)
    {
        NSArray *locationIdArray=[_otherUsersObject.locationId  componentsSeparatedByString:@","];
        [params setObject:locationIdArray forKey:@"locationIds"];
    }
    [[UserProfile sharedInstance]setDelegate:self];
    [[UserProfile sharedInstance] updateProfile:params method:@"user/updateProfile"];
}

#pragma mark - Upload Image To Web Service
-(void)UploadImageToWebservices
{
    [ImageUploading sharedInstance].delegate=self;
    [[ImageUploading sharedInstance] updatePhotos:totalPicArray selectedArray:selectedPicArray];
}

#pragma mark - Delete Image From Web Service
-(void)deleteImageWebservice:(NSInteger )tag
{
    NSMutableDictionary *dict=[totalPicArray objectAtIndex:tag];
    // UIImageView *userImage=[[view subviews] objectAtIndex:0];
    id image=[dict valueForKey:@"image"];
    if ([image isKindOfClass:[UIImage class]]) {
        
    }
    else
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSArray *arr=[[dict valueForKey:@"image"] componentsSeparatedByString:@"/"];
        NSString *deleteImageName=[arr lastObject];
        NSMutableDictionary *params=[NSMutableDictionary dictionary];
        [params setObject:deleteImageName forKey:@"imageFile"];
        [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
        
        [[UserProfile sharedInstance] deleteUserPic:params method:@"user/deletePhoto"];
        [UserProfile sharedInstance].delegate=self;
    }
}

#pragma mark profile updatation delegate
-(void)profileUpdateSuccessfully:(UserInfoEntity*)updatedUserInfoEntity
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Profile updated successfully." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
    [self updateCurrentUserInfo:updatedUserInfoEntity];
}

#pragma mark - Image Downloader delegate
- (void)imageUploaderDidFailed:(NSMutableDictionary *)errorDictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)imageUploaderDidFinish:(UserInfoEntity *)updatedUserInfoEntity
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self updateCurrentUserInfo:updatedUserInfoEntity];
    [self setImagesOnScrollview:_otherUsersObject.userPhotos];
    
}

#pragma mark - Update User Data
-(Users *)updateCurrentUserInfo:(UserInfoEntity *)updatedUserInfoEntity
{
    _otherUsersObject.firstName=updatedUserInfoEntity.firstName;
    _otherUsersObject.lastName=updatedUserInfoEntity.lastName;
    _otherUsersObject.aboutMe=updatedUserInfoEntity.aboutMe;
    _otherUsersObject.age=updatedUserInfoEntity.age;
    _otherUsersObject.userPhotos=updatedUserInfoEntity.userPhotos;
    _otherUsersObject.skillLevel=updatedUserInfoEntity.skillLevelName;
    _otherUsersObject.riderType=updatedUserInfoEntity.riderTypeName;
    _otherUsersObject.skillLevelID=updatedUserInfoEntity.skillLevelId;
    _otherUsersObject.plannedTripName=updatedUserInfoEntity.nextDestinationName;
    _otherUsersObject.plannedTripId=updatedUserInfoEntity.nextDestinationId;
    _otherUsersObject.profileImage=userInfoEntity.profileImage;
    _otherUsersObject.latitude=updatedUserInfoEntity.latitude;
    _otherUsersObject.longitude=updatedUserInfoEntity.longitude;
    _otherUsersObject.address=updatedUserInfoEntity.address;
    _otherUsersObject.interestId=updatedUserInfoEntity.interestId;
    _otherUsersObject.interestName=updatedUserInfoEntity.interestName;
    
    return _otherUsersObject;
}

-(void)failedWithError:(NSDictionary*)errorDictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:[errorDictionary valueForKey:@"Error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
}

#pragma mark image deleted delegate
-(void)imageDeletedSuccessfully:(UserInfoEntity*)updatedUserInfoEntity
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self updateCurrentUserInfo:updatedUserInfoEntity];
    [self setImagesOnScrollview:_otherUsersObject.userPhotos];
}

#pragma mark - Add Image by Notification Observer
-(void)addImageToScrollView:(NSNotification *)userdict
{
    // [self fetchUserInfoFromCoreData];
    UserInfoEntity *updatedUserInfoEntity=userdict.object;
    [self updateCurrentUserInfo:updatedUserInfoEntity];
    [self setImagesOnScrollview:userInfoEntity.userPhotos];
    
}

#pragma mark -
#pragma mark PagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(PagedFlowView *)flowView{
    if (WIDTH>320) {
        return CGSizeMake(200, 200);
    }
    else
    {
        return CGSizeMake(150, 150);
    }
}

- (void)flowView:(PagedFlowView *)flowView didScrollToPageAtIndex:(NSInteger)index {
    NSLog(@"Scrolled to page # %ld", (long)index);
    [self updateDots];
}

- (void)flowView:(PagedFlowView *)flowView didTapPageAtIndex:(NSInteger)index{
    NSLog(@"Tapped on page # %ld", (long)index);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark PagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(PagedFlowView *)flowView{
    return [totalPicArray count];
}

- (UIView *)flowView:(PagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    ProfilePic *imageView = (ProfilePic *)[flowView dequeueReusableCell];
    if (!imageView) {
        NSArray *nibContentss = [[NSBundle mainBundle] loadNibNamed:@"ProfilePic" owner:self options:nil];
        imageView=[nibContentss lastObject];
    }
    imageView.tag=index;
    
    NSMutableDictionary *dict=[totalPicArray objectAtIndex:index];
    UIImageView *userimage=[imageView.subviews objectAtIndex:0];
    UIButton *selectBtn=[imageView.subviews objectAtIndex:1];
    
    [self removeButtonsFromProfilePic:[imageView.subviews objectAtIndex:1] DeleteButton:[imageView.subviews objectAtIndex:2]];
    id obJ=[dict valueForKey:@"image"];
    BOOL isSelected=[[dict valueForKey:@"isSelected"] boolValue];
    if ([obJ isKindOfClass:[NSString class]]) {
        // [userimage seti];
        [userimage setImageWithURL:[NSURL URLWithString:[dict valueForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [userimage setAccessibilityIdentifier:[dict valueForKey:@"image"]];
    }
    else
    {
        [userimage setImage:[dict valueForKey:@"image"]];
    }
    
    if (isSelected)
        [selectBtn setImage:[UIImage imageNamed:@"active_selector"] forState:UIControlStateNormal];
    
    else
        [selectBtn setImage:[UIImage imageNamed:@"selector.png"] forState:UIControlStateNormal];
    return imageView;
}

-(void)removeButtonsFromProfilePic:(UIButton *)buttonSelect DeleteButton:(UIButton *)buttonDelete
{
    if ([totalPicArray count]==1) {
        buttonSelect.hidden=YES;
        buttonDelete.hidden=YES;
    }
    else{
        buttonSelect.hidden=NO;
        buttonDelete.hidden=NO;
    }
    
}
#pragma mark update pagecontrol image
-(void)updateDots
{
    for (int i = 0; i < [pageControl.subviews count]; i++)
    {
        UIView *dot = [pageControl.subviews objectAtIndex:i];
        if (i == pageControl.currentPage)
        {
            for (id subview in dot.subviews)
            {
                if ([subview isKindOfClass:[UIImageView class]]) {
                    [subview removeFromSuperview];
                }
            }
            UIImageView* fillImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"swipe_blue"]];
            [dot addSubview:fillImage];
        } else
        {
            for (id subview in dot.subviews) {
                if ([subview isKindOfClass:[UIImageView class]]) {
                    [subview removeFromSuperview];
                }
            }
            UIImageView* blankImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"swipe_stroke"]];
            [dot addSubview:blankImage];
        }
    }
}
#pragma mark custom popover delegate
-(void)nameEntered:(NSString *)name isLastName:(BOOL)isLastName
{
    if (isLastName) {
       
        _firstName.text=name;
    }
    else
    {
     _lastName.text=name;
    }
    for (UIView *subView in self.view.subviews)
    {
        
        if (subView.tag == 123) {
            
            [subView removeFromSuperview];
            break;
        }
        
    }

    
}
-(void)actionCancelled:(BOOL)isLastName
{
    for (UIView *subView in self.view.subviews)
    {
        
        if (subView.tag == 123) {
            
            [subView removeFromSuperview];
            break;
        }
        
    }

}
@end
