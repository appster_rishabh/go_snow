//
//  MyProfileAboutMeVC.h
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Users.h"
@interface MyProfileAboutMeVC : UIViewController
{
}
@property(nonatomic,assign)Users *userInfo;
@property (weak, nonatomic) IBOutlet UILabel *labelPleaseEnter;
@property (weak, nonatomic) IBOutlet UITextView *textViewAboutMe;

- (IBAction)doneButtonAction;
- (IBAction)backButtonAction;

@end
