//
//  MyProfileSkillLevelVC.h
//  GoSnow
//
//  Created by Varsha Singh on 03/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Users.h"

@interface MyProfileSkillLevelVC : UIViewController
{
   
}
@property(nonatomic,assign)Users *userInfo;
@property (nonatomic,weak)IBOutlet UIView *viewSkillSet;

- (IBAction)backButtonAction;
- (IBAction)skillLevelAction:(id)sender;

@end
