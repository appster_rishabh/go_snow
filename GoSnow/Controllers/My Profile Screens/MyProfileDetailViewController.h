//
//  MyProfileDetailViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "ImageUploading.h"
#import "PagedFlowView.h"
#import "UserProfile.h"
#import "Users.h"
#import "InfoViewController.h"
@interface MyProfileDetailViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,ImageUploaderDelegate,UITextFieldDelegate,PagedFlowViewDataSource,PagedFlowViewDelegate,ProfileUpdateDelegate,UserNameDelegate>
{
    int selectedIndex;
    NSInteger delete_index;
    UIView *picView;
    UIView *toDeleteView;
    NSInteger deletedIndex;
    UserInfoEntity *userInfoEntity;
   // Users *otherUsersObject;
    UIStoryboard *storyboardObject;
    UIDatePicker *datePicker;
    UIActionSheet *actionSheetAge;
    NSMutableArray *totalPicArray;
    NSMutableArray  *selectedPicArray;
}
@property(nonatomic,strong)Users *otherUsersObject;

@property(nonatomic,strong)ImageUploading *object;
@property (nonatomic,weak)IBOutlet PagedFlowView *photosHorizontalView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIScrollView *infoScrollView;
@property (weak, nonatomic) IBOutlet UILabel *firstName;
@property (weak, nonatomic) IBOutlet UILabel *lastName;
@property (weak, nonatomic) IBOutlet UILabel *age;
@property (weak, nonatomic) IBOutlet UILabel *gender;
@property (weak, nonatomic) IBOutlet UILabel *aboutMe;
@property (weak, nonatomic) IBOutlet UILabel *riderType;
@property (weak, nonatomic) IBOutlet UILabel *skillLevel;
@property (weak, nonatomic) IBOutlet UILabel *plannedTrip;
@property (weak, nonatomic) IBOutlet UILabel *interest;
@property (weak, nonatomic) IBOutlet UIImageView *noImage;
@property (weak, nonatomic) IBOutlet UILabel *currentLocation;
@property (weak, nonatomic) IBOutlet UIView *viewSkillLevel;
@property (weak, nonatomic) IBOutlet UILabel *favrouiteLocation;


@property (weak, nonatomic) UITextField *textFieldFirstAndLastName;

-(IBAction)pageControlValueChangeAction:(id)sender;
-(IBAction)selectPhotoAction:(id)sender;
-(IBAction)deletePhotoAction:(id)sender;
-(IBAction)nextButtonAction:(id)sender;
-(IBAction)takePhotoAction;
-(IBAction)doneButtonAction;
-(IBAction)backButtonAction;
-(IBAction)skillLevelAction:(id)sender;

@end
