//
//  MyProfileSkillLevelVC.m
//  GoSnow
//
//  Created by Varsha Singh on 03/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "MyProfileSkillLevelVC.h"
#import "ProfileCell.h"
#import "UserProfile.h"

@interface MyProfileSkillLevelVC ()

@end

@implementation MyProfileSkillLevelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setHidden:YES];
    _viewSkillSet.layer.cornerRadius=8.0;
}

#pragma mark -
#pragma mark -  IBActions

-(IBAction)skillLevelAction:(id)sender
{
    UIButton *buttonSkillLevel = (UIButton *)sender;
    if(buttonSkillLevel.tag == 21)
    {
        _userInfo.skillLevelID=@"1";
        _userInfo.skillLevel=@"Green";
    }
    else if(buttonSkillLevel.tag == 22)
    {
        _userInfo.skillLevelID=@"2";
        _userInfo.skillLevel=@"Red";
    }
    else if(buttonSkillLevel.tag == 23)
    {
        _userInfo.skillLevelID=@"3";
        _userInfo.skillLevel=@"Black";
    }
    else if(buttonSkillLevel.tag == 24)
    {
        _userInfo.skillLevelID=@"4";
        _userInfo.skillLevel=@"Double Black";
    }
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
