//
//  MyProfileCurrentLocationVC.m
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//


#import "MyProfileCurrentLocationVC.h"

@interface MyProfileCurrentLocationVC ()

@end

@implementation MyProfileCurrentLocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.navigationController.navigationBar setHidden:YES];
    if (_userInfo.address.length>0) {
        _labelCurrentLocation.text=_userInfo.address;
    }
      self.geocoder = [[CLGeocoder alloc] init];
    locationObject = [Location sharedInstance];
    locationObject.delegate = self;
    [locationObject callLocationManager];
    [locationObject startUpdateLocation];
    // Do any additional setup after loading the view.
}

#pragma mark -
#pragma mark Get Details -

- (void)getName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate
{
    
}

#pragma mark -
#pragma mark - IBActions

- (IBAction)doneButtonAction
{
    if (locatedaddress.length>0) {
        _userInfo.address=locatedaddress;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Location Delegates

-(void)gotLocationSuccessfully:(NSString *)latitudeValue longitude:(NSString *)longitudeValue
{
    [self plotCurrentLocation:(NSString *)latitudeValue longitude:(NSString *)longitudeValue];
    [locationObject stopUpdateLocation];
}

-(void)gotErrorInFindingLocation
{
    
}

#pragma mark -
#pragma Plot Current Location

-(void)plotCurrentLocation:(NSString *)latitudeValue longitude:(NSString *)longitudeValue
{
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [latitudeValue integerValue];
    zoomLocation.longitude= [longitudeValue integerValue];
    
    
   
   // MKPlacemark *placemark=[MKPlacemark al];
   // [_mapView addAnnotation:locationObject.locationManager.location];
   // CLLocation loc=[CLLocation ];
    //[_mapView addAnnotation:zoomLocation];
    [self.geocoder reverseGeocodeLocation: locationObject.locationManager.location completionHandler:
     ^(NSArray *placemarks, NSError *error) {
         
         //Get address
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         NSLog(@"Placemark array: %@",placemark.addressDictionary );
         
         //String to address
        locatedaddress = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
          MKPlacemark *placemarkPoint = [[MKPlacemark alloc] initWithPlacemark:placemark];
        // MKPlacemark *mPlacemark = [[MKPlacemark alloc] initWithCoordinate:_mapView.userLocation.location.coordinate addressDictionary:nil];
         _mapView.delegate = self;
         //[_mapView addAnnotation:mPlacemark];
         //[_mapView setRegion:region animated:YES];
         //[_mapView addAnnotation:mPlacemark];
         //Print the location in the console
         _labelCurrentLocation.text=locatedaddress;
         NSLog(@"Currently address is: %@",locatedaddress);
         [_mapView addAnnotation:placemarkPoint];
         
     }];
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion mapRegion;
    mapRegion.center = mapView.userLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    
    [mapView setRegion:mapRegion animated: YES];
}

#pragma mark -
#pragma mark - Map View lifecycle
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
   
    annotationView.canShowCallout = YES;
   
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)view
calloutAccessoryControlTapped:(UIControl *)control
{
    NSString *st=locatedaddress;
    st=[st  stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // http://maps.apple.com/?daddr=San+Francisco,+CA&saddr=cupertino
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:st]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:st]];
    }    
}

- (void)mapView:(MKMapView *)mapView
didSelectAnnotationView:(MKAnnotationView *)view
{
    // [mapView deselectAnnotation:view.annotation animated:YES];
    NSString *st=locatedaddress;
    st=[st  stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // http://maps.apple.com/?daddr=San+Francisco,+CA&saddr=cupertino
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:st]])
    {
        //   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:st]];
    }
}
@end
