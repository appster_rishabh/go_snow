//
//  MyProfilePlannedTripVC.m
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "MyProfilePlannedTripVC.h"
#import "DestinationManager.h"
#import "SignUpStepsViewController.h"
#import "MyProfileDetailViewController.h"
#import "DestinationCell.h"
#import "CreateATripViewController.h"

@interface MyProfilePlannedTripVC ()

@end

@implementation MyProfilePlannedTripVC
@synthesize isDestinationPage,isGroupTripPage;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
    
    arrayGetDestinations = [[NSMutableArray alloc]init];
    [self handleDestinationManager];
    
    UILabel *labelTitle = (UILabel *)[self.view viewWithTag:200];
    if (isDestinationPage)
        labelTitle.text = @"CHOOSE DESTINATION";
    else
        labelTitle.text = @"CHOOSE LOCATION";
}

-(void)getSelectedLocation
{
    for (int i=0; i<[arrayGetDestinations count]; i++) {
        Destinations *destinations=[arrayGetDestinations objectAtIndex:i];
        
        if ([destinations.destinationName isEqualToString:_userInfo.plannedTripName] || [destinations.destinationName  isEqualToString:_groupTripData.destinationName])
        {
            destinations.isSelect=YES;
            if (_userInfo.plannedTripName.length>0)
                _labelPlannedTrip.text =_userInfo.plannedTripName;
            
            if (_groupTripData.destinationName.length>0)
                _labelPlannedTrip.text =_groupTripData.destinationName;
            
            [arrayGetDestinations replaceObjectAtIndex:i withObject:destinations];
        }
    }
    [_tableViewPlannedTrip reloadData];
}

#pragma Handle Destination Manager
-(void)handleDestinationManager
{
    [self showLoader];
    [[DestinationManager sharedInstance]getDestinationsFromWebService];
    [DestinationManager sharedInstance].delegate = self;;
}

#pragma mark -
#pragma mark IBActions

- (IBAction)doneButtonAction
{
    BOOL isSelect=NO;
    for (int i=0; i<[arrayGetDestinations count]; i++)
    {
        Destinations *destinationObject=[arrayGetDestinations objectAtIndex:i];
        BOOL isSeleted =destinationObject.isSelect;
        if (isSeleted)
        {
            isSelect=YES;
            if (isGroupTripPage) {
                _groupTripData.destinationName=destinationObject.destinationName;
                _groupTripData.destinationId=destinationObject.destinationId;
            }
            else{
                _userInfo.plannedTripName=destinationObject.destinationName;
                _userInfo.plannedTripId=destinationObject.destinationId;
            }
        }
    }
    
    if (!isSelect) {
        if (isGroupTripPage) {
            _groupTripData.destinationName=@"";
            _groupTripData.destinationId=@"";
        }
        else{
            _userInfo.plannedTripName=@"";
            _userInfo.plannedTripId=@"";
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Network Manager Protocol
-(void)showLoader
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}
-(void)hideLoader
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)fetchDestinationSuccessfully:(NSMutableArray *)arrayDestinations
{
    [self hideLoader];
    [arrayGetDestinations removeAllObjects];
    [arrayGetDestinations addObjectsFromArray:arrayDestinations];
    if ([arrayGetDestinations count]>0)
    {
        [self getSelectedLocation];
        [_tableViewPlannedTrip reloadData];
    }
}

-(void)failedWithError:(NSDictionary*)dictionaryError
{
    [self hideLoader];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [arrayGetDestinations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    // [tableView setSeparatorColor:[UIColor clearColor]];
    static NSString *CellIdentifier = @"DestinationCell";
    DestinationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DestinationCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.sender=self;
    }
    
    Destinations *destonationObject=[arrayGetDestinations objectAtIndex:indexPath.row];
    [cell SetLayout:destonationObject.destinationName isSelect:destonationObject.isSelect index:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self selectLocation:indexPath];
}

#pragma mark -
#pragma mark Check Uncheck Selected Row Method

-(void)selectLocation:(NSIndexPath *)indexPath
{
    Destinations *destonationObject=[arrayGetDestinations objectAtIndex:indexPath.row];
    
    BOOL isSeleted=destonationObject.isSelect;
    if (isSeleted){
        _labelPlannedTrip.text =@"Not Chosen Yet";
        destonationObject.isSelect=NO;
        [_tableViewPlannedTrip reloadData];
        // return;
    }
    
    for (int i=0; i<[arrayGetDestinations count]; i++) {
        Destinations *destonationObjectNew=[arrayGetDestinations objectAtIndex:i];
        destonationObjectNew.isSelect=NO;
        [arrayGetDestinations replaceObjectAtIndex:i withObject:destonationObjectNew];
    }
    
    if (isSeleted){
        _labelPlannedTrip.text =@"Not Chosen Yet";
        destonationObject.isSelect=NO;
    }
    else{
        _labelPlannedTrip.text =destonationObject.destinationName;
        destonationObject.isSelect=YES;
    }
    
    [arrayGetDestinations replaceObjectAtIndex:indexPath.row withObject:destonationObject];
    [_tableViewPlannedTrip reloadData];
}

@end
