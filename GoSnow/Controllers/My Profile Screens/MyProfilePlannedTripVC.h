//
//  MyProfilePlannedTripVC.h
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DestinationManager.h"
#import "Users.h"
#import "GroupTrip.h"

@interface MyProfilePlannedTripVC : UIViewController<DestinationDelegate>
{
    NSMutableArray *arrayGetDestinations;
}
@property(nonatomic,assign)Users *userInfo;
@property(nonatomic,assign)GroupTrip *groupTripData;

@property BOOL isDestinationPage;
@property BOOL isGroupTripPage;

@property (weak, nonatomic) IBOutlet UITableView *tableViewPlannedTrip;
@property (weak, nonatomic) IBOutlet UILabel *labelPlannedTrip;

- (IBAction)doneButtonAction;
- (IBAction)backButtonAction;
- (void)selectLocation:(NSIndexPath *)indexPath;

@end
