//
//  MyProfileRiderTypeVC.m
//  GoSnow
//
//  Created by Varsha Singh on 24/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "MyProfileRiderTypeVC.h"
#import "ProfileCell.h"
#import "UserProfile.h"

@interface MyProfileRiderTypeVC ()

@end

@implementation MyProfileRiderTypeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
    
    riderTypeArray=[[NSMutableArray alloc]initWithObjects:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Skier",@"value",@"rider",@"Type",@"1" ,@"id",@"NO",@"isSelect",nil],[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Snowboarder",@"value",@"rider",@"Type",@"2" ,@"id",@"NO",@"isSelect",nil], nil];
    [self arrangeArray];
    if (_userInfo.riderType.length>0) {
        UILabel *labelRiderType = (UILabel *)[self.view viewWithTag:91];
        labelRiderType.text=_userInfo.riderType;
    }
}
-(void)arrangeArray
{
    for (int i=0; i<[riderTypeArray count]; i++) {
        NSMutableDictionary *dict=[riderTypeArray objectAtIndex:i];
        if ([[dict valueForKey:@"value"] isEqualToString:_userInfo.riderType]) {
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelect"];
        }
    }
}

#pragma mark -
#pragma mark -  IBActions

- (IBAction)doneButtonAction
{
    NSString *strSelectedRiderId;
    for (int i=0; i<[riderTypeArray count]; i++)
    {
        BOOL isSeleted =[[[riderTypeArray objectAtIndex:i]valueForKey:@"isSelect"]boolValue];
        if (isSeleted)
        {
            strSelectedRiderId =[[riderTypeArray objectAtIndex:i]valueForKey:@"id"];
            UILabel *labelRiderType = (UILabel *)[self.view viewWithTag:91];
            _userInfo.riderType = labelRiderType.text;
            _userInfo.riderTypeId = strSelectedRiderId;
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
    }
}

- (IBAction)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [riderTypeArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    static NSString *CellIdentifier = @"ProfileCell";
    ProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProfileCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.sender=self;
    }
    
    NSMutableDictionary *dict=[riderTypeArray objectAtIndex:indexPath.row];
    [cell SetLayout:dict index:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self selectValue:indexPath];
}

#pragma mark -
#pragma mark Check Uncheck Selected Row Method

-(void)selectValue:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dict=[riderTypeArray objectAtIndex:indexPath.row];
    UILabel *labelRiderType = (UILabel *)[self.view viewWithTag:91];

    BOOL isSeleted=[[dict valueForKey:@"isSelect"] boolValue];
    if (isSeleted){
         labelRiderType.text =@"Not Chosen Yet";
         [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isSelect"];
         [_tableViewRiderType reloadData];
        return;
    }

    for (int i=0; i<[riderTypeArray count]; i++) {
        NSMutableDictionary *dict = [riderTypeArray objectAtIndex:i];
        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isSelect"];
    }
    
    if (isSeleted){
        labelRiderType.text =@"Not Chosen Yet";
        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isSelect"];
    }
    else{
        labelRiderType.text =[[riderTypeArray objectAtIndex:indexPath.row]valueForKey:@"value"];
        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelect"];
    }
    
    [riderTypeArray replaceObjectAtIndex:indexPath.row withObject:dict];
    [_tableViewRiderType reloadData];
}

@end
