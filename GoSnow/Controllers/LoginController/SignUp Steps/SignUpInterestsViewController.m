//
//  SignUpInterestsViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 18/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "SignUpInterestsViewController.h"
#import "ProfileCell.h"

@interface SignUpInterestsViewController ()

@end

@implementation SignUpInterestsViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpInitials];
}

#pragma mark - Set Up Initial Information
-(void)setUpInitials
{
    [self.navigationController.navigationBar setHidden:YES];
    
    interestArray=[[NSMutableArray alloc]initWithObjects:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"On Piste",@"value",@"rider",@"Type",@"1" ,@"id",@"NO",@"isSelect",nil],[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Off Piste",@"value",@"rider",@"Type",@"2" ,@"id",@"NO",@"isSelect",nil],[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Parks",@"value",@"rider",@"Type",@"3" ,@"id",@"NO",@"isSelect",nil],[NSMutableDictionary dictionaryWithObjectsAndKeys:@"All Terrain",@"value",@"rider",@"Type",@"4" ,@"id",@"NO",@"isSelect",nil], nil];
    
    [self getSelectedInterest];
    
    if (_userInfo.interestName.length>0) {
        UILabel *labelInterest = (UILabel *)[self.view viewWithTag:91];
        labelInterest.text=_userInfo.interestName;
    }
}

#pragma mark - Get Selected Interest
-(void)getSelectedInterest
{
    for (int i=0; i<[interestArray count]; i++) {
        NSMutableDictionary *dict=[interestArray objectAtIndex:i];
        if ([[dict valueForKey:@"value"] isEqualToString:_userInfo.interestName]) {
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelect"];
        }
    }
}

#pragma mark -  IBActions

- (IBAction)doneButtonAction
{
    BOOL isSelect=NO;

    for (int i=0; i<[interestArray count]; i++)
    {
        BOOL isSeleted =[[[interestArray objectAtIndex:i]valueForKey:@"isSelect"]boolValue];
        if (isSeleted)
        {
            isSelect=YES;
            UILabel *labelInterest = (UILabel *)[self.view viewWithTag:91];
            _userInfo.interestName = labelInterest.text;
            _userInfo.interestId = [[interestArray objectAtIndex:i]valueForKey:@"id"];
        }
    }
    if (!isSelect) {
        _userInfo.interestName = @"";
        _userInfo.interestId = @"";
    }

    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [interestArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    static NSString *CellIdentifier = @"ProfileCell";
    ProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProfileCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.sender=self;
    }
    
    NSMutableDictionary *dict=[interestArray objectAtIndex:indexPath.row];
    [cell SetLayout:dict index:indexPath];
    
    return cell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self selectValue:indexPath];
}

#pragma mark - Select Interest 

-(void)selectValue:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dict=[interestArray objectAtIndex:indexPath.row];
    UILabel *labelRiderType = (UILabel *)[self.view viewWithTag:91];
    
    BOOL isSeleted=[[dict valueForKey:@"isSelect"] boolValue];
    if (isSeleted){
        labelRiderType.text =@"Not Chosen Yet";
        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isSelect"];
        [_tableViewInterests reloadData];
       // return;
    }
    
    for (int i=0; i<[interestArray count]; i++) {
        NSMutableDictionary *dict = [interestArray objectAtIndex:i];
        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isSelect"];
    }
    
    if (isSeleted){
        labelRiderType.text =@"Not Chosen Yet";
        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isSelect"];
    }
    else{
        labelRiderType.text =[[interestArray objectAtIndex:indexPath.row]valueForKey:@"value"];
        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelect"];
    }
    
    [interestArray replaceObjectAtIndex:indexPath.row withObject:dict];
    [_tableViewInterests reloadData];
}

@end
