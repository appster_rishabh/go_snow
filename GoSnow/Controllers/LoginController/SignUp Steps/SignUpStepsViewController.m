//
//  SignUpStepsViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 26/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "SignUpStepsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ImageUploading.h"
#import "MyProfilePlannedTripVC.h"
#import "UsersViewController.h"
#import "SignUpInterestsViewController.h"
#import "HomeViewController.h"
#import "FacebookManager.h"

@interface SignUpStepsViewController ()

@end

@implementation SignUpStepsViewController
BOOL isDestinationPage = NO;
BOOL isFavLocationPage = NO;

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpInitials];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self updateTextValue];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updateDots];
    
}
-(void)updateTextValue
{
    if (userInfo.plannedTripName.length>0)
    {
        [_buttonDestination setTitle:userInfo.plannedTripName forState:UIControlStateNormal];
    }
    else
    {
       [_buttonDestination setTitle:@"Choose Destination" forState:UIControlStateNormal];
    }
    
    if (userInfo.interestName.length>0)
    {
        [_buttonInterest setTitle:userInfo.interestName forState:UIControlStateNormal];
    }
    else
    {
        [_buttonInterest setTitle:@"Choose Interest" forState:UIControlStateNormal];
    }
}
#pragma mark - Set Up Initial Information
-(void)setUpInitials
{
    step=1;
    currentViewTag = 501;

    self.geocoder = [[CLGeocoder alloc] init];
    [Location sharedInstance].delegate = self;
    [[Location sharedInstance] callLocationManager];
    [[Location sharedInstance] startUpdateLocation];
    
    storyboardObject = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    _viewSkillLevel.layer.cornerRadius=8.0;
    _viewRiderType.layer.cornerRadius=8.0;
    _viewDistane.layer.cornerRadius=8.0;
    _viewInterest.layer.cornerRadius=8.0;
    
    [self fetchUserInformationFromCoreData];
    [self updateUserModel];
    if (!userInfoEntity.profileImage.length>0) {
        [self updateUserProfilePhoto];
    }
    
}

#pragma mark - Fetch Profile Information From CoreDate
-(void)fetchUserInformationFromCoreData
{
    userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}

#pragma mark - Set Profile Information in User Model
-(void)updateUserModel
{
    userInfo=[[Users alloc] init];
    userInfo.firstName=userInfoEntity.firstName;
    userInfo.lastName=userInfoEntity.lastName;
    userInfo.facebookId=userInfoEntity.facebookId;
    userInfo.aboutMe=userInfoEntity.aboutMe;
    userInfo.age=userInfoEntity.age;
    userInfo.gender=userInfoEntity.gender;
    userInfo.userPhotos=userInfoEntity.userPhotos;
    userInfo.skillLevel=userInfoEntity.skillLevelName;
    userInfo.riderType=userInfoEntity.riderTypeName;
    userInfo.skillLevelID=userInfoEntity.skillLevelId;
    userInfo.plannedTripName=userInfoEntity.nextDestinationName;
    userInfo.latitude=userInfoEntity.latitude;
    userInfo.longitude=userInfoEntity.longitude;
    userInfo.address=userInfoEntity.address;
    userInfo.interestName=userInfoEntity.interestName;
    userInfo.interestId=userInfoEntity.interestId;
}

#pragma mark - Update UIPageControl Images
-(void)updateDots
{
    for (int i = 0; i < [_pageControl.subviews count]; i++)
    {
        UIView *dot = [_pageControl.subviews objectAtIndex:i];
        if (i == _pageControl.currentPage)
        {
            for (id subview in dot.subviews)
            {
                if ([subview isKindOfClass:[UIImageView class]]) {
                    [subview removeFromSuperview];
                }
            }
            UIImageView* fillImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"swipe_active.png"]];
            [dot addSubview:fillImage];
        } else
        {
            for (id subview in dot.subviews) {
                if ([subview isKindOfClass:[UIImageView class]]) {
                    [subview removeFromSuperview];
                }
            }
            UIImageView* blankImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"swipe.png"]];
            [dot addSubview:blankImage];
        }
    }
}

#pragma mark - IBActions

-(IBAction)nextButtonAction
{

    if(step<5)
        [self checkSignUpStep:step];
    else
        [self updateUserProfile];
}

-(IBAction)backButtonAction
{
    step-=1;
    if(step>0)
      [self ManageSignStepViews:0];
}

-(IBAction)skillLevelAction:(id)sender
{
    UIButton *buttonSkillLevel = (UIButton *)sender;
    switch (buttonSkillLevel.tag) {
        case 21:
        {
            userInfo.skillLevelID=@"1";
        }
            break;
        case 22:
        {
            userInfo.skillLevelID=@"2";
        }
            break;
        case 23:
        {
            userInfo.skillLevelID=@"3";
        }
            break;
        case 24:
        {
            userInfo.skillLevelID=@"4";
        }
            break;
        default:
            break;
    }
    [self ManageSignStepViews:step];
}

-(IBAction)riderTypeAction:(id)sender
{
    UIButton *buttonSkillLevel = (UIButton *)sender;
    if(buttonSkillLevel.tag == 11)
    {
        userInfo.riderType=@"Snowboarder";
        userInfo.riderTypeId=@"2";
    }
    else
    {
        userInfo.riderType=@"Skier";
        userInfo.riderTypeId=@"1";
    }
    [self ManageSignStepViews:step];
}

-(IBAction)selectLocationAction
{
    MyProfilePlannedTripVC *myProfilePlannedTripVC =
    [storyboardObject instantiateViewControllerWithIdentifier:@"MyProfilePlannedTripVC"];
    isDestinationPage = YES;
    myProfilePlannedTripVC.userInfo=userInfo;
    myProfilePlannedTripVC.isDestinationPage = isDestinationPage;
    [self.navigationController pushViewController:myProfilePlannedTripVC animated:YES];
}

-(IBAction)selectInterestAction
{
    SignUpInterestsViewController *signUpInterestsViewController =
    [storyboardObject instantiateViewControllerWithIdentifier:@"SignUpInterestsViewController"];
    signUpInterestsViewController.userInfo=userInfo;
    [self.navigationController pushViewController:signUpInterestsViewController animated:YES];
}

#pragma mark - Manage All Sign Up Steps
-(void)ManageSignStepViews:(NSInteger)steps
{
    UIPageControl *pageControl = (UIPageControl*) [self.view viewWithTag:601];
    
    UIView *tmpView = (UIView *)[self.view viewWithTag:currentViewTag];
    UIView *nextView;
    [tmpView setHidden:YES];
    if(steps > 0)
    {
        currentViewTag+=1;
        step+=1;
    }
    else
    {
        currentViewTag-=1;
    }
    nextView = (UIView *)[self.view viewWithTag:currentViewTag];
    [UIView animateWithDuration:0.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [tmpView setHidden:YES];
                         [nextView setHidden:NO];
                         [self updateTextValue];
                         nextView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
    pageControl.currentPage = step-1;
    [self updateTextValue];
    [self updateDots];
}

-(void)checkSignUpStep:(NSInteger)steps
{
    switch (steps)
    {
        case 1:
        {
            if (userInfo.riderTypeId.length>0) {
                [self ManageSignStepViews:steps];
            }
            else{
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Please select rider type." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
            }
        }
            break;
        case 2:
        {
            if (userInfo.skillLevelID.length>0) {
                [self ManageSignStepViews:steps];
            }
            else{
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Please select skill level." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                
            }
        }
            break;
        case 3:
        {
            if (userInfo.plannedTripId.length>0) {
                [self ManageSignStepViews:steps];
            }
            else{
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Please choose your next destination." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                
            }
        }
            break;
        case 4:
        {
            if (userInfo.interestId.length>0) {
                [self updateUserProfile];
            }
            else{
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Please choose your interest." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark - Call web service

-(void)updateUserProfile
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:userInfo.riderTypeId forKey:@"riderTypeId"];
    [params setObject:userInfo.skillLevelID forKey:@"skillLevelId"];
    
    if (userInfo.longitude.length>0)
        [params setObject:userInfo.longitude forKey:@"longitude"];
    
    if (userInfo.latitude.length>0)
        [params setObject:userInfo.latitude forKey:@"latitude"];
    
    if (userInfo.plannedTripId.length>0)
        [params setObject:userInfo.plannedTripId forKey:@"nextDestinationId"];
    
    if (userInfo.interestId.length>0)
        [params setObject:userInfo.interestId forKey:@"interestId"];
    [UserProfile sharedInstance].delegate=self;
    [[UserProfile sharedInstance] updateProfile:params method:@"user/updateProfile"];
}

-(void)updateUserProfilePhoto
{
    NSString *stringFacebookId = userInfo.facebookId;
    NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=400&height=400", stringFacebookId];
    [UserProfile downloadImageWithURL:[NSURL URLWithString:userImageURL] completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
           
            NSMutableDictionary *params=[NSMutableDictionary dictionary];
            
             NSData* cData = UIImageJPEGRepresentation(image, 0.60);
            
            NSString *imageBase=[cData base64EncodedStringWithOptions:0];
            if (imageBase.length>0)
                [params setObject:imageBase forKey:@"image"];
            [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
            [params setObject:@"1" forKey:@"isProfileImage"];
           
            [[ImageUploading sharedInstance] UploadImage:params index:0];
        }
    }];
}

#pragma mark  - Profile Updatation Delegate
-(void)profileUpdateSuccessfully:(UserInfoEntity *)userInfoEntity
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
     [[FacebookManager sharedInstance]getFriendsFromFacebook];
    HomeViewController *homeViewController=nil;
    if (WIDTH>320) {
        homeViewController=[[HomeViewController alloc] initWithNibName:@"HomeViewController_iPhone6" bundle:nil];
    }
    else
    {
        homeViewController=[[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    }
    [self.navigationController pushViewController:homeViewController animated:YES];
}

-(void)failedWithError:(NSDictionary*)errorDictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error" message:[errorDictionary valueForKey:@"Error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
}

#pragma mark - Location Delegates

-(void)gotLocationSuccessfully:(NSString *)latitudeValue longitude:(NSString *)longitudeValue
{
    [[Location sharedInstance] stopUpdateLocation];
}

-(void)gotErrorInFindingLocation
{
    
}


@end
