//
//  SignUpInterestsViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 18/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Users.h"

@interface SignUpInterestsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *interestArray;
}
@property (nonatomic,assign)Users *userInfo;
@property (nonatomic, weak) IBOutlet UITableView *tableViewInterests;

- (IBAction)doneButtonAction;
- (IBAction)backButtonAction;
- (void)selectValue:(NSIndexPath *)indexPath;
@end
