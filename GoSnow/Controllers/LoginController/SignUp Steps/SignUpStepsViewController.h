//
//  SignUpStepsViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 26/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserProfile.h"
#import "Users.h"

@interface SignUpStepsViewController : UIViewController<LocationManagerDelegate,ProfileUpdateDelegate>
{
    NSInteger step;
    NSInteger currentViewTag;
    UserInfoEntity *userInfoEntity;
    Users *userInfo;
    UIStoryboard  *storyboardObject;
}

@property(nonatomic,strong)CLGeocoder *geocoder;

@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIButton *buttonDestination;
@property (nonatomic, weak) IBOutlet UIButton *buttonInterest;
@property (nonatomic, weak) IBOutlet UIView *viewSkillLevel;
@property (nonatomic, weak) IBOutlet UIView *viewRiderType;
@property (nonatomic, weak) IBOutlet UIView *viewDistane;
@property (nonatomic, weak) IBOutlet UIView *viewInterest;


-(IBAction)skillLevelAction:(id)sender;
-(IBAction)riderTypeAction:(id)sender;
-(IBAction)selectLocationAction;
-(IBAction)selectInterestAction;
-(IBAction)nextButtonAction;
-(IBAction)backButtonAction;

@end
