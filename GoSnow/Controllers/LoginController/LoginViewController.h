//
//  LoginViewController.h
//  GoSnow
//
//  Created by Rishabh on 19/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "FacebookManager.h"
#import "RESideMenu.h"

@interface LoginViewController : UIViewController<FBLoginViewDelegate,FacebookDelegate,RESideMenuDelegate,LocationManagerDelegate>
{
}
@property(nonatomic,strong)CLGeocoder *geocoder;

-(IBAction)termsAndConditionsAction;
-(IBAction)loginWithFacebookAction;

@end
