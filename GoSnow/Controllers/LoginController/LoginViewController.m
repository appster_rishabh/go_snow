//
//  LoginViewController.m
//  GoSnow
//
//  Created by Rishabh on 19/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "LoginViewController.h"
#import "SignUpStepsViewController.h"
#import "NSDictionary+CheckObject.h"
#import "HomeViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpInitials];
}

#pragma mark - Set Up Initial Information
-(void)setUpInitials
{
    [self.navigationController.navigationBar setHidden:YES];
    
    self.geocoder = [[CLGeocoder alloc] init];
    [Location sharedInstance].delegate = self;
    [[Location sharedInstance] callLocationManager];
    [[Location sharedInstance] startUpdateLocation];
}

#pragma mark - IBActions
- (IBAction)loginWithFacebookAction
{
    [[FacebookManager sharedInstance] fetchProfilewithDelegate:self];
}

-(IBAction)termsAndConditionsAction
{
    
}

#pragma mark - Loader Hide/Show
-(void)showLoader
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

}
-(void)hideLoader
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - Facebook Login Successfully
-(void)loginSuccessfully:(NSDictionary *)successDictionary
{
    [self hideLoader];
    if ([[successDictionary checkNumberForKey:@"errorCode"] integerValue]==112 || [[successDictionary checkValueforKey:@"status"] isEqualToString:@"OK"])
    {
        BOOL isSignupComplete=[[[successDictionary valueForKey:@"profile"] valueForKey:@"isSignupComplete"] boolValue];
        if (!isSignupComplete) {
            SignUpStepsViewController *signUpStepsViewController =[[SignUpStepsViewController alloc] initWithNibName:@"SignUpStepsViewController" bundle:nil];
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
            [self.navigationController pushViewController:signUpStepsViewController animated:YES];
        }
        else
        {
            [[FacebookManager sharedInstance]getFriendsFromFacebook];
            HomeViewController *homeViewController=nil;
            if (WIDTH>320) {
                homeViewController=[[HomeViewController alloc] initWithNibName:@"HomeViewController_iPhone6" bundle:nil];
            }
            else
            {
                homeViewController=[[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
            }
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
            [self.navigationController pushViewController:homeViewController animated:YES];
        }
    }
}

#pragma mark - Error in Facebook Login
-(void)loginFailedwitError:(NSDictionary *)error
{
    if ([[error checkNumberForKey:@"errorCode"] integerValue] == 112)
    {
        
    }
    else if ([error checkValueforKey:@"apiMessage"].length > 2) {
    }
    else
    {
    }
    [self hideLoader];
}

#pragma mark - Location Delegates

-(void)gotLocationSuccessfully:(NSString *)latitudeValue longitude:(NSString *)longitudeValue
{
    [[Location sharedInstance] stopUpdateLocation];
}
-(void)gotErrorInFindingLocation
{
    
}
@end
