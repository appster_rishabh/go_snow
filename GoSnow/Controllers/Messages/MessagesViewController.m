//
//  MessagesViewController.m
//  GoSnow
//
//  Created by Rishabh on 25/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessageCell.h"
#import "ChatViewController.h"
#import "ComposeMessageController.h"
@interface MessagesViewController ()

@end

@implementation MessagesViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initialSetUp];
}
#pragma mark viewWillAppear method
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self getConversation];
    [self updateMenuIcon];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenuIconByNotification) name:KNotificationRecieved object:nil];
}
#pragma mark viewWillDisappear method
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KNotificationRecieved object:nil];
}
#pragma mark updateMenuIconByNotification method
-(void)updateMenuIconByNotification
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    [menuIcon setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
}
#pragma mark updateMenuIcon method
-(void)updateMenuIcon
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    int notificationCount=[UserInfoEntity fetchPendingRequestsAndMessagesCountsinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    if (notificationCount>0)
        [menuIcon setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
    else
        [menuIcon setImage:[UIImage imageNamed:@"menuSetting"] forState:UIControlStateNormal];
    
}


-(void)initialSetUp
{
    [self.navigationController.navigationBar setHidden:YES];
    
    //totalMessages=[[NSMutableArray alloc] init];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    
    [self fetchUserInfoFromCoreData];
    
    [self.refreshControl addTarget:self action:@selector(getRefreshedMessage) forControlEvents:UIControlEventValueChanged];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.messagesTable;
    tableViewController.refreshControl = self.refreshControl;
    
    
}

#pragma mark - fetch user info from database
-(void)fetchUserInfoFromCoreData
{
    userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}
#pragma mark - Get All Messages From Web Service
-(void)getConversation
{
    UIImageView *lineBottom=(UIImageView *)[self.view viewWithTag:11];
    lineBottom.hidden = YES;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [MessageManager sharedInstance].delegate=self;
    [[MessageManager sharedInstance] callWebService:params methodName:@"tripMessage/getConversationList"];
}

#pragma mark - Delete Message Web Service

-(void)deleteMessage:(NSMutableArray *)messageIds
{
    // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:messageIds forKey:@"selectedUserIds"];
    [MessageManager sharedInstance].delegate=self;
    [[MessageManager sharedInstance] callWebService:params methodName:@"tripMessage/deleteConversation"];
}

#pragma mark - Refresh Web Service
-(void)getRefreshedMessage
{
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [MessageManager sharedInstance].delegate=self;
    [[MessageManager sharedInstance] callWebService:params methodName:@"tripMessage/getConversationList"];
    
}

#pragma mark - Message Delegate Methods
- (void)requestDidFailed:(NSDictionary *)errorDictionary
{
    [self.refreshControl endRefreshing];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [self hideBottomElement];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[errorDictionary valueForKey:@"Error"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
- (void)messageSentSuccessfully:(NSDictionary *)dictionary
{
    
}
- (void)getAllConversationSuccessfully:(NSMutableArray *)conversationArray messagesUnreadCounts:(int)counts
{
    [self.refreshControl endRefreshing];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    totalMessages=conversationArray;
    [self hideBottomElement];
    if ([totalMessages count]>0) {
        totalMessageCount=counts;
        [self updateUnreadCount:[NSString stringWithFormat:@"%d Unread",counts]];
        _unreadMessageText.hidden=NO;
        _editButton.hidden=NO;
    }
    else
    {
    }
    [_messagesTable reloadData];
}
#pragma mark update unread/selected count
-(void)updateUnreadCount:(NSString *)text
{
    self.unreadMessageText.text=text;
}

-(void)hideBottomElement
{
    UIImageView *lineBottom=(UIImageView *)[self.view viewWithTag:11];
    if ([totalMessages count]<1) {
        _unreadMessageText.hidden=YES;
        _editButton.hidden=YES;
        _noMessageFoundView.hidden=NO;
        //_messageHeaderText.text=@"MESSAGES (0)";
        lineBottom.hidden = YES;
    }
    else
    {
        _messageHeaderText.text=@"MESSAGES";
        _noMessageFoundView.hidden=YES;
        lineBottom.hidden = NO;

    }
    
    
}
#pragma mark - TableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [totalMessages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MessageCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.delegate=self;
    }
    
    cell.rightUtilityButtons=[self rightButtons];
    
    InboxMesages *inboxMessage=[totalMessages objectAtIndex:indexPath.row];
    [cell setLayout:inboxMessage isEditAll:isEditAll isSelect:isSelect index:indexPath];
    
    return cell;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor: [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] title:@"DELETE"];
    
    
    return rightUtilityButtons;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isEditAll) {
        isSelect=NO;
        [self selectMessagae:indexPath];
    }
    else
    {
        ChatViewController *chatViewController=[[ChatViewController alloc] initWithNibName:@"SPHViewController" bundle:nil];
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        chatViewController.isFromMessageSection=YES;
        chatViewController.inboxMessage=[totalMessages objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:chatViewController animated:YES];
    }
    
}

#pragma mark - Scroll View Delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    isSelect=NO;
}

#pragma mark - IBActions
-(IBAction)composeButtonAction
{
    isEditAll=NO;
    ComposeMessageController *compose=[self.storyboard instantiateViewControllerWithIdentifier:@"ComposeMessageController"];
    [self.navigationController pushViewController:compose animated:YES];
}
-(IBAction)menuButtonAction
{
    if (!isEditAll) {
        [self presentLeftMenuViewController:nil];
    }
    else
    {
        isEditAll=NO;
        [_messagesTable reloadData];
        isSelect=NO;
        [_editButton setTitle:@"EDIT" forState:UIControlStateNormal];
      
        [self updateUnreadCount:[NSString stringWithFormat:@"%d Unread",totalMessageCount]];
        _unreadMessageText.hidden=NO;
        [self updateMenuIcon];
       
        [self resetMessagesSelection];
    }
}

-(IBAction)editAllMessageAction:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (isEditAll) {
        isEditAll=NO;
        [_messagesTable reloadData];
        isSelect=NO;
        [btn setTitle:@"EDIT" forState:UIControlStateNormal];
        [self doneClicked];
        [self updateMenuIcon];
       
        if ([totalMessages count]>0) {
          
            [self updateUnreadCount:[NSString stringWithFormat:@"%d Unread",totalMessageCount]];
            _unreadMessageText.hidden=NO;
            
        }
        else
        {
            _unreadMessageText.hidden=YES;
        }
        
    }
    else
    {
        isEditAll=YES;
        isSelect=YES;
        [_menuButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        [self updateUnreadCount:@"0 Selected"];
        [_messagesTable reloadData];
        
        [btn setTitle:@"DELETE" forState:UIControlStateNormal];
    }
}

-(IBAction)selectAMessageAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.messagesTable];
    NSIndexPath *indexPath = [self.messagesTable indexPathForRowAtPoint:buttonPosition];
    isSelect=NO;
    [self selectMessagae:indexPath];
}

#pragma mark -
#pragma mark - Check Uncheck Selected Row Method

-(void)selectMessagae:(NSIndexPath *)indexPath
{
    InboxMesages *messageInfo=[totalMessages objectAtIndex:indexPath.row];
    
    BOOL isSeleted=messageInfo.isSelect;
    if (isSeleted){
    }
    
    if (isSeleted){
        
        messageInfo.isSelect=NO;
    }
    else{
        messageInfo.isSelect=YES;
    }
    
    [totalMessages replaceObjectAtIndex:indexPath.row withObject:messageInfo];
    [self updateUnreadCount:[self selectedMessageCount]];
    [_messagesTable reloadData];
}

-(NSString *)selectedMessageCount
{
    NSUInteger count = 0;
    for (int i=0; i<[totalMessages count]; i++)
    {
        InboxMesages *messageInfo=[totalMessages objectAtIndex:i];
        BOOL isSelected =messageInfo.isSelect;
        if (isSelected)
        {
            count++;
        }
    }
    return [NSString stringWithFormat:@"%lu Selected",(unsigned long)count];
}

#pragma mark -
#pragma mark - Done Method

- (void)doneClicked
{
    NSMutableIndexSet *discardedItems = [NSMutableIndexSet indexSet];
    NSMutableArray *messageIds=[NSMutableArray array];
    
    for (int i=0; i<[totalMessages count]; i++)
    {
        InboxMesages *messageInfo=[totalMessages objectAtIndex:i];
        BOOL isSelected =messageInfo.isSelect;
        if (isSelected)
        {
            [discardedItems addIndex:i];
            [messageIds addObject:messageInfo.sender_id];
            
        }
    }
    if ([messageIds count]>0) {
        [self deleteMessage:messageIds];
    }
    [totalMessages removeObjectsAtIndexes:discardedItems];
    [self hideBottomElement];
    [_messagesTable reloadData];
}
-(void)resetMessagesSelection
{
    for (int i=0; i<[totalMessages count]; i++)
    {
        InboxMesages *messageInfo=[totalMessages objectAtIndex:i];
        messageInfo.isSelect=NO;
        [totalMessages replaceObjectAtIndex:i withObject:messageInfo];
        
    }
    [_messagesTable reloadData];
    
}

#pragma mark -
#pragma mark - Display Left Side Buttons

-(void)displaAllLeftButtons
{
    NSMutableArray *cells = [[NSMutableArray alloc] init];
    for (NSInteger j = 0; j < [_messagesTable numberOfSections]; ++j)
    {
        for (NSInteger i = 0; i < [_messagesTable numberOfRowsInSection:j]; ++i)
        {
            [cells addObject:[_messagesTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:j]]];
        }
    }
    for (SWTableViewCell *cell in cells)
    {
        [cell showLeftUtilityButtonsAnimated:YES];
    }
}

#pragma mark - SWTableView Delegate Methods
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *cellIndexPath = [_messagesTable indexPathForCell:cell];
    slctdIndex = (int)cellIndexPath.row;
    switch (index)
    {
        case 0:
        {
            NSLog(@"edit button was pressed");
            // share button was pressed ...
            [self updateMessageCount:cellIndexPath];
            [cell hideUtilityButtonsAnimated:YES];
            [self deleteCellFromIndexPath:cellIndexPath];
            [self hideBottomElement];
            
        }
            break;
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    
    return YES;
}
- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            if (isEditAll) {
                return NO;
            }
            
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}

-(void)showLeftUtilityButtonsAnimated:(BOOL)animated
{
    
}
#pragma mark deleet from cell
-(void)updateMessageCount:(NSIndexPath *)indexPath
{
    InboxMesages *inboxMessage=[totalMessages objectAtIndex:indexPath.row];
    NSString *unreadCounts=inboxMessage.unreadMessageCount;
    if ([unreadCounts isEqualToString:@"0"]) {
        return;
    }
    totalMessageCount=totalMessageCount-[unreadCounts intValue];
    [self updateUnreadCount:[NSString stringWithFormat:@"%d Unread",totalMessageCount]];
    
    
    
    [UserInfoEntity updateRequestAndmessagesCounts:@"" messagesCounts:unreadCounts tripRequestCounts:@"" inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
     [self updateMenuIcon];
}
-(void)deleteCellFromIndexPath:(NSIndexPath *)indexPath
{
    [_messagesTable beginUpdates];
    InboxMesages *inboxMessage=[totalMessages objectAtIndex:indexPath.row];
    [self deleteMessage:[NSMutableArray arrayWithObject:inboxMessage.sender_id]];
    
    NSArray *deletedIndexPaths = [[NSArray alloc] initWithObjects:
                                  [NSIndexPath indexPathForRow:indexPath.row inSection:0],
                                  nil];
    [_messagesTable deleteRowsAtIndexPaths:deletedIndexPaths withRowAnimation:UITableViewRowAnimationFade];
    [totalMessages removeObjectAtIndex:indexPath.row];
    [_messagesTable endUpdates];
   
}


@end
