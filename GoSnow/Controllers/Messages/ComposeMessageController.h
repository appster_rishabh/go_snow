//
//  ComposeMessageController.h
//  GoSnow
//
//  Created by Rishabh on 19/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageManager.h"
@interface ComposeMessageController : UIViewController<MessageManagerDelegate>
{
    BOOL iskeyboard;
    NSMutableArray *contactListArray;
}

@property(nonatomic,weak)IBOutlet UITableView *composeTableView;
@property(nonatomic,weak)IBOutlet UISearchBar *searchBar;
-(IBAction)backButtonAction:(id)sender;
@end
