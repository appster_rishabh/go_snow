//
//  ChatViewController.m
//  GoSnow
//
//  Created by Rishabh on 10/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//


#import "ChatViewController.h"
#import "Messages.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "SPHBubbleCell.h"
#import "SPHBubbleCellOther.h"
#import "UIColor+Color.h"

@interface ChatViewController ()

@end

@implementation ChatViewController

@synthesize reloads = reloads_;

@synthesize Uploadedimage,inboxMessage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    allMessages=[[NSMutableArray alloc]init];
    [self fetchUserInfoFromCoreData];
    [self setUpUsername];
    [self setUpTextFieldforIphone];
    [self getHistory];
    [self updateMessageCount];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    
    //  [_chatTable setBackgroundColor:[UIColor redColor]];
    messageTimer= [NSTimer scheduledTimerWithTimeInterval:1.0
                                                   target: self
                                                 selector:@selector(updateChatOnTimer)
                                                 userInfo: nil repeats:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    if (messageTimer) {
        [messageTimer invalidate];
        messageTimer=nil;
    }
}
#pragma mark update messagesCount
-(void)updateMessageCount
{
    NSString *unreadCounts=inboxMessage.unreadMessageCount;
    if ([unreadCounts isEqualToString:@"0"]) {
        return;
    }
    [UserInfoEntity updateRequestAndmessagesCounts:@"" messagesCounts:unreadCounts tripRequestCounts:@"" inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}
#pragma mark back button action
-(IBAction)backButtonAction:(id)sender
{
    if (!_isFromMessageSection) {
        NSArray *totalViewCOntroller=self.navigationController.viewControllers;
        [self.navigationController popToViewController:[totalViewCOntroller objectAtIndex:[totalViewCOntroller count]-3] animated:YES];
        
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)fetchUserInfoFromCoreData
{
    userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}

-(void)setUpUsername
{
    UILabel *userName=(UILabel *)[self.view viewWithTag:201];
    UIImageView *userImage=(UIImageView *)[self.view viewWithTag:202];
    //202
    
    if (inboxMessage.firstName.length>0 && inboxMessage.lastName.length>0) {
        userName.text=[NSString stringWithFormat:@"%@ %@",inboxMessage.firstName,inboxMessage.lastName];
    }
    else
    {
        userName.text=[NSString stringWithFormat:@"%@",inboxMessage.firstName];
    }
    userImage.layer.cornerRadius=20.0f;
    userImage.layer.masksToBounds=YES;
    if (inboxMessage.profilePic.length<1) {
       [userImage setImage:[UIImage imageNamed:@"smallProfile"]];
    }
    else
    {
      [userImage setImageWithURL:[NSURL URLWithString:inboxMessage.profilePic] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    
    // userName.text=_inboxMessage.firstName;
}

-(void)setUpTextFieldforIphone
{
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height-50, 320, 50)];
    containerView.backgroundColor=[UIColor colorWithRed:(206/255.0f) green:(206/255.0f) blue:(206/255.0f) alpha:1.0];
    textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(5, 8, 260, 50)];
    textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    textView.placeholder=@"Type message";
    textView.minNumberOfLines = 1;
    textView.maxNumberOfLines = 6;
    textView.returnKeyType = UIReturnKeyDefault; //just as an example
    textView.font = [UIFont fontWithName:@"RobotoCondensed-Light" size:16.0f];
    textView.delegate = self;
    textView.internalTextView.font=[UIFont fontWithName:@"RobotoCondensed-Light" size:16.0f];
    textView.internalTextView.textColor=[UIColor colorwithHexString:@"#011129" alpha:1.0f];
    textView.internalTextView.autocorrectionType=UITextAutocorrectionTypeNo;
    textView.internalTextView.autocapitalizationType=UITextAutocapitalizationTypeSentences;
    textView.internalTextView.spellCheckingType=UITextSpellCheckingTypeNo;
   textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containerView];
    
    UIImage *rawEntryBackground = [UIImage imageNamed:@"MessageEntryInputField.png"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.frame = CGRectMake(10, 0,240, 40);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = [UIImage imageNamed:@"MessageEntryBackground.png"];
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [containerView addSubview:textView];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(containerView.frame.size.width - 40, 13, 35, 25);
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    //[doneBtn setTitle:@"send" forState:UIControlStateNormal];
    
    [doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    doneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(resignTextView) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setImage:[UIImage imageNamed:@"send_icon"] forState:UIControlStateNormal];
    [doneBtn setImage:[UIImage imageNamed:@"send_icon"] forState:UIControlStateSelected];
    [containerView addSubview:doneBtn];
    
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    //Code Review -- we are not using both the images anywhere
}

-(void)resignTextView{
    NSString *messagetext= [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([messagetext length]<1) {
        
    }
    else
    {
        NSData *data = [messagetext dataUsingEncoding:NSNonLossyASCIIStringEncoding];
        
        NSString *encodedString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        textView.text=@"";
        NSDate *date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"hh:mm a"];
        [self adddBubbledata:ktextByme mtext:messagetext mtime:[formatter stringFromDate:date] mimage:userInfoEntity.profileThumb msgstatus:kStatusSeding];
        
        [self sentMessageWebservice:encodedString date:nil];
    }
    
}

-(void)messageSent:(id)sender
{
    NSLog(@"row= %@", sender);
    Messages *messageData=[allMessages objectAtIndex:[sender intValue]];
    messageData.messageStatus=@"Sent";
    [allMessages addObject:messageData];
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:[sender intValue] inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.chatTable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}
-(void)uploadToServer
{
    NSString *chat_Message=textView.text;
    textView.text=@"";
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"hh:mm a"];
    
    NSString *rowNumber=[NSString stringWithFormat:@"%d",(int)allMessages.count];
    
    if (allMessages.count%2==0) {
        [self adddBubbledata:kImageByme mtext:chat_Message mtime:[formatter stringFromDate:date] mimage:Uploadedimage.image msgstatus:kStatusSeding];
    }else{
        [self adddBubbledata:kImageByOther mtext:chat_Message mtime:[formatter stringFromDate:date] mimage:Uploadedimage.image msgstatus:kStatusSeding];
        
    }
    
    [self performSelector:@selector(messageSent:) withObject:rowNumber afterDelay:1.0];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [allMessages count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //code review --we do not need to alloc init for this model class we can type cast this model as it has all the objects in allMessages array
    Messages *messageData=[allMessages objectAtIndex:indexPath.row];
    
    if ([messageData.messageType isEqualToString:ktextByme]||[messageData.messageType isEqualToString:ktextbyother])
    {
        float cellHeight;
        
        CGSize messageTextSize=[self getSizeForAboutText:messageData.messageText maxWidth:messageWidth-20 font:[UIFont fontWithName:@"RobotoCondensed-Light" size:14.0f]];
        
        cellHeight = messageTextSize.height;
        
        if (cellHeight<25) {
            
            cellHeight=30;
        }
        return cellHeight+30;
    }
    else{
        return 140;
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.chatTable deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    Messages *messageData=[allMessages objectAtIndex:indexPath.row];
    //code review --we do not need to alloc init for this model class we can type cast this model as it has all the objects in allMessages array
    
    static NSString *CellIdentifier1 = @"Cell1";
    static NSString *CellIdentifier2 = @"Cell2";
    
    
    if ([messageData.messageType isEqualToString:ktextByme])
    {
        SPHBubbleCellOther  *cell = (SPHBubbleCellOther *)[self.chatTable dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SPHBubbleCellOther" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            //[cell.bubbleImage addSubview:cell.messageText];
        }
        
        [cell SetCellData:messageData targetedView:self Atrow:indexPath.row];
        
        
        return cell;
    }
    
    if ([messageData.messageType isEqualToString:ktextbyother])
    {
        SPHBubbleCell  *cell = (SPHBubbleCell *)[self.chatTable dequeueReusableCellWithIdentifier:CellIdentifier2];
        
        if (cell == nil)
        {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SPHBubbleCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            // [cell.bubbleImage addSubview:cell.messageText];
        }
        
        [cell SetCellData:messageData targetedView:self Atrow:indexPath.row];
        
        return cell;
        
    }
    return nil;
}

-(void)adddBubbledata:(NSString*)messageType  mtext:(NSString*)messagetext mtime:(NSString*)messageTime mimage:(NSString*)userImageUrl  msgstatus:(NSString*)status;
{
    Messages *messageData=[[Messages alloc]init];
    messageData.messageText=messagetext;
    messageData.recieverImageUrl=userImageUrl;
    // messageData.messageImage=messageImage;
    messageData.messageTime=messageTime;
    messageData.messageType=messageType;
    messageData.messageStatus=status;
    
    
    NSArray *insertIndexPaths = [NSArray arrayWithObject:
                                 [NSIndexPath indexPathForRow:
                                  [allMessages count]
                                                    inSection:0]];
    
    [allMessages addObject:messageData];
    //[self.chatTable scrollsToTop];
    [[self chatTable] insertRowsAtIndexPaths:insertIndexPaths
                            withRowAnimation:UITableViewRowAnimationNone];
    ///[[self chatTable] reloadData];
    [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.1];
}

-(void)adddBubbledataatIndex:(NSInteger)rownum messagetype:(NSString*)messageType  mtext:(NSString*)messagetext mtime:(NSString*)messageTime mimage:(NSString*)userImageUrl  msgstatus:(NSString*)status;
{
    Messages *messageData=[[Messages alloc]init];
    messageData.messageText=messagetext;
    // messageData.messageImageURL=messagetext;
    messageData.senderImageUrl=userImageUrl;
    messageData.messageTime=messageTime;
    messageData.messageType=messageType;
    messageData.messageStatus=status;
    [allMessages  replaceObjectAtIndex:rownum withObject:messageData];
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:rownum inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.chatTable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    
    [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.1];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}


-(void)scrollTableview
{
    if (allMessages.count<3)
        return;
    
    NSInteger lastSection=0;
    NSInteger lastRowNumber = [allMessages count]-1;
    NSIndexPath* ip = [NSIndexPath indexPathForRow:lastRowNumber-1 inSection:lastSection];
    [self.chatTable scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark Keybaord show/hide method
-(void) keyboardWillShow:(NSNotification *)note
{
    if (allMessages.count>2)
        [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.0];
    
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    
    CGRect tableviewframe=self.chatTable.frame;
    if (!iskeyboardShown) {
        tableviewframe.size.height-=keyboardBounds.size.height;
        
    }
    iskeyboardShown=YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    containerView.frame = containerFrame;
    self.chatTable.frame=tableviewframe;
    
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note
{
    iskeyboardShown=NO;
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
    CGRect tableviewframe=self.chatTable.frame;
    tableviewframe.size.height+=keyboardBounds.size.height;
    
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    // set views with new info
    self.chatTable.frame=tableviewframe;
    containerView.frame = containerFrame;
    // commit animations
    [UIView commitAnimations];
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
    containerView.frame = r;
}
#pragma mark fetch data web service
-(void)getHistory
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:inboxMessage.sender_id forKey:@"selectedUserId"];
    //selectedUserId
    [MessageManager sharedInstance].delegate=self;
    [[MessageManager sharedInstance] callWebService:params methodName:@"tripMessage/getMyMessages"];
    
}

#pragma mark fetch data web service
-(void)updateChatOnTimer
{
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:inboxMessage.sender_id forKey:@"selectedUserId"];
    //selectedUserId
    [[MessageManager sharedInstance] callWebServiceOnTimer:params methodName:@"tripMessage/getMyMessages"];
    [MessageManager sharedInstance].delegate=self;
}

#pragma mark read message web service
-(void)readMessageWebservice:(Messages *)messageData
{
    BOOL isRead=[messageData.messageStatus boolValue];
    if (isRead) {
        return;
    }
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:inboxMessage.sender_id forKey:@"senderId"];
    
    [params setObject:messageData.messageId forKey:@"tripMessageReceiverId"];
    [MessageManager sharedInstance].delegate=self;
    [[MessageManager sharedInstance] callWebService:params methodName:@"tripMessage/markAsRead"];
}

#pragma mark sent message web service
-(void)sentMessageWebservice:(NSString *)message date:(NSString *)date
{
    NSData *textdata=[message dataUsingEncoding:NSUTF8StringEncoding];
    NSString *baseString=[textdata base64EncodedStringWithOptions:0];
    
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:baseString forKey:@"message"];
    [params setObject:inboxMessage.sender_id forKey:@"receiverId"];
    [MessageManager sharedInstance].delegate=self;
    [[MessageManager sharedInstance] callWebService:params methodName:@"tripMessage/add"];
}

#pragma mark web services delegate
- (void)getAllMessageSuccessfully:(NSMutableArray *)messageArray
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if ([messageArray count]==0) {
        return;
    }
    Messages *messageData=[messageArray lastObject];
    [self readMessageWebservice:messageData];
    int lastMessageIds=[messageData.messageId intValue];
    if (lastMessageIds==lastMessageId)
    {
        return;
    }
    lastMessageId=[messageData.messageId intValue];
    [allMessages removeAllObjects
     ];
    [allMessages addObjectsFromArray:messageArray];
    
    [_chatTable reloadData];
    [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.1];
    
}
- (void)messageSentSuccessfully:(NSDictionary *)dictionary
{
    
}
- (void)requestDidFailed:(NSDictionary *)errorDictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:[errorDictionary valueForKey:@"Error"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alert show];
}
-(IBAction)editViewAction:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark hide/show label
-(void)showLabel
{
    // _labelPleaseEnter.hidden=NO;
}

-(void)hideLabel
{
    //_labelPleaseEnter.hidden=YES;
}
#pragma mark calculate text size

- (CGSize)getSizeForAboutText:(NSString *)text maxWidth:(CGFloat)width font:(UIFont *)font  {
    NSArray *arr=[NSArray arrayWithObject:text];
    CGSize DXsize = [[arr objectAtIndex:0] sizeWithFont:font constrainedToSize:CGSizeMake(width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    return DXsize;
}
@end

