//
//  MessageCell.m
//  GoSnow
//
//  Created by Rishabh on 09/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "MessageCell.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import <QuartzCore/QuartzCore.h>
@implementation MessageCell

- (void)awakeFromNib {
    // Initialization code
}
-(void)setLayout:(InboxMesages *)messageInfo isEditAll:(BOOL)isEditAll isSelect:(BOOL)isSelect index:(NSIndexPath *)path
{
    
    UIView *view=(UIView *)[self viewWithTag:1001];
    if (isSelect) {
        if (isEditAll) {
            [UIView animateWithDuration:0.2
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 view.frame=CGRectMake(46, 0, view.frame.size.width, view.frame.size.height);
                                 
                                 
                             }
             
                             completion:^(BOOL finished){
                                 
                             }];
            
            
        }
        else
        {
            [UIView animateWithDuration:0.2
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 
                                 view.frame=CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
                                 
                             }
             
                             completion:^(BOOL finished){
                                 
                             }];
            
            
        }
        
    }
    else
    {
        if (isEditAll) {
            
            view.frame=CGRectMake(46, 0, view.frame.size.width, view.frame.size.height);
        }
        else
        {
            view.frame=CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
            
        }
        
    }
    UIImageView *userImage=(UIImageView *)[self viewWithTag:101];
    UIImageView *readImage=(UIImageView *)[self viewWithTag:107];
    UILabel *userName=(UILabel *)[self viewWithTag:102];
    UILabel *message=(UILabel *)[self viewWithTag:103];
    UILabel *messageTime=(UILabel *)[self viewWithTag:104];
    UIButton *checkUncheck=(UIButton *)[self viewWithTag:105];
    if (messageInfo.profilePic.length>0) {
        [userImage setImageWithURL:[NSURL URLWithString:messageInfo.profilePic] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [userImage setImage:[UIImage imageNamed:@"smallProfile"]];
    }
    userImage.layer.cornerRadius=25.0f;
    userImage.layer.masksToBounds=YES;
    messageTime.text=messageInfo.messageDate;
    //timeElapsed
    if (messageInfo.firstName.length>0 && messageInfo.lastName.length>0) {
        userName.text=[NSString stringWithFormat:@"%@ %@",messageInfo.firstName,messageInfo.lastName];
    }
    else
    {
        userName.text=[NSString stringWithFormat:@"%@",messageInfo.firstName];
    }
    
    BOOL isread=[messageInfo.messageStatus boolValue];
    if (!isread) {
        readImage.hidden=NO;
        message.frame=CGRectMake(message.frame.origin.x+12, message.frame.origin.y, message.frame.size.width, message.frame.size.height);
    }
    else
    {
        readImage.hidden=YES;
    }
   
   // DLog(@"Encode String Value: %@", base64String);
    message.text=messageInfo.userMessage;
    if (isEditAll) {
        BOOL isSeleted=messageInfo.isSelect;
        if (isSeleted)
            [checkUncheck setImage:[UIImage imageNamed:@"active_selector.png"] forState:UIControlStateNormal];
        
        else
            [checkUncheck setImage:[UIImage imageNamed:@"selector.png"] forState:UIControlStateNormal];
    }
    self.selectionStyle=UITableViewCellSelectionStyleNone;
}
-(void)editSingleRow:(NSMutableDictionary *)dict isedit:(BOOL)isEdit indexPath:(NSIndexPath *)indexPath
{
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
