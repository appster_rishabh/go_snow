//
//  ContactListCell.h
//  GoSnow
//
//  Created by Rishabh on 19/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Users.h"
#import "InboxMesages.h"
@interface ContactListCell : UITableViewCell
-(void)setLayout:(InboxMesages *)userInfo isSelect:(BOOL)isSelect index:(NSIndexPath *)path;
@end
