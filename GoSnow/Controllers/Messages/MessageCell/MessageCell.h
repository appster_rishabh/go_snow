//
//  MessageCell.h
//  GoSnow
//
//  Created by Rishabh on 09/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "InboxMesages.h"
@interface MessageCell : SWTableViewCell

-(void)setLayout:(InboxMesages *)messageInfo isEditAll:(BOOL)isEditAll isSelect:(BOOL)isSelect index:(NSIndexPath *)path;
-(void)editSingleRow:(InboxMesages *)messageInfo isedit:(BOOL)isEdit indexPath:(NSIndexPath *)indexPath;
@end
