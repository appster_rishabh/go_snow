//
//  ContactListCell.m
//  GoSnow
//
//  Created by Rishabh on 19/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ContactListCell.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import <QuartzCore/QuartzCore.h>
@implementation ContactListCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setLayout:(InboxMesages *)userInfo isSelect:(BOOL)isSelect index:(NSIndexPath *)path
{
    UIImageView *userImage=(UIImageView *)[self viewWithTag:101];
    userImage.layer.cornerRadius=userImage.frame.size.width/2;
    userImage.layer.masksToBounds=YES;
    UILabel *userName=(UILabel *)[self viewWithTag:102];
    if (userInfo.profilePic.length<1) {
        [userImage setImage:[UIImage imageNamed:@"smallProfile"]];
    }
    else
    {
       [userImage setImageWithURL:[NSURL URLWithString:userInfo.profilePic] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }

       if (userInfo.firstName.length>0 && userInfo.lastName.length>0) {
        userName.text=[NSString stringWithFormat:@"%@ %@",userInfo.firstName,userInfo.lastName];
    }
    else
    {
        userName.text=[NSString stringWithFormat:@"%@",userInfo.firstName];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
