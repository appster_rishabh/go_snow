//
//  MessagesViewController.h
//  GoSnow
//
//  Created by Rishabh on 25/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageManager.h"
#import "SWTableViewCell.h"
#import "RESideMenu.h"
@interface MessagesViewController : UIViewController<MessageManagerDelegate,SWTableViewCellDelegate>
{
    BOOL isEditAll;
    BOOL isSelect;
    UserInfoEntity *userInfoEntity;
    int slctdIndex;
    NSMutableArray *totalMessages;
    int totalMessageCount;
    
}
@property(nonatomic,strong)UIRefreshControl *refreshControl;
@property(nonatomic,weak)IBOutlet UITableView *messagesTable;
@property(nonatomic,weak)IBOutlet UILabel *unreadMessageText;
@property(nonatomic,weak)IBOutlet UIButton *editButton;
@property(nonatomic,weak)IBOutlet UIButton *menuButton;
@property(nonatomic,weak)IBOutlet UILabel *messageHeaderText;
@property(nonatomic,weak)IBOutlet UIView *noMessageFoundView;
//code review --no use to give strong property for this object as we are already giving memory for this array
-(IBAction)editAllMessageAction:(id)sender;
-(IBAction)selectAMessageAction:(id)sender;
-(IBAction)composeButtonAction;
-(IBAction)menuButtonAction;
@end
