//
//  SPHBubbleself.m
//  ChatBubble
//
//  Created by ivmac on 10/2/13.
//  Copyright (c) 2013 Conciergist. All rights reserved.
//

#import "SPHBubbleCell.h"
#import "RobotCondensedLightLabel.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@implementation SPHBubbleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)SetCellData:(Messages *)messageData targetedView:(id)ViewControllerObject Atrow:(NSInteger)indexRow;
{
    NSString *messageText = messageData.messageText;
    /*CGSize boundingSize = CGSizeMake(messageWidth-20, 10000000);
    CGSize itemTextSize = [messageText  sizeWithFont:[UIFont fontWithName:@"RobotoCondensed-Light" size:12.0]
                                   constrainedToSize:boundingSize
                                       lineBreakMode:NSLineBreakByWordWrapping];*/
    CGSize itemTextSize=[self getSizeForText:messageText maxWidth:messageWidth-20 font:[UIFont fontWithName:@"RobotoCondensed-Light" size:14.0]];
    float textHeight = itemTextSize.height+10;
    int x=0;
    if (textHeight>200)
    {
        x=65;
    }else
        if (textHeight>150)
        {
            x=50;
        }
        else if (textHeight>80)
        {
            x=30;
        }else
            if (textHeight>50)
            {
                x=20;
            }else
                if (textHeight>30) {
                    x=8;
                }
    UIImageView *bubbleImage=[[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"blue_msg"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]];
    [self.contentView addSubview:bubbleImage];
    [bubbleImage setFrame:CGRectMake(46,12, itemTextSize.width+23, textHeight+8)];
    bubbleImage.tag=56;
    //CGRectMake(260 - itemTextSize.width+5,2,itemTextSize.width+10, textHeight-2)];
    RobotCondensedLightLabel *messageTextview=[[RobotCondensedLightLabel alloc]initWithFrame:CGRectMake(12,4,itemTextSize.width, textHeight)];
    [bubbleImage addSubview:messageTextview];
    
       messageTextview.numberOfLines=0;
    messageTextview.text = messageText;
    
    // messageTextview.dataDetectorTypes=UIDataDetectorTypeAll;
    messageTextview.textAlignment=NSTextAlignmentJustified;
    messageTextview.backgroundColor=[UIColor clearColor];
    messageTextview.font=[UIFont fontWithName:@"RobotoCondensed-Light" size:14.0];
    //messageTextview.scrollEnabled=NO;
    messageTextview.tag=indexRow;
    messageTextview.textColor=[UIColor whiteColor];
    
    self.Avatar_Image.frame=CGRectMake(self.Avatar_Image.frame.origin.x, bubbleImage.frame.size.height+12-self.Avatar_Image.frame.size.height,self.Avatar_Image.frame.size.width,self.Avatar_Image.frame.size.height);
    self.Avatar_Image.layer.cornerRadius = 20.0;
    self.Avatar_Image.layer.masksToBounds = YES;
   // self.Avatar_Image.layer.borderColor = [UIColor whiteColor].CGColor;
    //self.Avatar_Image.layer.borderWidth = 2.0;
    if (messageData.recieverImageUrl.length>0) {
        [self.Avatar_Image setImageWithURL:[NSURL URLWithString:messageData.recieverImageUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    else
    {
        [self.Avatar_Image setImage:[UIImage imageNamed:@"smallProfile"]];
    }
    
    // self.Avatar_Image.image=[UIImage imageNamed:@"my_icon"];
    self.time_Label.text=messageData.messageTime;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
}
#pragma mark get text size
- (CGSize)getSizeForText:(NSString *)text maxWidth:(CGFloat)width font:(UIFont *)font
{
    NSArray *arr=[NSArray arrayWithObject:text];
    CGSize DXsize = [[arr objectAtIndex:0] sizeWithFont:font constrainedToSize:CGSizeMake(width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    return DXsize;
}

@end
