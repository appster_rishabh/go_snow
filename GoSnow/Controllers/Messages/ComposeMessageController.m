//
//  ComposeMessageController.m
//  GoSnow
//
//  Created by Rishabh on 19/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ComposeMessageController.h"
#import "ContactListCell.h"
#import "Users.h"
#import "ChatViewController.h"
@interface ComposeMessageController ()

@end

@implementation ComposeMessageController

- (void)viewDidLoad {
    [super viewDidLoad];
    contactListArray=[[NSMutableArray alloc] init];
    [self removeBackgroundImagesFromSearcbar];
    [self fetchContactListWebservice:@"" isSearch:NO];
}

-(void)removeBackgroundImagesFromSearcbar
{
    NSArray *subview=_searchBar.subviews;
    UIView *subsubviews=[subview objectAtIndex:0];
    UITextField *txfSearchField = [_searchBar
                                   valueForKey:@"_searchField"];
    [txfSearchField setBackgroundColor:[UIColor clearColor]];
    
    for ( UIView * subview in subsubviews.subviews )
    {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground") ] )
            subview.alpha = 0.0;
        
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarTextField") ] )
        {
            UIView *textfieldbackgroundView=[[subview subviews] objectAtIndex:0];
            textfieldbackgroundView.alpha=0.0;
            subview.backgroundColor =[UIColor clearColor];
            
        }
    }
}

-(IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark fetch contact list web service
-(void)fetchContactListWebservice:(NSString *)searchText isSearch:(BOOL)isSearch
{
    if (!isSearch) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:searchText forKey:@"searchText"];
    [params setObject:sessionId forKey:@"sessionId"];
    [MessageManager sharedInstance].delegate=self;
    [[MessageManager sharedInstance] callWebService:params methodName:@"contact/search"];
}

#pragma mark response delegate
- (void)requestDidFailed:(NSDictionary *)errorDictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:[errorDictionary valueForKey:@"Error"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
-(void)getAllContactList:(NSMutableArray *)contactList
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [contactListArray removeAllObjects];
    [contactListArray addObjectsFromArray:contactList];
    [_composeTableView reloadData];
}

#pragma mark - TableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [contactListArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ContactListCell";
    ContactListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ContactListCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    InboxMesages *userInfo=[contactListArray objectAtIndex:indexPath.row];
    [cell setLayout:userInfo isSelect:userInfo.isSelect index:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatViewController *chatViewController=[[ChatViewController alloc] initWithNibName:@"SPHViewController" bundle:nil];
    
    chatViewController.inboxMessage=[contactListArray objectAtIndex:indexPath.row];
    chatViewController.isFromMessageSection=NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self.navigationController pushViewController:chatViewController animated:YES];
}

#pragma scrollview dragging delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (iskeyboard) {
        [self hideKeyboard];
    }
    
}
#pragma mark hide keyboard
- (void) hideKeyboard {
    iskeyboard=NO;
    [self.searchBar resignFirstResponder];
}

-(InboxMesages *)requiredData:(Users *)users
{
    InboxMesages *inboxMessage=[[InboxMesages alloc] init];
    inboxMessage.firstName=users.firstName;
    inboxMessage.lastName=users.lastName;
    inboxMessage.profilePic=users.profileImage;
    inboxMessage.sender_id=users.user_id;
    return inboxMessage;
}

#pragma mark Search Bar --------
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    // iskeyboard=YES;
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
    iskeyboard=YES;
    return;
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
    if([searchText length] > 0) {
        iskeyboard=YES;
        [self fetchContactListWebservice:theSearchBar.text isSearch:YES];
        
    }
    else {
        
        iskeyboard=YES;
        //   [self.notice_arr removeAllObjects];
        [self fetchContactListWebservice:theSearchBar.text isSearch:YES];
        [self.searchBar resignFirstResponder];
        
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    //  [self.notice_arr removeAllObjects];
    [self fetchContactListWebservice:theSearchBar.text isSearch:YES];   // iskeyboard=NO;
    [theSearchBar resignFirstResponder];
}


@end
