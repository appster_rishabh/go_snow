//
//  ChatViewController.h
//  GoSnow
//
//  Created by Rishabh on 10/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"
#import "MessageManager.h"
#import "InboxMesages.h"

@class QBPopupMenu;

@interface ChatViewController : UIViewController<HPGrowingTextViewDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MessageManagerDelegate>
{
    NSMutableArray *allMessages;
    UIView *containerView;
    HPGrowingTextView *textView;
    UserInfoEntity *userInfoEntity;
    int selectedRow;
    int lastMessageId;
    BOOL newMedia;
    BOOL iskeyboardShown;
    NSTimer *messageTimer;
}

@property(nonatomic,strong)InboxMesages *inboxMessage;
@property BOOL isFromMessageSection;
@property (nonatomic, readwrite, assign) NSUInteger reloads;
@property (weak, nonatomic) IBOutlet UIImageView *Uploadedimage;
@property (weak, nonatomic) IBOutlet UITableView *chatTable;

- (IBAction)editViewAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;

@end