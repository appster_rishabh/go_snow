//
//  UiViewCircle.m
//  GoSnow
//
//  Created by Varsha Singh on 22/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "UiViewCircle.h"

@implementation UiViewCircle

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)drawRect:(CGRect)rect {
    /* Draw a circle */
    // Get the contextRef
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    // Set the border width
    CGContextSetLineWidth(contextRef, 2.0);
    
    // Set the circle fill color to GREEN
    CGContextSetRGBFillColor(contextRef, 0.0, 0.0, 0.0, 0.0);
    
    // Set the cicle border color to BLUE
    CGContextSetRGBStrokeColor(contextRef, 0.0, 0.0, 255.0, 1.0);
    
    // Fill the circle with the fill color
    CGRect rectangle;
    for (int i=0; i<4; i++) {
        // CGContextFillEllipseInRect(contextRef, rect);
        rectangle = CGRectMake(5+(50*i),5+(50*i),rect.size.width-(100*i),rect.size.width-(100*i));
        // Draw the circle border
        CGContextStrokeEllipseInRect(contextRef, rectangle);
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame =CGRectMake(0,0,60,60);
        btn.layer.cornerRadius = 30.0f;
        [btn setBackgroundColor:[UIColor grayColor]];
        [btn setCenter:CGPointMake(rectangle.origin.x, rectangle.origin.y)];

    }

//    rectangle = CGRectMake(15,15,rect.size.width-30,rect.size.height-30);
//    // Draw the circle border
//    CGContextStrokeEllipseInRect(contextRef, rectangle);
}

@end
