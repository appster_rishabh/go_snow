//
//  InfoViewController.m
//  GoSnow
//
//  Created by Rishabh on 13/04/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "InfoViewController.h"


//
//  CodeVerificationViewController.m
//  Jirno
//
//  Created by Anand Mishra on 03/02/15.
//  Copyright (c) 2015 AppSter. All rights reserved.
//



@interface InfoViewController ()
{
    
}

@property (nonatomic, strong) UITextField* currentTextField;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *verificationView;

@property (weak, nonatomic) IBOutlet UILabel *headerlabel;

@property (weak, nonatomic) IBOutlet UITextField *nameTextfield;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

- (IBAction)cancelAction:(id)sender;
- (IBAction)sumbitAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll;

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nameTextfield.text = self.userName;
    if (self.isFirstName) {
        self.headerlabel.text=@"Enter First Name";
        _nameTextfield.placeholder=@"First Name";
    }
    else
    {
       self.headerlabel.text=@"Enter Last Name";
         _nameTextfield.placeholder=@"Last Name";
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    self.verificationView.layer.cornerRadius = 5.0;
    self.mainView.alpha  = 0.0;
    
    [self performSelector:@selector(viewBouncingEffectAnimation) withObject:nil afterDelay:.2];
    
    self.verificationView.frame = CGRectMake(self.verificationView.frame.origin.x, self.view.frame.size.height, self.verificationView.frame.size.width, self.verificationView.frame.size.height);
}





-(void)viewBouncingEffectAnimation
{
    
    [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:.5 initialSpringVelocity:.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        self.mainView.alpha = 0.8;
        
        self.verificationView.frame = CGRectMake(self.verificationView.frame.origin.x, self.view.frame.size.height-self.verificationView.frame.size.height - 100, self.verificationView.frame.size.width, self.verificationView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)cancelAction:(id)sender {
    
    
    [UIView animateWithDuration:.8 delay:0.0 usingSpringWithDamping:.5 initialSpringVelocity:.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        self.mainView.alpha = 0.0;
        
        self.verificationView.frame = CGRectMake(self.verificationView.frame.origin.x, self.view.frame.size.height, self.verificationView.frame.size.width, self.verificationView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        if ([self.delegate respondsToSelector:@selector(actionCancelled:)]) {
            
            [self.delegate actionCancelled:self.isFirstName];
        }
        
        
    }];
    
}

- (IBAction)sumbitAction:(id)sender {
    if (self.isFirstName) {
        if (self.nameTextfield.text.length<1) {
             UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"First name can't be blank." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            return;
        };
    }
    
    [UIView animateWithDuration:.8 delay:0.0 usingSpringWithDamping:.5 initialSpringVelocity:.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        self.mainView.alpha = 0.0;
        
        self.verificationView.frame = CGRectMake(self.verificationView.frame.origin.x, self.view.frame.size.height, self.verificationView.frame.size.width, self.verificationView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        if ([self.delegate respondsToSelector:@selector(nameEntered:isLastName:)]) {
            
           [self.delegate nameEntered:self.nameTextfield.text isLastName:self.isFirstName];
        }
        
        
    }];

    
    
    
}


-(void)showAlertView:(NSString*)messageString
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Jirno", nil) message:messageString preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", nil)
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}


-(void)doneTapped:(UIButton*)btn
{
    [self dismissAllkeyboard];
}

#pragma mark- Text field delegate-


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    self.currentTextField = textField;
    
    [self.scroll setContentSize:self.scroll.frame.size];
    CGSize contentSize = self.scroll.contentSize;
    contentSize.height += (215.0 + 60.0); // keyboard height
    [self.scroll setContentSize:contentSize];
    CGRect rect = textField.frame;
    CGRect scrollViewFrame = self.scroll.frame;
    scrollViewFrame.size.height -= 215+75.0;
    if (scrollViewFrame.size.height < self.verificationView.frame.origin.y + rect.origin.y + rect.size.height - self.scroll.contentOffset.y)
    {
        // if textview is under keyboard
        CGPoint contentoffset = CGPointMake(0,(rect.origin.y + rect.size.height - scrollViewFrame.size.height- self.scroll.contentOffset.y + self.verificationView.frame.origin.y) + 20);
        [self.scroll setContentOffset:contentoffset animated:YES];
    }
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
  //  [textField setInputAccessoryView:[self gettoolbar]];
    return YES;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
   // [textView setInputAccessoryView:[self gettoolbar]];
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self.scroll setContentSize:self.scroll.frame.size];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.length==1) {
        return YES;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 20) ? NO : YES;
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self dismissAllkeyboard];
}

-(void)dismissAllkeyboard
{
    [self.currentTextField resignFirstResponder];
}

@end

