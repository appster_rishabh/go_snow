//
//  ViewUserProfileViewController.h
//  GoSnow
//
//  Created by Rishabh on 26/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "Users.h"
#import "PagedFlowView.h"
#import "RiderManager.h"

typedef enum {
    MyprofileScreenType,
    SuggestedRiderScreenType,
    RequestRiderScreenType,
    ContactSnowBuddiesScreenType,
    GroupBuddiesScreenType
}   FromScreenType;
@interface ViewUserProfileViewController : UIViewController<RESideMenuDelegate,PagedFlowViewDataSource,PagedFlowViewDelegate,RiderManagerDelegate>
{
    NSMutableArray *totalPhotosArray;
    NSInteger selectedPhotoIndex;
}
@property (nonatomic) FromScreenType fromScreenType;;
@property BOOL isMyProfile;
@property BOOL isFromRequestScreen;;
@property (nonatomic,strong)IBOutlet PagedFlowView *photosHorizontalView;
@property (nonatomic,weak)IBOutlet UIScrollView *infoScrollView;
@property (nonatomic,weak)IBOutlet UIScrollView *frndsScrollView;
@property (nonatomic,weak)IBOutlet UIScrollView *interestScrollView;
@property (nonatomic,weak)IBOutlet UIScrollView *locationsScrollView;
@property (nonatomic,weak)IBOutlet UIPageControl *horizontalPageControl;
@property (nonatomic,weak)IBOutlet UIView *sharedView;
@property(nonatomic,strong)Users *otherUsersObject;
//code review no need to give strong property for this array as we are already allocated
-(IBAction)pageControllerValueChangeAction:(id)sender;
-(IBAction)visitUserProfileAction;
-(IBAction)backButtonAction;
-(IBAction)mayBeLaterAction;
-(IBAction)rideTogetherAction;
-(IBAction)flagRiderProfileAction;

@end
