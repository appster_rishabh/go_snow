//
//  ViewUserProfileViewController.m
//  GoSnow
//
//  Created by Rishabh on 26/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ViewUserProfileViewController.h"
#import "ProfilePic.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "MyProfileDetailViewController.h"
#import "RiderManager.h"

@interface ViewUserProfileViewController ()

@end

@implementation ViewUserProfileViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updateDots];
    
}
#pragma mark viewWillAppear method
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self fillUsersInformation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenuIconByNotification) name:KNotificationRecieved object:nil];
}

#pragma mark viewWillDisappear method
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KNotificationRecieved object:nil];
}
#pragma mark updateMenuIconByNotification method
-(void)updateMenuIconByNotification
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:511];
    [menuIcon setImage:[UIImage imageNamed:@"menu_white_dot"] forState:UIControlStateNormal];
}
#pragma mark updateMenuIcon method
-(void)updateMenuIcon
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:511];
    int notificationCount=[UserInfoEntity fetchPendingRequestsAndMessagesCountsinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    if (notificationCount>0) {
        [menuIcon setImage:[UIImage imageNamed:@"menu_white_dot"] forState:UIControlStateNormal];
    }
    else
    {
        [menuIcon setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    }
}

-(void)initialSetUp
{
    selectedPhotoIndex = 0;
    [self.navigationController.navigationBar setHidden:YES];
    
    totalPhotosArray=[[NSMutableArray alloc] init];
    _photosHorizontalView.delegate = self;
    _photosHorizontalView.dataSource = self;
    _photosHorizontalView.pageControl = _horizontalPageControl;
    _photosHorizontalView.minimumPageAlpha = 0.3;
    _photosHorizontalView.minimumPageScale = 0.9;
    _infoScrollView.contentSize=CGSizeMake(WIDTH, HEIGHT+150);
    
}

#pragma mark -  Get Other Rider's Data
-(void)fillUsersInformation
{
    UILabel *labelName = (UILabel *)[self.view viewWithTag:501];
    UILabel *labelRiderType = (UILabel *)[self.view viewWithTag:502];
    UILabel *labelAboutRider = (UILabel *)[self.view viewWithTag:503];
    UILabel *labelNextTrip = (UILabel *)[self.view viewWithTag:504];
    UILabel *labelCurrentLocation = (UILabel *)[self.view viewWithTag:505];
    UIImageView *imageViewSkillType = (UIImageView *)[self.view viewWithTag:510];
    UIButton *crossButton = (UIButton *)[self.view viewWithTag:512];
    UIButton *menuButton = (UIButton *)[self.view viewWithTag:511];
    UIButton *editButton = (UIButton *)[self.view viewWithTag:513];
    UIButton *mayBeLaterButton = (UIButton *)[self.view viewWithTag:514];
    UIButton *letsRideButton = (UIButton *)[self.view viewWithTag:515];
    UIButton *flagButton = (UIButton *)[self.view viewWithTag:516];
    UIView *picCountView = (UIView *)[self.view viewWithTag:519];
    UILabel *labelPicCount = (UILabel *)[picCountView viewWithTag:520];
    UILabel *distanceLabel = (UILabel *)[self.view viewWithTag:521];
    UILabel *interestText = (UILabel *)[self.view viewWithTag:530];
    UILabel *locationText = (UILabel *)[self.view viewWithTag:531];
    if (_fromScreenType==MyprofileScreenType)
    {
        interestText.text=@"Your interests";
        locationText.text=@"Your locations";
        crossButton.hidden=YES;
        editButton.hidden=NO;
        mayBeLaterButton.hidden=YES;
        letsRideButton.hidden=YES;
        flagButton.hidden=YES;
        _horizontalPageControl.hidden=NO;
        [self updateMenuIcon];
        
    }
    else if (_fromScreenType==RequestRiderScreenType)
    {
        interestText.text=@"Shared interests";
        locationText.text=@"Shared locations";
        distanceLabel.hidden=NO;
        editButton.hidden=YES;
        menuButton.hidden=YES;
        mayBeLaterButton.hidden=NO;
        letsRideButton.hidden=NO;
        flagButton.hidden=NO;
        _horizontalPageControl.hidden=YES;
        picCountView.hidden=NO;
        [mayBeLaterButton setImage:[UIImage imageNamed:@"decline"] forState:UIControlStateNormal];
        [letsRideButton setImage:[UIImage imageNamed:@"accepted"] forState:UIControlStateNormal];
    }
    else if (_fromScreenType==SuggestedRiderScreenType)
    {
        interestText.text=@"Shared interests";
        locationText.text=@"Shared locations";
        distanceLabel.hidden=NO;
        editButton.hidden=YES;
        menuButton.hidden=YES;
        mayBeLaterButton.hidden=NO;
        letsRideButton.hidden=NO;
        flagButton.hidden=NO;
        _horizontalPageControl.hidden=YES;
        picCountView.hidden=NO;
    }
    else
    {
        interestText.text=@"Shared interests";
        locationText.text=@"Shared locations";
        distanceLabel.hidden=NO;
        editButton.hidden=YES;
        menuButton.hidden=YES;
        mayBeLaterButton.hidden=YES;
        letsRideButton.hidden=YES;
        flagButton.hidden=NO;
        _horizontalPageControl.hidden=YES;
        picCountView.hidden=NO;
 
    }
       NSString *stringFullNameWithAge;
    if (_otherUsersObject.firstName.length>0)
        stringFullNameWithAge = [NSString stringWithFormat:@"%@ ",_otherUsersObject.firstName];
    
    if (_otherUsersObject.lastName.length>0)
        stringFullNameWithAge = [stringFullNameWithAge stringByAppendingString:_otherUsersObject.lastName];
    
    if (_otherUsersObject.age.length>0)
        stringFullNameWithAge = [stringFullNameWithAge stringByAppendingString:[NSString stringWithFormat:@", %@",_otherUsersObject.age]];
    
    if (stringFullNameWithAge.length>0)
        labelName.text = [NSString stringWithFormat:@"%@",stringFullNameWithAge];
    
    if (_otherUsersObject.distance.length>0) {
        int distantInt=[_otherUsersObject.distance intValue];
        distanceLabel.text=[NSString stringWithFormat:@"%d miles away",distantInt];
    }
    if (_otherUsersObject.riderType.length>0)
        labelRiderType.text = [NSString stringWithFormat:@"%@",_otherUsersObject.riderType];
    
    if (_otherUsersObject.skillLevelID.length>0) {
        imageViewSkillType.image =[UIImage imageNamed:[NSString stringWithFormat:@"level%@.png",_otherUsersObject.skillLevelID]];
    }
    
    if (_otherUsersObject.aboutMe.length>0)
    {
        labelAboutRider.text = [NSString stringWithFormat:@"%@",_otherUsersObject.aboutMe];
        CGSize aboutTextSize=[self getSizeForAboutText:_otherUsersObject.aboutMe maxWidth:labelAboutRider.frame.size.width font:[UIFont fontWithName:@"RobotoCondensed-Light" size:16.0f]];
        labelAboutRider.frame=CGRectMake(labelAboutRider.frame.origin.x, labelAboutRider.frame.origin.y, labelAboutRider.frame.size.width, aboutTextSize.height);
        _sharedView.frame=CGRectMake(0, labelAboutRider.frame.origin.y+aboutTextSize.height+10 ,_sharedView.frame.size.width, _sharedView.frame.size.height);
        _infoScrollView.contentSize=CGSizeMake(WIDTH, HEIGHT+150+aboutTextSize.height);
    }
    
    if (_otherUsersObject.plannedTripName.length>0)
        labelNextTrip.text = [NSString stringWithFormat:@"%@",_otherUsersObject.plannedTripName];
    
    if (_otherUsersObject.address.length>0){
        labelCurrentLocation.text = [NSString stringWithFormat:@"in %@",_otherUsersObject.address];
    }
    
    if (_otherUsersObject.userPhotos.length>0) {
        NSMutableArray *imageArray=[[_otherUsersObject.userPhotos componentsSeparatedByString:@";"] mutableCopy];
        if ([imageArray containsObject:_otherUsersObject.profileImage]) {
            [imageArray removeObject:_otherUsersObject.profileImage];
            [imageArray insertObject:_otherUsersObject.profileImage atIndex:0];
        }
        [totalPhotosArray removeAllObjects];
        for (int i=0; i<[imageArray count]; i++) {
            NSMutableDictionary *dict=[NSMutableDictionary dictionary];
            
            [dict setObject:[imageArray objectAtIndex:i] forKey:@"image"];
            [totalPhotosArray addObject:dict];
        }

        [_photosHorizontalView reloadData];
        labelPicCount.text=[NSString stringWithFormat:@"%d/%lu",1,(unsigned long)[imageArray count]];
    }
    else
    {
        labelPicCount.text=@"1/1";
        NSMutableDictionary *dict=[NSMutableDictionary dictionary];
        
        [dict setObject:@"profileBg" forKey:@"image"];
        [totalPhotosArray addObject:dict];
    }
    [self displayFriendsOnScrollView:_otherUsersObject.contactsName];
    [self displayInterestsOnScrollView:_otherUsersObject.interestName];
    [self displayLocationsOnScrollView:_otherUsersObject.locationName];
}
-(void)displayFriendsOnScrollView:(NSString *)friends
{
    //NSString *friendString=[friends lastPathComponent];
    NSArray *totalFriends=[friends componentsSeparatedByString:@","];
    
    CGFloat xAxix=5;
    for (int i=0; i<[totalFriends count] ; i++)
    {
        NSString *imageUrl=[totalFriends objectAtIndex:i];
        if ([imageUrl isEqualToString:@","] || imageUrl.length<1 ) {
            return;
        }
        UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(xAxix, 10, 40, 40)];
        imageView.backgroundColor=[UIColor clearColor];
        imageView.layer.masksToBounds=YES;
        imageView.layer.cornerRadius=imageView.frame.size.width/2;
        xAxix+=50;
        [imageView setImageWithURL:[NSURL URLWithString:imageUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [_frndsScrollView addSubview:imageView];
    }
    if ([totalFriends count]>1)
        _frndsScrollView.contentSize=CGSizeMake(xAxix+90, _frndsScrollView.frame.size.height);
    
    else
        _frndsScrollView.contentSize=CGSizeMake(xAxix, _frndsScrollView.frame.size.height);
    
}

-(void)displayInterestsOnScrollView:(NSString *)interests
{
    UILabel *labelInterests = (UILabel *)[self.view viewWithTag:506];
    CGSize interestsStringSize=[self getSizeForText:interests maxHeight:labelInterests.frame.size.height font:[UIFont fontWithName:@"RobotoCondensed-Light" size:16.0f]];
    labelInterests.text=interests;
    labelInterests.frame=CGRectMake(labelInterests.frame.origin.x, labelInterests.frame.origin.y, interestsStringSize.width, labelInterests.frame.size.height);
    _interestScrollView.contentSize=CGSizeMake(interestsStringSize.width+40, _interestScrollView.frame.size.height);
}
-(void)displayLocationsOnScrollView:(NSString *)locations
{
   UILabel *labelLocations = (UILabel *)[self.view viewWithTag:507];
    labelLocations.text=locations;
    CGSize locationsStringSize=[self getSizeForText:locations maxHeight:labelLocations.frame.size.height font:[UIFont fontWithName:@"RobotoCondensed-Light" size:16.0f]];
    labelLocations.text=locations;
    labelLocations.frame=CGRectMake(labelLocations.frame.origin.x, labelLocations.frame.origin.y, locationsStringSize.width, labelLocations.frame.size.height);
    _locationsScrollView.contentSize=CGSizeMake(locationsStringSize.width+40, _locationsScrollView.frame.size.height);
}

#pragma mark -  IBActions
-(IBAction)visitUserProfileAction
{
    MyProfileDetailViewController *myProfileDetailViewController=[[MyProfileDetailViewController alloc] initWithNibName:@"MyProfileDetailViewController" bundle:nil];
    myProfileDetailViewController.otherUsersObject=_otherUsersObject;
    [self.navigationController pushViewController:myProfileDetailViewController animated:YES];
}

-(IBAction)backButtonAction
{
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromBottom;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)pageControllerValueChangeAction:(id)sender
{
    UIPageControl *pageControl = sender;
    [_photosHorizontalView scrollToPage:pageControl.currentPage];
}

#pragma mark -
#pragma mark PagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(PagedFlowView *)flowView{
    
    return CGSizeMake(WIDTH, WIDTH+19);
}

- (void)flowView:(PagedFlowView *)flowView didScrollToPageAtIndex:(NSInteger)index {
    [self updateDots];
    UILabel *labelPicCount = (UILabel *)[self.view viewWithTag:520];
    labelPicCount.text=[NSString stringWithFormat:@"%ld/%lu",(long)index+1,(unsigned long)[totalPhotosArray count]];
    selectedPhotoIndex = index;
}

- (void)flowView:(PagedFlowView *)flowView didTapPageAtIndex:(NSInteger)index{
    // NSLog(@"Tapped on page # %ld", (long)index);
}

#pragma mark -
#pragma mark PagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(PagedFlowView *)flowView{
    return [totalPhotosArray count];
    
}

- (UIView *)flowView:(PagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index
{
    UIView *imageView = (UIView *)[flowView dequeueReusableCell];
    if (!imageView) {
        NSArray *nibContentss = [[NSBundle mainBundle] loadNibNamed:@"UserProfilePhotos" owner:self options:nil];
        imageView=[nibContentss lastObject];
        
    }
    imageView.tag=index;
    
    NSMutableDictionary *dict=[totalPhotosArray objectAtIndex:index];
    UIImageView *userimage=[imageView.subviews objectAtIndex:0];
    id obJ=[dict valueForKey:@"image"];
    UIButton *flagButton = (UIButton *)[self.view viewWithTag:516];

    if ([obJ isKindOfClass:[NSString class]]) {
        if ([[dict valueForKey:@"image"] isEqualToString:@"profileBg"])
        {
            [userimage setImage:[UIImage imageNamed:@"profileBg.jpg"]];
            flagButton.hidden = YES;

        }
        else
        {
            [userimage setImageWithURL:[NSURL URLWithString:[dict valueForKey:@"image"]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            if (!_fromScreenType==MyprofileScreenType) {
                 flagButton.hidden = NO;
            }
           

        }
    }
    else
    {
        //[userimage setImage:[dict valueForKey:@"image"]];
    }
    
    return imageView;
}
#pragma mark - Update UIPageControl Images
-(void)updateDots
{
    for (int i = 0; i < [_horizontalPageControl.subviews count]; i++)
    {
        UIView *dot = [_horizontalPageControl.subviews objectAtIndex:i];
        if (i == _horizontalPageControl.currentPage)
        {
            for (id subview in dot.subviews)
            {
                if ([subview isKindOfClass:[UIImageView class]]) {
                    [subview removeFromSuperview];
                }
            }
            UIImageView* fillImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"swipe_active.png"]];
            [dot addSubview:fillImage];
        } else
        {
            for (id subview in dot.subviews) {
                if ([subview isKindOfClass:[UIImageView class]]) {
                    [subview removeFromSuperview];
                }
            }
            UIImageView* blankImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"swipe.png"]];
            [dot addSubview:blankImage];
        }
    }
}

#pragma mark - IBAction
-(IBAction)mayBeLaterAction
{
    if (_fromScreenType==RequestRiderScreenType) {
        
        [self backButtonAction];
        [[NSNotificationCenter defaultCenter] postNotificationName:kDeclineRequest object:nil];
        
    }
    else
    {
        [self backButtonAction];
        [[NSNotificationCenter defaultCenter] postNotificationName:kMayBeLater object:nil];
        
    }
}

-(IBAction)rideTogetherAction
{
     if (_fromScreenType==RequestRiderScreenType) {
        [self backButtonAction];
        [[NSNotificationCenter defaultCenter] postNotificationName:kAcceptRequest object:nil];
    }
    else
    {
        
        [self letUsRide];
        [[NSNotificationCenter defaultCenter] postNotificationName:kRideTogether object:nil];
    }
}

-(IBAction)flagRiderProfileAction
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Are your sure? You want to flag this photo." delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    alert.tag = 111;
    [alert show];
}

#pragma mark - Send Request Web Service
-(void)flagRiderServiceCall
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSMutableDictionary *dict=[totalPhotosArray objectAtIndex:selectedPhotoIndex];
    NSArray *arrPhotoName=[[dict valueForKey:@"image"] componentsSeparatedByString:@"/"];
    NSString *profileImageName=[arrPhotoName lastObject];
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    [params setObject:sessionId forKey:@"sessionId"];
    [params setObject:_otherUsersObject.user_id forKey:@"flaggedUserId"];
    [params setObject:profileImageName forKey:@"flaggedPhotoName"];
    [RiderManager sharedInstance].delegate = self;
    [[RiderManager sharedInstance] CallWebService:params methodName:@"photoFlag/add"];
}

-(void)letUsRide
{
    NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    [params setObject:sessionId forKey:@"sessionId"];
    [params setObject:_otherUsersObject.user_id forKey:@"receiverId"];
    [RiderManager sharedInstance].delegate = self;
    [[RiderManager sharedInstance] CallWebService:params methodName:@"contact/add"];
    [self backButtonAction];
}

#pragma mark - Web Service Delagete
-(void)riderFlaggedSuccessfully:(NSMutableDictionary *)dictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Photo flagged successfully."  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];

}

- (void)riderRequestSentSuccessfully:(NSMutableDictionary *)dictionary
{
    
}

- (void)requestDidFailed:(NSDictionary *)errorDictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:[errorDictionary valueForKey:@"Error"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark get text size
- (CGSize)getSizeForText:(NSString *)text maxHeight:(CGFloat)height font:(UIFont *)font  {
    NSArray *arr=[NSArray arrayWithObject:text];
    CGSize DXsize = [[arr objectAtIndex:0] sizeWithFont:font constrainedToSize:CGSizeMake(MAXFLOAT, height) lineBreakMode:NSLineBreakByWordWrapping];
    
    return DXsize;
}
- (CGSize)getSizeForAboutText:(NSString *)text maxWidth:(CGFloat)width font:(UIFont *)font  {
    NSArray *arr=[NSArray arrayWithObject:text];
    CGSize DXsize = [[arr objectAtIndex:0] sizeWithFont:font constrainedToSize:CGSizeMake(width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    return DXsize;
}

#pragma mark - AlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 111) {
        if (buttonIndex == 0) {
            [self flagRiderServiceCall];
        }
    }
}

@end
