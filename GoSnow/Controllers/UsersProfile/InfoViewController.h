//
//  InfoViewController.h
//  GoSnow
//
//  Created by Rishabh on 13/04/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserNameDelegate <NSObject>

-(void)nameEntered:(NSString *)name isLastName:(BOOL)isLastName;
-(void)actionCancelled:(BOOL)isLastName;

@end

@interface InfoViewController : UIViewController

@property (nonatomic, weak) id<UserNameDelegate> delegate;
@property BOOL isFirstName;
@property (nonatomic, strong) NSString* userName;
@property (nonatomic, strong) NSString* userId;


@end
