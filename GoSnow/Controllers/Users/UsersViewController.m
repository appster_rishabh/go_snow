//
//  UsersViewController.m
//  GoSnow
//
//  Created by Rishabh on 26/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "UsersViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ViewUserProfileViewController.h"
#import "Users.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIImage+ImageShape.h"
#import "GroupTripManager.h"
@interface UsersViewController ()

@end

@implementation UsersViewController
@synthesize isFromGroupTrip;

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    if (isFromGroupTrip)
        suggestedRidersArray=[[NSMutableArray alloc]init];
    
    [self fetchUserInfoEntityFromCoreData];
    [self initialSetUp];
     [self updateMenuIcon];
}

#pragma mark viewWillAppear method
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenuIconByNotification) name:KNotificationRecieved object:nil];
    //[self ad];
}
#pragma mark viewWillDisappear method
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
   
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KNotificationRecieved object:nil];
}
#pragma mark updateMenuIconByNotification method
-(void)updateMenuIconByNotification
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    [menuIcon setImage:[UIImage imageNamed:@"menu_white_dot"] forState:UIControlStateNormal];
}
#pragma mark updateMenuIcon method
-(void)updateMenuIcon
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    int notificationCount=[UserInfoEntity fetchPendingRequestsAndMessagesCountsinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    if (notificationCount>0) {
        [menuIcon setImage:[UIImage imageNamed:@"menu_white_dot"] forState:UIControlStateNormal];
    }
    else
    {
        [menuIcon setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    }
}



-(void)initialSetUp
{
    [self.navigationController.navigationBar setHidden:YES];
   
    _userImage.layer.cornerRadius=25.0f;
    _userImage.layer.masksToBounds=YES;
    
    [self manageDraggableView];
    
    _sharedFriendCountButton.layer.cornerRadius=_sharedFriendCountButton.frame.size.height/2;
    _sharedLocationCountButton.layer.cornerRadius=_sharedLocationCountButton.frame.size.height/2;
    _sharedInterestCountButton.layer.cornerRadius=_sharedLocationCountButton.frame.size.height/2;
    
    UIButton *buttonFriends =(UIButton *)[self.view viewWithTag:11];
    UIButton *buttonLocation =(UIButton *)[self.view viewWithTag:12];
    UIButton *buttonInterest =(UIButton *)[self.view viewWithTag:13];
    UIButton *buttonFriendsCounter =(UIButton *)[self.view viewWithTag:14];
    UIButton *buttonLocationCounter =(UIButton *)[self.view viewWithTag:15];
    UIButton *buttonInterestCounter =(UIButton *)[self.view viewWithTag:16];
    
    UIView *viewTop =(UIView *)[self.view viewWithTag:222];
    
    if (isFromGroupTrip)
    {
        [self hideElements];
        _noUserFoundView.hidden=YES;
        viewTop.backgroundColor =[UIColor whiteColor];
        [buttonFriends setImage:[UIImage imageNamed:@"blue_Shared_Friends.png"] forState:UIControlStateNormal];
        [buttonInterest setImage:[UIImage imageNamed:@"blue_Shared_Interests.png"] forState:UIControlStateNormal];
        [buttonLocation setImage:[UIImage imageNamed:@"blue_Shared_Location.png"] forState:UIControlStateNormal];
        
        [buttonFriendsCounter setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonLocationCounter setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonInterestCounter setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonFriendsCounter setBackgroundImage:[UIImage imageNamed:@"more_blue.png"] forState:UIControlStateNormal];
        [buttonLocationCounter setBackgroundImage:[UIImage imageNamed:@"more_blue.png"] forState:UIControlStateNormal];
        [buttonInterestCounter setBackgroundImage:[UIImage imageNamed:@"more_blue.png"] forState:UIControlStateNormal];
        
        _menuButton.hidden = YES;
        _closeButton.hidden = NO;
        [self getUsers];
    }
    else {
        viewTop.backgroundColor =[UIColor clearColor];
        
        [buttonFriends setImage:[UIImage imageNamed:@"shared_friendsW.png"] forState:UIControlStateNormal];
        [buttonInterest setImage:[UIImage imageNamed:@"shared_interestsW.png"] forState:UIControlStateNormal];
        [buttonLocation setImage:[UIImage imageNamed:@"shared_locationW.png"] forState:UIControlStateNormal];
        
        _menuButton.hidden = NO;
        _closeButton.hidden = YES;
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(swipeLeftAfterDelay) name:kMayBeLater object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(swipeRightAfterDelay) name:kRideTogether object:nil];
}

-(void)manageDraggableView
{
    currentIndex=0;
    
    [_userImage setImageWithURL:[NSURL URLWithString:userInfoEntity.profileThumb ]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    _draggableBackground.delegate=self;
    _draggableBackground.frame=CGRectMake(_draggableBackground.frame.origin.x, _locationButton.frame.origin.y+_locationButton.frame.size.height+7, _draggableBackground.frame.size.width, _draggableBackground.frame.size.height);
    if (isFromGroupTrip) {
         [_draggableBackground SetUpInitials:_suggestedUsers fromScreen:GGOverlayViewModeGroupScreen];
    }
    else
    {
         [_draggableBackground SetUpInitials:_suggestedUsers fromScreen:GGOverlayViewModeSuggestedScreen];
    }
   
    
    [self updateUserNameFirst:0];
    
}

-(void)fetchUserInfoEntityFromCoreData
{
    userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}
-(void)startRippleEffect
{
    [_rippleEffect drawImageWithFrame:[UIImage imageNamed:@""] Frame:_rippleEffect.frame Color:[UIColor colorWithRed:(29.0/255.0) green:(144.0/255.0) blue:(238.0/255.0) alpha:1]];
    [_rippleEffect setRippleColor:[UIColor colorWithRed:(29.0/255.0) green:(144.0/255.0) blue:(238.0/255.0) alpha:1]];
    [_rippleEffect setRippleTrailColor:[UIColor colorWithRed:(29.0/255.0) green:(144.0/255.0) blue:(238.0/255.0) alpha:1]];
}

#pragma mark - IBActions
-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Get Users
-(void)getUsers
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [[SuggestedUsers sharedInstance].totalUsers removeAllObjects];
    [[SuggestedUsers sharedInstance] WebSericeCall:@"" sessionId:userInfoEntity.sessionId latitude:@"" longtitude:@"" groupTripId:_groupTrip.groupId];
    [SuggestedUsers sharedInstance].delegate=self;
}

#pragma mark - Fetch Information Successfully
-(void)fetchUsersSuccessfully:(NSMutableArray*)successArray
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    [suggestedRidersArray removeAllObjects];
    [suggestedRidersArray addObjectsFromArray:successArray];
    _suggestedUsers = suggestedRidersArray;
    if ([_suggestedUsers count]>0) {
        [self manageDraggableView];
        [self displayElements];
    }
    else
    {
        [self hideElements];
        self.userName.text=@"RIDERS";
    }
}
#pragma mark - Fetch Information Failed

-(void)failedWithError:(NSDictionary*)errorDictionary
{
    [self hideElements];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[errorDictionary valueForKey:@"Error"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Notification Action
-(void)swipeLeftAfterDelay
{
    [self performSelector:@selector(swipeLeft) withObject:nil afterDelay:0.3];
}
-(void)swipeRightAfterDelay
{
    [self performSelector:@selector(swipeRight) withObject:nil afterDelay:0.3];
}
-(void)swipeLeft
{
    [_draggableBackground swipeLeft];
}
-(void)swipeRight
{
    [_draggableBackground swipeRight];
}
// Code review use appropraite naming convention
#pragma mark - Swipe Action

-(void)userSwipedLeftAction:(UIView *)user
{
    [self updateUserName:user.tag-400];
    currentIndex=user.tag-400;
}
-(void)userSwipedRightAction:(UIView *)user
{
   [self updateUserName:user.tag-400];
    currentIndex=user.tag-400;
     if (!isFromGroupTrip)
     {
        [self letUsRide:currentIndex];
     }
    else
    {
        [self inviteForGroup:currentIndex];
    }
}
-(void)userTappedAction:(UIView *)user
{
    [self myProfile:user.tag-400];
}

-(void)myProfile:(NSInteger)tag
{
    Users *userInfo=[_suggestedUsers objectAtIndex:tag];
    if ([userInfo.model isEqualToString:@"trip"]) {
        return;
    }

    ViewUserProfileViewController *viewUserProfileViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"ViewUserProfileViewController"];
    viewUserProfileViewController.otherUsersObject=userInfo;
    if (isFromGroupTrip) {
      viewUserProfileViewController.fromScreenType=GroupBuddiesScreenType;
    }
    else
    {
      viewUserProfileViewController.fromScreenType=SuggestedRiderScreenType;
    }
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromTop;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:viewUserProfileViewController animated:NO];
}
#pragma mark  - Update User Name
-(void)updateUserNameFirst:(NSInteger)index
{
    UIImageView *imageViewLeft=(UIImageView *)[self.view viewWithTag:901];
    UIImageView *imageViewRight=(UIImageView *)[self.view viewWithTag:902];
    
    if (index>=[_suggestedUsers count]) {
        imageViewLeft.hidden=YES;
        imageViewRight.hidden=YES;
        _noUsers.hidden=NO;
        self.userName.text=@"RIDERS";
        //  [locationButton setTitle:@"NO RIDER FOUND" forState:UIControlStateNormal];
        [self hideElements];
        return;
        
    }
    Users *userInfo=[_suggestedUsers objectAtIndex:index];
    
    [self updateLocation:userInfo button:_locationButton];
    
    NSString *usersName;
    if (userInfo.firstName.length>0 && userInfo.lastName.length>0) {
        usersName=[NSString stringWithFormat:@"%@ %@",userInfo.firstName,userInfo.lastName];
    }
    else
    {
        usersName=[NSString stringWithFormat:@"%@",userInfo.firstName];
    }
    if (userInfo.age.length>0) {
        usersName= [usersName stringByAppendingString:[NSString stringWithFormat:@", %@",userInfo.age]];
    }
    if ([userInfo.model isEqualToString:@"trip"]) {
        _sharedView.hidden=YES;
         _tripDescription.hidden=NO;
        _tripDescription.text=userInfo.tripDecription;
        _tripDescription.numberOfLines=0;
    }
    else
    {
        _sharedView.hidden=NO;
         _tripDescription.hidden=YES;
        [_sharedFriendCountButton setTitle:userInfo.friendCount forState:UIControlStateNormal];
        [_sharedInterestCountButton setTitle:userInfo.interestCount forState:UIControlStateNormal];
        [_sharedLocationCountButton setTitle:userInfo.locationCount forState:UIControlStateNormal];
    }
    
   
    self.userName.text=[usersName uppercaseString];
    imageViewLeft.hidden=NO;
    imageViewRight.hidden=NO;
}
#pragma mark  - Update User Name
-(void)updateUserName:(NSInteger)index
{
    
    UIImageView *imageViewLeft=(UIImageView *)[self.view viewWithTag:901];
    UIImageView *imageViewRight=(UIImageView *)[self.view viewWithTag:902];
    
    if (index+1>=[_suggestedUsers count]) {
        imageViewLeft.hidden=YES;
        imageViewRight.hidden=YES;
        _noUsers.hidden=NO;
        self.userName.text=@"RIDERS";
        //  [locationButton setTitle:@"NO RIDER FOUND" forState:UIControlStateNormal];
        [self hideElements];
        return;
        
    }
    Users *userInfo=[_suggestedUsers objectAtIndex:index+1];
    [self updateLocation:userInfo button:_locationButton];
    NSString *usersName;
    if (userInfo.firstName.length>0 && userInfo.lastName.length>0) {
        usersName=[NSString stringWithFormat:@"%@ %@",userInfo.firstName,userInfo.lastName];
    }
    else
    {
        usersName=[NSString stringWithFormat:@"%@",userInfo.firstName];
    }
    if (userInfo.age.length>0) {
        usersName= [usersName stringByAppendingString:[NSString stringWithFormat:@", %@",userInfo.age]];
    }
    
    if ([userInfo.model isEqualToString:@"trip"]) {
        _sharedView.hidden=YES;
        _tripDescription.hidden=NO;
        _tripDescription.text=userInfo.tripDecription;
    }
    else
    {
         _tripDescription.hidden=YES;
        _sharedView.hidden=NO;
        [_sharedFriendCountButton setTitle:userInfo.friendCount forState:UIControlStateNormal];
        [_sharedInterestCountButton setTitle:userInfo.interestCount forState:UIControlStateNormal];
        [_sharedLocationCountButton setTitle:userInfo.locationCount forState:UIControlStateNormal];
    }

    self.userName.text=[usersName uppercaseString];
    imageViewLeft.hidden=NO;
    imageViewRight.hidden=NO;
}

#pragma mark - Get Current and Destination Names
-(void )updateLocation:(Users *)userInfo button:(UIButton *)locationButton
{
    //Users *userInfo=[_suggestedUsers objectAtIndex:index];
    NSString *myLocationString=nil;
    if (userInfo.address.length>0) {
        myLocationString=[NSString stringWithFormat:@"I'M IN %@",userInfo.address];
    }
    if (userInfo.plannedTripName.length>0) {
        if (myLocationString.length>0) {
            myLocationString=[myLocationString stringByAppendingString:[NSString stringWithFormat:@", NEXT TRIP IS %@",userInfo.plannedTripName]];
        }
        else
        {
            myLocationString=[myLocationString stringByAppendingString:[NSString stringWithFormat:@"NEXT TRIP IS %@",userInfo.plannedTripName]];
        }
    }
    if (myLocationString.length>0)
    {
        myLocationString=[myLocationString uppercaseString];
        CGSize locationStringSize=[self getSizeForTextForLocation:myLocationString font:[UIFont fontWithName:@"PassionOne-Bold" size:12.0f]];
        if (locationStringSize.width>195)
        {
            _locationButton.frame=CGRectMake(_locationButton.frame.origin.x, _locationButton.frame.origin.y, locationStringSize.width+20, 23);
            _locationButton.center=CGPointMake(self.view.frame.size.width/2, _locationButton.frame.origin.y+(23/2));
            [_locationButton setBackgroundImage:[UIImage CreateAndReturnImageRounded:@"#1B79E9" outline:@"Fill" rect:_locationButton.frame] forState:UIControlStateNormal];
            [_locationButton setTitle:myLocationString forState:UIControlStateNormal];
        }
        else
        {
            [_locationButton setBackgroundImage:[UIImage CreateAndReturnImageRounded:@"#1B79E9" outline:@"Fill" rect:_locationButton.frame] forState:UIControlStateNormal];

            [_locationButton setTitle:myLocationString forState:UIControlStateNormal];
        }
        
    }
    
    // return myLocationString;
}

#pragma mark get text width
- (CGSize)getSizeForTextForLocation:(NSString *)text font:(UIFont *)font  {
    NSArray *arr=[NSArray arrayWithObject:text];
    CGSize DXsize = [[arr objectAtIndex:0] sizeWithFont:font constrainedToSize:CGSizeMake(WIDTH, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    //CGSize stringSize = frame.size;
    
    return DXsize;
}

#pragma mark  - Hide/Unhide Elements
-(void)hideElements
{
    UIImageView *imageViewLeft=(UIImageView *)[self.view viewWithTag:901];
    UIImageView *imageViewRight=(UIImageView *)[self.view viewWithTag:902];
    imageViewLeft.hidden = YES;
    imageViewRight.hidden = YES;
    
    _draggableBackground.hidden=YES;
    _noUserFoundView.hidden=NO;
    _sharedView.hidden=YES;
    _locationButton.hidden=YES;
    _rippleEffect.hidden=YES;
    _userImage.hidden=YES;
    _tripDescription.hidden=YES;
}
-(void)displayElements
{
    UIImageView *imageViewLeft=(UIImageView *)[self.view viewWithTag:901];
    UIImageView *imageViewRight=(UIImageView *)[self.view viewWithTag:902];
    imageViewLeft.hidden = NO;
    imageViewRight.hidden = NO;
    _draggableBackground.hidden=NO;
    _sharedView.hidden=NO;
    _locationButton.hidden=NO;
    _rippleEffect.hidden=YES;
    _noUsers.hidden=YES;
    _userImage.hidden=YES;
    _noUserFoundView.hidden=YES;
    
}
#pragma mark - Send Request Web Service
-(void)letUsRideForgroup:(NSInteger)index
{
    NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
    Users *userInfo=[_suggestedUsers objectAtIndex:index];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:userInfo.user_id forKey:@"receiverId"];
    [[RiderManager sharedInstance] CallWebService:params methodName:@"tripUser/requestToJoin"];
}
#pragma mark - Send Request Web Service
-(void)letUsRide:(NSInteger)index
{
    NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
    Users *userInfo=[_suggestedUsers objectAtIndex:index];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    
    if ([userInfo.model isEqualToString:@"trip"]) {
        //tripUser/requestToJoin
        [params setObject:userInfo.user_id forKey:@"selectedTripId"];
        [[RiderManager sharedInstance] CallWebService:params methodName:@"tripUser/requestToJoin"];
        //selectedTripId
    }
    else
    {
        [params setObject:userInfo.user_id forKey:@"receiverId"];
      [[RiderManager sharedInstance] CallWebService:params methodName:@"contact/add"];
    }
    
   
}
#pragma mark - Send Request Web Service
-(void)inviteForGroup:(NSInteger)index
{
    Users *userInfo=[_suggestedUsers objectAtIndex:index];
    
    NSString *sessionId=[UserInfoEntity fetchSessionIdinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    
    [params setObject:_groupTrip.groupId forKey:@"selectedTripId"];
    [params setObject:[NSArray arrayWithObject:userInfo.user_id] forKey:@"selectedUserIds"];
    [params setObject:sessionId forKey:@"sessionId"];
    [[GroupTripManager sharedInstance]setDelegate:self];
    [[GroupTripManager sharedInstance]handleDataForGroupTrip:params method:@"tripUser/add"];
}
#pragma mark user invited
-(void)inviteSnowBuddiesToGroup
{
    
}
#pragma mark - Web Service Delagete
- (void)riderRequestSentSuccessfully:(NSMutableDictionary *)dictionary
{
    
}

@end
