//
//  UsersViewController.h
//  GoSnow
//
//  Created by Rishabh on 26/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DraggableViewBackground.h"
#import "RESideMenu.h"
#import "RiderManager.h"
#import "SuggestedUsers.h"
#import "LNBRippleEffect.h"
#import "GroupTrip.h"
#import "GroupTripManager.h"
@interface UsersViewController : UIViewController<GoSnowSwipeDelegate,RESideMenuDelegate,RiderManagerDelegate,SuggestedUsersDelegate,GroupTripDelegate>
{
    NSInteger currentIndex;
    UserInfoEntity *userInfoEntity;
    NSMutableArray *suggestedRidersArray;
}
@property(nonatomic,assign)GroupTrip *groupTrip;
@property(nonatomic,assign)BOOL isFromGroupTrip;
@property(nonatomic,weak)IBOutlet LNBRippleEffect *rippleEffect;
@property(nonatomic,weak)IBOutlet UILabel *userName;
@property(nonatomic,weak)IBOutlet UILabel *tripDescription;
@property(nonatomic,weak)IBOutlet UIButton *closeButton;
@property(nonatomic,weak)IBOutlet UIButton *menuButton;
@property(nonatomic,weak)IBOutlet UIButton *sharedFriendCountButton;
@property(nonatomic,weak)IBOutlet UIButton *sharedInterestCountButton;
@property(nonatomic,weak)IBOutlet UIButton *sharedLocationCountButton;
@property(nonatomic,weak)IBOutlet UIButton *locationButton;
@property(nonatomic,weak)IBOutlet UIView *sharedView;
@property(nonatomic,weak)IBOutlet UIImageView *noUsers;
@property(nonatomic,weak)IBOutlet UIImageView *userImage;//need to deleet
@property(nonatomic,weak)IBOutlet UIView *noUserFoundView;
@property(nonatomic,weak)IBOutlet DraggableViewBackground *draggableBackground;
@property(nonatomic,strong)NSMutableArray *suggestedUsers;

-(IBAction)backButtonAction;
@end
