//
//  DraggableViewBackground.m
//  testing swiping
//
//  Created by Richard Kim on 8/23/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//

#import "DraggableViewBackground.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "PassionOneBoldLabel.h"

@implementation DraggableViewBackground{
    NSInteger usersLoadedIndex; //%%% the index of the user you have loaded into the loadedusers array last
    NSMutableArray *loadedusers; //%%% the array of user loaded (change max_buffer_size to increase or decrease the number of users this holds)
    
    UIButton* menuButton;
    UIButton* messageButton;
    UIButton* checkButton;
    UIButton* xButton;
    //
    UIButton *mayBeLaterButton;
    UIButton *letUsRideButton;
    UIButton *infoButton;
    PassionOneBoldLabel *mayBeLaterLabel;
    PassionOneBoldLabel *rideTogetherLabel;
   // BOOL isGroupTrip;
}
//this makes it so only two users are loaded at a time to
//avoid performance and memory costs
static const int MAX_BUFFER_SIZE = 2; //%%% max number of users loaded at any given time, must be greater than 1
//static const float user_HEIGHT = 386; //%%% height of the draggable user
//static const float user_WIDTH = 290; //%%% width of the draggable user

@synthesize suggestedUsers; //%%% all the labels I'm using as example data at the moment
@synthesize allUsers;//%%% all the users

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [super layoutSubviews];
  //code review --we are already allocating this in setupinitials
    }
    return self;
}
-(void)SetUpInitials:(NSMutableArray *)array fromScreen:(GGOverlayViewFromScreenMode)fromScreenMode
{
   // isGroupTrip = isGroupTripClass;
    suggestedUsers=array;
    loadedusers = [[NSMutableArray alloc] init];
    allUsers = [[NSMutableArray alloc] init];
    usersLoadedIndex = 0;
    UIImageView *avatar=[[UIImageView alloc] initWithFrame:self.bounds];
    [avatar setImage:[UIImage imageNamed:@"no_avatar@2x"]];
    // [self addSubview:avatar];
    [self renderBottomView:fromScreenMode];
    [self loadusers:fromScreenMode];
}
- (void)renderBottomView:(GGOverlayViewFromScreenMode)fromScreenMode
{
    UIImage *mayBeLaterImage;
    if (fromScreenMode==GGOverlayViewModeGroupScreen || fromScreenMode==GGOverlayViewModeGroupRequestScreen)
        mayBeLaterImage = [UIImage imageNamed:@"decline.png"];
    else
       mayBeLaterImage = [UIImage imageNamed:@"Later_icon.png"];
    mayBeLaterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    mayBeLaterButton.frame =CGRectMake(53, 248, mayBeLaterImage.size.width, mayBeLaterImage.size.height);
        [mayBeLaterButton setImage:mayBeLaterImage forState:UIControlStateNormal];
    [mayBeLaterButton addTarget:self action:@selector(swipeLeft) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:mayBeLaterButton];
    
    UIImage *rideImage;
    if (fromScreenMode==GGOverlayViewModeGroupScreen || fromScreenMode==GGOverlayViewModeGroupRequestScreen)
      rideImage = [UIImage imageNamed:@"accept"];
    else
      rideImage = [UIImage imageNamed:@"Ride_icon.png"];
        letUsRideButton = [UIButton buttonWithType:UIButtonTypeCustom];
    letUsRideButton.frame =CGRectMake(207, 248, rideImage.size.width, rideImage.size.height);
    [letUsRideButton setImage:rideImage forState:UIControlStateNormal];
    [letUsRideButton addTarget:self action:@selector(swipeRight) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:letUsRideButton];
    mayBeLaterLabel=[[PassionOneBoldLabel alloc ]init];
    mayBeLaterLabel.textAlignment=NSTextAlignmentCenter;
    mayBeLaterLabel.numberOfLines=2;
    mayBeLaterLabel.font=[UIFont fontWithName:@"PassionOne-Bold" size:14.0f];
    if (fromScreenMode==GGOverlayViewModeGroupScreen)
    {
       mayBeLaterLabel.frame =CGRectMake(35, 286+mayBeLaterImage.size.height+10-46, 90, 42);
       mayBeLaterLabel.text=@"CANCEL INVITATION";
    }
    else{
        mayBeLaterLabel.frame =CGRectMake(46, 286+mayBeLaterImage.size.height+10-46, 70, 42);
        mayBeLaterLabel.text=@"MAYBE LATER";
    }
   
    [self addSubview:mayBeLaterLabel];
    rideTogetherLabel=[[PassionOneBoldLabel alloc]initWithFrame:CGRectMake(198, 286+mayBeLaterImage.size.height+10-46, 85, 42)];
    rideTogetherLabel.textAlignment=NSTextAlignmentCenter;
    rideTogetherLabel.numberOfLines=2;
    rideTogetherLabel.font=[UIFont fontWithName:@"PassionOne-Bold" size:14.0f];
   if (fromScreenMode==GGOverlayViewModeGroupScreen)
       rideTogetherLabel.text=@"INVITE TO GROUP";
    else
        rideTogetherLabel.text=@"LET'S RIDE TOGETHER";
   
    [self addSubview:rideTogetherLabel];
    //[mayBeLaterLabel setTextColor:[UIColor colorWithRed:() green:() blue:() alpha:()]];
    UIImage *infoImage = [UIImage imageNamed:@"Info_icon.png"];
    
    // int width = screenSize.size.width - infoImage.size.width - SIDE_MARGIN ;
    
    infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    infoButton.frame =CGRectMake(139, 284, infoImage.size.width, infoImage.size.height);
    
    [infoButton setImage:infoImage forState:UIControlStateNormal];
    [infoButton addTarget:self action:@selector(infoProfile) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:infoButton];
    if (fromScreenMode==GGOverlayViewModeGroupRequestScreen) {
        mayBeLaterLabel.hidden=YES;
        rideTogetherLabel.hidden=YES;
        infoButton.hidden=YES;
    }
    [mayBeLaterButton setExclusiveTouch:YES];
    [infoButton setExclusiveTouch:YES];
    [letUsRideButton setExclusiveTouch:YES];
}

-(DraggableView *)createDraggableViewWithDataAtIndex:(NSInteger)index fromScreenMode:(GGOverlayViewFromScreenMode)fromScreenMode
{
    UIImage *noAvatar =nil;
    if (fromScreenMode==GGOverlayViewModeGroupScreen) {
        noAvatar=[UIImage imageNamed:@"Placeholder"];
    }
    else
    {
      noAvatar=[UIImage imageNamed:@"no_avatar"];  
    }
    
    DraggableView *draggableView = [[DraggableView alloc]initWithFrame:CGRectMake((self.frame.size.width-(noAvatar.size.width))/2, 20, noAvatar.size.width, noAvatar.size.height) screenMode:fromScreenMode];
    draggableView.delegate = self;
  draggableView.overlayView.screenMode=fromScreenMode;
    
draggableView.backgroundColor=[UIColor redColor];
    draggableView.tag=index+400;
   // draggableView.center=CGPointMake(self.frame.size.width/2, self.frame.size.height);
    if (fromScreenMode==GGOverlayViewModeGroupRequestScreen) {
        [draggableView.userImageView setImage:[UIImage imageNamed:@"Placeholder"]];
    }
    else
    {
        
    Users *userinfo=[suggestedUsers objectAtIndex:index];
    if (userinfo.userPhotos.length<1) {
        if ([userinfo.model isEqualToString:@"trip"]) {
            [draggableView.userImageView setImage:[UIImage imageNamed:@"Placeholder"]];
  
        }
        else
        {
            [draggableView.userImageView setImage:noAvatar];

        }
               return draggableView;
    }
    NSArray *userPhotos=[userinfo.userPhotos componentsSeparatedByString:@";"];
    if ([userPhotos count]>0)
    {
       
         [draggableView.userImageView setImageWithURL:[NSURL URLWithString:[userPhotos objectAtIndex:0]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    
    else
    {
        [draggableView.userImageView setImage:[UIImage imageNamed:@"Avatar"]];
    }
    }
    // DraggableView
    //%%% placeholder for user-specific information
    
    return draggableView;
}
//-(GGOverlayViewFromScreenMode)returnMode
//{
//    if (isGroupTrip) {
//        return GGOverlayViewModeGroupScreen;
//    }
//    else
//    {
//        return GGOverlayViewModeSuggestedScreen;
//    }
//}
//%%% loads all the users and puts the first x in the "loaded users" array
-(void)loadusers:(GGOverlayViewFromScreenMode)fromScreenMode
{
    if([suggestedUsers count] > 0) {
        NSInteger numLoadedusersCap =(([suggestedUsers count] > MAX_BUFFER_SIZE)?MAX_BUFFER_SIZE:[suggestedUsers count]);
      
        for (int i = 0; i<[suggestedUsers count]; i++) {
            DraggableView* newUser = [self createDraggableViewWithDataAtIndex:i fromScreenMode:fromScreenMode];
            newUser.backgroundColor=[UIColor clearColor];
            [allUsers addObject:newUser];
            
            if (i<numLoadedusersCap) {
                //%%% adds a small number of users to be loaded
                [loadedusers addObject:newUser];
            }
        }
       
        for (int i = 0; i<[loadedusers count]; i++) {
            if (i>0) {
                [self insertSubview:[loadedusers objectAtIndex:i] belowSubview:[loadedusers objectAtIndex:i-1]];
            } else {
                [self addSubview:[loadedusers objectAtIndex:i] ];
            }
            usersLoadedIndex++; //%%% we loaded a user into loaded users, so we have to increment
        }
    }
}


-(void)userSwipedLeft:(UIView *)user;
{    [loadedusers removeObjectAtIndex:0];
    if (usersLoadedIndex < [allUsers count]) {
        [loadedusers addObject:[allUsers objectAtIndex:usersLoadedIndex]];
        usersLoadedIndex++;//%%% loaded a user, so have to increment count
        [self insertSubview:[loadedusers objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedusers objectAtIndex:(MAX_BUFFER_SIZE-2)]];
    }
    [_delegate userSwipedLeftAction:user];
}

-(void)userSwipedRight:(UIView *)user
{
        [loadedusers removeObjectAtIndex:0];
    if (usersLoadedIndex < [allUsers count]) {
        [loadedusers addObject:[allUsers objectAtIndex:usersLoadedIndex]];
        usersLoadedIndex++;//%%% loaded a user, so have to increment count
        [self insertSubview:[loadedusers objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedusers objectAtIndex:(MAX_BUFFER_SIZE-2)]];
    }
    [_delegate userSwipedRightAction:user];
}
-(void)userTapped:(UIView *)user
{
    [_delegate userTappedAction:user];
}
-(void)swipeRight
{
    DraggableView *dragView = [loadedusers firstObject];
    dragView.overlayView.mode = GGOverlayViewModeRight;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView rightClickAction];
}

-(void)swipeLeft
{
    DraggableView *dragView = [loadedusers firstObject];
    dragView.overlayView.mode = GGOverlayViewModeLeft;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView leftClickAction];
}
-(void)infoProfile
{
    DraggableView *dragView = [loadedusers firstObject];
    [_delegate userTappedAction:dragView];
}

@end
