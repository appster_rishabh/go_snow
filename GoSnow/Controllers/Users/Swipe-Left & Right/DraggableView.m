//
//  DraggableView.m
//  testing swiping
//
//  Created by Richard Kim on 5/21/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//
//  @cwRichardKim for updates and requests


#import "DraggableView.h"
#import <QuartzCore/QuartzCore.h>
@implementation DraggableView {
    CGFloat xFromCenter;
    CGFloat yFromCenter;
    UIButton* menuButton;
    UIButton* messageButton;
}

//delegate is instance of ViewController
@synthesize delegate;

@synthesize panGestureRecognizer;
@synthesize userImageView;
@synthesize overlayView;

- (id)initWithFrame:(CGRect)frame screenMode:(GGOverlayViewFromScreenMode)screenMode
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
        UIImage *noAvatar =nil;
        if (screenMode==GGOverlayViewModeGroupScreen) {
            noAvatar=[UIImage imageNamed:@"Placeholder"];
        }
        else
        {
            noAvatar=[UIImage imageNamed:@"no_avatar"];
        }

      //  UIImage *noAvatar = [UIImage imageNamed:@"no_avatar"];
        if (screenMode==GGOverlayViewModeSuggestedScreen || screenMode==GGOverlayViewModeGroupScreen || screenMode==GGOverlayViewModeGroupRequestScreen) {
            CALayer *mask = [CALayer layer];
            mask.contents = (id)[noAvatar CGImage];
            mask.frame = CGRectMake(0, 0, noAvatar.size.width,noAvatar.size.width);
            
            userImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, noAvatar.size.width, noAvatar.size.height)];
            userImageView.center=CGPointMake(self.frame.size.width/2, self.frame.size.height);
            userImageView.layer.mask=mask;
            // userImageView.layer.cornerRadius=noAvatar.size.height/2;
        }
        else
        {
            CALayer *mask = [CALayer layer];
            mask.contents = (id)[[UIImage imageNamed:@"no_avatar"] CGImage];
            mask.frame = CGRectMake(0, 0, noAvatar.size.width-80,noAvatar.size.width-80);
            UIImage *noAvatar = [UIImage imageNamed:@"no_avatar"];
            userImageView = [[UIImageView alloc]initWithFrame:CGRectMake(50, 0, noAvatar.size.width-80, noAvatar.size.height-80)];
            
            userImageView.layer.mask=mask;
        }
        
        userImageView.center=CGPointMake(frame.size.width/2, frame.size.height/2);
        // userImageView.image = [UIImage imageNamed:@"Avatar"];
        userImageView.layer.masksToBounds=YES;
        
        menuButton = [[UIButton alloc]initWithFrame:CGRectMake(17, 34, 58, 59)];
        [menuButton setImage:[UIImage imageNamed:@"MaybeLater_icon"] forState:UIControlStateNormal];
        messageButton = [[UIButton alloc]initWithFrame:CGRectMake(140, 34, 58, 59)];
        [messageButton setImage:[UIImage imageNamed:@"Yes_icon"] forState:UIControlStateNormal];
        // [information setTextAlignment:NSTextAlignmentCenter];
        //  information.textColor = [UIColor blackColor];
        
        self.backgroundColor = [UIColor redColor];
        
        
        panGestureRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(beingDragged:)];
        
        [self addGestureRecognizer:panGestureRecognizer];
        [self addSubview:userImageView];
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(beingTapped:)];
        
        [self addGestureRecognizer:_tapGestureRecognizer];
        
        menuButton.hidden=YES;
        messageButton.hidden=YES;
        
        
        overlayView = [[OverlayView alloc]initWithFrame:CGRectMake((self.frame.size.width-70)/2, (self.frame.size.height-25)/2, 100, 100) screenMode:screenMode];
        overlayView.alpha = 0;
        overlayView.center=CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        
       
        [self addSubview:overlayView];
    }
    return self;
}

-(void)setupView
{
    self.layer.cornerRadius = 4;
    self.layer.shadowRadius = 3;
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowOffset = CGSizeMake(1, 1);
}

//%%% called when you move your finger across the screen.
// called many times a second
-(void)beingDragged:(UIPanGestureRecognizer *)gestureRecognizer
{
    //%%% this extracts the coordinate data from your swipe movement. (i.e. How much did you move?)
    xFromCenter = [gestureRecognizer translationInView:self].x; //%%% positive for right swipe, negative for left
    yFromCenter = [gestureRecognizer translationInView:self].y; //%%% positive for up, negative for down
    
    //%%% checks what state the gesture is in. (are you just starting, letting go, or in the middle of a swipe?)
    switch (gestureRecognizer.state) {
            //%%% just started swiping
        case UIGestureRecognizerStateBegan:{
            self.originalPoint = self.center;
            break;
        };
            //%%% in the middle of a swipe
        case UIGestureRecognizerStateChanged:{
            //%%% dictates rotation (see ROTATION_MAX and ROTATION_STRENGTH for details)
            CGFloat rotationStrength = MIN(xFromCenter / ROTATION_STRENGTH, ROTATION_MAX);
            
            //%%% degree change in radians
            CGFloat rotationAngel = (CGFloat) (ROTATION_ANGLE * rotationStrength);
            
            //%%% amount the height changes when you move the user up to a certain point
            CGFloat scale = MAX(1 - fabsf(rotationStrength) / SCALE_STRENGTH, SCALE_MAX);
            
            //%%% move the object's center by center + gesture coordinate
            self.center = CGPointMake(self.originalPoint.x + xFromCenter, self.originalPoint.y + yFromCenter);
            
            //%%% rotate by certain amount
            CGAffineTransform transform = CGAffineTransformMakeRotation(rotationAngel);
            
            //%%% scale by certain amount
            CGAffineTransform scaleTransform = CGAffineTransformScale(transform, scale, scale);
            
            //%%% apply transformations
            self.transform = scaleTransform;
            [self updateOverlay:xFromCenter];
            
            break;
        };
            //%%% let go of the user
        case UIGestureRecognizerStateEnded: {
            menuButton.hidden=YES;
            messageButton.hidden=YES;
            [self afterSwipeAction];
            break;
        };
        case UIGestureRecognizerStatePossible:break;
        case UIGestureRecognizerStateCancelled:break;
        case UIGestureRecognizerStateFailed:break;
    }
}
-(void)beingTapped:(UITapGestureRecognizer *)gesture
{
    [delegate userTapped:self];
}
//%%% checks to see if you are moving right or left and applies the correct overlay image
-(void)updateOverlay:(CGFloat)distance
{
       if (distance > 0) {
        overlayView.mode = GGOverlayViewModeRight;
        
        
    } else {
        overlayView.mode = GGOverlayViewModeLeft;
        
    }
    
    overlayView.alpha = MIN(fabsf(distance)/100, 0.4);
}

//%%% called when the user is let go
- (void)afterSwipeAction
{
    if (xFromCenter > ACTION_MARGIN) {
        [self rightAction];
    } else if (xFromCenter < -ACTION_MARGIN) {
        [self leftAction];
    } else { //%%% resets the user
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.center = self.originalPoint;
                             self.transform = CGAffineTransformMakeRotation(0);
                             overlayView.alpha = 0;
                         }];
    }
}

//%%% called when a swipe exceeds the ACTION_MARGIN to the right
-(void)rightAction
{
    CGPoint finishPoint = CGPointMake(500, 2*yFromCenter +self.originalPoint.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate userSwipedRight:self];
    
    NSLog(@"YES");
}

//%%% called when a swip exceeds the ACTION_MARGIN to the left
-(void)leftAction
{
    CGPoint finishPoint = CGPointMake(-500, 2*yFromCenter +self.originalPoint.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate userSwipedLeft:self];
    
    NSLog(@"NO");
}

-(void)rightClickAction
{
    CGPoint finishPoint = CGPointMake(600, self.center.y);
    [UIView animateWithDuration:0.8
                     animations:^{
                         self.center = finishPoint;
                         self.transform = CGAffineTransformMakeRotation(1);
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate userSwipedRight:self];
    
    NSLog(@"YES");
}

-(void)leftClickAction
{
    CGPoint finishPoint = CGPointMake(-600, self.center.y);
    [UIView animateWithDuration:0.8
                     animations:^{
                         self.center = finishPoint;
                         self.transform = CGAffineTransformMakeRotation(-1);
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate userSwipedLeft:self];
    
    NSLog(@"NO");
}



@end
