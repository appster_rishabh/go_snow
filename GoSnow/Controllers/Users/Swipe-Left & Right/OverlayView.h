//
//  OverlayView.h
//  testing swiping
//
//  Created by Richard Kim on 5/22/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//
//  @cwRichardKim for updates and requests

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger , GGOverlayViewMode) {
    GGOverlayViewModeLeft,
    GGOverlayViewModeRight
};
typedef NS_ENUM(NSUInteger , GGOverlayViewFromScreenMode) {
    GGOverlayViewModeSuggestedScreen,
    GGOverlayViewModeGroupScreen,
    GGOverlayViewModeGroupRequestScreen,
    GGOverlayViewRequestScreen
};

@interface OverlayView : UIView
{
     BOOL isSuggestedUser;
}

- (id)initWithFrame:(CGRect)frame screenMode:(GGOverlayViewFromScreenMode)screenMode;
@property (nonatomic) GGOverlayViewMode mode;
@property (nonatomic) GGOverlayViewFromScreenMode screenMode;
@property (nonatomic, strong) UIImageView *imageView;

@end
