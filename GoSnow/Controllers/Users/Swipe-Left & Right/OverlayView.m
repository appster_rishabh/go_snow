//
//  OverlayView.m
//  testing swiping
//
//  Created by Richard Kim on 5/22/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//
//  @cwRichardKim for updates and requests

#import "OverlayView.h"

@implementation OverlayView
@synthesize imageView;

#pragma mark - Intialization

- (id)initWithFrame:(CGRect)frame screenMode:(GGOverlayViewFromScreenMode)screenMode
{
    self = [super initWithFrame:frame];
    if (self) {
        //isSuggestedUser=isSuggestedUsers;
        if (screenMode==GGOverlayViewModeGroupScreen || screenMode==GGOverlayViewModeGroupRequestScreen) {
            imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"decline"]];
        }
        else if (screenMode==GGOverlayViewModeSuggestedScreen) {
            imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Later_icon"]];
        }
        else
        {
            imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"decline"]];
        }
       // imageView.backgroundColor=[UIColor grayColor];
        [self addSubview:imageView];
    }
    return self;
}
-(void)setscreenMode:(GGOverlayViewFromScreenMode)mode
{
    if (_screenMode==GGOverlayViewModeGroupScreen  || _screenMode==GGOverlayViewModeGroupRequestScreen) {
        imageView.image=[UIImage imageNamed:@"decline"];
    }
    else if (_screenMode==GGOverlayViewModeSuggestedScreen) {
        imageView.image=[UIImage imageNamed:@"Later_icon"];
    }
    else
    {
        imageView.image=[UIImage imageNamed:@"decline"];
    }
 
}
-(void)setMode:(GGOverlayViewMode)mode
{
    if (_mode == mode) {
        return;
    }
    
    _mode = mode;
    
    if (_screenMode==GGOverlayViewModeSuggestedScreen) {
        if(mode == GGOverlayViewModeLeft) {
                            imageView.image = [UIImage imageNamed:@"Later_icon.png"];
            
            
        } else {
            
                imageView.image = [UIImage imageNamed:@"Ride_icon.png"];
           
        }
 
    }
    else if (_screenMode==GGOverlayViewModeGroupScreen || _screenMode==GGOverlayViewModeGroupRequestScreen)
    {
        if(mode == GGOverlayViewModeLeft) {
           
                imageView.image = [UIImage imageNamed:@"decline.png"];
            
        } else {
            
                imageView.image = [UIImage imageNamed:@"accept"];
        }

    }
    else
    {
        if(mode == GGOverlayViewModeLeft) {
          
                imageView.image = [UIImage imageNamed:@"decline"];
            
        } else {
            
            imageView.image = [UIImage imageNamed:@"accept"];
        }

    }
    
    }

-(void)layoutSubviews
{
    [super layoutSubviews];
    imageView.frame = self.bounds;
    imageView.center=CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
}


@end
