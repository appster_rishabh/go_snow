//
//  DraggableView.h
//  testing swiping
//
//  Created by Richard Kim on 5/21/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//
//  @cwRichardKim for updates and requests

/*
 
 Copyright (c) 2014 Choong-Won Richard Kim <cwrichardkim@gmail.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 */

#import <UIKit/UIKit.h>
#import "OverlayView.h"
//code review use proper naming convention we are not using cards in all the functions
//typedef NS_ENUM(NSUInteger , GGdragableFromScreenMode) {
//    GGdragableViewModeSuggestedScreen,
//    GGdragableViewModeGroupScreen,
//    GGdragableViewRequest
//};
@protocol DraggableViewDelegate <NSObject>

-(void)userSwipedLeft:(UIView *)user;
-(void)userSwipedRight:(UIView *)user;
-(void)userTapped:(UIView *)user;
//-(void)userSwipedRight:(UIView *)user;

@end

@interface DraggableView : UIView


@property (weak) id <DraggableViewDelegate> delegate;
//@property (nonatomic) GGdragableFromScreenMode screenMode;
@property (nonatomic, strong)UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic, strong)UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic)CGPoint originalPoint;
@property (nonatomic,strong)OverlayView* overlayView;
@property (nonatomic,strong)UIImageView* userImageView; //%%% a placeholder for any user-specific information
- (id)initWithFrame:(CGRect)frame screenMode:(GGOverlayViewFromScreenMode)screenMode;
-(void)leftClickAction;
-(void)rightClickAction;

@end
