//
//  RequestViewController.h
//  GoSnow
//
//  Created by Madhavi Yalamaddi on 09/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestDraggableView.h"
#import "RESideMenu.h"
#import "RiderManager.h"

@interface RequestViewController : UIViewController <RequestSwipeDelegate,RiderManagerDelegate>
{
    NSInteger currentIndex;
    UserInfoEntity *userInfoEntity;
    NSMutableArray *requestedUsers;
}
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *requestsLbl;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (weak, nonatomic) IBOutlet UILabel *sendRequestLabel;
@property (weak, nonatomic) IBOutlet UIButton *sharedFriendCountButton;
@property (weak, nonatomic) IBOutlet UIButton *sharedInterestCountButton;
@property (weak, nonatomic) IBOutlet UIButton *sharedLocationCountButton;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UIView *noUserFoundView;
@property (weak, nonatomic) IBOutlet RequestDraggableView *requestDraggableView;
//code review strong propert not required
@end
