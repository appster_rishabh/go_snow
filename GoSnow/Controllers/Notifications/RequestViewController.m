//
//  RequestViewController.m
//  GoSnow
//
//  Created by Madhavi Yalamaddi on 09/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "RequestViewController.h"
#import "RiderManager.h"
#import "RequestUsers.h"
#import <QuartzCore/QuartzCore.h>
#import "ViewUserProfileViewController.h"
#import "UIImage+ImageShape.h"
@interface RequestViewController ()

@end

@implementation RequestViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    [self initialSetUp];
    
    
}
#pragma mark viewWillAppear method
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(swipeRightAfterDelay) name:kAcceptRequest object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(swipeLeftAfterDelay) name:kDeclineRequest object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenuIconByNotification) name:KNotificationRecieved object:nil];
    //[self ad];
}
#pragma mark viewWillDisappear method
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:KNotificationRecieved object:nil];
}
#pragma mark updateMenuIconByNotification method
-(void)updateMenuIconByNotification
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    [menuIcon setImage:[UIImage imageNamed:@"menu_white_dot"] forState:UIControlStateNormal];
}
#pragma mark updateMenuIcon method
-(void)updateMenuIcon
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    int notificationCount=[UserInfoEntity fetchPendingRequestsAndMessagesCountsinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    if (notificationCount>0) {
        [menuIcon setImage:[UIImage imageNamed:@"menu_white_dot"] forState:UIControlStateNormal];
    }
    else
    {
        [menuIcon setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    }
}

#pragma mark update messagesCount
-(void)updateRequestCount
{
    [UserInfoEntity updateRequestAndmessagesCounts:@"1" messagesCounts:@"" tripRequestCounts:@""  inManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}
#pragma mark initial setup
-(void)initialSetUp
{
    [self.navigationController.navigationBar setHidden:YES];
    [self updateMenuIcon];
    requestedUsers=[[NSMutableArray alloc]init];

    self.requestDraggableView.delegate=self;
    currentIndex=0;
    _sharedFriendCountButton.layer.cornerRadius=_sharedFriendCountButton.frame.size.height/2;
    _sharedLocationCountButton.layer.cornerRadius=_sharedLocationCountButton.frame.size.height/2;
    _sharedInterestCountButton.layer.cornerRadius=_sharedLocationCountButton.frame.size.height/2;
    
    [self getRequestsFromOtherRiders];
}

#pragma mark - Accept Request Web Service
-(void)acceptRideRequest:(NSInteger)index
{
    NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
    RequestUsers *userInfo=[requestedUsers objectAtIndex:index];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:userInfo.user_id forKey:@"contactId"];
    [[RiderManager sharedInstance] CallWebService:params methodName:@"contact/accept"];
}

#pragma mark  - Accept Request Web Service Response
- (void)riderRequestAcceptedSuccessfully:(NSMutableDictionary *)dictionary
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Request accepted." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Reject Requests Web Service
-(void)rejectRideRequest:(NSInteger)index
{
    NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
    RequestUsers *userInfo=[requestedUsers objectAtIndex:index];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:userInfo.user_id forKey:@"contactId"];
    [[RiderManager sharedInstance] CallWebService:params methodName:@"contact/reject"];
}

#pragma mark  - Reject Request Web Service Response
- (void)riderRequestRejectedSuccessfully:(NSMutableDictionary *)dictionary
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GoSnow" message:@"Request rejected." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark  - Get Requests Web Service
-(void)getRequestsFromOtherRiders
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [RiderManager sharedInstance].delegate=self;
    NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    [params setObject:@"0" forKeyedSubscript:@"status"];
    [[RiderManager sharedInstance] CallWebService:params methodName:@"contact/getByReceiverId"];
}

#pragma mark  - Get Requests Web Service Response
- (void)getRequestSuccessfully:(NSMutableArray *)requestArray
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [requestedUsers removeAllObjects];
    [requestedUsers addObjectsFromArray:requestArray];
    if ([requestArray count]>0) {
        self.requestsLbl.text=[NSString stringWithFormat:@"REQUESTS (%lu)",(unsigned long)[requestArray count]];
        [self updateUserNameFirst:0];
        [self displayAllElement];
    }
    else
    {
        //self.requestsLbl.text=@"REQUESTS (0)";
        _noUserFoundView.hidden=NO;
        [self hideAllElement];
    }
    
    [self.requestDraggableView SetUpInitials:requestedUsers];
}

#pragma mark  - Web Service Response Failed
- (void)requestDidFailed:(NSMutableDictionary *)errorDictionary
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    _noUserFoundView.hidden=NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[errorDictionary valueForKey:@"Error"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];

}
 // Code review use appropraite naming convention
#pragma mark - Swipe Actions
-(void)userSwipedLeftAction:(UIView *)user
{
    [self updateUserName:user.tag-400];
    currentIndex=user.tag-400;
    [self updateRequestCount];
    [self rejectRideRequest:currentIndex];
}

-(void)userSwipedRightAction:(UIView *)user
{
    [self updateUserName:user.tag-400];
    currentIndex=user.tag-400;
     [self updateRequestCount];
    [self acceptRideRequest:currentIndex];
}
-(void)userTappedAction:(UIView *)user
{
    [self myProfile:user.tag-400];
}
#pragma mark Go to my profile
-(void)myProfile:(NSInteger)tag
{
    RequestUsers *userInfo=[requestedUsers objectAtIndex:tag];
    
    ViewUserProfileViewController *viewUserProfileViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"ViewUserProfileViewController"];
    viewUserProfileViewController.fromScreenType=RequestRiderScreenType;
    viewUserProfileViewController.isFromRequestScreen=YES;
    viewUserProfileViewController.otherUsersObject=[self getRequestedUserInfo:userInfo];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromTop;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:viewUserProfileViewController animated:NO];
}

-(Users *)getRequestedUserInfo:(RequestUsers *)requestUsers
{
    Users *userInfo=[[Users alloc] init];
    userInfo.firstName=requestUsers.firstName;
    userInfo.lastName=requestUsers.lastName;
    userInfo.aboutMe=requestUsers.aboutMe;
    userInfo.age=requestUsers.age;
    userInfo.userPhotos=requestUsers.userPhotos;
    userInfo.skillLevel=requestUsers.skillLevel;
    userInfo.riderType=requestUsers.riderType;
    userInfo.skillLevelID=requestUsers.skillLevelID;
    userInfo.plannedTripName=requestUsers.plannedTripName;
    userInfo.currentLocation=requestUsers.currentLocation;
    userInfo.latitude=requestUsers.latitude;
    userInfo.longitude=requestUsers.longtitude;
    userInfo.address=requestUsers.address;
    userInfo.distance=requestUsers.distance;
    userInfo.contactsName=userInfoEntity.facebookFriends;
    userInfo.locationName=userInfoEntity.locationName;
    userInfo.interestName=userInfoEntity.interestName;
    
    return userInfo;
    
}
#pragma mark - Update User Name
-(void)updateUserNameFirst:(NSInteger)index
{
    UIImageView *noUser=(UIImageView *)[self.view viewWithTag:600];
    if (index+1>[requestedUsers count]) {
        noUser.hidden=NO;
      //  self.requestsLbl.text=@"REQUESTS (0)";
        [_locationButton setTitle:@"NO REQUEST FOUND" forState:UIControlStateNormal];
        
        [self hideAllElement];
        
        return;
    }
    RequestUsers *userInfo=[requestedUsers objectAtIndex:index];
    
    NSString *usersName;
    if (userInfo.firstName.length>0 && userInfo.lastName.length>0)
        usersName=[NSString stringWithFormat:@"%@ %@",userInfo.firstName,userInfo.lastName];
    else
        usersName=[NSString stringWithFormat:@"%@",userInfo.firstName];
    
    if (userInfo.age.length>0) {
        usersName= [usersName stringByAppendingString:[NSString stringWithFormat:@" (%@)",userInfo.age]];
    }
    [self updateLocation:userInfo button:_locationButton];
    [_sharedFriendCountButton setTitle:userInfo.friendCount forState:UIControlStateNormal];
    [_sharedInterestCountButton setTitle:userInfo.interestCount forState:UIControlStateNormal];
    [_sharedLocationCountButton setTitle:userInfo.locationCount forState:UIControlStateNormal];
    self.userNameLbl.text=usersName;
}
-(void)updateUserName:(NSInteger)index
{
    UIImageView *noUser=(UIImageView *)[self.view viewWithTag:600];
    if (index+1>=[requestedUsers count]) {
        noUser.hidden=NO;
       // self.requestsLbl.text=@"REQUESTS (0)";
        [_locationButton setTitle:@"NO REQUEST FOUND" forState:UIControlStateNormal];
        
        [self hideAllElement];
        
        return;
        
    }
    RequestUsers *userInfo=[requestedUsers objectAtIndex:index+1];
    
    NSString *usersName;
    if (userInfo.firstName.length>0 && userInfo.lastName.length>0)
        usersName=[NSString stringWithFormat:@"%@ %@",userInfo.firstName,userInfo.lastName];
    else
        usersName=[NSString stringWithFormat:@"%@",userInfo.firstName];
    
    if (userInfo.age.length>0) {
        usersName= [usersName stringByAppendingString:[NSString stringWithFormat:@" (%@)",userInfo.age]];
    }
    [self updateLocation:userInfo button:_locationButton];
    [_sharedFriendCountButton setTitle:userInfo.friendCount forState:UIControlStateNormal];
    [_sharedInterestCountButton setTitle:userInfo.interestCount forState:UIControlStateNormal];
    [_sharedLocationCountButton setTitle:userInfo.locationCount forState:UIControlStateNormal];
    self.userNameLbl.text=usersName;
}

#pragma mark - Display All Information
-(void)displayAllElement
{
    self.requestsLbl.hidden=NO;
    self.userNameLbl.hidden=NO;
    self.sendRequestLabel.hidden=NO;
    self.locationButton.hidden=NO;
    self.requestDraggableView.hidden=NO;
    _noUserFoundView.hidden=YES;
}
-(void)hideAllElement
{
    self.requestsLbl.hidden=NO;
    self.userNameLbl.hidden=YES;
    self.sendRequestLabel.hidden=YES;
    self.locationButton.hidden=YES;
    self.requestDraggableView.hidden=YES;
    _noUserFoundView.hidden=NO;
    [self updateMenuIcon];
}
#pragma mark - Get Current and Destination Names
-(void )updateLocation:(RequestUsers *)userInfo button:(UIButton *)locationButton
{
    //Users *userInfo=[_suggestedUsers objectAtIndex:index];
    NSString *myLocationString=nil;
    if (userInfo.address.length>0) {
        myLocationString=[NSString stringWithFormat:@"I'M IN %@",userInfo.address];
    }
    if (userInfo.plannedTripName.length>0) {
        if (myLocationString.length>0) {
            myLocationString=[myLocationString stringByAppendingString:[NSString stringWithFormat:@", NEXT TRIP IS %@",userInfo.plannedTripName]];
        }
        else
        {
            myLocationString=[myLocationString stringByAppendingString:[NSString stringWithFormat:@"NEXT TRIP IS %@",userInfo.plannedTripName]];
        }
    }
    if (myLocationString.length>0)
    {
        myLocationString=[myLocationString uppercaseString];
        CGSize locationStringSize=[self getSizeForTextForLocation:myLocationString font:[UIFont fontWithName:@"PassionOne-Bold" size:12.0f]];
        if (locationStringSize.width>195)
        {
            _locationButton.frame=CGRectMake(_locationButton.frame.origin.x, _locationButton.frame.origin.y, locationStringSize.width+20, 23);
            _locationButton.center=CGPointMake(self.view.frame.size.width/2, _locationButton.frame.origin.y+(23/2));
            [_locationButton setBackgroundImage:[UIImage CreateAndReturnImageRounded:@"#1B79E9" outline:@"Fill" rect:_locationButton.frame] forState:UIControlStateNormal];
            [_locationButton setTitle:myLocationString forState:UIControlStateNormal];
        }
        else
        {
            [_locationButton setBackgroundImage:[UIImage CreateAndReturnImageRounded:@"#1B79E9" outline:@"Fill" rect:_locationButton.frame] forState:UIControlStateNormal];
            
            [_locationButton setTitle:myLocationString forState:UIControlStateNormal];
        }
        
    }
    
    // return myLocationString;
}

#pragma mark notification action
-(void)swipeLeftAfterDelay
{
    [self performSelector:@selector(swipeLeft) withObject:nil afterDelay:0.3];
}
-(void)swipeRightAfterDelay
{
    [self performSelector:@selector(swipeRight) withObject:nil afterDelay:0.3];
}
-(void)swipeLeft
{
    [_requestDraggableView swiptLeftFromProfile];
}
-(void)swipeRight
{
    [_requestDraggableView swiptRightFromProfile];
}
#pragma mark get text width
- (CGSize)getSizeForTextForLocation:(NSString *)text font:(UIFont *)font  {
    NSArray *arr=[NSArray arrayWithObject:text];
    CGSize DXsize = [[arr objectAtIndex:0] sizeWithFont:font constrainedToSize:CGSizeMake(WIDTH, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    //CGSize stringSize = frame.size;
    
    return DXsize;
}

@end
