//
//  RequestDraggableView.m
//  GoSnow
//
//  Created by Rishabh on 11/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "RequestDraggableView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "RequestUsers.h"

//code review rather than wring macros in every class you should make one constatnts class which will have all the constatnats you are using within the app
@implementation RequestDraggableView{
    NSInteger usersLoadedIndex;
    NSMutableArray *loadedusers;
    
    UIButton* menuButton;
    UIButton* messageButton;
    UIButton* checkButton;
    UIButton* xButton;
    
    IBOutlet UIButton *sharedFriendButton;
    IBOutlet UIButton *sharedInterestButton;
    IBOutlet UIButton *sharedLocationButton;
    IBOutlet UILabel *sharedfriendLabel;
    IBOutlet UILabel *sharedInterestLabel;
    IBOutlet UILabel *sharedLocationLabel;
  
    UIButton *mayBeLaterButton;
    UIButton *letUsRideButton;
    UIButton *infoButton;
   
    UILabel *mayBeLaterLabel;
    UILabel *rideTogetherLabel;
}

static const int MAX_BUFFER_SIZE = 2;

@synthesize userRequestArray;
@synthesize allUsers;

#pragma mark -  Intial Set Up
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [super layoutSubviews];
       //code review --we are already allocating this in setupinitials
    }
    return self;
}

-(void)SetUpInitials:(NSMutableArray *)array
{
    userRequestArray=array;
    loadedusers = [[NSMutableArray alloc] init];
    allUsers = [[NSMutableArray alloc] init];
    usersLoadedIndex = 0;
    UIImageView *avatar=[[UIImageView alloc] initWithFrame:self.bounds];
    [avatar setImage:[UIImage imageNamed:@"no_avatar@2x"]];
   
    [self loadusers];
   
}

#pragma mark -  Create Draggable View
-(DraggableView *)createDraggableViewWithDataAtIndex:(NSInteger)index
{
    UIImage *noAvatar = [UIImage imageNamed:@"no_avatar"];
    DraggableView *draggableView = [[DraggableView alloc]initWithFrame:CGRectMake(0, 0, noAvatar.size.width-80, noAvatar.size.height-80) screenMode:GGOverlayViewRequestScreen];
    draggableView.overlayView.screenMode=GGOverlayViewRequestScreen;
    draggableView.center=CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    draggableView.delegate = self;
    draggableView.backgroundColor=[UIColor greenColor];
    draggableView.tag=index+400;
    RequestUsers *userinfo=[userRequestArray objectAtIndex:index];
    if (userinfo.userPhotos.length<1) {
        [draggableView.userImageView setImage:[UIImage imageNamed:@"no_avatar"]];
        return draggableView;
    }
    NSArray *userPhotos=[userinfo.userPhotos componentsSeparatedByString:@";"];
    if ([userPhotos count]>0)
        [draggableView.userImageView setImageWithURL:[NSURL URLWithString:[userPhotos objectAtIndex:0]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
           else
        [draggableView.userImageView setImage:[UIImage imageNamed:@"no_avatar"]];
    
    return draggableView;
}

#pragma mark -  user Load/ Swipe Left/Right Methods
-(void)loadusers
{
    if([userRequestArray count] > 0) {
        NSInteger numLoadedusersCap =(([userRequestArray count] > MAX_BUFFER_SIZE)?MAX_BUFFER_SIZE:[userRequestArray count]);
   
        for (int i = 0; i<[userRequestArray count]; i++) {
            DraggableView* newuser = [self createDraggableViewWithDataAtIndex:i];
            newuser.backgroundColor=[UIColor clearColor];
            [allUsers addObject:newuser];
            
            if (i<numLoadedusersCap) {
                [loadedusers addObject:newuser];
            }
        }
        
        for (int i = 0; i<[loadedusers count]; i++) {
            if (i>0) {
                [self insertSubview:[loadedusers objectAtIndex:i] belowSubview:[loadedusers objectAtIndex:i-1]];
            } else {
                [self addSubview:[loadedusers objectAtIndex:i]];
            }
            usersLoadedIndex++;
        }
    }
}

-(void)userSwipedLeft:(UIView *)user;
{
    [loadedusers removeObjectAtIndex:0];
    if (usersLoadedIndex < [allUsers count]) {
        [loadedusers addObject:[allUsers objectAtIndex:usersLoadedIndex]];
        usersLoadedIndex++;
        [self insertSubview:[loadedusers objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedusers objectAtIndex:(MAX_BUFFER_SIZE-2)]];
    }
    [_delegate userSwipedLeftAction:user];
}

-(void)userSwipedRight:(UIView *)user
{
    [loadedusers removeObjectAtIndex:0];
    if (usersLoadedIndex < [allUsers count]) {
        [loadedusers addObject:[allUsers objectAtIndex:usersLoadedIndex]];
        usersLoadedIndex++;
        [self insertSubview:[loadedusers objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedusers objectAtIndex:(MAX_BUFFER_SIZE-2)]];
    }
    [_delegate userSwipedRightAction:user];
}
-(void)userTapped:(UIView *)user
{
    [_delegate userTappedAction:user];
}

#pragma mark -  IBActions
-(IBAction)swipeRightAction:(id)sender
{
    [self swiptRightFromProfile];
}

-(void)swiptRightFromProfile
{
    DraggableView *dragView = [loadedusers firstObject];
    dragView.overlayView.mode = GGOverlayViewModeRight;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView rightClickAction];
}

-(IBAction)swipeLeftAction:(id)sender
{
    [self swiptLeftFromProfile];
}

-(void)swiptLeftFromProfile
{
    DraggableView *dragView = [loadedusers firstObject];
    dragView.overlayView.mode = GGOverlayViewModeLeft;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView leftClickAction];
}

-(IBAction)profileInformationAction:(id)sender
{
    DraggableView *dragView = [loadedusers firstObject];
    [_delegate userTappedAction:dragView];
}


@end