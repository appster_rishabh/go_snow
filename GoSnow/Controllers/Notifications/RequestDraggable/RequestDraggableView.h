//
//  RequestDraggableView.h
//  GoSnow
//
//  Created by Rishabh on 11/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DraggableView.h"
//code review use proper naming convention we are not using cards in all the functions

@protocol RequestSwipeDelegate <NSObject>

@optional

-(void)userSwipedLeftAction:(UIView *)user;
-(void)userSwipedRightAction:(UIView *)user;
-(void)userTappedAction:(UIView *)user;
@end

@interface RequestDraggableView : UIView <DraggableViewDelegate>

-(void)SetUpInitials:(NSMutableArray *)array;
-(void)userSwipedLeft:(UIView *)user;
-(void)userSwipedRight:(UIView *)user;

-(IBAction)swipeRightAction:(id)sender;
-(IBAction)swipeLeftAction:(id)sender;
-(IBAction)profileInformationAction:(id)sender;

-(void)swiptLeftFromProfile;
-(void)swiptRightFromProfile;

@property (nonatomic, weak) id<RequestSwipeDelegate> delegate;
@property (copy,nonatomic)NSArray *userRequestArray;
@property (retain,nonatomic)NSMutableArray *allUsers;


@end
