//
//  AboutUsViewController.h
//  GoSnow
//
//  Created by Madhavi Yalamaddi on 23/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsViewController : UIViewController

@property (weak,nonatomic) IBOutlet UITextView *aboutUsTxtView;

- (IBAction)backButtonAction;

@end
