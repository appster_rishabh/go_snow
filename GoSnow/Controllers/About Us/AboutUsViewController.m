//
//  AboutUsViewController.m
//  GoSnow
//
//  Created by Madhavi Yalamaddi on 23/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "AboutUsViewController.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark- IBActions
-(IBAction)backButtonAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
