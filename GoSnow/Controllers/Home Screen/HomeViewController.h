//
//  HomeViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 18/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LNBRippleEffect.h"
#import "RESideMenu.h"
#import "SuggestedUsers.h"
#import "Users.h"
#import "UIButton+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "FacebookManager.h"

@interface HomeViewController : UIViewController<LocationManagerDelegate,RESideMenuDelegate,SuggestedUsersDelegate>
{
    CGPoint centerPoint;
    NSMutableArray *suggestedUsers;
    UserInfoEntity *userInfoEntity;
}

@property (nonatomic, weak) IBOutlet UIView *noUserFoundView;
@property (nonatomic, weak) IBOutlet UIButton *nearByPeaople;
@property (nonatomic, weak) IBOutlet LNBRippleEffect *rippleEffect;

-(IBAction)visitProfileAction:(id)sender;
-(IBAction)refreshRidersListAction:(id)sender;

@end
