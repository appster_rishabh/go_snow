//
//  HomeViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 18/02/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "HomeViewController.h"
#import "LNBRippleEffect.h"
#import "UsersViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.navigationController.navigationBar setHidden:YES];
    
    suggestedUsers=[[NSMutableArray alloc] init];
    
    [[Location sharedInstance] callLocationManager];
    [[Location sharedInstance] setDelegate:self];

    [self updateMenuIcon];
    [self setUpInitials];

}

#pragma mark viewWillAppear method
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   
    [self getAllSuggestedRiders];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenuIconByNotification) name:KNotificationRecieved object:nil];
}

#pragma mark viewWillDisappear method
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KNotificationRecieved object:nil];
}
#pragma mark updateMenuIconByNotification method
-(void)updateMenuIconByNotification
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    [menuIcon setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
}
#pragma mark updateMenuIcon method
-(void)updateMenuIcon
{
    UIButton *menuIcon=(UIButton *)[self.view viewWithTag:3001];
    int notificationCount=[UserInfoEntity fetchPendingRequestsAndMessagesCountsinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
    if (notificationCount>0)
        [menuIcon setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
    else
        [menuIcon setImage:[UIImage imageNamed:@"menuSetting"] forState:UIControlStateNormal];
}
#pragma mark - Set Up Initial Information
-(void)setUpInitials
{
    _nearByPeaople.hidden=YES;
    
    [self fetchUserInformationFromCoreData];
    
    UIImageView *userImage=(UIImageView *)[self.view viewWithTag:601];
    UIImageView *circleImageView=(UIImageView *)[self.view viewWithTag:602];
    userImage.layer.cornerRadius=25;
    userImage.layer.masksToBounds=YES;
    circleImageView.hidden=YES;
    if (userInfoEntity.profileThumb.length<1) {
        [userImage setImage:[UIImage imageNamed:@"smallProfile"]];
    }
    else
    {
        [userImage setImageWithURL:[NSURL URLWithString:userInfoEntity.profileThumb] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }

        [self startRippleEffect];
}

#pragma mark - Fetch Basic Information From CoreDate
-(void)fetchUserInformationFromCoreData
{
    userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
}

#pragma mark - IBActions
-(IBAction)refreshRidersListAction:(id)sender
{
    [self initiallyHideAllSuggestedRiders];
    [self hideSuggestedRiders:YES rippleIsHidded:NO userImage:NO circleView:YES];
    [self getAllSuggestedRiders];
}

-(IBAction)visitProfileAction:(id)sender
{
    UIStoryboard *storyboardObject = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIView *view=(UIView *)[sender superview];
    UsersViewController *usersViewController =
    [storyboardObject instantiateViewControllerWithIdentifier:@"UsersViewController"];
    usersViewController.suggestedUsers=[self manageRiders:view.tag-300];
    usersViewController.isFromGroupTrip = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self.navigationController pushViewController:usersViewController animated:YES];
}

#pragma mark Location Delegates
-(void)gotLocationSuccessfully:(NSString *)latitudeValue longitude:(NSString *)longitudeValue
{
    [[Location sharedInstance] stopUpdateLocation];
}

-(void)gotErrorInFindingLocation
{
    
}

#pragma mark - Fetch Information Delegates
-(void)fetchUsersSuccessfully:(NSMutableArray*)successArray
{
    [suggestedUsers removeAllObjects];
    [suggestedUsers addObjectsFromArray:successArray];
    if ([suggestedUsers count]>0) {
        UIImageView *circleImageView=(UIImageView *)[self.view viewWithTag:602];
        circleImageView.hidden=NO;
        [self displaySuggestedRiders];
        [self hideSuggestedRiders:YES rippleIsHidded:YES userImage:NO circleView:NO];
    }
    else
    {
        [self hideSuggestedRiders:NO rippleIsHidded:YES userImage:YES circleView:YES];
    }
}

-(void)failedWithError:(NSDictionary*)errorDictionary
{
    [self hideSuggestedRiders:NO rippleIsHidded:YES userImage:YES circleView:YES];
    _rippleEffect.hidden=YES;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[errorDictionary valueForKey:@"Error"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Ripple Effect Method
-(void)startRippleEffect
{
    [_rippleEffect drawImageWithFrame:[UIImage imageNamed:@""] Frame:_rippleEffect.frame Color:[UIColor colorWithRed:(28.0/255.0) green:(212.0/255.0) blue:(255.0/255.0) alpha:1]];
    
    [_rippleEffect setRippleColor:[UIColor colorWithRed:(28.0/255.0) green:(212.0/255.0) blue:(255.0/255.0) alpha:1]];
    [_rippleEffect setRippleTrailColor:[UIColor colorWithRed:(28.0/255.0) green:(212.0/255.0) blue:(255.0/255.0) alpha:0.5]];
}

#pragma mark - Ripple Effect Method
-(void)getAllSuggestedRiders
{
    [[SuggestedUsers sharedInstance].totalUsers removeAllObjects];
    [SuggestedUsers sharedInstance].delegate=self;
    [[SuggestedUsers sharedInstance] WebSericeCallForSuggestedRiders:@"user/listUserTripByDistance" sessionId:userInfoEntity.sessionId ];  
}

#pragma mark - Ripple Effect Method
-(NSMutableArray *)manageRiders:(NSInteger)index
{
    [[SuggestedUsers sharedInstance].totalUsers exchangeObjectAtIndex:0 withObjectAtIndex:index-1];
    return [SuggestedUsers sharedInstance].totalUsers;
}

#pragma mark - Hide Suggested Riders
-(void)initiallyHideAllSuggestedRiders
{
    for (int i=0; i<12; i++)
    {
        UIView *userView=(UIView *)[self.view viewWithTag:i+301];
        userView.hidden=YES;
    }
}

-(void)hideSuggestedRiders:(BOOL)isHide rippleIsHidded:(BOOL)isRippleHide userImage:(BOOL)isImageHide circleView:(BOOL)isCirclehidden
{
    UIImageView *circleImageView=(UIImageView *)[self.view viewWithTag:602];
    UIImageView *userImage=(UIImageView *)[self.view viewWithTag:601];
    _noUserFoundView.hidden=isHide;
    _rippleEffect.hidden=isRippleHide;
    circleImageView.hidden=isCirclehidden;
    userImage.hidden=isImageHide;
}

#pragma mark - Display Suggested Riders
-(void)displaySuggestedRiders
{
    _nearByPeaople.hidden=NO;
    for (int i=0; i<[suggestedUsers count]; i++)
    {
        UIView *userView=(UIView *)[self.view viewWithTag:i+301];
        
        userView.hidden=NO;
        Users *userInfo=[suggestedUsers objectAtIndex:i];
        
        UIButton *userPhotoButton=[[userView subviews] objectAtIndex:0];
        userPhotoButton.layer.cornerRadius=25.0f;
        userPhotoButton.layer.masksToBounds=YES;
        userPhotoButton.userInteractionEnabled=YES;
        
        UILabel *nameLabel=[[userView subviews] objectAtIndex:1];
        
        if ([userInfo.model isEqualToString:@"trip"]) {
            nameLabel.text=@"Group";
            [userPhotoButton setImage:[UIImage imageNamed:@"Placeholder"] forState:UIControlStateNormal];
            
        }
        else
        {
           NSString *firstName= [userInfo.firstName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if (firstName.length==0 && userInfo.lastName.length>0)
                nameLabel.text=userInfo.lastName;
            else
                nameLabel.text=firstName;

        if (userInfo.userPhotos.length>0) {
            NSArray *arr=[userInfo.userPhotos componentsSeparatedByString:@";"];
            [self downLoadProfilePhoto:userPhotoButton url:[arr objectAtIndex:0]];
        }
        else
        {
            [userPhotoButton setImage:[UIImage imageNamed:@"smallProfile"] forState:UIControlStateNormal];
        }
        }
    }
    [_nearByPeaople setTitle:[NSString stringWithFormat:@"FOUND %lu PEOPLE NEAR YOU",(unsigned long)[suggestedUsers count]] forState:UIControlStateNormal];
}

#pragma mark - Download Suggested Riders Profile Photos
-(void)downLoadProfilePhoto:(UIButton *)buttonPhoto url:(NSString *)stringPhotoURL
{
    UIActivityIndicatorView  *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityIndicator.autoresizingMask = UIViewAutoresizingNone;
    activityIndicator.frame=buttonPhoto.bounds;
    [activityIndicator startAnimating];
    [buttonPhoto sd_setImageWithURL:[NSURL URLWithString:stringPhotoURL] forState:UIControlStateNormal
                   placeholderImage:nil
                          completed:^(UIImage *image , NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         [activityIndicator stopAnimating];
         [activityIndicator removeFromSuperview];
     }];
    [buttonPhoto addSubview:activityIndicator];
}

@end
