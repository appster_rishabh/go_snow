//
//  ContactUsViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 11/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "ContactUsViewController.h"
#import "AppDelegate.h"


@interface ContactUsViewController ()

@end

@implementation ContactUsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
//    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(100, 200, 100, 30)];
//    [btn addTarget:self action:@selector(sendMail:) forControlEvents:UIControlEventTouchUpInside];
//    btn.backgroundColor=[UIColor blueColor];
//    [self.view addSubview:btn];
}

#pragma mark - UIButton Actions

- (IBAction)backButtonAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendMailAction:(UIButton *)sender {
    [self sendMail:sender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sendMail:(id)sender{
    if  ([MFMailComposeViewController  canSendMail])
    {
        NSString *strToEmail = @"seanbellerby@outlook.com";
        if ([strToEmail length ] > 0 )
        {
            MFMailComposeViewController *mailer = [[ MFMailComposeViewController alloc ] init ];
            mailer. mailComposeDelegate = self ;
            [mailer setSubject: @"GoSnow:" ];
            NSString *strEmailId = strToEmail;
            NSArray *toRecipients = [ NSArray arrayWithObjects :strEmailId, nil ];
            [mailer setToRecipients:toRecipients];
            NSString *emailBody = @"GoSnow App For IOS";
            [mailer setMessageBody:emailBody isHTML:NO];
            [self presentViewController:mailer animated:YES completion:nil];
        }
    }
    else
    {
        UIAlertView *alertComposerSupport = [[UIAlertView alloc]initWithTitle: @"GoSnow" message : @"Email account is not configured in your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertComposerSupport show ];
    }
}

#pragma mark - mail compose delegate
- ( void )mailComposeController:( MFMailComposeViewController *)controller didFinishWithResult:( MFMailComposeResult )result error:( NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled :
            //NSLog ( @"Mail cancelled" );
            break ;
            
        case MFMailComposeResultSaved :
            //NSLog ( @"Mail saved" );
            break ;
            
        case MFMailComposeResultSent :
            NSLog ( @"Mail send" );
            break ;
            
        case MFMailComposeResultFailed :
            NSLog ( @"Mail failed" );
            break ;
            
        default :
            //NSLog ( @"Mail not sent" );
            break ;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
