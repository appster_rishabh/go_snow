//
//  TermsAndConditionsVC.m
//  GoSnow
//
//  Created by Varsha Singh on 04/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "TermsAndConditionsVC.h"

@interface TermsAndConditionsVC ()

@end

@implementation TermsAndConditionsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:htmlString baseURL:nil];
    [self.view addSubview:self.webView];
    
}

#pragma mark - UIButton Action

- (IBAction)backButtonAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
