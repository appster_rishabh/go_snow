//
//  TermsAndConditionsVC.h
//  GoSnow
//
//  Created by Varsha Singh on 04/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndConditionsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)backButtonAction;

@end
