//
//  SettingsViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 11/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "LoginViewController.h"
#import "UserProfile.h"

@interface SettingsViewController : UIViewController<MFMailComposeViewControllerDelegate,UIWebViewDelegate,UIActionSheetDelegate,ProfileUpdateDelegate>
{
   UserInfoEntity *userInfoEntity;
}

-(IBAction)settingsButtonAction:(id)sender;
-(IBAction)logOutButtonAction;

@end
