//
//  ContactUsViewController.h
//  GoSnow
//
//  Created by Varsha Singh on 11/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface ContactUsViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController *mailComposer;
}
- (IBAction)backButtonAction:(UIButton *)sender;
- (IBAction)sendMailAction:(UIButton *)sender;

@end
