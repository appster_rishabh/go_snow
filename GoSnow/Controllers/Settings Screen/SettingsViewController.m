//
//  SettingsViewController.m
//  GoSnow
//
//  Created by Varsha Singh on 11/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "SettingsViewController.h"
#import "ContactUsViewController.h"
#import "TermsAndConditionsVC.h"
#import "LeftSideViewController.h"
#import "AboutUsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
    [self fetchUserInfoFromCoreData];

}
#pragma mark fetc user info from database
 -(void)fetchUserInfoFromCoreData
    {
   userInfoEntity=[UserInfoEntity fetchUserInfoinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
   if (userInfoEntity==nil) {
            return;
        }
    }

#pragma mark IBActions

-(IBAction)settingsButtonAction:(id)sender
{
   UIButton *buttonSettings = (UIButton *)sender;
    switch (buttonSettings.tag) {
        case 501:
        {
           [self sendMail:sender];
        }
            break;
        case 502:
        {
            TermsAndConditionsVC *termsAndConditionsViewController =
            [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditionsVC"];
            [self presentViewController:termsAndConditionsViewController animated:YES completion:nil];
        }
            break;
        case 503:
        {
            AboutUsViewController *aboutUsViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsVC"];
            [self presentViewController:aboutUsViewController animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
}

-(void)sendMail:(id)sender{
    if  ([MFMailComposeViewController  canSendMail])
    {
        NSString *strToEmail = @"madhavi.appster@gmail.com";
        if ([strToEmail length ] > 0 )
        {
            MFMailComposeViewController *mailer = [[ MFMailComposeViewController alloc ] init ];
            mailer. mailComposeDelegate = self ;
            [mailer setSubject: @"GoSnow:" ];
            NSString *strEmailId = strToEmail;
            NSArray *toRecipients = [ NSArray arrayWithObjects :strEmailId, nil ];
            [mailer setToRecipients:toRecipients];
            NSString *emailBody = @"";
            [mailer setMessageBody:emailBody isHTML:NO];
            [self presentViewController:mailer animated:YES completion:nil];
        }
    }
    else
    {
        UIAlertView *alertComposerSupport = [[UIAlertView alloc]initWithTitle: @"GoSnow" message : @"Email account is not configured in your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertComposerSupport show ];
    }
}

# pragma mark-Logout Action
-(IBAction)logOutButtonAction
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                          delegate:self
                                                 cancelButtonTitle:@"Cancel"
                                            destructiveButtonTitle:@"Logout"
                                                 otherButtonTitles:nil,nil];
    [actionSheet setActionSheetStyle:UIActionSheetStyleDefault];
    [actionSheet showInView:self.view];
    
}

#pragma mark  - Action Sheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==0) {
        [self logoutWebservice];
    }
}
#pragma mark  -Logout Webservice Method
-(void)logoutWebservice
{
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"]]animated:YES];
    [self.sideMenuViewController hideMenuViewController];
    
    NSMutableDictionary *params=[NSMutableDictionary dictionary];
    [params setObject:userInfoEntity.sessionId forKey:@"sessionId"];
    
    [UserProfile sharedInstance].delegate=self;
    [[UserProfile sharedInstance] logoutUser:params method:@"user/logout"];
    [UserInfoEntity deleteUserprofileinManagedObjectContext:[CoreDataManager sharedInstance].managedObjectContext];
   
}

#pragma mark - user logout succesfully
-(void)userLogoutSuccessfully
{
    
}


#pragma mark - mail compose delegate
- ( void )mailComposeController:( MFMailComposeViewController *)controller didFinishWithResult:( MFMailComposeResult )result error:( NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled :
            //NSLog ( @"Mail cancelled" );
            break ;
            
        case MFMailComposeResultSaved :
            //NSLog ( @"Mail saved" );
            break ;
            
        case MFMailComposeResultSent :
            NSLog ( @"Mail send" );
            break ;
            
        case MFMailComposeResultFailed :
            NSLog ( @"Mail failed" );
            break ;
            
        default :
            //NSLog ( @"Mail not sent" );
            break ;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
