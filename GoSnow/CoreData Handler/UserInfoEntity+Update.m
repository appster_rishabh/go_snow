//
//  UserInfoEntity+Update.m
//  GoSnow
//
//  Created by Rishabh on 17/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "UserInfoEntity+Update.h"

@implementation UserInfoEntity (Update)

+(void)addUserInfo:(NSDictionary *)profileInfo sessionId:(NSString *)sessionId inManagedObjectContext:(NSManagedObjectContext *)context
{
    // UserInfoEntity *userInfoEntity;
    [self deleteUserprofileinManagedObjectContext:context];
    UserInfoEntity *userInfoEntity =nil;
    userInfoEntity = [NSEntityDescription insertNewObjectForEntityForName:@"UserInfoEntity"inManagedObjectContext:context];
    userInfoEntity.firstName=[profileInfo objectForKey:@"firstName"];
    userInfoEntity.lastName=[profileInfo objectForKey:@"lastName"];
    userInfoEntity.facebookId=[profileInfo objectForKey:@"facebookId"];
    userInfoEntity.user_id=[profileInfo objectForKey:@"id"];
    userInfoEntity.sessionId=sessionId;
    userInfoEntity.skillLevelName=[profileInfo objectForKey:@"skillLevelName"];
    userInfoEntity.riderTypeName=[profileInfo objectForKey:@"riderTypeName"];
    userInfoEntity.riderTypeId=[profileInfo objectForKey:@"riderTypeId"];
    userInfoEntity.skillLevelId=[profileInfo objectForKey:@"skillLevelId"];
    
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[profileInfo objectForKey:@"aboutMe"] options:0];
    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    NSData *newdata=[decodedString dataUsingEncoding:NSUTF8StringEncoding
                                allowLossyConversion:YES];
    NSString *aboutMe=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
    
    userInfoEntity.aboutMe=aboutMe;
    userInfoEntity.interestName=[self getIneterests:[profileInfo valueForKey:@"interests"]];
    userInfoEntity.locationName=[self getLocations:[profileInfo valueForKey:@"locations"]];
    userInfoEntity.facebookFriends=[self getContacts:[profileInfo valueForKey:@"contacts"]];
    userInfoEntity.userPhotos=[profileInfo objectForKey:@"userPhotos"];
    userInfoEntity.age=[profileInfo objectForKey:@"age"];
    userInfoEntity.gender=[profileInfo objectForKey:@"gender"];
    userInfoEntity.nextDestinationName=[profileInfo objectForKey:@"nextDestinationName"];
    userInfoEntity.nextDestinationId=[profileInfo objectForKey:@"nextDestinationId"];
    userInfoEntity.profileImage=[profileInfo objectForKey:@"profileImage"];
    userInfoEntity.profileThumb=[profileInfo objectForKey:@"profileImage250"];
    userInfoEntity.latitude=[NSString stringWithFormat:@"%@",[profileInfo objectForKey:@"latitude"]];
    userInfoEntity.longitude=[NSString stringWithFormat:@"%@",[profileInfo objectForKey:@"longitude"]];
    userInfoEntity.requestCounts=[profileInfo valueForKey:@"contactRequestCount"];
    userInfoEntity.tripRequestCount=[profileInfo valueForKey:@"tripRequestCount"];
   // [self getRequestCount:[profileInfo valueForKey:@"contactRequestCount"] tripRequest:[profileInfo valueForKey:@"tripRequestCount"]];
    userInfoEntity.messagesCounts=[profileInfo valueForKey:@"unreadMessageCount"];
    int totalCounts=[userInfoEntity.requestCounts intValue]+[userInfoEntity.messagesCounts intValue]+[userInfoEntity.tripRequestCount intValue];
    [UIApplication sharedApplication].applicationIconBadgeNumber = totalCounts;
//    contactRequestCount
//    tripRequestCount = 0;
//    unreadMessageCount = 1;
    //userInfoEntity.longitude=[profileInfo objectForKey:@"longitude"];
    userInfoEntity.address=[profileInfo objectForKey:@"address"];
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    
}
+(NSString *)getRequestCount:(id)contactRequests tripRequest:(id)tripRequests
{
    int requestCount=[contactRequests intValue]+[tripRequests intValue];
    return [NSString stringWithFormat:@"%d",requestCount];
}
+(UserInfoEntity *)updateUserInfo:(NSDictionary *)profileInfo sessionId:(NSString *)sessionId inManagedObjectContext:(NSManagedObjectContext *)context
{
    UserInfoEntity *userInfoEntity =nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"UserInfoEntity" inManagedObjectContext:context]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if (profileInfo==nil && [results count]==1) {
        return [results objectAtIndex:0];
    }
    if ([results count] == 1)
    {
        userInfoEntity = [results objectAtIndex:0];
        userInfoEntity.firstName=[profileInfo objectForKey:@"firstName"];
        userInfoEntity.lastName=[profileInfo objectForKey:@"lastName"];
        userInfoEntity.facebookId=[profileInfo objectForKey:@"facebookId"];
        userInfoEntity.user_id=[profileInfo objectForKey:@"id"];
        userInfoEntity.sessionId=sessionId;
        userInfoEntity.skillLevelName=[profileInfo objectForKey:@"skillLevelName"];
        userInfoEntity.riderTypeName=[profileInfo objectForKey:@"riderTypeName"];
        userInfoEntity.riderTypeId=[profileInfo objectForKey:@"riderTypeId"];
        userInfoEntity.skillLevelId=[profileInfo objectForKey:@"skillLevelId"];
        
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[profileInfo objectForKey:@"aboutMe"] options:0];
        NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        NSData *newdata=[decodedString dataUsingEncoding:NSUTF8StringEncoding
                                    allowLossyConversion:YES];
        NSString *aboutMe=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
        
        userInfoEntity.aboutMe=aboutMe;
        userInfoEntity.userPhotos=[profileInfo objectForKey:@"userPhotos"];
        userInfoEntity.age=[profileInfo objectForKey:@"age"];
        userInfoEntity.gender=[profileInfo objectForKey:@"gender"];
        userInfoEntity.interestName=[self getIneterests:[profileInfo valueForKey:@"interests"]];
        userInfoEntity.locationName=[self getLocations:[profileInfo valueForKey:@"locations"]];
        userInfoEntity.facebookFriends=[self getContacts:[profileInfo valueForKey:@"contacts"]];
        userInfoEntity.nextDestinationName=[profileInfo objectForKey:@"nextDestinationName"];
        userInfoEntity.nextDestinationId=[profileInfo objectForKey:@"nextDestinationId"];
        userInfoEntity.profileImage=[profileInfo objectForKey:@"profileImage"];
        userInfoEntity.profileThumb=[profileInfo objectForKey:@"profileImage250"];
        userInfoEntity.latitude=[NSString stringWithFormat:@"%@",[profileInfo objectForKey:@"latitude"]];
        userInfoEntity.longitude=[NSString stringWithFormat:@"%@",[profileInfo objectForKey:@"longitude"]];
       userInfoEntity.address=[profileInfo objectForKey:@"address"];
      //  userInfoEntity.requestCounts=[self getRequestCount:[profileInfo valueForKey:@"contactRequestCount"] tripRequest:[profileInfo valueForKey:@"tripRequestCount"]];
      //  userInfoEntity.messagesCounts=[profileInfo valueForKey:@"unreadMessageCount"];
        NSError *error;
        if (![context save:&error])
        {
            
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            return nil;
        }
        
    }
    return userInfoEntity;
    
}
+(UserInfoEntity *)updateUserPhotos:(NSDictionary *)profileInfo sessionId:(NSString *)sessionId inManagedObjectContext:(NSManagedObjectContext *)context
{
    UserInfoEntity *userInfoEntity =nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"UserInfoEntity" inManagedObjectContext:context]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ([results count] == 1)
    {
        userInfoEntity = [results objectAtIndex:0];
        userInfoEntity.userPhotos=[profileInfo objectForKey:@"userPhotos"];
        userInfoEntity.profileImage=[profileInfo objectForKey:@"profileImage"];
        userInfoEntity.profileThumb=[profileInfo objectForKey:@"profileImage250"];
        
        NSError *error;
        if (![context save:&error])
        {
            
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            return nil;
        }
        
    }
    return userInfoEntity;
    
}
+(void)updateUserLocation:(NSDictionary *)profileInfo sessionId:(NSString *)sessionId inManagedObjectContext:(NSManagedObjectContext *)context
{
    UserInfoEntity *userInfoEntity =nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"UserInfoEntity" inManagedObjectContext:context]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ([results count] == 1)
    {
        userInfoEntity = [results objectAtIndex:0];
        userInfoEntity.latitude=[NSString stringWithFormat:@"%@",[profileInfo objectForKey:@"latitude"]];
        userInfoEntity.longitude=[NSString stringWithFormat:@"%@",[profileInfo objectForKey:@"longitude"]];
        
        userInfoEntity.address=[profileInfo objectForKey:@"address"];
        userInfoEntity.requestCounts=[profileInfo valueForKey:@"contactRequestCount"];
        userInfoEntity.tripRequestCount=[profileInfo valueForKey:@"tripRequestCount"];
        // [self getRequestCount:[profileInfo valueForKey:@"contactRequestCount"] tripRequest:[profileInfo valueForKey:@"tripRequestCount"]];
        userInfoEntity.messagesCounts=[profileInfo valueForKey:@"unreadMessageCount"];
        
        NSError *error;
        if (![context save:&error])
        {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            
        }
    }
}
+(UserInfoEntity *)updateRequestAndmessagesCounts:(NSString *)requestCounts messagesCounts:(NSString *)messagesCounts tripRequestCounts:(NSString *)tripRequestCount inManagedObjectContext:(NSManagedObjectContext *)context
{
    
    UserInfoEntity *userInfoEntity =nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"UserInfoEntity" inManagedObjectContext:context]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ([results count] == 1)
    {
        userInfoEntity = [results objectAtIndex:0];
        if (messagesCounts.length>0) {
            NSString *currentMessagesCounts=userInfoEntity.messagesCounts;
            int updatedMessagesCounts=[currentMessagesCounts intValue]-[messagesCounts intValue];
            if (updatedMessagesCounts<0)
                userInfoEntity.messagesCounts=@"0";
            else
            userInfoEntity.messagesCounts=[NSString stringWithFormat:@"%d",updatedMessagesCounts];
            
        }
        if (requestCounts.length>0) {
            int currentRequestCount=[userInfoEntity.requestCounts intValue]-[requestCounts intValue];
            if (currentRequestCount<0)
                userInfoEntity.requestCounts=@"0";
            else
                userInfoEntity.requestCounts=[NSString stringWithFormat:@"%d",currentRequestCount];
            
        }
        if (tripRequestCount.length>0) {
            int currentTripCount=[userInfoEntity.tripRequestCount intValue]-[tripRequestCount intValue];
            if (currentTripCount<0)
                userInfoEntity.tripRequestCount=@"0";
            else
                userInfoEntity.tripRequestCount=[NSString stringWithFormat:@"%d",currentTripCount];
           
        }
       int totalCounts=[userInfoEntity.requestCounts intValue]+[userInfoEntity.messagesCounts intValue]+[userInfoEntity.tripRequestCount intValue];
        [UIApplication sharedApplication].applicationIconBadgeNumber = totalCounts;
        
        NSError *error;
        if (![context save:&error])
        {
            
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            return nil;
        }
        
    }
    return userInfoEntity;
    
}
+(UserInfoEntity *)increaseRequestAndmessagesCounts:(NSString *)requestCounts messagesCounts:(NSString *)messagesCounts tripRequestCounts:(NSString *)tripRequestCount inManagedObjectContext:(NSManagedObjectContext *)context
{
    UserInfoEntity *userInfoEntity =nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"UserInfoEntity" inManagedObjectContext:context]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ([results count] == 1)
    {
        userInfoEntity = [results objectAtIndex:0];
        if (messagesCounts.length>0) {
            NSString *currentMessagesCounts=userInfoEntity.messagesCounts;
            int updatedMessagesCounts=[currentMessagesCounts intValue]+[messagesCounts intValue];
            userInfoEntity.messagesCounts=[NSString stringWithFormat:@"%d",updatedMessagesCounts];
        }
        if (requestCounts.length>0) {
            int currentRequestCount=[userInfoEntity.requestCounts intValue]+[requestCounts intValue];
            userInfoEntity.requestCounts=[NSString stringWithFormat:@"%d",currentRequestCount];
        }
        if (tripRequestCount.length>0) {
            int currentRequestCount=[userInfoEntity.tripRequestCount intValue]+[tripRequestCount intValue];
            userInfoEntity.tripRequestCount=[NSString stringWithFormat:@"%d",currentRequestCount];
        }
        int totalCounts=[userInfoEntity.requestCounts intValue]+[userInfoEntity.messagesCounts intValue]+[userInfoEntity.tripRequestCount intValue];
        [UIApplication sharedApplication].applicationIconBadgeNumber = totalCounts;
        
        NSError *error;
        if (![context save:&error])
        {
            
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            return nil;
        }
        
    }
    return userInfoEntity;
    
}
+(UserInfoEntity *)fetchUserInfoinManagedObjectContext:(NSManagedObjectContext* )context
{
    UserInfoEntity *userInfoEntity =nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"UserInfoEntity" inManagedObjectContext:context]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ([results count]==0) {
        return nil;
    }
    userInfoEntity = [results objectAtIndex:0];
    return userInfoEntity;
}

+(NSString *)fetchSessionIdinManagedObjectContext:(NSManagedObjectContext* )context
{
    UserInfoEntity *userInfoEntity =nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"UserInfoEntity" inManagedObjectContext:context]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ([results count]==0) {
        return nil;
    }
    userInfoEntity = [results objectAtIndex:0];
    return userInfoEntity.sessionId;
}
+(int)fetchPendingRequestsAndMessagesCountsinManagedObjectContext:(NSManagedObjectContext* )context
{
    UserInfoEntity *userInfoEntity =nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"UserInfoEntity" inManagedObjectContext:context]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if ([results count]==0) {
        return 0;
    }
    userInfoEntity = [results objectAtIndex:0];
    int totalCounts=[userInfoEntity.requestCounts intValue]+[userInfoEntity.messagesCounts intValue]+[userInfoEntity.tripRequestCount intValue];
    return totalCounts;
}

+(void)deleteUserprofileinManagedObjectContext:(NSManagedObjectContext *)context
{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"UserInfoEntity" inManagedObjectContext:context]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    for (int i=0; i<[results count]; i++) {
        UserInfoEntity *userInfoEntity =[results objectAtIndex:i];
        [context deleteObject:userInfoEntity];
        if (![context save:&error])
        {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
}
+(NSString *)getIneterests:(NSArray *)interestArray
{
    NSString *interests=[[NSString alloc] init];;
    for (int i=0; i<[interestArray count]; i++) {
        interests= [interests stringByAppendingString:[[interestArray objectAtIndex:i] valueForKey:@"interestName"]];
        if (i<[interestArray count]-1)
            interests= [interests stringByAppendingString:@", "];
        
    }
    if (interests.length<1) {
        return  interests=@"";
    }
    return interests;
}
+(NSString *)getLocations:(NSArray *)locationsArray
{
    NSString *locations=[[NSString alloc] init];;
    for (int i=0; i<[locationsArray count]; i++) {
        locations=[locations stringByAppendingString:[[locationsArray objectAtIndex:i] valueForKey:@"destinationName"]];
        
        if (i<[locationsArray count]-1)
            locations= [locations stringByAppendingString:@", "];
    }
    if (locations.length<1) {
        return  locations=@"";
    }
    return locations;
}
+(NSString *)getContacts:(NSArray *)contactsArray
{
    NSString *contacts=[[NSString alloc] init];;
    for (int i=0; i<[contactsArray count]; i++) {
        contacts= [contacts stringByAppendingString:[[contactsArray objectAtIndex:i] valueForKey:@"contactProfileThumb"]];
        if (i<[contactsArray count]-1)
            contacts= [contacts stringByAppendingString:@","];
        
    }
    if (contacts.length<1) {
        return contacts=@"";
    }
    return contacts;
}

+(NSString *)removeLastCharacter:(NSString *)stringText
{
    NSString *lastCharacter=[stringText substringFromIndex:[stringText length]-1];
    if ([lastCharacter isEqualToString:@","]) {
        stringText=[stringText substringToIndex:[stringText length]-1];
    }
    return stringText;
}


@end
