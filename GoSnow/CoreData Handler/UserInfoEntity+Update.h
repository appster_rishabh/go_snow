//
//  UserInfoEntity+Update.h
//  GoSnow
//
//  Created by Rishabh on 17/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "UserInfoEntity.h"

@interface UserInfoEntity (Update)

+(void)addUserInfo:(NSDictionary *)profileInfo sessionId:(NSString *)sessionId inManagedObjectContext:(NSManagedObjectContext *)context;
+(UserInfoEntity *)updateUserInfo:(NSDictionary *)profileInfo  sessionId:(NSString *)sessionId inManagedObjectContext:(NSManagedObjectContext *)context;
+(UserInfoEntity *)fetchUserInfoinManagedObjectContext:(NSManagedObjectContext* )context;
+(void)updateUserLocation:(NSDictionary *)profileInfo sessionId:(NSString *)sessionId inManagedObjectContext:(NSManagedObjectContext *)context;
+(void)deleteUserprofileinManagedObjectContext:(NSManagedObjectContext *)context;
+(UserInfoEntity *)updateUserPhotos:(NSDictionary *)profileInfo sessionId:(NSString *)sessionId inManagedObjectContext:(NSManagedObjectContext *)context;
+(NSString *)fetchSessionIdinManagedObjectContext:(NSManagedObjectContext* )context;
+(UserInfoEntity *)updateRequestAndmessagesCounts:(NSString *)requestCounts messagesCounts:(NSString *)messagesCounts tripRequestCounts:(NSString *)tripRequestCount inManagedObjectContext:(NSManagedObjectContext *)context;
+(UserInfoEntity *)increaseRequestAndmessagesCounts:(NSString *)requestCounts messagesCounts:(NSString *)messagesCounts tripRequestCounts:(NSString *)tripRequestCount inManagedObjectContext:(NSManagedObjectContext *)context;
+(int)fetchPendingRequestsAndMessagesCountsinManagedObjectContext:(NSManagedObjectContext* )context;
@end
