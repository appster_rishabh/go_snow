//
//  UserInfoEntity.h
//  GoSnow
//
//  Created by Varsha Singh on 19/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserInfoEntity : NSManagedObject

@property (nonatomic, retain) NSString * aboutMe;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * age;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * distance;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * facebookId;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSString * isSignupComplete;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * nextDestinationId;
@property (nonatomic, retain) NSString * nextDestinationName;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * profileImage;
@property (nonatomic, retain) NSString * profileImage250;
@property (nonatomic, retain) NSString * profileThumb;
@property (nonatomic, retain) NSString * riderTypeId;
@property (nonatomic, retain) NSString * riderTypeName;
@property (nonatomic, retain) NSString * sessionId;
@property (nonatomic, retain) NSString * skillLevelId;
@property (nonatomic, retain) NSString * skillLevelName;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * userPhotos;
@property (nonatomic, retain) NSString * userPhotos250;
@property (nonatomic, retain) NSString * interestId;
@property (nonatomic, retain) NSString * interestName;
@property (nonatomic, retain) NSString * facebookFriends;
@property (nonatomic, retain) NSString * locationName;
@property (nonatomic, retain) NSString * locationId;
@property (nonatomic, retain) NSString * messagesCounts;
@property (nonatomic, retain) NSString * requestCounts;
@property (nonatomic, retain) NSString * tripRequestCount;
//tripRequestCount
@end
