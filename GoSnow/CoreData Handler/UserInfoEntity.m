//
//  UserInfoEntity.m
//  GoSnow
//
//  Created by Varsha Singh on 19/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "UserInfoEntity.h"


@implementation UserInfoEntity

@dynamic aboutMe;
@dynamic address;
@dynamic age;
@dynamic city;
@dynamic country;
@dynamic distance;
@dynamic email;
@dynamic facebookId;
@dynamic firstName;
@dynamic gender;
@dynamic isSignupComplete;
@dynamic lastName;
@dynamic latitude;
@dynamic longitude;
@dynamic nextDestinationId;
@dynamic nextDestinationName;
@dynamic phone;
@dynamic profileImage;
@dynamic profileImage250;
@dynamic profileThumb;
@dynamic riderTypeId;
@dynamic riderTypeName;
@dynamic sessionId;
@dynamic skillLevelId;
@dynamic skillLevelName;
@dynamic state;
@dynamic user_id;
@dynamic userPhotos;
@dynamic userPhotos250;
@dynamic interestId;
@dynamic interestName;
@dynamic locationName;
@dynamic facebookFriends;
@dynamic locationId;
@synthesize requestCounts;
@synthesize messagesCounts;
@synthesize tripRequestCount;
@end
