//
//  GSRequestWebservices.h
//  GoSnow
//
//  Created by Rishabh on 26/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//


@class GSRequestWebservices;

typedef void (^NetworkRequestCompletionHandler)(NSError *error, NSData* responceData);




@interface GSRequestWebservices : NSObject
{
    NSMutableData*         connection_response;
    
}

@property (nonatomic, copy) NetworkRequestCompletionHandler completionHandler;


- (void)downloadArticlewithSessionDictionary:(NSDictionary*)dictionary;

-(id)initWithMethod:(NSString *)methodName withDictionary:(NSDictionary *)dictionary andCompletionHandler:(NetworkRequestCompletionHandler)handler;




@end
