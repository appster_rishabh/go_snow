//
//  GSRequestWebservices.m
//  GoSnow
//
//  Created by Rishabh on 26/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "GSRequestWebservices.h"

#import "CoreDataManager.h"


@implementation GSRequestWebservices
@synthesize completionHandler = _completionHandler;

static GSRequestWebservices * requestWebservices;

+(GSRequestWebservices* )sharedNetworkManager {
    if (!requestWebservices) {
        @synchronized ([UIApplication sharedApplication]) {
            
            requestWebservices = [[GSRequestWebservices alloc] init];
            
        }
    }
    return requestWebservices;
}



-(id)initWithMethod:(NSString *)methodName withDictionary:(NSDictionary *)dictionary andCompletionHandler:(NetworkRequestCompletionHandler)handler{
    
    _completionHandler = [handler copy];
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //NSData* cData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KSERVICEURL,methodName ]] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:40.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[NSData dataWithBytes:[jsonString UTF8String] length:[jsonString length]]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    connection_response = [NSMutableData data];
    
    if(connection)
    {
        
    }
    else
    {
        
        NSError* error = [NSError errorWithDomain:@"Sever Error" code:400 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"NSLocalizedDescription", @"Server error ", nil]];
        if (self.completionHandler)
        {
            self.completionHandler(error, nil);
        }
    }
    
    
    return self;
    
}

#pragma mark- ManagedContextMerging-

- (void) mergeChangesFromInsertionContext:(NSNotification *)saveNotification
{
    
    NSManagedObjectContext *context = (NSManagedObjectContext *)saveNotification.object;
    
    if( context.persistentStoreCoordinator == [CoreDataManager sharedInstance].persistentStoreCoordinator )
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[CoreDataManager sharedInstance].managedObjectContext mergeChangesFromContextDidSaveNotification:saveNotification];
        });
        
    }
}


#pragma mark - NSURL delegate-

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse* l_response = (NSHTTPURLResponse*)response;
    NSUInteger errorCode = [l_response statusCode];
    if (errorCode != 200) {
        
        NSError* error = [NSError errorWithDomain:@"Sever Error" code:errorCode userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"NSLocalizedDescription", @"Server error ", nil]];
        if (self.completionHandler)
        {
            self.completionHandler(error, nil);
        }
        self.completionHandler = nil;
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [connection_response appendData:data];
    
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (self.completionHandler)
    {
        self.completionHandler(error, nil);
    }
    self.completionHandler = nil;
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *) connection{
    
    if (self.completionHandler)
    {
        // NSDictionary *dict = [connection_response objectFromJSONData];
        
        self.completionHandler(nil, connection_response);
    }
    self.completionHandler = nil;
}

@end
