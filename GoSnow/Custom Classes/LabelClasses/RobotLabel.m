//
//  RobotLabel.m
//  GoSnow
//
//  Created by Rishabh on 03/03/15.
//  Copyright (c) 2015 Varsha Singh. All rights reserved.
//

#import "RobotLabel.h"

@implementation RobotLabel
-(void)setup{
    
    self.font = [UIFont fontWithName:@"RobotoCondensed-Bold" size:self.font.pointSize];
    //self.textColor = [UIColor redColor];
}
-(id)initWithFrame:(CGRect)frame {
    
    if((self = [super initWithFrame:frame])) {
        
        [self setup];
    }
    
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    
    if((self = [super initWithCoder:aDecoder])) {
        
        [self setup];
    }
    
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
