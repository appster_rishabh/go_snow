//
//  UIImage+ImageShape.m
//  ContainingAppForKeyboard
//
//  Created by Rishabh Yadav on 17/09/2014.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import "UIImage+ImageShape.h"
#import "UIColor+Color.h"
@implementation UIImage (ImageShape)
+(UIImage *)CreateAndReturnImageRounded:(NSString *)clr outline:(NSString *)outlin rect:(CGRect)rect
{
    UIColor *colr=[UIColor Colorname:clr];
    UIImage *image ;
    CGFloat scale = 1.0;
    if([[UIScreen mainScreen]respondsToSelector:@selector(scale)]) {
        CGFloat tmp = [[UIScreen mainScreen]scale];
        if (tmp > 1.5) {
            scale = 2.0;
        }
    }
    
    if(scale > 1.5) {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(rect.size.width, rect.size.height), NO, 2.0);
    } else {
        UIGraphicsBeginImageContext(CGSizeMake(rect.size.width, rect.size.height));
    }
    UIBezierPath *triangle4 = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, rect.size.width, rect.size.height)
                                                    byRoundingCorners:UIRectCornerAllCorners
                                                          cornerRadii:CGSizeMake(rect.size.height/2.0, rect.size.height/2)];
    //  [triangle addLineToPoint:CGPointMake(0, 0)];
    [triangle4 closePath];
    
    triangle4.lineWidth=2;
    if ([outlin isEqualToString:@"Fill"]) {
        [colr setFill];
        [triangle4 fill];
    }
    else
    {
        [[UIColor clearColor] setFill];
        [triangle4 fill];
        [colr setStroke];
        [triangle4 stroke];
    }
    
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return image;
    
}
+(UIImage *)CreateAndReturnImageRoundedLines:(NSString *)clr outline:(NSString *)outlin rect:(CGRect)rect
{
    UIColor *colr=[UIColor Colorname:clr];
    UIImage *image ;
    CGFloat scale = 1.0;
    if([[UIScreen mainScreen]respondsToSelector:@selector(scale)]) {
        CGFloat tmp = [[UIScreen mainScreen]scale];
        if (tmp > 1.5) {
            scale = 2.0;
        }
    }
    
    if(scale > 1.5) {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(rect.size.width, rect.size.height), NO, 2.0);
    } else {
        UIGraphicsBeginImageContext(CGSizeMake(rect.size.width, rect.size.height));
    }
    UIBezierPath *triangle4 = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(2, 4, rect.size.width-4, rect.size.height-8)
                                               byRoundingCorners:UIRectCornerAllCorners
                                                     cornerRadii:CGSizeMake(12.0, 12.0)];    //  [triangle addLineToPoint:CGPointMake(0, 0)];
    [triangle4 closePath];
    
    triangle4.lineWidth=2;
    if ([outlin isEqualToString:@"Fill"]) {
        [colr setFill];
        [triangle4 fill];
    }
    else
    {
        [[UIColor clearColor] setFill];
        [triangle4 fill];
        [colr setStroke];
        [triangle4 stroke];
    }
    
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return image;
    
}
+(UIImage *)CreateAndReturnImageToneFill:(NSString *)shape color:(NSString *)clr outline:(NSString *)outlin rect:(CGRect)rect
{
    UIColor *colr=[UIColor Colorname:clr];
    UIImage *image ;
    CGFloat scale = 1.0;
    if([[UIScreen mainScreen]respondsToSelector:@selector(scale)]) {
        CGFloat tmp = [[UIScreen mainScreen]scale];
        if (tmp > 1.5) {
            scale = 2.0;
        }
    }
    
    if(scale > 1.5) {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(rect.size.width, rect.size.height), NO, 2.0);
    } else {
        UIGraphicsBeginImageContext(CGSizeMake(rect.size.width, rect.size.height));
    }
    UIBezierPath *tone = [UIBezierPath bezierPath];
    
    [tone moveToPoint:CGPointMake(2, 2)];
    // [triangle addLineToPoint:CGPointMake(0, 0)];
    [tone addLineToPoint:CGPointMake(rect.size.width-2, 2)];
    [tone closePath];
   // [tone addLineToPoint:CGPointMake(rect.size.width-2, rect.size.height-2)];
   // [tone addLineToPoint:CGPointMake(2, rect.size.height-2)];
   // [tone addLineToPoint:CGPointMake(2, rect.size.height/2)];
    
    //  [triangle addLineToPoint:CGPointMake(0, 0)];
   // [tone closePath];
    
    tone.lineWidth=2;
    if ([outlin isEqualToString:@"Fill"]) {
        [colr setFill];
        [tone fill];
    }
    else
    {
        [[UIColor clearColor] setFill];
        [tone fill];
        // triangle4.lineWidth=2.0f;
        [colr setStroke];
        [tone stroke];
    }
    
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return image;
}
+(UIImage *)CreateAndReturnImagesCirclebig:(NSString *)clr rect:(CGRect)rect;
{
    UIColor *colr=[UIColor redColor];
    UIImage *image ;
   UIGraphicsBeginImageContextWithOptions(CGSizeMake(rect.size.width, rect.size.width), NO, 2.0);
           // UIGraphicsBeginImageContext(CGSizeMake(rect.size.width, rect.size.width));
    
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path addArcWithCenter:CGPointMake(rect.size.width/2, rect.size.width/2)
                        radius:rect.size.width/2-2
                    startAngle:0.0
                      endAngle:M_PI * 2.0
                     clockwise:YES];
        
        path.lineWidth = 2;
        
            [colr setFill];
            [path fill];
        
           // [colr setStroke];
            //[path stroke];
                image = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
   
       return image;
}
+(UIImage *)CreateAndReturnImagesCircle:(NSString *)clr rect:(CGRect)rect text:(NSString *)text;
{
    UIColor *colr=[UIColor colorwithHexString:clr alpha:1.0];
    UIImage *image ;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(20, 20), NO, 2.0);
    // UIGraphicsBeginImageContext(CGSizeMake(rect.size.width, rect.size.width));
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path addArcWithCenter:CGPointMake(10, 10)
                    radius:8
                startAngle:0.0
                  endAngle:M_PI * 2.0
                 clockwise:YES];
    
    path.lineWidth = 1;
    
    [colr setFill];
    [path fill];
    
     [[UIColor whiteColor] setStroke];
    [path stroke];
    
    UIFont *font = [UIFont fontWithName:@"RobotoCondensed-Bold" size:11.f];
    
    
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
                             NSFontAttributeName : font,
                             NSTextEffectAttributeName : NSTextEffectLetterpressStyle};
    
    
    //[[UIColor blackColor] set];
    [text drawAtPoint:CGPointMake(7.5, 1) withAttributes:attrs];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    //image= [self drawText:@"1" inImage:image atPoint:CGPointMake(0, 0)];
    return image;
}

@end