//
//  UIImage+ImageShape.h
//  ContainingAppForKeyboard
//
//  Created by Rishabh Yadav on 17/09/2014.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageShape)
+(UIImage *)CreateAndReturnImageRounded:(NSString *)clr outline:(NSString *)outlin rect:(CGRect)rect;

+(UIImage *)CreateAndReturnImageRoundedLines:(NSString *)clr outline:(NSString *)outlin rect:(CGRect)rect;
+(UIImage *)CreateAndReturnImagesCirclebig:(NSString *)clr rect:(CGRect)rect;
+(UIImage *)CreateAndReturnImageToneFill:(NSString *)shape color:(NSString *)clr outline:(NSString *)outlin rect:(CGRect)rect;

+(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point;
+(UIImage *)CreateAndReturnImagesCircle:(NSString *)clr rect:(CGRect)rect text:(NSString *)text;
@end
