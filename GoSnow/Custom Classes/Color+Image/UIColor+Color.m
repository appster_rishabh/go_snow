//
//  UIColor+Color.m
//  ContainingAppForKeyboard
//
//  Created by Rishabh Yadav on 17/09/2014.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import "UIColor+Color.h"

@implementation UIColor (Color)
+(UIColor *)Colorname:(NSString *)name
{
   
    UIColor *colr=[self colorwithHexString:name alpha:1.0f];
    if (colr==nil) {
        return [UIColor darkGrayColor];
    }
  
    return colr;
}
+(UIColor *)ColornameDefault:(NSString *)name
{
    
    UIColor *colr=[self colorwithHexString:name alpha:0.6f];
    if (colr==nil) {
        return [UIColor darkGrayColor];
    }
    
    return colr;
}
+ (UIColor *)colorwithHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    //-----------------------------------------
    // Convert hex string to an integer
    //-----------------------------------------
    unsigned int hexint = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet
                                       characterSetWithCharactersInString:@"#"]];
    [scanner scanHexInt:&hexint];
    
    //-----------------------------------------
    // Create color object, specifying alpha
    //-----------------------------------------
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    
    return color;
}
@end
