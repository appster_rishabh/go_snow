//
//  UIColor+Color.h
//  ContainingAppForKeyboard
//
//  Created by Rishabh Yadav on 17/09/2014.
//  Copyright (c) 2014 Appster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Color)

+(UIColor *)Colorname:(NSString *)name;
+(UIColor *)ColornameDefault:(NSString *)name;
+(UIColor *)colorwithHexString:(NSString *)hexStr alpha:(CGFloat)alpha;

@end
